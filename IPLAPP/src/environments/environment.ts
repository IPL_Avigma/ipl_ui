// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBGLOeLiPXh1WsdRpdr_YokWcDv9rM2th4",
    authDomain: "rare-lambda-245821.firebaseapp.com",
    databaseURL: "https://rare-lambda-245821.firebaseio.com",
    projectId: "rare-lambda-245821",
    storageBucket: "rare-lambda-245821.appspot.com",
    messagingSenderId: "1095460168527",
    appId: "1:1095460168527:web:1b194864d96f51a1f21b3a",
    measurementId: "G-62E7QE63L7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
