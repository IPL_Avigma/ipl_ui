import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { ListBoxAllModule, DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';

import { WorkOrderViewComponent } from "./WorkOrderViewComponent";
import { SaveWorkOrderViewServices } from "./WorkOrderViewServices";
import { ClientResultOldPhotoServices } from "../../ClientResult/ClientResultPhoto/ClientResultphotosOldServices";

import { ClientResultServices } from "../../ClientResult/ClientResults/ClientResultServices";


const WorkOrderviewRouts = [

  {path:'view',component:WorkOrderViewComponent}

]

@NgModule({
  declarations: [WorkOrderViewComponent],
  imports: [
    RouterModule.forChild(WorkOrderviewRouts),
    CommonModule,
    GridModule,
    ExcelModule,
    HttpClientModule,
    NgbModule,
    FormsModule, ReactiveFormsModule,
    ListBoxAllModule, DropDownListModule,




  ],
  providers: [SaveWorkOrderViewServices,ClientResultOldPhotoServices,ClientResultServices],
  bootstrap: [WorkOrderViewComponent]
})

export class WorkOrderViewModule {

  constructor(){
    console.log('Work Order View Model Loaded');
  }
}
