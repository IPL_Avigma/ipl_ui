import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {WorkOderViewModel,WorOrderColumn,WorOrderColumnjson,WorkOrderActions,ActionSentStore} from './WorkOrderViewModel';
import {BasetUrl} from '../../Utility/DomainUrl';
import {HomepageServices} from '../../Home/HomeServices';
import{CommonStatusDTO} from '../../ClientResult/CommonClientHeader/CommonStatusModel'






@Injectable({
  providedIn: "root"
})
export class SaveWorkOrderViewServices {
  public Errorcall;
  private token: any;
  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices:HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  // get user data
  private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetWorkOrderData";


  public WorkorderViewPostData(Modelobj:WorkOderViewModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    debugger;
    var ANYDTO: any = {};

        ANYDTO.workOrder_ID  = Modelobj.workOrder_ID;
        ANYDTO.UserID  = Modelobj.UserID;
        ANYDTO.Type = 1;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        //catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  //get workorderColumn 
  private apiUrlGetdata = BasetUrl.Domain + "api/RESTIPL/GetJsonColumnWorkOrder";


  public WorkorderColumnPostData(Modelobj:WorOrderColumn) {
    const User  = JSON.parse(localStorage.getItem('usertemp_'));
    var ANYDTO: any = {};
 
    ANYDTO.WC_UserId  = User[0].User_pkeyID ;
    ANYDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  
    return this._http
      .post<any>(this.apiUrlGetdata, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        //catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
 
  //post json column
  private apiUrlpo = BasetUrl.Domain + "api/RESTIPL/postColumnWorkOrder";


  public jsonColumnPostData(Modelobj:WorOrderColumnjson) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    debugger;
    var ANYDTO: any = {};

        ANYDTO.WC_Show_Column_Jsonarr  = Modelobj.WC_Show_Column_Jsonarr;
        ANYDTO.WC_Hide_Column_Jsonarr  = Modelobj.WC_Hide_Column_Jsonarr;
        ANYDTO.WC_IsActive  = Modelobj.WC_IsActive;
        ANYDTO.WC_IsDelete  = Modelobj.WC_IsDelete;
        ANYDTO.WC_UserId  = Modelobj.WC_UserId;
        ANYDTO.Type = 1;
        // if(Modelobj){
        //   ANYDTO.Type = 2;
        // }
      



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlpo, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        //catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  //filter workorder status data

  private apiUrlstu = BasetUrl.Domain + "api/RESTIPL/FilterWorkOrderStatus";


  public FilterWoStatusPostData(Modelobj:CommonStatusDTO) {
  
    debugger;
    var ANYDTO: any = {};

    ANYDTO.whereclause = Modelobj.whereclause;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlstu, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        //catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  /////Action details
  private apiUrlA = BasetUrl.Domain + "api/RESTIPL/GetWorkOrderActionDetail";


  public ActionData(Modelobj:WorkOrderActions) {
  
    debugger;
    

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlA, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        //catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
/////post Action data
  private apiUrlAc = BasetUrl.Domain + "api/RESTIPL/PostWorkOrderActionDetail";


  public PostWOActionData(Modelobj:ActionSentStore) {
  
    debugger;
    var ANYDTO: any = {};
    ANYDTO.Assign_Contractor = Modelobj.Assign_Contractor;
    ANYDTO.Assign_Coordinator = Modelobj.Assign_Coordinator;
    ANYDTO.Assign_Processor = Modelobj.Assign_Processor;
    ANYDTO.Client_Company = Modelobj.Client_Company;
    ANYDTO.Work_Type = Modelobj.Work_Type;
    ANYDTO.Due_Date = Modelobj.Due_Date;
    ANYDTO.Start_Date = Modelobj.Start_Date;
    ANYDTO.Client_Due_Date = Modelobj.Client_Due_Date;
    ANYDTO.Recurring_Order = Modelobj.Recurring_Order;
    ANYDTO.Comments = Modelobj.Comments;
    ANYDTO.Estimated_Date = Modelobj.Estimated_Date;
    ANYDTO.Send_Message = Modelobj.Send_Message;
    ANYDTO.Task = Modelobj.Task;
    ANYDTO.Instructions = Modelobj.Instructions;
    ANYDTO.Category = Modelobj.Category;
    ANYDTO.Background_Provider = Modelobj.Background_Provider;
    ANYDTO.Assign_PCR = Modelobj.Assign_PCR;
    ANYDTO.Cancel_Work_Order = Modelobj.Cancel_Work_Order;
    ANYDTO.Delete_Work_Order = Modelobj.Delete_Work_Order;
    ANYDTO.Mark_Client_Invoice_Paid = Modelobj.Mark_Client_Invoice_Paid;
    ANYDTO.Write_off_Invoice = Modelobj.Write_off_Invoice;
    ANYDTO.Mark_Contractor_Invoice_Paid = Modelobj.Mark_Contractor_Invoice_Paid;
    ANYDTO.Print_WO_Instructions = Modelobj.Print_WO_Instructions;
    ANYDTO.Print_Client_Invoice = Modelobj.Print_Client_Invoice;
    ANYDTO.Export_to_Excel = Modelobj.Export_to_Excel;
    ANYDTO.Download_Photos = Modelobj.Download_Photos;
    ANYDTO.Attach_Document = Modelobj.Attach_Document;
    ANYDTO.Route = Modelobj.Route;
    ANYDTO.WorkActionArray = Modelobj.WorkActionArray;
    ANYDTO.Type = 1;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlAc,ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        //catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }




  // common handler
  private handleError(error: HttpErrorResponse) {
    if(error.status == 401){
      alert('Unauthorized User Found...!');
      window.location.href = '/admin/login';
    }else{
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}


