import {
  Component,
  Injectable,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from "@angular/core";
import { Router } from "@angular/router";

import { NgbTypeahead } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  merge
} from "rxjs/operators";

import { WorkOderViewModel, WorOrderColumn, WorOrderColumnjson, WorkOrderActions, ActionSentStore } 
from "./WorkOrderViewModel";
import { SaveWorkOrderViewServices } from "./WorkOrderViewServices";

import { MasterlayoutComponent } from "../../Home/MasterComponent";

import { CommonModel } from "../../Home/HomeCommonModel";

import { ClientResultOldPhotoServices } from "../../ClientResult/ClientResultPhoto/ClientResultphotosOldServices";
import { ClientResultPhotoModel } from "../../ClientResult/ClientResultPhoto/ClientResultPhotoModel";

import { ClientResultServices } from "../../ClientResult/ClientResults/ClientResultServices";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  BindDataModel, TaskBidMasterModel,
  Task_Invoice_MasterModel,
  TaskDamageMasterModel,
} from '../../ClientResult/ClientResults/ClientResultModel';

import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { DataManager, ODataAdaptor } from '@syncfusion/ej2-data';
import { FieldSettingsModel, DragEventArgs } from '@syncfusion/ej2-dropdowns';
import{CommonStatusDTO} from '../../ClientResult/CommonClientHeader/CommonStatusModel'
import { ClientResultsInvoiceServices } from 'src/IPLAPP/ClientResult/ClientResultsInvoice/ClientResultsInvoiceServices';
import {WorkOrderDrodownServices} from '../../CommonServices/commonDropDown/DropdownService'
import { FormBuilder, FormGroup } from '@angular/forms';
import * as $ from "jquery";


@Component({
  templateUrl: "./WorkOrderView.html",
  styleUrls: ["WorkOrderView.scss"],
  encapsulation: ViewEncapsulation.None
})
export class WorkOrderViewComponent implements OnInit {
  @ViewChild('listbox1') public listObj1: ListBoxComponent; @ViewChild('listbox2')
 
  public listObj2: ListBoxComponent;

  public WorkOderViewModelobj: WorkOderViewModel = new WorkOderViewModel();
  public WorOrderColumnObj: WorOrderColumn = new WorOrderColumn();
  public WorOrderColumnjsonObj: WorOrderColumnjson = new WorOrderColumnjson();
  public CommonModelobj: CommonModel = new CommonModel();
  CommonStatusDTOObj: CommonStatusDTO = new CommonStatusDTO();
  WorkOrderActionsObj:WorkOrderActions = new WorkOrderActions();
  ActionSentStoreObj:ActionSentStore = new ActionSentStore();

  ClientResultPhotoModelObj: ClientResultPhotoModel = new ClientResultPhotoModel();
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel(); // task
  formUsrCommonGroup: FormGroup;
  MessageFlag: String;
  title: string;
  labelname: string;
  public griddata: any[];
  public Name: string = "";
  ClientList: any;
  Client: string;
  public getdata: any[];
  myModel = true;
  chkclick = true;
  hidedata: boolean = false;
  workd: boolean = false;
  sts: boolean = false;
  address: boolean = false;
  addr: boolean = false;
  workinfo: boolean = false;
  add2: boolean = false;
  cit: boolean = false;
  cont: boolean = false;
  rec: boolean = false;
  est: boolean = false;
  clntd: boolean = false;
  comn: boolean = false;
  compd: boolean = false;
  Cancd: boolean = false;
  createdd: boolean = false;
  strtd: boolean = false;
  loannum: boolean = false;
  loaninf: boolean = false;
  lots : boolean = false;
  lockc: boolean = false;
  adm: boolean = false;
  due: boolean = false;
  stat: boolean = false;
  zp: boolean = false;
  ipl: boolean = false;
  jsondataA = [];
  jsondataB = [];
  public contentx;
  // jsondataC = [];
  // jsondataD = [];
  public dataA: DataManager = new DataManager({

  });
 

  constructor(
    private xSaveWorkOrderViewServices: SaveWorkOrderViewServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xClientResultOldPhotoServices: ClientResultOldPhotoServices,
    private xClientResultServices: ClientResultServices,
    private EncrDecr: EncrDecrServiceService,
    private xClientResultsInvoiceServices:ClientResultsInvoiceServices,
    private modalService: NgbModal,
    private xWorkOrderDrodownServices:WorkOrderDrodownServices,
    private formBuilder: FormBuilder,

  ) {


    this.getautoworkorderviewdata();
    this.Client = "0";

  }
  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
   
      
    });
    this.GetDropDowndata();
    this.GetStatusDropDown();
    this.GetWorkorderActionData();
  }

  getautoworkorderviewdata() {
    debugger;
    this.xSaveWorkOrderViewServices
      .WorkorderViewPostData(this.WorkOderViewModelobj)
      .subscribe(Response => {
        console.log("Work Order Details", Response);
        this.griddata = Response[0];
        this.getWoColumnData();
      });

  }


  ModelObj: any;
  BindData: any;
  // this code selected event row
  showDetails(event, dataItem) {

    debugger;


    // asyc photos
    this.ClientResultPhotoModelObj.IPLNO = dataItem.IPLNO;
    this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID = dataItem.workOrder_ID;
    this.ClientResultPhotoModelObj.Type = 1;
    this.ClientResultPhotoModelObj.Client_Result_Photo_ID = 0;

    this.AsycWaitPhotoscall();

    this.TaskBidMasterModelObj.workOrder_ID = this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID;
    this.xClientResultServices
      .WorkorderViewClient(this.TaskBidMasterModelObj)
      .subscribe(response => {
        console.log("response clinet but work order se click", response);

        this.BindData = response[0][0];
        this.xMasterlayoutComponent.masterFunctionCall(this.BindData);


        this.GetWhichAction();

        // dataItem.workOrder_ID =  this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID;

        var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.workOrder_ID);

        this.xRouter.navigate(["/client/clientresult/" + btoa(encrypted)]);




      });

  }

  GetWhichAction() {
    let ClassData = {
      Class1: "active",
      Class2: "",
      Class3: "",
      Class4: "",
      ClassP: "",
    }
    this.xMasterlayoutComponent.masterFunctionCallClinetNavigateSet(ClassData);
  }

  public modelx: any;
  // this code for auto complete search only
  @ViewChild("instance") instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  SearchArrayobj = [
    { id: 1, Name: "Conoli, stave" },
    { id: 2, Name: "Company, Training" },
    { id: 3, Name: "Hill, Corey" },
    { id: 4, Name: "Jon Due" },
    { id: 5, Name: "Scott Tiger" },
    { id: 6, Name: "John Wik" },
    { id: 7, Name: "THe Rock" },
    { id: 7, Name: "RKO" },
    { id: 7, Name: "Jon cena" },
    { id: 7, Name: "TRICHITRICHITRICHITRICHITRICHI " }
  ];

  //public modelx: any;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      merge(this.focus$),
      merge(this.click$.pipe(filter(() => !this.instance.isPopupOpen()))),
      map(term =>
        (term === ""
          ? this.SearchArrayobj
          : this.SearchArrayobj.filter(
            v => v.Name.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
        ).slice(0, 7)
      )
    );
  formatter = (x: { Name: string }) => x.Name;




  //  photos call photos call

  AsycWaitPhotoscall() {


    debugger;
    var promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("Async Work Complete");
        resolve();
        // un comment this
        //this.GetClientImages();

      }, 7000);
    });
  }



  // photos call
  GetClientImages() {
    this.xClientResultOldPhotoServices
      .ViewCLientImagesData(this.ClientResultPhotoModelObj)
      .subscribe(response => {
        console.log("ClientPhotos MAIN", response);
        // this.GetPhotos = response[0];
        // this.GetLocalPhotos = response[1];

        //this.xMasterlayoutComponent.MasterFunctionClientResultAsycPhotosSet(this.GetLocalPhotos);

      });

  }
  collist1: any;
  collist2: any;
  getWoColumnData(): any {
    debugger;

    this.xSaveWorkOrderViewServices
      .WorkorderColumnPostData(this.WorOrderColumnObj)
      .subscribe(Response => {
        console.log("column", Response);
        this.collist1 = Response[0];
        this.collist2 = Response[1];

        this.jsondataA = Response[0];
        this.jsondataB = Response[1];
        console.log('arrraj', this.jsondataB);

        console.log('First A',JSON.stringify(this.jsondataA));
        console.log('First B',JSON.stringify(this.jsondataB));

        localStorage.removeItem("DataA");
        localStorage.removeItem("DataB");
 
         localStorage.setItem("DataA", JSON.stringify(this.jsondataA));
         localStorage.setItem("DataB", JSON.stringify(this.jsondataB));
       
          if ( this.jsondataB != undefined) {
            this.matchdata();
          }


        console.log('Firsttime A');
        console.log('Firsttime B');
       // this.matchdata();
       
  //       if (this.collist1 != null) {
  //         for(let i = 0; i < this.collist1.length; i++)
  //         {
  //          this.jsondataA.push(this.collist1[i]);
  //         }
  //         this.matchdata();
  //       }
  // if (  this.collist2 != null) {
  //   for (let j = 0; j < this.collist2.length; j++) {
  //     this.jsondataB.push(this.collist2[j]);
       
  //    }
  //    this.matchdata();
  // }
  
       


       


        this.dataA = new DataManager({
          json: this.collist1
        });
       
        this.dataB = new DataManager({
          json: this.collist2
         
        });
       
      });
     
  }
  //match parameter json obje to grid
  dataBB : any;
  dataAA: any;
  matchdata() {
   
    debugger;
   console.log('match data called');
   
    this.dataAA = JSON.parse(localStorage.getItem('DataA'));
    this.dataBB = JSON.parse(localStorage.getItem('DataB'));
    
    let dataobj = this.griddata.map((o) => {
      return Object.keys(o)
    }).reduce((prev, curr) => {
      return prev.concat(curr)
    }).filter((col, i, array) => {
      return array.indexOf(col) === i
    });
   

    



   for (let i = 0; i < dataobj.length; i++) {
    console.log('key',dataobj[i]);
   
    var result =  this.dataBB.some(t=>t.Keydata === dataobj[i]);
      
         if (result) {
            console.log('matchval',dataobj[i])

          switch (dataobj[i]) {
            case "workOrderNumber":
              {
                this.workd = true;
                break;
              }
            case "status":
              {
                this.sts = true;
                break;
              }
            case "address1":
              {
                this.address = true;
                break;
              }
              case "workOrderInfo":
                {
                  this.workinfo = true;
                  break;
                }
                case "address2":
                  {
                    this.add2 = true;
                    break;
                  }
                  case "city":
                    {
                      this.cit = true;
                      break;
                    }
                    case "country":
                      {
                        this.cont = true;
                        break;
                      }
                      case "Received_Date":
                        {
                          this.rec = true;
                          break;
                        }
                        case "EstimatedDate":
                          {
                            this.est = true;
                            break;
                          }
                          case "clientDueDate":
                            {
                              this.clntd = true;
                              break;
                            }
                            case "Com_Name":
                              {
                                this.comn = true;
                                break;
                              }
                              case "Complete_Date":
                                {
                                  this.compd = true;
                                  break;
                                }
                                case "Cancel_Date":
                                  {
                                    this.Cancd = true;
                                    break;
                                  }
                                  case "DateCreated":
                                  {
                                    this.createdd = true;
                                    break;
                                  }
                                  case "startDate":
                                    {
                                      this.strtd = true;
                                      break;
                                    }
                                    case "Loan_Number":
                                      {
                                        this.loannum = true;
                                        break;
                                      }
                                      case "Loan_Info":
                                        {
                                          this.loaninf = true;
                                          break;
                                        }
                                        case "Lotsize":
                                          {
                                            this.lots = true;
                                            break;
                                          }
                                          case "Lock_Code":
                                            {
                                              this.lockc = true;
                                              break;
                                            }
                                            case "assigned_admin":
                                              {
                                                this.adm = true;
                                                break;
                                              }
                                              case "dueDate":
                                                {
                                                  this.due = true;
                                                  break;
                                                }
                                                case "state":
                                                  {
                                                    this.stat = true;
                                                    break;
                                                  }
                                                  case "zip":
                                                    {
                                                      this.zp = true;
                                                      break;
                                                    }
                                                    case "IPLNO":
                                                      {
                                                        this.ipl = true;
                                                        break;
                                                      }

          }

        }

      

  
   }

   for (let i = 0; i < dataobj.length; i++) {
    console.log('key',dataobj[i]);
   
    var result1 =  this.dataAA.some(t=>t.Keydata === dataobj[i]);
      
         if (result1) {
          switch (dataobj[i]) {
            case "workOrderNumber":
              {
                this.workd = false;
                break;
              }
            case "status":
              {
                this.sts = false;
                break;
              }
            case "address1":
              {
                this.address = false;
                break;
              }
              case "workOrderInfo":
                {
                  this.workinfo = true;
                  break;
                }
                case "address2":
                  {
                    this.add2 = true;
                    break;
                  }
                  case "city":
                    {
                      this.cit = false;
                      break;
                    }
                    case "country":
                      {
                        this.cont = true;
                        break;
                      }
                      case "Received_Date":
                        {
                          this.rec = true;
                          break;
                        }
                        case "EstimatedDate":
                          {
                            this.est = true;
                            break;
                          }
                          case "clientDueDate":
                            {
                              this.clntd = true;
                              break;
                            }
                            case "Com_Name":
                              {
                                this.comn = true;
                                break;
                              }
                              case "Complete_Date":
                                {
                                  this.compd = true;
                                  break;
                                }
                                case "Cancel_Date":
                                  {
                                    this.Cancd = true;
                                    break;
                                  }
                                  case "DateCreated":
                                  {
                                    this.createdd = true;
                                    break;
                                  }
                                  case "startDate":
                                    {
                                      this.strtd = true;
                                      break;
                                    }
                                    case "Loan_Number":
                                      {
                                        this.loannum = true;
                                        break;
                                      }
                                      case "Loan_Info":
                                        {
                                          this.loaninf = true;
                                          break;
                                        }
                                        case "Lotsize":
                                          {
                                            this.lots = false;
                                            break;
                                          }
                                          case "Lock_Code":
                                            {
                                              this.lockc = false;
                                              break;
                                            }
                                            case "assigned_admin":
                                              {
                                                this.adm = true;
                                                break;
                                              }
                                              case "dueDate":
                                                {
                                                  this.due = false;
                                                  break;
                                                }
                                                case "state":
                                                  {
                                                    this.stat = false;
                                                    break;
                                                  }
                                                  case "zip":
                                                    {
                                                      this.zp = false;
                                                      break;
                                                    }
                                                    case "IPLNO":
                                                      {
                                                        this.ipl = false;
                                                        break;
                                                      }

          }

        }

      

  
   }
   
  }

  SaveChangesColumn() {
    debugger
    var WC_Show_Column_Jsonarr = [];
    var WC_Hide_Column_Jsonarr = [];

    // WC_Show_Column_Jsonarr = this.jsondataD;
    // WC_Hide_Column_Jsonarr = this.jsondataC;
    let User = JSON.parse(localStorage.getItem('usertemp_'));
    console.log('user', User)
    this.WorOrderColumnjsonObj.WC_UserId = User[0].User_pkeyID;
   //   this.WorOrderColumnjsonObj.WC_Show_Column_Jsonarr = JSON.stringify(this.jsondataA );
   
   //   this.WorOrderColumnjsonObj.WC_Hide_Column_Jsonarr = JSON.stringify(this.jsondataB );
   
      this.WorOrderColumnjsonObj.WC_Show_Column_Jsonarr = localStorage.getItem('DataA');
   
      this.WorOrderColumnjsonObj.WC_Hide_Column_Jsonarr = localStorage.getItem('DataB');


   

     // localStorage.getItem('DataA')

    this.xSaveWorkOrderViewServices
      .jsonColumnPostData(this.WorOrderColumnjsonObj)
      .subscribe(Response => {
        console.log("addcolumn", Response);
        debugger
        alert('Column record has been updated...');
        //  localStorage.removeItem("DataA");
        //   localStorage.removeItem("DataB");
       
      })
     
      this.matchdata();  
     
  }

  
  public fields: FieldSettingsModel = { text: 'Wo_Column_Name' };
  public dataB: DataManager = new DataManager({
    json: this.collist2

  });
  public modifiedDataA: ModifiedRecords = { addedRecords: [], deletedRecords: [], changedRecords: [] };
  public modifiedDataB: ModifiedRecords = { addedRecords: [], deletedRecords: [], changedRecords: [] };
  public saveChanges(): void {

    debugger
    this.dataA.saveChanges(this.modifiedDataA, this.fields.text);

    this.dataB.saveChanges(this.modifiedDataB, this.fields.text);

    this.modifiedDataA.addedRecords = []; this.modifiedDataB.addedRecords = [];
   // this.jsondataC = [];
   //this.jsondataD = [];
    //this.jsondataA = JSON.parse(JSON.stringify(this.dataA.dataSource.json));
   // this.jsondataB = JSON.parse(JSON.stringify(this.dataB.dataSource.json));

    // for (let i = 0; i <this.jsondataB.length; i++) {
    // var resultC = this.jsondataC.some(t=>t.Keydata === this.jsondataB[i].Keydata);
    //  console.log('check',resultC)
    //  if (!resultC)
    //  {
    //    this.jsondataC.push(this.jsondataB[i]);  
    //  }    
    // //this.jsondataC.push(this.dataB.dataSource.json[i]);  
    
    //  }
    //console.log( 'checkdataC', this.jsondataC)

  //   for (let i = 0; i <this.jsondataA.length; i++) {
  //     var resultD = this.jsondataD.some(t=>t.Keydata === this.jsondataA[i]);
  //     // console.log('check',result)
  //      if (!resultD)
  //      {
  //      this.jsondataD.push(this.jsondataA[i]);  
  //      }    
  //  //   this.jsondataD.push(this.dataA.dataSource.json[i]);  
      
  //     }

  //   console.log( 'checkdataD', this.jsondataD)
   this.SaveChangesColumn();
  }
  public onDropGroupA(args: DragEventArgs): void {
    console.log("arr", args)
    debugger
    args.items.forEach((item: { [key: string]: Object; }): void => {
      /*Preventing item manipulation while doing drag and drop within same list box.*/
      


    
      this.jsondataA = [];
      this.jsondataA = JSON.parse(localStorage.getItem('DataA'))
      console.log( 'dataBefore A', JSON.stringify(this.jsondataA))

       if (!this.listObj1.getDataByValue(item[this.fields.text] as string)) {
        this.modifiedDataB.addedRecords.push(item);
        this.modifiedDataA.deletedRecords.push(item);
       debugger
       if(this.jsondataA != null){
        for (let i = 0; i < this.jsondataA.length; i++) 
        {
          
             if(this.jsondataA[i].Keydata == item.Keydata)
             {
               console.log('mtA',item.Keydata);
               console.log('mtA',i);
               console.log('mtA',this.jsondataA[i]);
              this.jsondataA.splice(i,1);
              //break;
             }   
        }
      }

        if (this.jsondataB != undefined )
        {

          var resultB = this.jsondataB.some(t=>t.Keydata === item.Keydata);
          console.log('resultB',resultB);
          if (!resultB) {
            console.log('resultBloop');
            this.jsondataB.push(item); 
          }


         
        } 
        else
        {
          this.jsondataB = [];
         this.jsondataB.push(item);
        } 
   
      
        console.log( 'DataAa',JSON.stringify(this.jsondataA))
        console.log( 'DataB',JSON.stringify(this.jsondataB))
      
 
        localStorage.removeItem("DataA");
         localStorage.removeItem("DataB");
 
         localStorage.setItem("DataA", JSON.stringify(this.jsondataA));
         localStorage.setItem("DataB", JSON.stringify(this.jsondataB));



        // console.log( 'dataa', this.modifiedDataB.addedRecords)
        // console.log( 'dataad', this.modifiedDataA.deletedRecords)
       
      }
    });
  }
  public onDropGroupB(args: DragEventArgs): void {
   debugger
   console.log("arrB", args)
    args.items.forEach((item: { [key: string]: Object; }): void => {


     

     this.jsondataB = [];
     this.jsondataB = JSON.parse(localStorage.getItem('DataB'))
     console.log( 'dataBefore B', JSON.stringify(this.jsondataB))


      if (!this.listObj2.getDataByValue(item[this.fields.text] as string)) {
        this.modifiedDataA.addedRecords.push(item);
        this.modifiedDataB.deletedRecords.push(item);
       debugger
       if(this.jsondataB != null){
        for (let i = 0; i <this.jsondataB.length; i++) 
        {
          //var result = this.jsondataB.some(t=>t.Keydata === item.Keydata);
             if(this.jsondataB[i].Keydata == item.Keydata)
             {
              console.log('mtB',item.Keydata);
              console.log('mtB',i);
              console.log('mtB',this.jsondataB[i]);
              this.jsondataB.splice(i,1);
            //  break;
             }   
        }
      }

        if (this.jsondataA != undefined )
        {
          var resultA = this.jsondataA.some(t=>t.Keydata === item.Keydata);
          console.log('resultA',resultA);
          if (!resultA) {
            console.log('resultAloop');
            this.jsondataA.push(item);   
          }
         
        } 
        else
        {this.jsondataA = [];
         this.jsondataA.push(item);
        } 


        // console.log( 'datba', this.modifiedDataA.addedRecords)
        // console.log( 'databd', this.modifiedDataB.deletedRecords)
         console.log( 'DataBb',JSON.stringify(this.jsondataB))
        console.log( 'DataA',JSON.stringify(this.jsondataA))
       
        localStorage.removeItem("DataA");
       localStorage.removeItem("DataB");

        localStorage.setItem("DataA", JSON.stringify(this.jsondataA));
        localStorage.setItem("DataB", JSON.stringify(this.jsondataB));

      }
    });
  }

// for status 
Statuslst:any;
GetStatusDropDown()
{
  debugger
  this.CommonStatusDTOObj.Status_ID = 0;
  this.CommonStatusDTOObj.Type = 1;
  this.xClientResultsInvoiceServices
  .DropdownGetStatus(this.CommonStatusDTOObj)
  .subscribe(response => {
    console.log("status data", response);
    debugger;
    this.Statuslst = response[0];
   
})

}


FilterData(item){
debugger
console.log('dummy',item); 
this.CommonStatusDTOObj.whereclause = JSON.stringify(item);
this.xSaveWorkOrderViewServices
.FilterWoStatusPostData(this.CommonStatusDTOObj)
.subscribe(response => {
console.log('staus filter',response);
this.griddata = response[0];
})
 
}

//actions
Actionlst: any;
GetWorkorderActionData()
{
this.xSaveWorkOrderViewServices
.ActionData(this.WorkOrderActionsObj)
.subscribe(response =>{
console.log('action details',response);
this.Actionlst = response[0];
});
}
arr : any;
ActionChanges(item, content){
  debugger
  this.contentx = content;
  switch (item.Wo_Column_PkeyId) {
    case 31:
      {
        this.title = 'Change Contractor';
    this.labelname = 'Contractor';
      this.arr = 31
    this.commonMessage(this.contentx);
        break;
      }
      case 32:
        {
          this.title = 'Change Coordinator';
          this.labelname = 'Coordinator';
          this.arr = 32
          this.commonMessage(this.contentx);
          break;
        }
        case 33:
          {
            this.title = 'Change Processor';
            this.labelname = 'Processor';
            this.arr = 33
            this.commonMessage(this.contentx);
            break;
          }
          case 34:
            {
              this.title = 'Change Client Company';
              this.labelname = 'Client Company';
              this.arr = 34
              this.commonMessage(this.contentx);
              break;
            }
            case 35:
            {
              this.title = 'Change Work Type';
              this.labelname = 'Work Type';
              this.arr = 35
              this.commonMessage(this.contentx);
              break;
            }
            case 36:
              {
                this.title = 'Change Due Date';
                this.labelname = 'Due Date';
                this.arr = 36
                this.commonMessage(this.contentx);
                break;
              }
              case 37:
                {
                  this.title = 'Change Start Date';
                  this.labelname = 'Start Date';
                  this.arr = 37
                  this.commonMessage(this.contentx);
                  break;
                }
                case 38:
                  {
                    this.title = 'Change Client Due Date';
                    this.labelname = 'Client Due Date';
                    this.arr = 38
                    this.commonMessage(this.contentx);
                    break;
                  }
                  case 39:
                  {
                    this.title = 'Change Recurring Order';
                    this.labelname = 'Recurring Order';
                    this.arr = 39
                    this.commonMessage(this.contentx);
                    break;
                  }
                  case 40:
                  {
                    this.title = 'Change Modify Comments';
                    this.labelname = 'Modify Comments';
                    this.arr = 40
                    this.commonMessage(this.contentx);
                    break;
                  }
                  case 41:
                  {
                    this.title = 'Change Estimated Date';
                    this.labelname = 'Estimated Date';
                    this.arr = 41
                    this.commonMessage(this.contentx);
                    break;
                  }
                  case 45:
                    {
                      this.title = 'Change  Category';
                      this.labelname = ' Category';
                      this.arr = 45
                      this.commonMessage(this.contentx);
                      break;
                    }
                    case 46:
                      {
                        this.title = 'Change  Background Provider';
                        this.labelname = ' Background Provider';
                        this.arr = 46
                        this.commonMessage(this.contentx);
                        break;
                      }
                      case 48:
                        {
                          this.title = 'Cancel Work Order';
                          this.labelname = 'Work Order';
                          this.arr = 48
                          this.commonMessage(this.contentx);
                          break;
                        }
                        case 49:
                          {
                            this.title = 'Delete Work Order';
                            this.labelname = 'Work Order';
                            this.arr = 49
                            this.commonMessage(this.contentx);
                            break;
                          }
    }

}

commonMessage(content) {
  this.modalService
    .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
    .result.then(result => {}, reason => {});
}


//dropdown list
CompanyList:any;
WorkTypeList:any;
CategoryList:any;
ContractorList:any;
BackgroundList:any;
CordinatorList:any;
ProcessorList:any;

GetDropDowndata() {
  this.xWorkOrderDrodownServices
    .DropdownGetWorkOrder()
    .subscribe(response => {
      console.log("drop down  data ", response);
      //debugger;
      if (response.length != 0) {
        this.CompanyList = response[0];
        this.WorkTypeList = response[1];
        this.CategoryList = response[2];
        this.ContractorList = response[3];
        this.BackgroundList = response[8];
        this.CordinatorList = response[4];
        this.ProcessorList = response[5];
       // this.CustomerNumberList = response[9];
      }


    });
}
//Action checkbox grid
actionarr = [];
SelectRow(i, dataitem){
  if (dataitem.chkdata) 
  {
    this.chkclick = false;
    this.actionarr.push(dataitem)
    console.log('actionarr',this.actionarr);  }
  else
  {
    this.actionarr.splice(i,1)
  }
}

// update action workorder data

ActionFormButton(contentdata){
  debugger
 
  this.contentx = contentdata;
 
console.log('postdatacheck',this.ActionSentStoreObj);
this.ActionSentStoreObj.WorkActionArray = JSON.stringify( this.actionarr);
this.xSaveWorkOrderViewServices
.PostWOActionData(this.ActionSentStoreObj)
.subscribe(response => {
  debugger

  this.MessageFlag = "Record has been Updated...!";
 
  //$("").modal("hide");
 this.commonMessage(this.contentx);

 this.getautoworkorderviewdata();

});
}




}

//
interface ModifiedRecords {
  addedRecords: { [key: string]: Object }[];
  deletedRecords: { [key: string]: Object }[];
  changedRecords: { [key: string]: Object }[];
}
