
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { WorkOrderMapComponent } from './WorkOrderMapComponent';
import {WorkOrderMapServices} from '../WorkOrderMap/WorkOrderMapService'


import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../../environments/environment';
 
import { AgmCoreModule } from '@agm/core';
export const mapRouts = [
    { path: "map", component: WorkOrderMapComponent }
  ];
@NgModule({
  declarations: [
    WorkOrderMapComponent
  ],
  imports: [
    CommonModule,
    AngularFireMessagingModule,
    AngularFireDatabaseModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    RouterModule.forChild(mapRouts),
   // BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCMZDQNjWNzYVeZvpCz2vcQrq1tH_tWVlg",
      libraries: ['places']
    })
  ],
  providers: [WorkOrderMapServices],
  bootstrap: []
})
export class WorkOrderMapModule { }