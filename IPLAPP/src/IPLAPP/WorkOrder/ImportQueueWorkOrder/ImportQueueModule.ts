import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';


import { ImportQueueComponent } from './ImportQueueComponent';

import {HomepageServices} from '../../Home/HomeServices';


const ImportQueueRouts =[

  {path:'importqueue',component:ImportQueueComponent}

]

@NgModule({
  declarations: [ImportQueueComponent],
  imports: [
    RouterModule.forChild(ImportQueueRouts),
    CommonModule,
    GridModule,ExcelModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,

  ],
  providers: [HomepageServices],
  bootstrap: [ImportQueueComponent]
})

export class ImportQueueModule {}
