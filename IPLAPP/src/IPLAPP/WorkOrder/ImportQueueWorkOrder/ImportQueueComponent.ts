import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Router } from "@angular/router";

import {HomeModel} from '../../Home/HomeModel';
import {WorkOderViewModel} from '../WorkOrderView/WorkOrderViewModel';
import {HomepageServices} from '../../Home/HomeServices';
@Component({
  templateUrl: "./ImportQueueView.html"
})

export class ImportQueueComponent implements OnInit{

  HomeModelobj:HomeModel = new HomeModel();
  WorkOderViewModelobj:WorkOderViewModel = new WorkOderViewModel();
  formUsrLoginGroup: FormGroup; // create obj
  Test:string;
  button = 'Get Order'; // buttom loading..
  isLoading = false; // buttom loading..
  public griddata: any[];
  public GridShow = false;
  public LoginDiv = true;
  constructor(private xHomeServices: HomepageServices,private formBuilder: FormBuilder,private xRoute: Router,){


    var chk = localStorage.getItem('tempadmin');
    if(chk == 'QWERTYUIOP'){

    }else{
     this.xRoute.navigate(["/admin/login"]);
    }

  }

  ngOnInit() {


    this.formUsrLoginGroup = this.formBuilder.group({
      Eamilname: ["", Validators.required],
      passwordname: ["", Validators.required]
    });

  }
 // shortcurt Namefor form sathi
 get fx() {
  return this.formUsrLoginGroup.controls;
  }
  LoginBtn() {


    // stop here if form is invalid
    if (this.formUsrLoginGroup.invalid) {
      return;
    }
    this.isLoading = true;
    this.button = 'Processing';


    this.xHomeServices
      .WorkorderGet(this.HomeModelobj)
      .subscribe(response => {
        console.log("count  data ", response);
        debugger
        if(response.length != 0){
          var id = response[0].workOrder_ID;
          this.HomeModelobj.attribute7 = response[0].attribute7;

          this.xHomeServices.Workorder(this.WorkOderViewModelobj)
          .subscribe(response => {
            console.log("Grid data ", response);
            this.GridShow = true;
            this.griddata = response[0];
            this.LoginDiv = false;
          });
          this.isLoading = false;
          this.button = 'Get Order';
        }else{
          this.isLoading = false;
          this.button = 'Please Try Again';
          this.GridShow = false;
          this.griddata = [];
          alert("User or Token is incorrect");
        }


      });

  }



  click() {

  }

  public barChartOptions:any = {
    scaleShowVerticalLines: true,
    responsive: true
  };

    public mbarChartLabels:string[] = ['Gaurav', 'Sumit', 'Yesh','Mohit', 'Sadaf', 'Roshani', 'Pooja' ];
    public barChartType:string = 'bar';
    public barChartLegend:boolean = true;

    public barChartColors:Array<any> = [
    {
      backgroundColor: 'rgba(105,159,177,0.2)',
      borderColor: 'rgba(105,159,177,1)',
      pointBackgroundColor: 'rgba(105,159,177,1)',
      pointBorderColor: '#fafafa',
      pointHoverBackgroundColor: '#fafafa',
      pointHoverBorderColor: 'rgba(105,159,177)'
    },
    {
      backgroundColor: 'rgba(77,20,96,0.3)',
      borderColor: 'rgba(77,20,96,1)',
      pointBackgroundColor: 'rgba(77,20,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,20,96,1)'
    }
  ];
    public barChartData:any[] = [
      {data: [55, 60, 75, 82, 76, 62, 80], label: 'Activate User'},
      {data: [30, 55, 60, 79, 66, 57, 60], label: 'Deactive User'}
    ];

    // events
    public chartClicked(e:any):void {
      console.log(e);
    }

    public chartHovered(e:any):void {
      console.log(e);
    }

    public randomize():void {
      let data = [
        Math.round(Math.random() * 100),
        Math.round(Math.random() * 100),
        Math.round(Math.random() * 100),
        (Math.random() * 100),
        Math.round(Math.random() * 100),
        (Math.random() * 100),
        Math.round(Math.random() * 100)];
      let clone = JSON.parse(JSON.stringify(this.barChartData));
      clone[0].data = data;
      this.barChartData = clone;
    }





}
