import { Component, OnInit,ViewChild } from '@angular/core';
import { WorkOrderDrodownServices } from '../../CommonServices/commonDropDown/DropdownService';
import { AddGroupsServices } from '../../Admin/Groups/AddGroups/AddGroupsServices';
import { GrouproleModel } from '../../Admin/Groups/AddGroups/AddGroupsModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import{DocumentAndFormsDTO,FileMasterModel} from './DocumentAndFormModel';
import {DocumentAndFormsServices} from './DocumentAndFormsService';
import {ClientResultOldPhotoServices} from '../../ClientResult/ClientResultPhoto/ClientResultphotosOldServices'
import { BindDataModel } from "../../ClientResult/ClientResults/ClientResultModel";
import { NgbPanelChangeEvent, NgbAccordion } from '@ng-bootstrap/ng-bootstrap';

declare var require: any
const FileSaver = require('file-saver');


@Component({
  
  templateUrl: './DocumentAndForm.html',
  styleUrls: ['./DocumentAndForm.css']
 
})
export class DocumentAndFormComponent implements OnInit {

  nrSelect = 'Select';
  submitted = false; 
  submitted1 = false;// submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  button1 = "Save"; // buttom loading..
  isLoading = false; 
  isLoading1 = false;// buttom loading..
 isdisable = false;
 isdisable1 = false;
 IsEditDisable = false;
 FoldervalFlag = false;
 FilevalFlag = false;
 folderhide = true;
 foldershow = true;
  public contentx;
  MessageFlag: string;
  dropCkck = false; // common dropdown
 
  BindDataModelObj: BindDataModel = new BindDataModel();
  FileMasterModelObj:FileMasterModel = new FileMasterModel();
  DocumentAndFormsDTOObj: DocumentAndFormsDTO = new DocumentAndFormsDTO();
  GrouproleModelObj: GrouproleModel = new GrouproleModel();




  @ViewChild('acc') accordion: NgbAccordion;
  model = {};
  currentStep = 1;
  
  toggle(step) {
    this.currentStep = this.currentStep < step ? step : this.currentStep;
    setTimeout(() => this.accordion.toggle('toggle-' + step), 0);
  }
  
  beforeChange($event: NgbPanelChangeEvent) {
    const availStep = 'toggle-' + this.currentStep;
    if ($event.panelId > availStep) {
      $event.preventDefault();
    }
  };  




    constructor(
    private formBuilder: FormBuilder,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xAddGroupsServices: AddGroupsServices,
    private xmodalService: NgbModal,
    private xDocumentAndFormsServices : DocumentAndFormsServices,
    private xClientResultOldPhotoServices : ClientResultOldPhotoServices
    ) { 
      this.FormArrayVal = [
        {
          Task_sett_State: [],
          Task_sett_Country: [],
          Task_sett_Zip: null,
          Task_sett_Customer: [],
          Task_sett_Company: [],
          Task_sett_Lone: [],
          Task_Work_TypeGroup: [],
          WTTaskWorkType: [],
          Task_sett_IsActive: true,
          Task_sett_IsDelete: false
        }
      ];
    }




  

    FormArrayVal = [];


    dropdownSettings = {};
    dropdownSettingsState = {};
  
    dropdownSettingsCustomer = {};
    dropdownSettingsLoanType = {};
    dropdownSettingsCountry = {};
    dropdownSettingsWorkTypeList = {};
    dropdownSettingsWorkTypeCategory = {};

  ngOnInit() {

    this.formUsrCommonGroup = this.formBuilder.group({
      
      GroupRoleVal:["", Validators.nullValidator],
      Folderval: ["", Validators.nullValidator],
      FoldValName: ["", Validators.nullValidator],
      FileDataval:["", Validators.nullValidator],
      FileDesc:["", Validators.nullValidator],
      FoldDesc:["", Validators.nullValidator]
    });
    // this.GetParentModelData();
   
   


    // this setting for multiple drop down select values
    this.dropdownSettings = {
      singleSelection: false,
      idField: "Client_pkeyID",
      textField: "Client_Company_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    // this setting for multiple drop down select values state
    this.dropdownSettingsState = {
      singleSelection: false,
      idField: "IPL_StateID",
      textField: "IPL_StateName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
   
   
    // this setting for multiple drop down select values Customer
    this.dropdownSettingsCustomer = {
      singleSelection: false,
      idField: "Cust_Num_pkeyId",
      textField: "Cust_Num_Number",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsLoanType = {
      singleSelection: false,
      idField: "Loan_pkeyId",
      textField: "Loan_Type",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsCountry = {
      singleSelection: false,
      idField: "Country_pkeyId",
      textField: "Country_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsWorkTypeList = {
      singleSelection: false,
      idField: "WT_pkeyID",
      textField: "WT_WorkType",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsWorkTypeCategory = {
      singleSelection: false,
      idField: "Work_Type_Cat_pkeyID",
      textField: "Work_Type_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
  }

  ngAfterViewInit() {

    this.GetParentModelData();
  }



  parentlst:any;
GetParentModelData(){
  this.xDocumentAndFormsServices
  .GetParentFolder(this.DocumentAndFormsDTOObj)
  .subscribe(response => {
    this.parentlst = response;
    console.log('getpapa', response)

  })
  this.GetDropDowndata();
}

//Edit parent Folder Details
EditData(ParentDetails)
{
  this.folderhide = false;
  this.foldershow = true;
  this.DocumentAndFormsDTOObj.Fold_Pkey_Id = ParentDetails.Fold_Pkey_Id;
  this.xDocumentAndFormsServices
  .GetsingleParentFolder(this.DocumentAndFormsDTOObj)
  .subscribe(response => {
    debugger
    console.log('editdata', response)
    
    // this.assinearr.push(response[2]);
    this.FormArrayVal = response[1];
    if (response[0].length > 0) {
      this.DocumentAndFormsDTOObj.Fold_Pkey_Id = response[0][0].Fold_Pkey_Id;
      this.DocumentAndFormsDTOObj.Fold_Name = response[0][0].Fold_Name;
      this.DocumentAndFormsDTOObj.Fold_Parent_Id = response[0][0].Fold_Parent_Id;
      this.DocumentAndFormsDTOObj.Fold_Desc = response[0][0].Fold_Desc;
      this.DocumentAndFormsDTOObj.Fold_IsActive = response[0][0].Fold_IsActive;

    }
    if (response[2].length > 0) {
     
     for (let i = 0; i < response[2].length; i++) {

     debugger
       console.log('Roleid',response[2][i].Fold_Role_GroupRole_Id);
       console.log('arrrole',JSON.stringify(this.GroupRoleList));
          this.GroupRoleList.find(v => v.Group_DR_PkeyID == response[2][i].Fold_Role_GroupRole_Id )
          .checkitem = response[2][i].Fold_Role_IsActive;

         
       
     }
    }
    
    console.log('afterarrrole', JSON.stringify(this.GroupRoleList));

    if (this.FormArrayVal.length > 0) {
      for (let i = 0; this.FormArrayVal.length > i; i++) {
        if (this.FormArrayVal[i].Fold_Auto_Assine_Client) {
          this.FormArrayVal[i].Task_sett_Company = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_Client

          );
        }
       
        if (this.FormArrayVal[i].Fold_Auto_Assine_County) {
          this.FormArrayVal[i].Task_sett_Country = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_County
          );
        }

        if (this.FormArrayVal[i].Fold_Auto_Assine_Customer) {
          this.FormArrayVal[i].Task_sett_Customer = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_Customer
          );
        }

        if (this.FormArrayVal[i].Fold_Auto_Assine_LoneType) {
          this.FormArrayVal[i].Task_sett_Lone = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_LoneType
          );
        }
        if (this.FormArrayVal[i].Fold_Auto_Assine_State) {
          this.FormArrayVal[i].Task_sett_State = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_State
          );
        }
        if (this.FormArrayVal[i].Fold_Auto_Assine_WorkType_Group) {
          this.FormArrayVal[i].Task_Work_TypeGroup = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_WorkType_Group
          );
        }
        if (this.FormArrayVal[i].Fold_Auto_Assine_WorkType) {
          this.FormArrayVal[i].WTTaskWorkType = JSON.parse(
            this.FormArrayVal[i].Fold_Auto_Assine_WorkType
           
          );
          console.log('wt', this.FormArrayVal[i].Task_Work_Type)
        }
      
          this.FormArrayVal[i].Task_sett_Zip =  this.FormArrayVal[i].Fold_Auto_Assine_Zip;
          this.DocumentAndFormsDTOObj.Fold_Auto_Assine_PkeyId = this.FormArrayVal[i].Fold_Auto_Assine_PkeyId;
         
       
      }
    } else {
      this.FormArrayVal = [
        {
          Task_sett_State: [],
          Task_sett_Country: [],
          Task_sett_Zip: null,
          Task_sett_Customer: [],
          Task_sett_Company: [],
          Task_sett_Lone: [],
          Task_Work_TypeGroup: [],
          WTTaskWorkType: [],
          Task_sett_IsActive: true,
          Task_sett_IsDelete: false
        }
      ];
    }
   
  })

}



  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
 

  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // common message modal popup
  commonMessage(tent) {
    this.xmodalService
      .open(tent, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {},
        reason => {}
      );
  }
  /// end common model
  assinearr = [];
  checkitem: any;
  assinerole(arg, i){
    debugger
    console.log('argval',arg)
    console.log('ivall',i)
    this.assinearr= this.GroupRoleList
   // this.GroupRoleList.find(v => v.Group_DR_PkeyID == response[2][i].Fold_Role_GroupRole_Id )
    //.checkitem = response[2][i].Fold_Role_IsActive;



  // if (arg.checkitem == true) {
  //   this.assinearr.push(arg);
  //   console.log('pushcode',this.assinearr)
  // }else
  // {
  //   this.assinearr.splice(i, 1)
  //   console.log('remove',this.assinearr)
  // }

  }
// submit form  start
regis:boolean = true;
FormButton(content) {
  debugger;
  this.contentx = content;
 
  this.submitted = true;
  this.isdisable = false;
  

  this.DocumentAndFormsDTOObj.AutoAssinArray =  this.FormArrayVal;
  //this.DocumentAndFormsDTOObj.PermisionArray = this.assinearr;
  this.DocumentAndFormsDTOObj.PermisionArray = this.GroupRoleList;
  // console.log('arraydata',  this.DocumentAndFormsDTOObj.AutoAssinArray );
  console.log('arraydata',  this.DocumentAndFormsDTOObj.Fold_Name );

  if (this.DocumentAndFormsDTOObj.Fold_Name == '' || this.DocumentAndFormsDTOObj.Fold_Name == null) {
    this.regis = false;
  
    return ;
  }

  // only drop down
  //this.dropCkck = false;

  // if (this.DocumentAndFormsDTOObj.Fold_Parent_Id == 0) {
  //   this.FoldervalFlag = true;
  //   //return;
  //   this.dropCkck = true;
  // }
  // stop here if form is invalid
  // if (this.formUsrCommonGroup.invalid) {
  //   return;
  // }

  this.isLoading = true;
  this.button = "Processing";
  this.FolderDetaisDataPost()

}

FolderDetaisDataPost(){
  console.log('chkjson', this.DocumentAndFormsDTOObj);
  
   this.xDocumentAndFormsServices
      .FolderdataPost(this.DocumentAndFormsDTOObj)
      .subscribe(response => {
        debugger;
        console.log("folderId", response);
        if (response[0] != "0") {
          this.MessageFlag = "Folder Created ...!";
          this.isLoading = false;
         
          this.button = "Save";
        
          this.commonMessage(this.contentx);

         this.formUsrCommonGroup.reset()
          this.DocumentAndFormsDTOObj.Fold_Pkey_Id = 0;
          this.GetParentModelData();
          this.folderhide = true;
          this.foldershow = true;
          this.regis = true;
        
         
          this.FormArrayVal = [
            {
              Task_sett_State: [],
              Task_sett_Country: [],
              Task_sett_Zip: null,
              Task_sett_Customer: [],
              Task_sett_Company: [],
              Task_sett_Lone: [],
              Task_Work_TypeGroup: [],
              WTTaskWorkType: [],
              Task_sett_IsActive: true,
              Task_sett_IsDelete: false
            }
          ];
        }
        })
       
      }
      
// file upload
inputelement:any;
uploadfiledoc(documentInput: any){
  debugger;
  
this.inputelement = documentInput.target.files[0]
 
}
File_Method(){
  this.FilevalFlag = false;
}

fileList:any;
FileDatastore(content) {
  debugger
  this.contentx = content;
 
  this.submitted1 = true;
  this.isdisable1 = false;

  // only drop down
  this.dropCkck = false;

  if (this.FileMasterModelObj.Fold_File_ParentId == 0) {
    this.FilevalFlag = true;
   
    this.dropCkck = true;
  }
 
  // if (this.formUsrCommonGroup.invalid) {
  //   return;
  // }

  this.isLoading1 = true;
  this.button1 = "Processing";

 
 
  if (this.inputelement == '' || this.inputelement == undefined) 
  {

    this.xDocumentAndFormsServices
    .UpdateFileDetails(this.FileMasterModelObj)
    .subscribe(response => {
      console.log("udated flie",response )
      if (response[0] != "0") {
       
        this.MessageFlag = "File Details Updated Successfully ...!";
        this.isLoading1 = false;
        this.button1 = "Save";
        this.formUsrCommonGroup.reset()

        this.commonMessage(this.contentx);
        this.folderhide = true;
        this.foldershow = true;
        this.GetParentModelData();
       
      }
    });
   
 
  }
  else
  {
    this.BindDataModelObj.Client_Result_Photo_ID = this.FileMasterModelObj.Fold_File_Pkey_Id;
    this.BindDataModelObj.Client_Result_Photo_Ch_ID = this.FileMasterModelObj.Fold_File_ParentId;
    this.BindDataModelObj.Client_Result_Photo_Ch_ID = this.FileMasterModelObj.Fold_File_ParentId;
    this.BindDataModelObj.Client_PageCalled = 4;
   this.BindDataModelObj.IPLNO = 'Forms and Docs'
   this.BindDataModelObj.documentx = this.inputelement;
   this.BindDataModelObj.Client_Result_File_Desc = this.FileMasterModelObj.Fold_File_Desc;
   this.BindDataModelObj.Client_Result_Photo_FileName = this.inputelement.name;
 
 
   this.xClientResultOldPhotoServices
   .CommonDocumentsUpdate(this.BindDataModelObj)
   .then((res) => {
     res.subscribe(response => {
       if (response[0] != "0") {
         this.MessageFlag = "File Details Save Successfully ...!";
         this.isLoading1 = false;
         this.button1 = "Save";
       
         this.commonMessage(this.contentx);
         this.folderhide = true;
         this.foldershow = true;
         this.GetParentModelData();
       }
     });
    
   })
    
  }
 
  
}
  
 
//End
  
  
  ///get drop
  
  dropdownList = [];
  StateList: any;
  CustomerNumberList: any;
  CompanyList: any; // temp array
  CustomerList: any; // temp array
  WorkTypeList: any; // temp array
  CategoryList: any; // temp array
  ContractorList: any; // temp array
  CordinatorList: any;
  LoanTypeList: any;
  CountryList: any;
  WorkTypeCategory: any; //group
  
  
  
  GetDropDowndata() {
    this.xWorkOrderDrodownServices
      .DropdownGetWorkOrder()
      .subscribe(response => {
        console.log("drop down  data ", response);
        //debugger;
        if (response.length != 0) {
          this.StateList = response[6];
          this.CustomerNumberList = response[9];
          this.CompanyList = response[0];
          this.WorkTypeList = response[1];
          this.CategoryList = response[2];

          this.ContractorList = response[3];
         
          this.CordinatorList = response[4];
         
          this.LoanTypeList = response[10];

          this.dropdownList = response[0];

          this.CountryList = [{ Country_pkeyId: 1, Country_Name: "USA" }];

          this.WorkTypeCategory = response[11];
        }
      });
      this.getGroupRoleDrd();
  }


  //get role dropdown
  GroupRoleList: any;  
  folderlst :any;
  getGroupRoleDrd() {
    debugger;
    this.xAddGroupsServices.GetGroupdrd(this.GrouproleModelObj)
    .subscribe(response => {
      debugger;
      console.log('drddata',response );
      this.GroupRoleList = response[0];
      this.folderlst = response[1]
      
    });
    // this.GetMenuDetails()
  }

  Folder_Method(){
    this.FoldervalFlag = false;
  }

  flag:boolean = false;
  hidedaa:boolean = true;
  HideshowDetails(arg){
    debugger;
   console.log('chk',arg)
if (arg) 
{
  this.hidedaa = false;
}else{
  this.hidedaa = true;
}
  }

  HideShowDiv(arg){
    if (arg == 1) {
      this.folderhide = false;
      this.foldershow = true;
    }
    else
    {
      this.folderhide = true;
      this.foldershow = false;
    }
  }


  

 // test accorditon

//  toggleAccordian(event, index) {
//    debugger
//    console.log(event);
//   var element = event.target;
//   element.classList.toggle("active");
//   if(this.parentlst[index].isActive) {
//     this.parentlst[index].isActive = false;
//   } else {
//     this.parentlst[index].isActive = true;
//   }      
//   var panel = element.nextElementSibling;
//   if (panel.style.maxHeight) {
//     panel.style.maxHeight = 50;
//   } else {
//     panel.style.maxHeight = panel.scrollHeight + "px";
//   }
// }

   EditForms() {
     debugger
    this.IsEditDisable = false;
   
    this.formUsrCommonGroup.enable();

   
  }

  filelst:any;
EditFileData(arg){
  console.log('Editfile',arg)
  this.folderhide = true;
  this.foldershow = false;
  this.FileMasterModelObj.Fold_File_Pkey_Id = arg.Fold_File_Pkey_Id;
  this.FileMasterModelObj.Type = 2;

  this.xDocumentAndFormsServices
  .GetFileeditDetails(this.FileMasterModelObj)
  .subscribe(response => {
    this.filelst = response[0];
    debugger
    console.log('editfiledata', response)
   
    this.FileMasterModelObj.Fold_File_Pkey_Id = response[0][0].Fold_File_Pkey_Id;
    this.FileMasterModelObj.Fold_File_ParentId = response[0][0].Fold_File_ParentId;
    this.FileMasterModelObj.Fold_File_Role_Folder_Id = response[0][0].Fold_File_Role_Folder_Id;
    this.FileMasterModelObj.Fold_File_Folder_Name = response[0][0].Fold_File_Folder_Name;
    this.FileMasterModelObj.Fold_File_Local_Path = response[0][0].Fold_File_Local_Path;
    this.FileMasterModelObj.Fold_File_Object_Name = response[0][0].Fold_File_Object_Name;
    this.FileMasterModelObj.Fold_File_Folder_Name = response[0][0].Fold_File_Folder_Name;
    this.FileMasterModelObj.Fold_File_Desc = response[0][0].Fold_File_Desc;

  });

}

downloadPdf(pdfUrl: string, pdfName: string ) {
  console.log('url',pdfUrl);
  console.log('name',pdfName)
  //const pdfUrl = './assets/sample.pdf';
  //const pdfName = 'your_pdf_file';
  FileSaver.saveAs(pdfUrl, pdfName);
}


  clearfolderForm(form: any) {
    form.reset();
    this.folderhide = true;
    this.foldershow = true;
    this.regis = true;
    this.FormArrayVal = [
      {
        Task_sett_State: [],
        Task_sett_Country: [],
        Task_sett_Zip: null,
        Task_sett_Customer: [],
        Task_sett_Company: [],
        Task_sett_Lone: [],
        Task_Work_TypeGroup: [],
        WTTaskWorkType: [],
        Task_sett_IsActive: true,
        Task_sett_IsDelete: false
        
      }
    ];
    }

    fileClear(form: any){
      debugger;
      form.reset();
      this.FileMasterModelObj.Fold_File_ParentId = 0;
      this.FileMasterModelObj.Fold_File_Desc = '';
      this.inputelement = ''
      this.folderhide = true;
      this.foldershow = true;
      
    }



}
