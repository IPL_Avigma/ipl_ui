import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DocumentAndFormComponent } from './DocumentAndFormComponent';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PanelBarModule } from '@progress/kendo-angular-layout';


export const DocsRouts = [
  { path: "formsanddocs", component: DocumentAndFormComponent }
];

@NgModule({
  declarations: [DocumentAndFormComponent],
  imports: [
    RouterModule.forChild(DocsRouts),
    CommonModule,
    FormsModule,
    NgMultiSelectDropDownModule,
    NgbModule,
    ReactiveFormsModule,
    PanelBarModule
  ],
  providers: [],
  bootstrap: [DocumentAndFormComponent]
})
export class DocumentAndFormModule { }
