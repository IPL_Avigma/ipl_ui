export class WorkOrderModel {
  workOrder_ID: Number = 0;
  workOrderNumber: string = "";
  clientName: string = "";
  clientPhone: string = "";
  clientEmail: string = "";
  clientTypeName: string = "";

  Company: Number = 0;
  CustomerNumber: Number = 0;
  WorkType: Number = 0;
  Category: Number = 0;
  address1: string = "";
  LoanNumber: string = "";
  LoanType: string = "";
  address2: string = "";
  state: Number = 0;
  city: string = "";
  zip: Number;
  country: string = "";
  Contractor: Number = 0;
  Processor: Number = 0;
  AssignedAdmin: Number = 0;
  Rush: Number = 0;
  BATF: boolean = false;
  LotSize: string = "";
  LockCode: string = "";
  Mortgager: string = "";
  KeyCode: string = "";
  Background: Number = 0;
  LockLocation: string = "";
  GateCode: string = "";
  Recurring: boolean = false;
  ReceivedDate: any;
  startDate: any;
  dueDate: any;
  clientDueDate: any;
  CompleteDate: any;
  CancelDate: any;
  Comments: string = "";
  Office_Approved: string = "";

  workOrderInfo: string = "";

  options: string = "";
  reference: string = "";
  description: string = "";
  instructions: string = "";
  status: String = "";

  clientInstructions: string = "";
  clientStatus: string = "";

  gpsLatitude: string = "";
  gpsLongitude: string = "";
  attribute7: Number = 0;
  attribute8: Number = 0;
  attribute9: Number = 0;
  attribute10: Number = 0;
  attribute11: Number = 0;
  attribute12: Number = 0;
  attribute13: Number = 0;
  attribute14: Number = 0;
  attribute15: Number = 0;
  source_wo_provider: string = "";
  source_wo_number: string = "";
  source_wo_id: Number = 0;
  controlConfig: Number = 0;
  services_Id: Number = 0;
  IsActive: boolean = true;
  IsDelete: boolean = true;
  currUserId: Number = 0;
  Type: Number = 0;

  IPLNumber: string = "";

  Cordinator: Number = 0;


  Com_Name: string = "";
  Com_Phone: string = "";
  Com_Email: string = "";

  Received_Date: string = "";
  Complete_Date: string = "";
  Cancel_Date: string = "";

  IPLNO: string = "";

  Loan_Info: string = "";

  Customer_Number: string = "";


  ISInspection: string = "";
  Lotsize: string = "";


  Lock_Code: string = "";
  Broker_Info: string = "";
  Lock_Location: string = "";
  Key_Code: string = "";
  Gate_Code: string = "";
  Loan_Number: string = "";
  valdef: Number = 0;
  state_name: string = "";

}

export class GetUserMetaDataModel {
  client_pkyeId: Number = 0;
  Type: Number = 1;
}
 
export class UpdateStausDataModel {
  workOrder_ID: Number = 0;
  status:String = '';
  IsActive: boolean = true;
  UserId: Number = 0;
  Type: Number = 1;
}
