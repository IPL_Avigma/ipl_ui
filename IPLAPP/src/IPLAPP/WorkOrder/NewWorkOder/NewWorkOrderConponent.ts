import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { WorkOrderDrodownServices } from "../../CommonServices/commonDropDown/DropdownService";
import {
  WorkOrderModel,
  GetUserMetaDataModel
} from "./NewWorkOrderModel";
import { SaveWorkOrderServices } from "./NewWorkOrderServices";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";
import { MasterlayoutComponent } from "../../Home/MasterComponent";
import { from } from 'rxjs';
import { ClientResultServices } from "../../ClientResult/ClientResults/ClientResultServices";
import { TaskBidMasterModel } from 'src/IPLAPP/ClientResult/ClientResults/ClientResultModel';


@Component({
  templateUrl: "./NewWorkOrder.html"
})
export class WorkOrderComponent implements OnInit {
  CompanyValFlag = false; // for dropdown
  CustomerNumbervalFlag = false; // for dropdown
  WorkTypevalFlag = false; // for dropdown
  CategoryvalFlag = false; // for dropdown
  statevalvalFlag = false; // for dropdown
  ContractorvalFlag = false; // for dropdown
  ProcessorvalFlag = false; // for dropdown
  AssignedAdminvalFlag = false; // for dropdown
  RushvalFlag = false; // for dropdown
  BackgroundvalFlag = false; // for dropdown

  CompanyList: any; // temp array
  CustomerList: any; // temp array
  WorkTypeList: any; // temp array
  CategoryList: any; // temp array
  StateList: any; // temp array
  ContractorList: any; // temp array
  AssignedAdminList: any; // temp array
  BackgroundList: any; // temp array
  RushList: any;
  CordinatorList: any;
  ProcessorList: any;
  CustomerNumberList: any;

  submitted = false; // submitted;
  isnew = true;

  formUsrCommonGroup: FormGroup; // create obj
  WorkOrderModelObj: WorkOrderModel = new WorkOrderModel(); // model obj
  GetUserMetaDataModelObj: GetUserMetaDataModel = new GetUserMetaDataModel();
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel(); 
  // only drop down
  dropCkck = false;

  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi


  FormDisabledCustom = false;

  constructor(
    private formBuilder: FormBuilder,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xSaveWorkOrderServices: SaveWorkOrderServices,
    private modalService: NgbModal,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xRoute : ActivatedRoute,
     private EncrDecr: EncrDecrServiceService,
     private xClientResultServices:ClientResultServices

  ) {
    //alert('hi');

    this.CustomerNumberList = [
      { Id: 1, Name: "Option1" },
      { Id: 2, Name: "Option2" }
    ];


    this.GetDropDowndata();

    this.WorkOrderModelObj.Company = 0;
    this.WorkOrderModelObj.CustomerNumber = 0;
    this.WorkOrderModelObj.WorkType = 0;
    this.WorkOrderModelObj.Category = 0;
    this.WorkOrderModelObj.state = 0;
    this.WorkOrderModelObj.Contractor = 0;
    this.WorkOrderModelObj.Rush = 0;
    this.WorkOrderModelObj.Background = 0;
    // var xxx = "2019-10-12T06:30:00.000Z";

    // this.WorkOrderModelObj.ReceivedDate = new Date(xxx);
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      WONumber: ["", Validators.required],
      EmailVal: ["", [Validators.email]],

      //CustomerNumberval: ["", Validators.required],
      WorkTypeval: ["", Validators.required],
      //Categoryval: ["", Validators.required],
      AddressVal: ["", Validators.required],
      cityVal: ["", Validators.required],
      stateval: ["", Validators.required],
      ZipVal: ["", Validators.required],
     // IPLNumberVal: ["", Validators.required],
      CompanyVal:["", Validators.nullValidator],
      NameVal:["", Validators.nullValidator],
      PhoneVal:["", Validators.nullValidator],
      ContractorVal:["", Validators.nullValidator],
      CordinatorVal:["", Validators.nullValidator],
      ProcessorVal:["", Validators.nullValidator],
      CategoryVal:["", Validators.nullValidator],
      LoanVal:["", Validators.nullValidator],
      LoanNumberVal:["", Validators.nullValidator],
      CustomerVal:["", Validators.nullValidator],
      BATFVal:["", Validators.nullValidator],
      IsInspectionVal:["", Validators.nullValidator],
      LotSizeVal:["", Validators.nullValidator],
      RushVal:["", Validators.nullValidator],
      LockCodeVal:["", Validators.nullValidator],
      LockLocationVal:["", Validators.nullValidator],
      KeyCodeVal:["", Validators.nullValidator],
      GateCodeVal:["", Validators.nullValidator],
      BrokerInfoVal:["", Validators.nullValidator],
      CommentsVal:["", Validators.nullValidator],
    });


    this.getModelData();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  /// button called
  FormButton(content) {
    this.contentx = content;
    debugger;
    this.WorkOrderModelObj;
    this.submitted = true;

    ////
    // only drop down
    this.dropCkck = false;


    if (this.WorkOrderModelObj.WorkType == 0) {
      this.WorkTypevalFlag = true;
      //return;
      this.dropCkck = true;
    }

    if (this.WorkOrderModelObj.state == 0) {
      this.statevalvalFlag = true;
      //return;
      this.dropCkck = true;
    }
    if (this.formUsrCommonGroup.invalid) {
      return;
    }
    if (this.dropCkck) {
      return;
    }
    // stop here if form is invalid
    // un comment plz
    // if (this.formUsrCommonGroup.invalid) {
    //   return;
    // }
    // if ( this.ModelObj.Edit == 2) {
    // this.WorkOrderModelObj.valdef == 6
    // }
   

    this.isLoading = true;
    this.button = "Processing";
    //alert(JSON.stringify(this.WorkOrderModelObj));

    var resultC =   this.StateList.filter(t=>t.IPL_StateID === this.WorkOrderModelObj.state);
    console.log('as',resultC);
    this.WorkOrderModelObj.state = resultC[0].IPL_StateID;
    this.WorkOrderModelObj.state_name = resultC[0].IPL_StateName;

    // all valid data to save
    this.xSaveWorkOrderServices
      .WorkorderPostData(this.WorkOrderModelObj)
      .subscribe(response => {
        console.log("resp pkey", response);
        //alert("Work Order Saved...!");
        this.MessageFlag = "Work Order Saved...!";
        this.isLoading = false;
        this.button = "Save";
        this.isnew = false;
        this.commonMessage(this.contentx);
        // this.ModelObj  = new WorkOrderModel(); 
      if(this.workid != 0){
        var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workid);

        this.xRouter.navigate(["/client/clientresult/" + btoa(encrypted)]);
      }
       

      });
  }

  // btn code end

  // drop down valid or not
  Company_Method() {
    //alert('select');
    this.CompanyValFlag = false;

    if (this.WorkOrderModelObj.Company != 0) {
      this.GetUserMetaDataModelObj.client_pkyeId = this.WorkOrderModelObj.Company;
      this.GetGetUserMetaData();
    } else {
      this.WorkOrderModelObj.clientName = "";
      this.WorkOrderModelObj.clientPhone = "";
      this.WorkOrderModelObj.clientEmail = "";
      this.WorkOrderModelObj.clientTypeName = "";
    }
  }
  CustomerNumber_Method() {
    this.CustomerNumbervalFlag = false;
  }
  WorkType_Method() {
    this.WorkTypevalFlag = false;
  }

  Category_Method() {
    this.CategoryvalFlag = false;
  }

  state_Method() {
   
    this.statevalvalFlag = false;
  }
  Contractor_Method() {
    this.ContractorvalFlag = false;
  }
  Processor_Method() {
    this.ProcessorvalFlag = false;
  }
  Assigned_Method() {
    this.AssignedAdminvalFlag = false;
  }
  Rush_Method() {
    this.RushvalFlag = false;
  }
  Background_Method() {
    this.BackgroundvalFlag = false;
  }


  GetDropDowndata() {
    this.xWorkOrderDrodownServices
      .DropdownGetWorkOrder()
      .subscribe(response => {
        console.log("drop down  data ", response);
        //debugger;
        if (response.length != 0) {
          this.CompanyList = response[0];
          this.WorkTypeList = response[1];
          this.CategoryList = response[2];
          this.StateList = response[6];
          this.ContractorList = response[3];
          this.AssignedAdminList = response[3];
          this.BackgroundList = response[8];
          this.RushList = response[7];
          this.CordinatorList = response[4];
          this.ProcessorList = response[5];
          this.CustomerNumberList = response[9];
        }


      });
  }

  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  /// end common model

  // clear form
  Clear() {
    this.WorkOrderModelObj = new WorkOrderModel();
  }
  // clear end

  GetGetUserMetaData() {
    this.xSaveWorkOrderServices
      .WorkorderGetUserMetaData(this.GetUserMetaDataModelObj)
      .subscribe(response => {
        console.log("repos meta", response);
        if (response[0].length != 0) {
          this.WorkOrderModelObj.Com_Name = response[0][0].Clnt_Con_List_Name;
          this.WorkOrderModelObj.Com_Phone = response[0][0].Clnt_Con_List_Phone;
          this.WorkOrderModelObj.Com_Email = response[0][0].Clnt_Con_List_Email;
          this.WorkOrderModelObj.clientTypeName =
            response[0][0].Clnt_Con_List_TypeName;
        }
      });
  }

  ModelObj: any;
  workid: Number = 0;
  IsEditDisable = false;
  isDisabled = false;
  getModelData() {
    debugger;
    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.WorkOrderModelObj = new WorkOrderModel();
      this.GetWorkOrderId();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('rus_pkeyId', id);
    this.workid = parseInt(id);
    this.getworkorderdetails();
    }


    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
   
//     if (this.ModelObj == undefined) {
//       this.WorkOrderModelObj = new WorkOrderModel();

     


//     } else if (this.ModelObj.Edit == 2)
//     {
//      
      
//     }
    
    
//     else 
//     {
//       //this.AddRushModelObj.rus_pkeyID = this.ModelObj.rus_pkeyID;

     


//       this.WorkOrderModelObj.workOrder_ID = this.ModelObj.workOrder_ID;

//       this.WorkOrderModelObj.BATF = this.ModelObj.BATF;
//       this.WorkOrderModelObj.Broker_Info = this.ModelObj.Broker_Info;
//       this.WorkOrderModelObj.Cancel_Date = this.ModelObj.Cancel_Date;
//       this.WorkOrderModelObj.Category = this.ModelObj.Category;
//       this.WorkOrderModelObj.Com_Email = this.ModelObj.Com_Email;
//       this.WorkOrderModelObj.Com_Name = this.ModelObj.Com_Name;
//       this.WorkOrderModelObj.Com_Phone = this.ModelObj.Com_Phone;
//       this.WorkOrderModelObj.Comments = this.ModelObj.Comments;
//       this.WorkOrderModelObj.Company = parseInt(this.ModelObj.Company) ;
//       this.WorkOrderModelObj.Complete_Date = this.ModelObj.Complete_Date;
//       this.WorkOrderModelObj.Contractor = parseInt(this.ModelObj.Contractor);
//       this.WorkOrderModelObj.Cordinator = this.ModelObj.Cordinator;
//       this.WorkOrderModelObj.Customer_Number = this.ModelObj.Customer_Number;
//       this.WorkOrderModelObj.IPLNO = this.ModelObj.IPLNO;
//       this.WorkOrderModelObj.ISInspection = this.ModelObj.ISInspection;
//       this.WorkOrderModelObj.Loan_Info = this.ModelObj.Loan_Info;
//       this.WorkOrderModelObj.Lock_Code = this.ModelObj.Lock_Code;
//       this.WorkOrderModelObj.Lotsize = this.ModelObj.Lotsize;
//       this.WorkOrderModelObj.Received_Date = this.ModelObj.Received_Date;
//       this.WorkOrderModelObj.Rush = this.ModelObj.Rush;
//       this.WorkOrderModelObj.WorkType = this.ModelObj.WorkType;
//       this.WorkOrderModelObj.address1 = this.ModelObj.address1;
//       this.WorkOrderModelObj.address2 = this.ModelObj.address2;
//       this.WorkOrderModelObj.city = this.ModelObj.city;
//       this.WorkOrderModelObj.clientDueDate = this.ModelObj.clientDueDate;
//       this.WorkOrderModelObj.clientInstructions = this.ModelObj.clientInstructions;
//       this.WorkOrderModelObj.clientStatus = this.ModelObj.clientStatus;
//       this.WorkOrderModelObj.controlConfig = this.ModelObj.controlConfig;
//       this.WorkOrderModelObj.country = this.ModelObj.country;
//       this.WorkOrderModelObj.currUserId = this.ModelObj.currUserId;
//       this.WorkOrderModelObj.description  = this.ModelObj.description;
//       this.WorkOrderModelObj.dueDate  = this.ModelObj.dueDate;
//       this.WorkOrderModelObj.gpsLatitude = this.ModelObj.gpsLatitude;
//       this.WorkOrderModelObj.gpsLongitude = this.ModelObj.gpsLongitude;
//       this.WorkOrderModelObj.instructions = this.ModelObj.instructions;
//       this.WorkOrderModelObj.options = this.ModelObj.options;
//       this.WorkOrderModelObj.reference = this.ModelObj.reference;
//       this.WorkOrderModelObj.services_Id = this.ModelObj.services_Id;
//       this.WorkOrderModelObj.source_wo_id = this.ModelObj.source_wo_id;
//       this.WorkOrderModelObj.source_wo_number = this.ModelObj.source_wo_number;
//       this.WorkOrderModelObj.source_wo_provider = this.ModelObj.source_wo_provider;
//       this.WorkOrderModelObj.startDate = this.ModelObj.startDate;
//       this.WorkOrderModelObj.state = parseInt(this.ModelObj.state);
//       this.WorkOrderModelObj.status = this.ModelObj.status;
//       this.WorkOrderModelObj.workOrderInfo = this.ModelObj.workOrderInfo;
//       this.WorkOrderModelObj.workOrderNumber = this.ModelObj.workOrderNumber;
//       this.WorkOrderModelObj.zip = this.ModelObj.zip;

//       this.WorkOrderModelObj.Lock_Location = this.ModelObj.Lock_Location;
//       this.WorkOrderModelObj.Key_Code = this.ModelObj.Key_Code;
//       this.WorkOrderModelObj.Gate_Code = this.ModelObj.Gate_Code;
//       this.WorkOrderModelObj.Loan_Number = this.ModelObj.Loan_Number;
//       this.WorkOrderModelObj.Processor = this.ModelObj.Processor;


//       this.formUsrCommonGroup.disable();
//       this.IsEditDisable = true;
//       this.isDisabled = true;
//       this.FormDisabledCustom = true;
     

//       this.button = "Update";
     
//       this.WorkOrderModelObj.Type = 2;
//     }
  }

  getworkorderdetails(){
    this.TaskBidMasterModelObj.workOrder_ID =   this.workid;
  this.xClientResultServices
      .WorkorderViewClient(this.TaskBidMasterModelObj)
      .subscribe(response => {
        debugger
      console.log('checkeditnew',response)
      this.WorkOrderModelObj.workOrder_ID = response[0][0].workOrder_ID;

            this.WorkOrderModelObj.BATF = response[0][0].BATF;
            this.WorkOrderModelObj.Broker_Info = response[0][0].Broker_Info;
            this.WorkOrderModelObj.Cancel_Date = response[0][0].Cancel_Date;
            this.WorkOrderModelObj.Category = response[0][0].Category;
            this.WorkOrderModelObj.Com_Email = response[0][0].Com_Email;
            this.WorkOrderModelObj.Com_Name = response[0][0].Com_Name;
            this.WorkOrderModelObj.Com_Phone = response[0][0].Com_Phone;
            this.WorkOrderModelObj.Comments = response[0][0].Comments;
            this.WorkOrderModelObj.Company = parseInt(response[0][0].Company) ;
            this.WorkOrderModelObj.Complete_Date = response[0][0].Complete_Date;
            this.WorkOrderModelObj.Contractor = parseInt(response[0][0].Contractor);
            this.WorkOrderModelObj.Cordinator = response[0][0].Cordinator;
            this.WorkOrderModelObj.Customer_Number = response[0][0].Customer_Number;
            this.WorkOrderModelObj.IPLNO = response[0][0].IPLNO;
            this.WorkOrderModelObj.ISInspection = response[0][0].ISInspection;
            this.WorkOrderModelObj.Loan_Info = response[0][0].Loan_Info;
            this.WorkOrderModelObj.Lock_Code = response[0][0].Lock_Code;
            this.WorkOrderModelObj.Lotsize = response[0][0].Lotsize;
            this.WorkOrderModelObj.Received_Date = response[0][0].Received_Date;
            this.WorkOrderModelObj.Rush = response[0][0].Rush;
            this.WorkOrderModelObj.WorkType = response[0][0].WorkType;
            this.WorkOrderModelObj.address1 = response[0][0].address1;
            this.WorkOrderModelObj.address2 = response[0][0].address2;
            this.WorkOrderModelObj.city = response[0][0].city;
            this.WorkOrderModelObj.clientDueDate = response[0][0].clientDueDate;
            this.WorkOrderModelObj.clientInstructions = response[0][0].clientInstructions;
            this.WorkOrderModelObj.clientStatus = response[0][0].clientStatus;
            this.WorkOrderModelObj.controlConfig = response[0][0].controlConfig;
            this.WorkOrderModelObj.country = response[0][0].country;
            this.WorkOrderModelObj.currUserId = response[0][0].currUserId;
            this.WorkOrderModelObj.description  = response[0][0].description;
            this.WorkOrderModelObj.dueDate  = response[0][0].dueDate;
            this.WorkOrderModelObj.gpsLatitude = response[0][0].gpsLatitude;
            this.WorkOrderModelObj.gpsLongitude = response[0][0].gpsLongitude;
            this.WorkOrderModelObj.instructions = response[0][0].instructions;
            this.WorkOrderModelObj.options = response[0][0].options;
            this.WorkOrderModelObj.reference = response[0][0].reference;
            this.WorkOrderModelObj.services_Id = response[0][0].services_Id;
            this.WorkOrderModelObj.source_wo_id = response[0][0].source_wo_id;
            this.WorkOrderModelObj.source_wo_number = response[0][0].source_wo_number;
            this.WorkOrderModelObj.source_wo_provider = response[0][0].source_wo_provider;
            this.WorkOrderModelObj.startDate = response[0][0].startDate;
            this.WorkOrderModelObj.state = parseInt(response[0][0].state);
            this.WorkOrderModelObj.status = response[0][0].status;
            this.WorkOrderModelObj.workOrderInfo = response[0][0].workOrderInfo;
            this.WorkOrderModelObj.workOrderNumber = response[0][0].workOrderNumber;
            this.WorkOrderModelObj.zip = response[0][0].zip;
      
            this.WorkOrderModelObj.Lock_Location = response[0][0].Lock_Location;
            this.WorkOrderModelObj.Key_Code = response[0][0].Key_Code;
            this.WorkOrderModelObj.Gate_Code = response[0][0].Gate_Code;
            this.WorkOrderModelObj.Loan_Number = response[0][0].Loan_Number;
            this.WorkOrderModelObj.Processor = response[0][0].Processor;
      
            this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
            console.log('chkdata',this.ModelObj);
   
               if (this.ModelObj.Edit == 2) 
               {
                  //this.formUsrCommonGroup.disable();
                    //this.IsEditDisable = true;
                    this.isDisabled = true;
                    this.FormDisabledCustom = true;
                    

                    this.button = "Save";
                    
                    this.WorkOrderModelObj.Type = 6;
               }
              else {
                this.formUsrCommonGroup.disable();
                this.IsEditDisable = true;
                this.isDisabled = true;
                this.FormDisabledCustom = true;
               
          
                this.button = "Update";
               
                this.WorkOrderModelObj.Type = 2;

               }
           
  })
}

   // end code
   EditForms() {
     debugger
    this.IsEditDisable = false;
    this.isDisabled = true;
    this.formUsrCommonGroup.enable();

    this.FormDisabledCustom = false;// for calander
  }
  // end code



  // get auto Work Order id
  GetWorkOrderId(){

    this.xSaveWorkOrderServices.WorkorderAutoGenerateId()
    .subscribe(response =>{
      console.log('auto WO id',response);
      this.WorkOrderModelObj.IPLNO = response[0];
    });
  } 
  // reset fiels from workorder
  Reset(){
    debugger;
    this.WorkOrderModelObj.workOrder_ID = 0;
    this.WorkOrderModelObj.BATF = false;
    this.WorkOrderModelObj.Broker_Info = '';
    this.WorkOrderModelObj.Cancel_Date = '';
    this.WorkOrderModelObj.Category = 0;
    this.WorkOrderModelObj.Com_Email = '';
    this.WorkOrderModelObj.Com_Name = '';
    this.WorkOrderModelObj.Com_Phone = '';
    this.WorkOrderModelObj.Comments = '';
    this.WorkOrderModelObj.Company = 0;
    this.WorkOrderModelObj.Complete_Date = '';
    this.WorkOrderModelObj.Contractor = 0;
    this.WorkOrderModelObj.Cordinator = 0;
    this.WorkOrderModelObj.Customer_Number = '';
    this.WorkOrderModelObj.IPLNO = '';
    this.WorkOrderModelObj.ISInspection = '';
    this.WorkOrderModelObj.Loan_Info = '';
    this.WorkOrderModelObj.Lock_Code = '';
    this.WorkOrderModelObj.Lotsize = '';
    this.WorkOrderModelObj.Received_Date = '';
    this.WorkOrderModelObj.Rush = 0;
    this.WorkOrderModelObj.WorkType = 0;
    this.WorkOrderModelObj.address1 = '';
    this.WorkOrderModelObj.address2 = '';
    this.WorkOrderModelObj.city = '';
    this.WorkOrderModelObj.clientDueDate = '';
    this.WorkOrderModelObj.clientInstructions = '';
    this.WorkOrderModelObj.clientStatus = '';
    this.WorkOrderModelObj.controlConfig = 0;
    this.WorkOrderModelObj.country = '';
    this.WorkOrderModelObj.currUserId = 0;
    this.WorkOrderModelObj.description  = '';
    this.WorkOrderModelObj.dueDate  = '';
    this.WorkOrderModelObj.gpsLatitude = '';
    this.WorkOrderModelObj.gpsLongitude = '';
    this.WorkOrderModelObj.instructions = '';
    this.WorkOrderModelObj.options = '';
    this.WorkOrderModelObj.reference = '';
    this.WorkOrderModelObj.services_Id = 0;
    this.WorkOrderModelObj.source_wo_id = 0;
    this.WorkOrderModelObj.source_wo_number = '';
    this.WorkOrderModelObj.source_wo_provider = '';
    this.WorkOrderModelObj.startDate = '';
    this.WorkOrderModelObj.state = 0;
    this.WorkOrderModelObj.status = '';
    this.WorkOrderModelObj.workOrderInfo = '';
    this.WorkOrderModelObj.workOrderNumber = '';
    this.WorkOrderModelObj.zip = 0;

    this.WorkOrderModelObj.Lock_Location = '';
    this.WorkOrderModelObj.Key_Code = '';
    this.WorkOrderModelObj.Gate_Code = '';
    this.WorkOrderModelObj.Loan_Number = '';
    this.button = "Save";
    this.isnew = true;
    location.reload();
  }
}
