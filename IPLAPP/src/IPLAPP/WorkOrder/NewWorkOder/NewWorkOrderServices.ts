import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {
  WorkOrderModel,
  GetUserMetaDataModel,
  UpdateStausDataModel
} from "./NewWorkOrderModel";
import { BasetUrl } from "../../Utility/DomainUrl";
import { HomepageServices } from "../../Home/HomeServices";

@Injectable({
  providedIn: "root"
})
export class SaveWorkOrderServices {
  
  public Errorcall;
  private token: any;
  
  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data
  private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/PostWorkOrderData";

  public WorkorderPostData(Modelobj: WorkOrderModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    debugger;
    console.log('state array new work',Modelobj.state);
    var ANYDTO: any = {};
    ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
    ANYDTO.workOrderNumber = Modelobj.workOrderNumber;
    ANYDTO.workOrderInfo = Modelobj.workOrderInfo;
    ANYDTO.address1 = Modelobj.address1;
    ANYDTO.address2 = Modelobj.address2;
    ANYDTO.city = Modelobj.city;
    ANYDTO.state = Modelobj.state
    ANYDTO.state_name = Modelobj.state_name
    
    ANYDTO.zip = Modelobj.zip;
    ANYDTO.country = Modelobj.country;
    ANYDTO.options = Modelobj.options;
    ANYDTO.reference = Modelobj.reference;
    ANYDTO.description = Modelobj.description;
    ANYDTO.instructions = Modelobj.instructions;
    ANYDTO.status = Modelobj.status;
    ANYDTO.dueDate = Modelobj.dueDate;
    ANYDTO.startDate = Modelobj.startDate;
    ANYDTO.clientInstructions = Modelobj.Comments;
    ANYDTO.clientStatus = Modelobj.clientStatus;
    ANYDTO.clientDueDate = Modelobj.clientDueDate;
    ANYDTO.gpsLatitude = Modelobj.gpsLatitude;
    ANYDTO.gpsLongitude = Modelobj.gpsLongitude;
    ANYDTO.attribute7 = Modelobj.attribute7;
    ANYDTO.attribute8 = Modelobj.attribute8;
    ANYDTO.attribute9 = Modelobj.attribute9;
    ANYDTO.attribute10 = Modelobj.attribute10;
    ANYDTO.attribute11 = Modelobj.attribute11;
    ANYDTO.attribute12 = Modelobj.attribute12;
    ANYDTO.attribute13 = Modelobj.attribute13;
    ANYDTO.attribute14 = Modelobj.attribute14;
    ANYDTO.attribute15 = Modelobj.attribute15;
    ANYDTO.source_wo_provider = Modelobj.source_wo_provider;
    ANYDTO.source_wo_number = Modelobj.source_wo_number;
    ANYDTO.source_wo_id = Modelobj.source_wo_id;
    ANYDTO.controlConfig = Modelobj.controlConfig;
    ANYDTO.services_Id = Modelobj.services_Id;
    ANYDTO.IsActive = Modelobj.IsActive;
    ANYDTO.currUserId = Modelobj.currUserId;


    ANYDTO.WorkType = Modelobj.WorkType;
    ANYDTO.Company = Modelobj.Company;
    ANYDTO.Com_Name = Modelobj.Com_Name;
    ANYDTO.Com_Phone = Modelobj.Com_Phone;
    ANYDTO.Com_Email = Modelobj.Com_Email;
    ANYDTO.Contractor = Modelobj.Contractor;
    ANYDTO.Received_Date = Modelobj.Received_Date;
    ANYDTO.Complete_Date = Modelobj.Complete_Date;
    ANYDTO.Cancel_Date = Modelobj.Cancel_Date;

    ANYDTO.IPLNO = Modelobj.IPLNO;
    ANYDTO.Category = Modelobj.Category;
    ANYDTO.Loan_Info = Modelobj.Loan_Info;

    ANYDTO.Customer_Number = Modelobj.Customer_Number;
    ANYDTO.Cordinator = Modelobj.Cordinator;
    ANYDTO.BATF = Modelobj.BATF;
    ANYDTO.ISInspection = Modelobj.ISInspection;
    ANYDTO.Lotsize = Modelobj.Lotsize;
    ANYDTO.Rush = Modelobj.Rush;

    ANYDTO.Lock_Code = Modelobj.Lock_Code;
    ANYDTO.Broker_Info = Modelobj.Broker_Info;
    ANYDTO.Comments = Modelobj.Comments;
    ANYDTO.Lock_Location = Modelobj.Lock_Location;
    ANYDTO.Key_Code = Modelobj.Key_Code;
    ANYDTO.Gate_Code = Modelobj.Gate_Code;
    ANYDTO.Loan_Number = Modelobj.Loan_Number;
    ANYDTO.Processor = Modelobj.Processor;

    ANYDTO.Type = 1;
    if(Modelobj.workOrder_ID != 0){
      if (Modelobj.valdef == 5) {
        ANYDTO.Type = 5;
      }
      else if(Modelobj.Type == 6)
      {
        ANYDTO.Type = 6;
      }
      else
      {
        ANYDTO.Type = 2;
      }

     
    }

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  ////////////////////////////////////////////
  // get user data
  private apiUrlGet1 = BasetUrl.Domain + "api/RESTIPL/GetUserMetaData";

  public WorkorderGetUserMetaData(Modelobj: GetUserMetaDataModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    //debugger;
    var ANYDTO: any = {};

    ANYDTO.client_pkyeId = Modelobj.client_pkyeId;
    ANYDTO.Type = 3;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet1, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
///////////// staus update 
private apiUrlGet2 = BasetUrl.Domain + "api/RESTIPL/UpadateWorkOrderStatusDetails";

public Workorderstatus(Modelobj: UpdateStausDataModel) {

  debugger;
  var ANYDTO: any = {};

  ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
  ANYDTO.status = Modelobj.status;
  ANYDTO.IsActive = Modelobj.IsActive;
  ANYDTO.UserId = Modelobj.UserId;
  ANYDTO.Type = 1;

  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http
    .post<any>(this.apiUrlGet2, ANYDTO, { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      // catchError(this.handleError)
      catchError(this.xHomepageServices.CommonhandleError)
      //catchError( this.Errorcall.handleError)
    );
}


   ////////////////////////////////////////////
  // get user data
  private apiUrlGet12 = BasetUrl.Domain + "api/WOCTASK/PostIPLAutoData";

  public WorkorderAutoGenerateId() {

    //debugger;
    var ANYDTO: any = {};


    ANYDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet12, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {

          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)

      );
  }
  ////////////////////////// delete Work Order
  private apiUrldel = BasetUrl.Domain + "api/RESTIPL/PostdeleteWorkOrderData";

  public deleteworkorder(Modelobj: WorkOrderModel) {

    debugger;
    var ANYDTO: any = {};

    ANYDTO.workOrder_ID = Modelobj.workOrder_ID ;
    ANYDTO.Type = 4;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrldel, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {

          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)

      );
  }


  // common handler
  private handleError(error: HttpErrorResponse) {

    if(error.status == 401) {
      alert('Unauthorized User..!');
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}
