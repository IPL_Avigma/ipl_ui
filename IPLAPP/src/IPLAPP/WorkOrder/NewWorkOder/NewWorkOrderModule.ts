import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { WorkOrderComponent } from './NewWorkOrderConponent';
import {WorkOrderDrodownServices} from '../../CommonServices/commonDropDown/DropdownService';
import {CommonDirectiveModule} from '../../AppDirectives/DirectiveModule';


const routes: Routes = [
  {path: "createworkorder/:id",component:WorkOrderComponent}
];

@NgModule({
  declarations: [WorkOrderComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    CommonDirectiveModule,


  ],
  exports: [RouterModule],

  providers: [WorkOrderDrodownServices],
  bootstrap: [WorkOrderComponent]
})

export class WorkOrderModule {
  constructor(){
    console.log('Work Order New Model Loaded');
  }
}
