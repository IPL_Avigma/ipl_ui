import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { WorkOrderSettingsPageComponent } from './WorkOrderSettingsPageComponent';

import {WorkOrderSettingsPageServices} from './WorkOrderSettingsPageServices';

const WorkOrderSettingsPageComponentRouts =[

  {path:'workordersettings',component:WorkOrderSettingsPageComponent}

]

@NgModule({
  declarations: [WorkOrderSettingsPageComponent],
  imports: [
    RouterModule.forChild(WorkOrderSettingsPageComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,

  ],
  providers: [WorkOrderSettingsPageServices],
  bootstrap: [WorkOrderSettingsPageComponent]
})

export class WorkOrderSettingsPageModule {}
