import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { WorkOrderSettingsPageModel,GeneralWorkOrderSettingsModel } from "./WorkOrderSettingsPageModel";
import {WorkOrderSettingsPageServices} from './WorkOrderSettingsPageServices';

@Component({
  templateUrl: "./WorkOrderSettingsPage.html"
})
export class WorkOrderSettingsPageComponent implements OnInit {
  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi

  WorkOrderSettingsPageModelObj:WorkOrderSettingsPageModel = new WorkOrderSettingsPageModel();

  GeneralWorkOrderSettingsModelObj:GeneralWorkOrderSettingsModel = new GeneralWorkOrderSettingsModel();

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xWorkOrderSettingsPageServices:WorkOrderSettingsPageServices,
  ) {}

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      //StateName: ["", Validators.required]
    });
  }

   // shortcurt Namefor form sathi
   get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;

    this.WorkOrderSettingsPageModelObj;
    this.GeneralWorkOrderSettingsModelObj;

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    this.xWorkOrderSettingsPageServices
      .FirstDataPost(this.WorkOrderSettingsPageModelObj)
      .subscribe(response => {
        console.log("resp data 1", response);
        if (response[0].Status != "0") {
          this.WorkOrderSettingsPageModelObj.WO_Sett_pkeyID = parseInt(response[0].WO_Sett_pkeyID);

          this.SencondFormcall();
          // this.MessageFlag = "Work order settings Saved...!";
          // this.isLoading = false;
          // this.button = "Update";
          // this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  SencondFormcall(){
    this.xWorkOrderSettingsPageServices
      .SecondGWDataPost(this.GeneralWorkOrderSettingsModelObj)
      .subscribe(response => {
        console.log("resp data 2", response);
        if (response[0].Status != "0") {
          this.GeneralWorkOrderSettingsModelObj.GW_Sett_pkeyID = parseInt(response[0].GW_Sett_pkeyID);

          this.MessageFlag = "Work order settings Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }

  ModelObj: any;
  IsEditDisable = false;
}
