import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {GeneralWorkOrderSettingsModel,WorkOrderSettingsPageModel} from './WorkOrderSettingsPageModel';
import {BasetUrl} from '../../Utility/DomainUrl';
import { HomepageServices } from '../../Home/HomeServices';

@Injectable({
  providedIn: "root"
})
export class WorkOrderSettingsPageServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse (localStorage.getItem('TOKEN'));
  }
  // get user data
  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostWorkOrderSettingData";


  public FirstDataPost(Modelobj:WorkOrderSettingsPageModel) {

    //debugger;
    var ANYDTO: any = {};

    ANYDTO.WO_Sett_pkeyID  = Modelobj.WO_Sett_pkeyID;
    ANYDTO.WO_Sett_CompanyID  = Modelobj.WO_Sett_CompanyID;
    ANYDTO.WO_Sett_Allow_Dup_Num  = Modelobj.WO_Sett_Allow_Dup_Num;
    ANYDTO.WO_Sett_Auto_Inc_GoBack  = Modelobj.WO_Sett_Auto_Inc_GoBack;
    ANYDTO.WO_Sett_Auto_Inc_NeedInfo  = Modelobj.WO_Sett_Auto_Inc_NeedInfo;
    ANYDTO.WO_Sett_Auto_Inc_Dup  = Modelobj.WO_Sett_Auto_Inc_Dup;
    ANYDTO.WO_Sett_Auto_Inc_Recurring  = Modelobj.WO_Sett_Auto_Inc_Recurring;
    ANYDTO.WO_Sett_Auto_Assign  = Modelobj.WO_Sett_Auto_Assign;
    ANYDTO.WO_Sett_Detect_Pricing  = Modelobj.WO_Sett_Detect_Pricing;
    ANYDTO.WO_Sett_Remove_Doller  = Modelobj.WO_Sett_Remove_Doller;
    ANYDTO.WO_Sett_IsActive  = Modelobj.WO_Sett_IsActive;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;
    if(Modelobj.WO_Sett_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.WO_Sett_IsDelete){
      ANYDTO.Type = 4;
    }

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  ////////////////////////////////////////////////////////////////////
  private apiUrlPOST2 = BasetUrl.Domain +"api/RESTIPL/PostGeneralWorkOrderSettingData";


  public SecondGWDataPost(Modelobj2:GeneralWorkOrderSettingsModel) {

    //debugger;
    var ANYDTO: any = {};
    ANYDTO.GW_Sett_pkeyID = Modelobj2.GW_Sett_pkeyID;
    ANYDTO.GW_Sett_CompanyID = Modelobj2.GW_Sett_CompanyID;
    ANYDTO.GW_Sett_Field_Complete = Modelobj2.GW_Sett_Field_Complete;
    ANYDTO.GW_Sett_Allow_Contractor = Modelobj2.GW_Sett_Allow_Contractor;
    ANYDTO.GW_Sett_Assigned_Unread = Modelobj2.GW_Sett_Assigned_Unread;
    ANYDTO.GW_Sett_Allow_Estimated = Modelobj2.GW_Sett_Allow_Estimated;
    ANYDTO.GW_Sett_Require_Estimated = Modelobj2.GW_Sett_Require_Estimated;
    ANYDTO.GW_Sett_Sent_Ass_Cooradinator = Modelobj2.GW_Sett_Sent_Ass_Cooradinator;
    ANYDTO.GW_Sett_Sent_Ass_Processor = Modelobj2.GW_Sett_Sent_Ass_Processor;
    ANYDTO.GW_Sett_Sent_Email_Multiple = Modelobj2.GW_Sett_Sent_Email_Multiple;
    ANYDTO.GW_Sett_StaffName = Modelobj2.GW_Sett_StaffName;

    ANYDTO.Type = Modelobj2.GW_Sett_IsActive;
    ANYDTO.Type = Modelobj2.UserID;


    ANYDTO.Type = 1;
    if(Modelobj2.GW_Sett_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj2.GW_Sett_IsDelete){
      ANYDTO.Type = 4;
    }
    
    
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {

          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)

      );
  }





  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found..!');
    } else {
    alert("Invalid Request..");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}


