import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../Home/MasterComponent";

@Component({
  templateUrl: "./GeneralSettings.html"
})
export class GeneralSettingsComponent implements OnInit {
  TimezoneList: any;
  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router
  ) {
    this.TimezoneList = [
      { Id: "Central(CDT)", Name: "Central(CDT)" },
      { Id: "Eastern(EDT)", Name: "Eastern(EDT)" },
      { Id: "Mountain (MDT)", Name: "Mountain (MDT)" },
      { Id: "Pacific (PDT)", Name: "Pacific (PDT)" },
      { Id: "Hawaii (HST)", Name: "Hawaii (HST)" },
      { Id: "Guam (ChST)", Name: "Guam (ChST)" }
    ];
  }

  ngOnInit() {}

  // clear data

  GetLocal: any;
  AddNewClient() {
    //debugger;
    this.GetLocal = this.xMasterlayoutComponent.LocalGetmasterfunction();

    //const userData = JSON.parse(localStorage.getItem('usertemp_'));
    if (this.GetLocal != null) {
      //debugger;
      const pkeyuserId = this.GetLocal[0].User_pkeyID;
      console.log("userpkey id", pkeyuserId);
      var data = {
        pkeyuserId: pkeyuserId
      };

      this.xMasterlayoutComponent.masterFunctionCall(data);

      this.xRouter.navigate(["/company/companyinfo"]);
    } else {
      var faltu = undefined;
      this.xMasterlayoutComponent.masterFunctionCall(faltu);

      this.xRouter.navigate(["/company/companyinfo"]);
    }
  }
  // end code
}
