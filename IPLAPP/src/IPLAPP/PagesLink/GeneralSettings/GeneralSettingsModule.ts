import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { GeneralSettingsComponent } from './GeneralSettingsComponent';


const GeneralSettingsRouts =[

  {path:'generalsettings',component:GeneralSettingsComponent}

]


@NgModule({
  declarations: [GeneralSettingsComponent],
  imports: [
    RouterModule.forChild(GeneralSettingsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,

  ],
  providers: [],
  bootstrap: [GeneralSettingsComponent]
})

export class GeneralSettingsModule {}
