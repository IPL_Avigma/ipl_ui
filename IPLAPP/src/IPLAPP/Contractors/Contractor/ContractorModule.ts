import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ContractorComponent } from "./ContractorComponent";



const ContractorRouts =[

  {path:'add',component:ContractorComponent}

]

@NgModule({
  declarations: [ContractorComponent],
  imports: [
    RouterModule.forChild(ContractorRouts),
    CommonModule

  ],
//   providers: [StartupProfileServices],
  bootstrap: [ContractorComponent]
})

export class ContractorModule {}
