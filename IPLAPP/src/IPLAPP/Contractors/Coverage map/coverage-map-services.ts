import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {CoverageMapModel} from './coverage-map-model';
import {BasetUrl} from '../../Utility/DomainUrl';
import {HomepageServices} from '../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class CoverageMapServices {
    public Errorcall;
    private token: any;
    constructor(private _http: HttpClient, private _Route: Router, 
      private xHomepageServices:HomepageServices) {
      this.token = JSON.parse(localStorage.getItem('TOKEN'));
    }
    private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetUserAddressDetail";


    public GetUserLatData(Modelobj:CoverageMapModel) {
      debugger;
      var ANYDTO: any = {};
          ANYDTO.Type = 1;
    
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            return data;
          }),
          catchError(this.xHomepageServices.CommonhandleError)
          
        );
    }

}