
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { CoverageMapComponent } from './coverage-map-component';
import {CoverageMapServices} from './coverage-map-services'

import { environment } from '../../../environments/environment';
import { AgmCoreModule } from '@agm/core';
export const mapRouts = [
    { path: "covmap", component: CoverageMapComponent }
  ];
@NgModule({
  declarations: [
    CoverageMapComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(mapRouts),
   // BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCMZDQNjWNzYVeZvpCz2vcQrq1tH_tWVlg",
      libraries: ['places']
    })
  ],
  providers: [CoverageMapServices],
  bootstrap: []
})
export class CoverageMapModule { }