import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {HomeModel} from './HomeModel';
import {WorkOderViewModel} from '../WorkOrder/WorkOrderView/WorkOrderViewModel';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
import {BasetUrl} from '../Utility/DomainUrl';







@Injectable({
  providedIn: "root"
})

export class HomepageServices {
  
  public Errorcall;
  public token: any;
  constructor(private _http: HttpClient, private _Route: Router) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
    console.log('charttoken:',this.token)
  }
  // get user data
  private apiUrlGet = BasetUrl.Domain+"api/RESTIPL/PostAddLoginData";


  public WorkorderGet(HomeModelobj:HomeModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    //debugger;
    var ANYDTO: any = {};
    ANYDTO.username = HomeModelobj.username;
    ANYDTO.token = HomeModelobj.token;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.handleError)
        //catchError( this.Errorcall.handleError)
      );
  }

// get work order details
private apiUrl = BasetUrl.Domain+"api/RESTIPL/GetWorkOrderData";

public Workorder(WorkOderViewModelobj:WorkOderViewModel) {
  ////debugger;
  //debugger;
  var ANYDTO: any = {};
  ANYDTO.workOrder_ID  = WorkOderViewModelobj.workOrder_ID;
  ANYDTO.Type = 3;

  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  return this._http
    .post<any>(this.apiUrl, ANYDTO, { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.handleError)
      //catchError( this.Errorcall.handleError)
    );
}
///////chart data 
private apiUrlGetc = BasetUrl.Domain+"api/RESTAuthentication/PostChartData";


public GetChartDetails(HomeModelobj:HomeModel) {
 debugger
 var ANYDTO: any = {};

 //ANYDTO.User_pkeyID = HomeModelobj.currUserId;
  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  
  return this._http
    .post<any>(this.apiUrlGetc,ANYDTO,  { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.handleError)
      //catchError( this.Errorcall.handleError)
    );
}




  // common handler
  private handleError(error: HttpErrorResponse) {
    //debugger;
    alert("Something bad happened; please try again later....");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }

    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }

   // common handler
   public CommonhandleError(error: HttpErrorResponse) {
    if(error.status == 401){
      alert('Unauthorized User Found...!');
      window.location.href = '/';
    }else{
      alert("Something bad happened, please try again later...😌");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
