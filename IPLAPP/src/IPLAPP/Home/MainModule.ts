import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from "@angular/common/http";
import {HashLocationStrategy,LocationStrategy} from '@angular/common'; // for uri sathi 404

import {AppRoutingModule} from '../Routing/UserRoutingModule';
//import {AdminRoutingModule} from '../Routing/AdminRoutingModule';

import { ChartsModule } from 'ng2-charts';

import {NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter} from '@ng-bootstrap/ng-bootstrap';// common date sathi



import { MasterlayoutComponent } from './MasterComponent';
import {HomepageServices} from './HomeServices';



import {AdminLinkPageModule} from '../Admin/AdminLinkPage/AdminLinkPageModule';

import {CommonDirectiveModule} from '../AppDirectives/DirectiveModule';
import {CustomPreloadingStrategy} from '../Routing/RoutingPreloadModule';

@NgModule({
  declarations: [
    MasterlayoutComponent
  ],
  imports: [
    AppRoutingModule,
    //AdminRoutingModule,
    //RouterModule.forRoot(MainRount),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    AdminLinkPageModule,
    CommonDirectiveModule,

  ],
  providers: [
    {provide:LocationStrategy,useClass:HashLocationStrategy},
    HomepageServices,
    {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter},
    CustomPreloadingStrategy
  ],
  bootstrap: [MasterlayoutComponent]
})
export class MainModule {
  constructor(){
    console.log('Main module View Model Loaded');
  }
 }
