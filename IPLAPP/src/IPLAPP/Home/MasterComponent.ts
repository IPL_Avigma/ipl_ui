import { Component, Injectable, OnInit } from "@angular/core";
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
  Event
} from "@angular/router";

import { CommonModel } from "./HomeCommonModel";
import { WorkOrderModel } from '../WorkOrder/NewWorkOder/NewWorkOrderModel';
import { CommonMenuServices } from '../CommonUserAccessServices/CommonUserAccessSevices';
import { UserAccessModel } from '../CommonUserAccessServices/CommonUserAccessModel';
import { EncrDecrServiceService } from "../../app/encr-decr-service.service";
import { _appIdRandomProviderFactory } from '@angular/core/src/application_tokens';

@Component({
  selector: "app-root",
  templateUrl: "./MasterLayout.html",
  styleUrls: ["./HomeLoader.scss"]
})
export class MasterlayoutComponent implements OnInit {
  // the loading indicator
  showLoadingIndicator = true;
  userDiv: boolean = true;
  public CommonModelobj: CommonModel = new CommonModel();
  public UserAccessModelObj: UserAccessModel = new UserAccessModel();
  WorkOrderModelObj: WorkOrderModel = new WorkOrderModel();
  private User: any;
  constructor(
    private XRoute: Router,
    private xCommonMenuServices: CommonMenuServices,
    private EncrDecr: EncrDecrServiceService,


  ) {

    // Subscribe to the router events observable
    this.XRoute.events.subscribe((routerEvent: Event) => {
      // On NavigationStart, set showLoadingIndicator to ture
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator = true;
      }

      // On NavigationEnd or NavigationError or NavigationCancel
      // set showLoadingIndicator to false
      if (
        routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationError ||
        routerEvent instanceof NavigationCancel
      ) {
        this.showLoadingIndicator = false;
      }

      this.UserLoginHeaderSet();



    });
  }

  ngOnInit() {
    this.LocalGetmasterfunction();
   }

  // for admin sathi conditional hide
  UserLoginHeaderSet() {
    ////debugger;

    var rurl = window.location.href;
    if (rurl.split("#").slice(1)[0]) {
      var resx = rurl.split("#").slice(1)[0];
      var urlarray = [];
      urlarray = resx.split("/");
      if (urlarray[1]) {
        //alert(urlarray[1]);
        if (urlarray[1] == "admin") {
          this.userDiv = false;
        } else {
          this.userDiv = true;
        }
      }
    }



  }

  // common sath
  goData: any;
  masterFunctionCall(dataval: any) {
    this.goData = dataval;
  }
  masterFunctionGetdata() {
    return this.goData;
  }

  // this code for Work order data  go to client result
  goDatax: any;
  masterFunctionCallClinet(datavalx: any) {
    this.goDatax = datavalx;
  }
  masterFunctionGetdataClinet() {
    return this.goDatax;
  }


  // this for drop down task list
  goDataZ: any;
  masterFunctionCallClinetTaskDropDown(DataZ: any) {
    this.goDataZ = DataZ;
  }
  masterFunctionGetdataClinetTaskDropDown() {
    return this.goDataZ;
  }


  // this for Cleint result photots local set 7 sec
  GetPhots: any;
  MasterFunctionClientResultAsycPhotosSet(SetPhotos: any) {
    this.GetPhots = SetPhotos;
  }
  MasterFunctionClientResultAsycPhotosGet() {
    return this.GetPhots;
  }



  // this for drop down task list
  classi: any;
  masterFunctionCallClinetNavigateSet(Datac: any) {
    this.classi = Datac;
  }
  masterFunctionCallClinetNavigateGet() {
    return this.classi;
  }




  // get local stoarage data common for all
  // for image set and user name dynamically
  User_FirstName: any;
  usersx: any;
  LocalGetmasterfunction() {

    debugger;
    // swapnil_200410 start
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      this.usersx = this.decuser ;
    if (this.usersx != undefined && this.usersx != null && this.usersx != "undefined") {
      this.User_FirstName = this.usersx[0].User_FirstName;

    } 
    else 
    {
      this.User_FirstName = null;
      this.XRoute.navigate(['/admin/login']);

    }
      // swapnil_200410 End

    }

    
  }


  decuser: any
  
  CreateNewWO(arg) {
    debugger
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));

      console.log('encuserdecdd',encuser);
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      console.log('testdec',decval);
      this.decuser  =JSON.parse(decval) ;
      console.log('this.decuser',this.decuser );

    }
    // this.User = JSON.parse(localStorage.getItem('usertemp_'));
    this.User =(this.decuser);
    console.log('user', this.User)
    this.UserAccessModelObj.MenuID = arg;
    this.UserAccessModelObj.UserID = this.User[0].User_pkeyID;

    this.xCommonMenuServices
      .UserAccessGet(this.UserAccessModelObj)
      .subscribe(response => {
        debugger;
        console.log('check data', response);



        if (response[0] > 0) {

          switch (arg) {
            case 2:
              {
                this.XRoute.navigate(['/workorder/view'])
                break;
              }
            case 3:
              {
                this.XRoute.navigate(['/workorder/createworkorder/new'])
                break;
              }
            case 5:
              {
                this.XRoute.navigate(['/import/importqueue'])
                break;
              }
            case 6:
              {
                this.XRoute.navigate(['/queueworkorder/importqueuedata'])
                break;
              }
            case 7:
              {
                this.XRoute.navigate(['/formdoc/formsanddocs'])
                break;
              }
            case 8:
              {
                this.XRoute.navigate(['/home/adminlinkpage'])
                break;
              }
            case 9:
              {
                this.XRoute.navigate(['/user/viewduser'])
                break;
              }

            case 10:
              {
                this.XRoute.navigate(['/worktype/viewworktypecate'])
                break;
              }
            case 17:
              {
                this.XRoute.navigate(['/client/viewclientcompanies'])
                break;
              }
            case 49:
              {
                this.XRoute.navigate(['/task/bidinvoiceitemviewtask'])
                break;
              }

            case 53:
              {
                this.XRoute.navigate(['/contractor/add'])
                break;
              }
            case 54:
              {
                this.XRoute.navigate(['/report/reportsdetails'])
                break;
              }
            case 55:
              {
                this.XRoute.navigate(['/accounting/accountingdetails'])
                break;
              }
            case 56:
              {
                this.XRoute.navigate(['/support/supportdetails'])
                break;
              }
            case 57:
              {
                this.XRoute.navigate(['/contractor/covmap'])
                break;
              }
            case 58:
              {
                this.XRoute.navigate(['/workorder/map'])
                break;
              }
            case 59:
              {
                this.XRoute.navigate(['/home/scorecard'])
                break;
              }
              case 61:
                {
                  this.XRoute.navigate(['/message/messagedetails'])
                  break;
                }
          }



        } 
        else
         {
          this.XRoute.navigate(['/access/accessdenied'])
        }

      })

    // var faltu = undefined;
    // this.masterFunctionCall(faltu);
    // this.XRoute.navigate(['/workorder/new']);

  }



  CreateUser() {

    var faltu = undefined;
    this.masterFunctionCall(faltu);
    this.XRoute.navigate(['/user/adduser']);
  }

}
