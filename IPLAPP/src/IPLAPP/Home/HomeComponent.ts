import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { HomeModel } from "./HomeModel";
import { HomepageServices } from "./HomeServices";


import { MasterlayoutComponent } from './MasterComponent';
import { AngularFireDatabase } from '@angular/fire/database';
import { MessagingDetailsService } from '../ClientResult/MessagesDetails/MessageDetailsServices';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./Homeview.html"
})
export class HomeComponent implements OnInit {
  HomeModelobj: HomeModel = new HomeModel();
  currentMessage = new BehaviorSubject(null);
  formUsrLoginGroup: FormGroup; // create obj
  Test: string;
  button = "Get Order"; // buttom loading..
  isLoading = false; // buttom loading..
  ModelObj: any;
  message: any;

  yeardata =[]; countdata=[];
  pieData = []; piecount = [];
 shriganesh = this.piecount;

  constructor(
    private xHomeServices: HomepageServices,
    private formBuilder: FormBuilder,
    private xRoute: Router,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xdatabase: AngularFireDatabase,
    private messagingService: MessagingDetailsService,
    private angularFireMessaging: AngularFireMessaging,
    private EncrDecr: EncrDecrServiceService,

  ) {
    var chk = localStorage.getItem("tempadmin");
    if (chk == "QWERTYUIOP") {

    } else {
      this.xRoute.navigate(["/admin/login"]);
    }
   
  }

  ngOnInit() {
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage
    console.log("Checking notification: ", this.message)
    this.formUsrLoginGroup = this.formBuilder.group({
      Eamilname: ["", Validators.required],
      passwordname: ["", Validators.required]
    });
    this.getchartdata();
    this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    console.log('tokenyesh', this.ModelObj)
    //this.saveToFirebaseDb(this.ModelObj);

  }

  // saveToFirebaseDb(notify_token) {
  //   let encuser = localStorage.getItem('UserName');
  //   if (encuser != null || encuser != '') {
  //     this.xdatabase.object('Token/' + encuser).set({
  //       token: notify_token
  //     })
  //   }
  // }

  onRecieveMessage() {
    // this.angularFireMessaging.messages.subscribe(
    //   (payload) => {
    //     console.log("new message received. ", payload);
    //     this.currentMessage.next(payload);
    //   })
    this.angularFireMessaging.messages.subscribe(
      ({ data }: { data: any }) =>{
        console.log("new message received: ", data)
      },
      err => console.log(err),
    );
  }

  //  getreload(){
  //   window.location.reload();
  //  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrLoginGroup.controls;
  }
  LoginBtn() {
    // stop here if form is invalid
    if (this.formUsrLoginGroup.invalid) {
      return;
    }
    this.isLoading = true;
    this.button = "Processing";

    this.xHomeServices.WorkorderGet(this.HomeModelobj).subscribe(response => {
      console.log("count  data ", response);
      //debugger;
      if (response.length != 0) {
        var id = response[0].workOrder_ID;
        this.HomeModelobj.attribute7 = response[0].attribute7;
        this.HomeModelobj.attribute8 = response[0].attribute8;
        this.HomeModelobj.city = response[0].city;
        this.HomeModelobj.clientInstructions = response[0].clientInstructions;
        this.HomeModelobj.clientStatus = response[0].clientStatus;
        this.HomeModelobj.controlConfig = response[0].controlConfig;
        this.HomeModelobj.country = response[0].country;
        this.HomeModelobj.currUserId = response[0].currUserId;
        this.HomeModelobj.description = response[0].description;
        this.HomeModelobj.reference = response[0].reference;
        this.HomeModelobj.source_wo_id = response[0].source_wo_id;
        this.HomeModelobj.source_wo_number = response[0].source_wo_number;
        this.HomeModelobj.source_wo_provider = response[0].source_wo_provider;
        this.HomeModelobj.state = response[0].state;
        this.HomeModelobj.status = response[0].status;
        this.HomeModelobj.workOrderNumber = response[0].workOrderNumber;
        this.HomeModelobj.zip = response[0].zip;
        this.HomeModelobj.address1 = response[0].address1;

        this.isLoading = false;
        this.button = "Get Order";
      } else {
        this.isLoading = false;
        this.button = "Please Try Again";
      }
    });
  }



  // bar chart start
  chartdata: any;
  public barChartOptions: any = {
  
    scaleShowVerticalLines: true,
    responsive: true
  };

  decuser:any;
  getchartdata(){
debugger
if(localStorage.getItem('usertemp_') != null)
{
  var encuser = JSON.parse(localStorage.getItem('usertemp_'));
  var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
  this.decuser  =JSON.parse(decval) ;
 
}

    this.xHomeServices.GetChartDetails(this.HomeModelobj).subscribe(response => {
      this.chartdata = response;
      for (let i = 0; i < this.chartdata[2].length; i++) {
       this.yeardata.push(this.chartdata[2][i].MonthYear);
       this.countdata.push(this.chartdata[2][i].countval);
      }

      for (let j = 0; j < this.chartdata[1].length; j++) {
        this.pieData.push(this.chartdata[1][j].Status_Name);
        this.piecount.push(this.chartdata[1][j].countval);
      
      }
       
     })

   
  
  }




   public mbarChartLabels: string[] =   this.yeardata;
   //[
  //   "aaaa",
  //   "bbbb",
  //   "cccc",
  //   "dddd",
  //   "eeee",
  //   "ffff",
  //   "gggg"
  // ];
  public barChartType: string = "bar";
  public barChartLegend: boolean = true;

  public barChartColors: Array<any> = [
    // {
    //   backgroundColor: "rgba(105,159,177,0.2)",
    //   borderColor: "rgba(105,159,177,1)",
    //   pointBackgroundColor: "rgba(105,159,177,1)",
    //   pointBorderColor: "#fafafa",
    //   pointHoverBackgroundColor: "#fafafa",
    //   pointHoverBorderColor: "rgba(105,159,177)"
    // },
    {
      backgroundColor: "rgb(52, 204, 255)",
      borderColor: "rgba(77,20,96,1)",
      pointBackgroundColor: "rgba(77,20,96,1)",
      pointBorderColor: "rgb(52, 204, 255)",
      pointHoverBackgroundColor: "rgb(52, 204, 255)",
      pointHoverBorderColor: "rgba(77,20,96,1)"
    }
  ];
  public barChartData: any[] = [
    // { data: [55, 60, 75, 82, 76, 62, 80], label: "Activate User" },
    { data:  this.countdata, label: "Active Work Order" }
  ];



  public randomize(): void {
    let data = [
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      Math.random() * 100,
      Math.round(Math.random() * 100),
      Math.random() * 100,
      Math.round(Math.random() * 100)
    ];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }
  // end  code bar chart

  // Doughnut chart start
  // Doughnut

  public barChartOptionsx: any = {
    scaleShowVerticalLines: true,
    responsive: true
  };
  public mbarChartLabelsx: string[] = this.pieData;
  // [
  //   "aaaa",
  //   "bbbb",
  //   "cccc",
  //   "dddd",
  //   "eeee",
  //   "ffff",
  //   "gggg"
  // ];
  public barChartTypex: string = "doughnut";
  public barChartLegendx: boolean = true;

  public barChartColorsx: Array<any> = [
    // {
    //   backgroundColor: "rgba(105,159,177,0.2)",
    //   borderColor: "rgba(105,159,177,1)",
    //   pointBackgroundColor: "rgba(105,159,177,1)",
    //   pointBorderColor: "#fafafa",
    //   pointHoverBackgroundColor: "#fafafa",
    //   pointHoverBorderColor: "rgba(105,159,177)"
    // },
    {
      backgroundColor: "rgb(52, 204, 255)",
      borderColor: "rgb(214, 212, 212)",
      pointBackgroundColor: "rgb(247, 2, 19)",
      pointBorderColor: "rgb(247, 2, 19)",
      pointHoverBackgroundColor: "rgb(247, 2, 19)",
      pointHoverBorderColor: "rgba(77,20,96,1)"
    }
  ];
  public barChartDatax: any[] = [
    // { data: [55, 60, 75, 82, 76, 62, 80], label: "Activate User" },
    { data: this.piecount, label: "Status" }
  ];



  public randomizex(): void {
    let data = [
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      Math.random() * 100,
      Math.round(Math.random() * 100),
      Math.random() * 100,
      Math.round(Math.random() * 100)
    ];
    let clonex = JSON.parse(JSON.stringify(this.barChartDatax));
    clonex[0].data = data;
    this.barChartDatax = clonex;
  }
  //end Doughnut
 

 

///pie chart
public pieChartLabels:any = this.pieData;
public pieChartData:any = this.shriganesh;
public pieChartType:string = 'pie';








}
