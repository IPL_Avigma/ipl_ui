import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { ChartsModule } from 'ng2-charts';



import { HomeComponent } from './HomeComponent';

import {HomepageServices} from './HomeServices';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../environments/environment';
import { AsyncPipe } from '../../../node_modules/@angular/common';
import { MessagingDetailsService } from '../ClientResult/MessagesDetails/MessageDetailsServices';
 

const HomeRouts =[

  {path:'index',component:HomeComponent}

]

@NgModule({
  declarations: [HomeComponent],
  imports: [
    RouterModule.forChild(HomeRouts),
    CommonModule,
    FormsModule,
    AngularFireMessagingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ChartsModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
 AngularFireModule.initializeApp(environment.firebase),

  ],
  providers: [HomepageServices,AsyncPipe,MessagingDetailsService],
  bootstrap: [HomeComponent]
})

export class HomeModule {}
