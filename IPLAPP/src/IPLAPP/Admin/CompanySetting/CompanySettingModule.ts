import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { CompanySettingComponent } from './CompanySettingCompnent';

const CompanySettingRouts = [
  { path: "companysetting", component: CompanySettingComponent }
];

@NgModule({
  declarations: [CompanySettingComponent],
  imports: [
    RouterModule.forChild(CompanySettingRouts),
    CommonModule

  ],
  providers: [],
  bootstrap: [CompanySettingComponent]
})

export class CompanySettingModule {}
