import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';


import { ViewGroupsComponent } from './ViewGroupsComponent';
import {ViewGroupsServices} from './ViewGroupsServices';
import {AddGroupsServices} from '../AddGroups/AddGroupsServices';



 const ViewGroupsRouts = [
  { path: "viewgroups", component: ViewGroupsComponent }
];
@NgModule({
  declarations: [ViewGroupsComponent],
  imports: [
    RouterModule.forChild(ViewGroupsRouts),
    CommonModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    GridModule,ExcelModule

  ],
  providers: [ViewGroupsServices,AddGroupsServices],
  bootstrap: [ViewGroupsComponent]
})

export class ViewGroupsModule {}
