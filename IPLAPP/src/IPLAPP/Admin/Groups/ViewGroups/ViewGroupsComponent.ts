import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ViewGroupsServices } from "./ViewGroupsServices";
import { ViewGroupsModel } from "./ViewGroupsModel";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import {AddGroupsServices} from '../AddGroups/AddGroupsServices';
import {AddGroupsModel} from '../AddGroups/AddGroupsModel';


import { process, State } from "@progress/kendo-data-query";
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";

@Component({
  templateUrl: "./ViewGroups.html"
})
export class ViewGroupsComponent implements OnInit {
  ViewGroupsModelObj: ViewGroupsModel = new ViewGroupsModel();
  AddGroupsModelObj: AddGroupsModel = new AddGroupsModel();
  public griddata: any[];

  constructor(
    private xViewGroupsServices: ViewGroupsServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xAddGroupsServices :AddGroupsServices,
  ) {

    this.GetGroupData();
  }

  ngOnInit() {}

  GetGroupData() {
    this.xViewGroupsServices
      .ViewGroupsData(this.ViewGroupsModelObj)
      .subscribe(response => {
        console.log("respo group view", response);
        this.griddata = response[0];
      });


  }

  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    this.ViewGroupsModelObj.Type = 3;

    this.xViewGroupsServices
      .ViewGroupsData(this.ViewGroupsModelObj)
      .subscribe(response => {
        console.log("filter data", response);
        this.griddata = response[0];
      });
  }

  ClearData() {
    this.ViewGroupsModelObj = new ViewGroupsModel();
    this.GetGroupData();
  }
  SaveFilterData() {
    alert("save called");
  }
  ///

  // this code selected event row
  showDetails(event, dataItem) {
    //debugger;

    this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    this.xRouter.navigate(["/group/addgroups"]);
  }

  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {

      this.AddGroupsModelObj.Type = 4;
      this.AddGroupsModelObj.Grp_IsDelete = true;
      this.AddGroupsModelObj.Grp_pkeyID = dataItem.Grp_pkeyID;

      this.xAddGroupsServices
      .GruopDataPost(this.AddGroupsModelObj)
      .subscribe(response => {
        console.log("resp delete", response);
        this.GetGroupData();

      });
    }
  }
  //end code


  // clear data
  AddNewGroup() {
    var faltu = undefined;

    this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/group/addgroups"]);
  }



  //kendo check box event action

  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    // filter: {
    //   logic: "and",
    //   filters: [{ field: "UOM_Name", operator: "contains", value: "" }]
    // }
  };

  //public gridData: GridDataResult = process(this.griddata, this.state);

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

    //this.griddata = process(this.faltu, this.state);
  }
}
