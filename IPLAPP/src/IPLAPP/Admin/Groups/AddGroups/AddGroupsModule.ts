import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";


import { AddGroupsComponent } from './AddGroupsComponent';
import {AddGroupsServices} from './AddGroupsServices';

export const AddGroupsRouts = [
  { path: "addgroups", component: AddGroupsComponent }
];
@NgModule({
  declarations: [AddGroupsComponent],
  imports: [
    RouterModule.forChild(AddGroupsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    NgbModule,


  ],
  providers: [AddGroupsServices],
  bootstrap: [AddGroupsComponent]
})

export class AddGroupsModule {}
