import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { BasetUrl } from "../../../Utility/DomainUrl";
import { AddGroupsModel,MenuMasterModel,GrouproleModel} from "./AddGroupsModel";
import { AddGroupsServices } from "./AddGroupsServices";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";


@Component({
  templateUrl: "./AddGroups.html"
})
export class AddGroupsComponent implements OnInit {
  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  IsAssigned = false;
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  GroupRoleValFlag = false;
  dropCkck = false;

  AddGroupsModelObj: AddGroupsModel = new AddGroupsModel();
  MenuMasterModelObj: MenuMasterModel = new MenuMasterModel();
  GrouproleModelObj: GrouproleModel = new GrouproleModel();

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddGroupsServices: AddGroupsServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
   
  ) {}

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      FName: ["", Validators.required],
      disact: ["", Validators.nullValidator],
      IsAssigned: ["",Validators.nullValidator],
      GroupRoleVal:["", Validators.nullValidator],

    });
     // becoz instance create nahi hot
    
     this.getModelData();
     //this.GetMenuDetails();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    debugger;
    this.submitted = true;
    this.AddGroupsModelObj;
    this.dropCkck = false;

    if (this.AddGroupsModelObj.GroupRoleId == 0) {
      this.GroupRoleValFlag = true;
      this.dropCkck = true;
    }
    if (this.dropCkck) {
      return;
    }

    // if(this.AddGroupsModelObj.Grp_IsActive == false){
    // alert('not avalie');

    // }

    ////
    // only drop down
    //this.dropCkck = false;

    // if (this.AddGroupsModelObj.YR_Company_State == 0) {
    //   this.StateFlag = true;
    //   this.dropCkck = true;
    // }

    // if (this.dropCkck) {
    //   return;
    // }

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    // all valid data to save
    this.AddGroupsModelObj.MenuArray = this.Menuarr;
   
    this.xAddGroupsServices
      .GruopDataPost(this.AddGroupsModelObj)
      .subscribe(response => {
        console.log("resp data pkey", response);
        if (true) {
          this.AddGroupsModelObj.Grp_pkeyID = parseInt(response[0]);
          this.MessageFlag = "Group information Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end

  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  /// end common model


  ModelObj: any;
  IsEditDisable = false;
  getModelData() {
    //debugger;
    this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    if (this.ModelObj == undefined) {
      this.AddGroupsModelObj =  new AddGroupsModel();
      

    } else 
    {
     
      debugger
      console.log('lol',this.ModelObj);
      this.AddGroupsModelObj.Type = this.ModelObj.Type;
      this.AddGroupsModelObj.Grp_IsActive = this.ModelObj.Grp_IsActive;
      this.AddGroupsModelObj.Grp_Name = this.ModelObj.Grp_Name;
      this.AddGroupsModelObj.Grp_pkeyID = this.ModelObj.Grp_pkeyID;
      this.AddGroupsModelObj.GroupRoleId = this.ModelObj.GroupRoleId;
      
    

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;
      this.IsAssigned = true;
      this.button = "Update";
      this.AddGroupsModelObj.Type = 2;
      //this.GetDocumentFile();
      // get address multiple
    
    }
    this.getGroupRoleDrd();
 
  }
  // end code
  EditForms(){
    this.IsEditDisable = false;
    this.IsAssigned = false;
    this.formUsrCommonGroup.enable();
  }
   // end code

   Group_Method(){
    this.GroupRoleValFlag = false;
   }


   // for Group Menu 
   Menulst: any;
   GetMenuDetails(){
     debugger;
    this.MenuMasterModelObj.Ipre_MenuID = 0;
    if(this.ModelObj == undefined)
    {
      this.MenuMasterModelObj.Mgr_Group_Id = 0;
    }
    else
    {
      this.MenuMasterModelObj.Mgr_Group_Id = this.ModelObj.Grp_pkeyID;
      console.log('as',this.ModelObj.Grp_pkeyID)
    }
    
   
    this.MenuMasterModelObj.Type = 1;
    this.xAddGroupsServices
    .GetMenuData(this.MenuMasterModelObj)
    .subscribe(response => {
      this.Menulst = response[1];
      
      console.log("Menudata", response);
    
      
   });
 
  }

//Menu Selection
Menuarr = [];
menuselection(item,i)
{
  debugger
  if (item.IsAssignedMenu == true) {
    this.Menuarr.push(item)
  }
  else{
    this.Menuarr.push(item)
    console.log('uncheck',item);
   // this.Menuarr.splice(i, 1);
  }
  console.log('Menuarr', this.Menuarr )

}

//get role dropdown
GroupRoleList: any;
getGroupRoleDrd(){
  debugger;
  this.xAddGroupsServices.GetGroupdrd(this.GrouproleModelObj)
  .subscribe(response => {
    debugger;
    console.log('drddata',response );
    this.GroupRoleList = response[0];
    
 });
 this.GetMenuDetails()
}

selectall(itemx, i){
  debugger

  if (itemx.checked == true) {
   
    
    //this.Menuarr =[];

    for (let l = 0; l < this.Menulst.length; l++)
    {
      for (let j = 0; j < this.Menulst[l][0].ChildMenu.length; j++)
      {
        for (let k = 0; k < itemx.ChildMenu.length; k++) {
        
          if (this.Menulst[l][0].ChildMenu[j].MenuId == itemx.ChildMenu[k].MenuId )
          {
            this.Menulst[l][0].ChildMenu[j].IsAssignedMenu = true;
            this.Menuarr.push(itemx.ChildMenu[k]);
          } 
        }

      }
      

    }

//  for (let i = 0; i < itemx.ChildMenu.length; i++) {
//   Object.assign( itemx.ChildMenu[i], {IsAssignedMenu: "true"});
//   this.Menuarr.push(itemx.ChildMenu[i]);
//   console.log('chec',  itemx.ChildMenu[i]);
   
//  }
 console.log('lenght',  this.Menuarr.length );
   
  }
  else
  {
  

    this.Menuarr =[];
    for (let l = 0; l < this.Menulst.length; l++)
    {
      for (let j = 0; j < this.Menulst[l][0].ChildMenu.length; j++)
      {
        for (let k = 0; k < itemx.ChildMenu.length; k++) {
        
          if (this.Menulst[l][0].ChildMenu[j].MenuId == itemx.ChildMenu[k].MenuId )
          {
            this.Menulst[l][0].ChildMenu[j].IsAssignedMenu = false;
            this.Menuarr.push(itemx.ChildMenu[k]);
          } 
        }

      }
      

    }
     
  }

}


}
