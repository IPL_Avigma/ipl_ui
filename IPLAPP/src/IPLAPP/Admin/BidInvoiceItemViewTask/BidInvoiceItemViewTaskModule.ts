import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownListModule } from '@progress/kendo-angular-dropdowns';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { BidInvoiceItemViewTaskComponent } from './BidInvoiceItemViewTaskComponent';
import {BidInvoiceItemViewTaskServices} from './BidInvoiceItemViewTaskServices';
import {BidInvoiceItemServices} from '../BidInvoiceItem/BidInvoiceItemServices';

//import {DropDownListFilterComponent} from './BidInvoiceItemViewTaskdropdownlistfilter';

const BidInvoiceItemViewTaskRouts = [
  { path: "bidinvoiceitemviewtask", component: BidInvoiceItemViewTaskComponent }
];
@NgModule({
  declarations: [BidInvoiceItemViewTaskComponent],
  imports: [
    RouterModule.forChild(BidInvoiceItemViewTaskRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    GridModule,
    DropDownListModule

  ],
  providers: [BidInvoiceItemViewTaskServices,BidInvoiceItemServices],
  bootstrap: [BidInvoiceItemViewTaskComponent]
})

export class BidInvoiceItemViewTaskModule {}
