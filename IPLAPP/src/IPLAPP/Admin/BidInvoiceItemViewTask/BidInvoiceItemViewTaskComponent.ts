import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { BidInvoiceItemViewTaskServices } from "./BidInvoiceItemViewTaskServices";
import { BidInvoiceItemViewTaskModel } from "./BidInvoiceItemViewTaskModel";
import { MasterlayoutComponent } from "../../Home/MasterComponent";

import {BidInvoiceItemModel} from '../BidInvoiceItem/BidInvoiceItemModel';
import {BidInvoiceItemServices} from '../BidInvoiceItem/BidInvoiceItemServices';

import { State } from "@progress/kendo-data-query";

import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

// for kendo drop down
const distinct = data => data
    .map(x => x.Task_TypeName)
    .filter((x, idx, xs) => xs.findIndex(y => y.Task_TypeName === x.Task_TypeName) === idx);

@Component({
  templateUrl: "./BidInvoiceItemViewTask.html"
})
export class BidInvoiceItemViewTaskComponent implements OnInit {
  BidInvoiceItemViewTaskModelObj: BidInvoiceItemViewTaskModel = new BidInvoiceItemViewTaskModel();

  BidInvoiceItemModelObj:BidInvoiceItemModel = new BidInvoiceItemModel();


  public griddata: any[];
  constructor(
    private xBidInvoiceItemViewTaskServices: BidInvoiceItemViewTaskServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xBidInvoiceItemServices:BidInvoiceItemServices,
    private EncrDecr: EncrDecrServiceService
  ) {
    this.GetGridData();
  }

  ngOnInit() {}

  GetGridData() {
    this.xBidInvoiceItemViewTaskServices
      .ViewTaskMasterData(this.BidInvoiceItemViewTaskModelObj)
      .subscribe(response => {
        console.log("task list", response);
        this.griddata = response[0];
      });
  }

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/task/addinvoiceitems"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Task_pkeyID);
    this.xRouter.navigate(["/task/addinvoiceitems", btoa(encrypted)]);
  }

  // common code End
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.BidInvoiceItemModelObj.Task_pkeyID = dataItem.Task_pkeyID;
      this.BidInvoiceItemModelObj.Task_IsDelete = true;

      this.xBidInvoiceItemServices
        .TaskMasterPost(this.BidInvoiceItemModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  // clear data
  AddNewKr() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/task/bidinvoiceitem",'new']);
  }


  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

  }

  // for kendo drop down

  //public distinctCategories: any[] = distinct(this.griddata);



}



