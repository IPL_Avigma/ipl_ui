import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { SupportComponent } from './SupportDetailsComponent';
// import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

export const ReportsRouts = [
  { path: "supportdetails", component: SupportComponent }
];

@NgModule({
  declarations: [SupportComponent],
  imports: [
    RouterModule.forChild(ReportsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    



  ],
  providers: [],
  bootstrap: [SupportComponent]
})

export class SupportModule {}
