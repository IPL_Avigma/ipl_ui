import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddLoanTypeComponent } from './AddLoanTypeComponent';

import { AddLoanTypeServices } from "./AddLoanTypeService";

const AddLoanTypeComponentComponentRouts = [
  { path: "addloantype/:id", component: AddLoanTypeComponent }
];

@NgModule({
  declarations: [AddLoanTypeComponent],
  imports: [
    RouterModule.forChild(AddLoanTypeComponentComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,

  ],
  providers: [AddLoanTypeServices],
  bootstrap: [AddLoanTypeComponent]
})

export class AddLoanTypeModule {}
