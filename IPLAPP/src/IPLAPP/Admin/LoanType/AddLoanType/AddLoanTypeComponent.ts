import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddLoanTypeModel } from "./AddLoanTypeModel";
import { AddLoanTypeServices } from "./AddLoanTypeService";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewLoanTypeModel } from '../ViewLoanType/ViewLoanTypeModel';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ActivatedRoute } from '@angular/router';
import { ViewLoanTypeServices } from '../ViewLoanType/ViewLoanTypeServices';

@Component({
  templateUrl: "./AddLoanType.html"
})
export class AddLoanTypeComponent implements OnInit {
  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi

  AddLoanTypeModelObj: AddLoanTypeModel = new AddLoanTypeModel();
  ViewLoanTypeModelObj: ViewLoanTypeModel = new ViewLoanTypeModel();


  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddLoanTypeServices:AddLoanTypeServices,
    private xViewLoanTypeServices: ViewLoanTypeServices,
    private xMasterlayoutComponent:MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService
  ) {}

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      ComName: ["", Validators.required],
      loanactive: ["", Validators.required]
    });
    this.getModelData();
  }
   // shortcurt Namefor form sathi
   get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;




    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    
    if(this.WorkOrderObj) {
      this.AddLoanTypeModelObj.Loan_pkeyId = this.WorkOrderObj;
    }

    this.xAddLoanTypeServices
      .LoanTypeDataPost(this.AddLoanTypeModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddLoanTypeModelObj.Loan_pkeyId = parseInt(response[0].Loan_pkeyId);

          this.MessageFlag = "Load Type Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  WorkOrderObj: any;
  IsEditDisable = false;
  getModelData() {
    debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddLoanTypeModelObj = new AddLoanTypeModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Cust_Num_pkeyId', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
    }


    // if (this.ModelObj == undefined) {
    //   this.AddLoanTypeModelObj = new AddLoanTypeModel();
    // } else {

    //   this.AddLoanTypeModelObj.Loan_pkeyId = this.ModelObj.Loan_pkeyId;
    //   this.AddLoanTypeModelObj.Loan_Type = this.ModelObj.Loan_Type;
    //   this.AddLoanTypeModelObj.Loan_IsActive = this.ModelObj.Loan_IsActive;
    //   this.AddLoanTypeModelObj.UserID = this.ModelObj.UserID;

    
  }


  GetSingleData() {
    this.ViewLoanTypeModelObj.Loan_pkeyId = this.WorkOrderObj;
    this.ViewLoanTypeModelObj.Type = 2;
    
    this.xViewLoanTypeServices.ViewLoanTypeData(this.ViewLoanTypeModelObj)
    .subscribe( response => {
      this.AddLoanTypeModelObj.Loan_Type = 
      response[0][0].Loan_Type;
      this.AddLoanTypeModelObj.Loan_IsActive = 
      response[0][0].Loan_IsActive;
    });


    this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;

      this.button = "Update";
      // this.AddLoanTypeModelObj.Type = 2;


  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code
}
