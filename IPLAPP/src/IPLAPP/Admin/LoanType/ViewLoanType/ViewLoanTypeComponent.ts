import { Component, Injectable, OnInit } from "@angular/core";

import { Router } from "@angular/router";

import { ViewLoanTypeServices } from "./ViewLoanTypeServices";
import { ViewLoanTypeModel } from "./ViewLoanTypeModel";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import {AddLoanTypeServices} from '../AddLoanType/AddLoanTypeService';
import {AddLoanTypeModel} from '../AddLoanType/AddLoanTypeModel';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewLoanType.html"
})
export class ViewLoanTypeComponent implements OnInit {
  public griddata: any[];

  ViewLoanTypeModelObj: ViewLoanTypeModel = new ViewLoanTypeModel();
  AddLoanTypeModelObj: AddLoanTypeModel = new AddLoanTypeModel();
  constructor(
    private xViewLoanTypeServices: ViewLoanTypeServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter:Router,
    private EncrDecr: EncrDecrServiceService,
    private xAddLoanTypeServices:AddLoanTypeServices,
  ) {
    this.GetGridData();
  }

  ngOnInit() {}

  //get grid
  GetGridData() {
    this.xViewLoanTypeServices
      .ViewLoanTypeData(this.ViewLoanTypeModelObj)
      .subscribe(response => {
        console.log("resp Loan Type", response);
        this.griddata = response[0];
      });
  }

  // common code
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    // this.ViewUserModelObj.Type = 4;
    // this.xViewUserServices
    //   .ViewUserData(this.ViewUserModelObj)
    //   .subscribe(response => {
    //     console.log("User Details", response);
    //     this.griddata = response[0];
    //   });
  }

  ClearData() {
    this.ViewLoanTypeModelObj = new ViewLoanTypeModel();
    //this.getautoUserdata();
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/loan/addloantype"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Loan_pkeyId);
    this.xRouter.navigate(["/loan/addloantype", btoa(encrypted)]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AddLoanTypeModelObj.Loan_pkeyId = dataItem.Loan_pkeyId;
      this.AddLoanTypeModelObj.Loan_IsDelete = true;

      this.xAddLoanTypeServices
        .LoanTypeDataPost(this.AddLoanTypeModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  // clear data
  AddNewItem() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/loan/addloantype", 'new']);
  }
}
