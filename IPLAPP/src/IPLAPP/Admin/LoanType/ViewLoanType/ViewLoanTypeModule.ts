import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule } from '@progress/kendo-angular-grid';

import { ViewLoanTypeComponent } from './ViewLoanTypeComponent';

import { ViewLoanTypeServices } from "./ViewLoanTypeServices";
import {AddLoanTypeServices} from '../AddLoanType/AddLoanTypeService';

const ViewLoanTypeComponentRouts = [
  { path: "viewloantype", component: ViewLoanTypeComponent }
];

@NgModule({
  declarations: [ViewLoanTypeComponent],
  imports: [
    RouterModule.forChild(ViewLoanTypeComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule

  ],
  providers: [ViewLoanTypeServices,AddLoanTypeServices],
  bootstrap: [ViewLoanTypeComponent]
})

export class ViewLoanTypeModule {}
