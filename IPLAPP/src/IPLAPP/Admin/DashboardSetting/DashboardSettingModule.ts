import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { DashboardSettingComponent } from './DashboardSettingComponent';

const DashboardSettingRouts = [
  { path: "dashboardsetting", component: DashboardSettingComponent }
];


@NgModule({
  declarations: [DashboardSettingComponent],
  imports: [
    RouterModule.forChild(DashboardSettingRouts),
    CommonModule

  ],
  providers: [],
  bootstrap: [DashboardSettingComponent]
})

export class DashboardSettingModule {}
