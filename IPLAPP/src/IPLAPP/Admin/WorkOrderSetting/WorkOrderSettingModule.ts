import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { WorkOrderSettingComponent } from './WorkOrderSettingComponent';

const WorkOrderSettingRouts = [
  { path: "workordersetting", component: WorkOrderSettingComponent }
];


@NgModule({
  declarations: [WorkOrderSettingComponent],
  imports: [
    RouterModule.forChild(WorkOrderSettingRouts),
    CommonModule

  ],
  providers: [],
  bootstrap: [WorkOrderSettingComponent]
})

export class WorkOrderSettingModule {}
