import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";


import {AddCustomLableModel} from './CustomPhotoLableModel';
import {ViewCustomPhotoLabelModel} from '../ViewcustomPhotoLabel/ViewCustomPhotoLableModel';
import {AddCustomPhotoServices} from './CustomPhotoLableServices';

import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ActivatedRoute } from '@angular/router';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ViewCustomPhotoLabelServices } from '../ViewcustomPhotoLabel/ViewCustomPhotoLableServices';



@Component({
  templateUrl: "./CustomPhotoLable.html"
})
export class CustomPhotoLableComponent implements OnInit {


  AddCustomLableModelObj:AddCustomLableModel = new AddCustomLableModel();
  ViewCustomPhotoLableModelObj: ViewCustomPhotoLabelModel = new ViewCustomPhotoLabelModel();


  submitted = false;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  isLoading = false;

  public contentx;
  MessageFlag: string;

  WorkTypevalFlag = false; // for dropdown
  CustomerNumbervalFlag = false; // for dropdown
  
   // only drop down
   dropCkck = false;

  constructor( private formBuilder: FormBuilder,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xmodalService: NgbModal,
    private xAddCustomPhotoServices:AddCustomPhotoServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xViewCustomPhotoLabelServices: ViewCustomPhotoLabelServices
    ) {

  }

  ngOnInit() {

    this.formUsrCommonGroup = this.formBuilder.group({
      CustomName: ["", Validators.required],
      disbledFaltu1: ["", Validators.nullValidator],
      disbledFaltu2: ["", Validators.required],
      disbledFaltu3: ["", Validators.nullValidator],
      disbledFaltu4: ["", Validators.nullValidator],
      disbledFaltu5: ["", Validators.nullValidator],
    });

    this.GetDropDowndata();
    this.getModelData();
  }

  ngOnDestroy(): void {
    this.submitted = false;
    this.button = "Save";
    this.isLoading = false;
    this.AddCustomLableModelObj = new AddCustomLableModel();
  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // WorkType_Method() {
  //   this.WorkTypevalFlag = false ;
  // }
  CustomerNumber_Method() {
    this.CustomerNumbervalFlag = false;
  }
  // submit form
  FormButton(content) {
    debugger
    this.contentx = content;
    debugger;
    this.submitted = true;
    this.dropCkck = false;
    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    // if (this.AddCustomLableModelObj.PhotoLabel_WorkType_Id == 0) {
    //   this.WorkTypevalFlag = true;
    //   //return;
    //   this.dropCkck = true;
     
    // }
    if (this.dropCkck) {
      return;
    }

    this.isLoading = true;
   
    this.button = "Processing";

    if(this.WorkOrderObj !== undefined) {
      this.AddCustomLableModelObj.PhotoLabel_pkeyID = this.WorkOrderObj;
    } else {
      this.AddCustomLableModelObj.PhotoLabel_pkeyID = 0;
    }
    
    this.xAddCustomPhotoServices
      .CustomPhotoDataPost(this.AddCustomLableModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0][0].Status != "0") {
          this.AddCustomLableModelObj.PhotoLabel_pkeyID = parseInt(response[0][0].PhotoLabel_pkeyID);

          this.MessageFlag = "Custom Photos Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model
  CompanyList: any; // temp array
  CustomerList: any; // temp array
  WorkTypeList: any; // temp array
  CustomerNumberList: any;
  Loan_TypeList:any;

  GetDropDowndata() {
    this.xWorkOrderDrodownServices
      .DropdownGetWorkOrder()
      .subscribe(response => {
        console.log("drop down  data custom label ", response);
        //debugger;
        if (response.length != 0) {
          this.CompanyList = response[0];
          this.WorkTypeList = response[1];
          this.CustomerNumberList = response[9];
          this.Loan_TypeList =  response[10];

        }


      });
  }


  WorkOrderObj: any;
  IsEditDisable = false;
  
  getModelData() {
    debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddCustomLableModelObj = new AddCustomLableModel(); 
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('PhotoLabel_pkeyID', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
    }
  }
    // if (this.ModelObj == undefined) {
    //   this.AddCustomLableModelObj = new AddCustomLableModel();
    // } else {
    //   this.AddCustomLableModelObj.PhotoLabel_pkeyID = this.ModelObj.PhotoLabel_pkeyID;
    //   this.AddCustomLableModelObj.PhotoLabel_Name = this.ModelObj.PhotoLabel_Name;
    //   this.AddCustomLableModelObj.PhotoLabel_IsCustom = this.ModelObj.PhotoLabel_IsCustom;
    //   this.AddCustomLableModelObj.PhotoLabel_IsActive = this.ModelObj.PhotoLabel_IsActive;
    //   this.AddCustomLableModelObj.PhotoLabel_IsDelete = this.ModelObj.PhotoLabel_IsDelete;
    //   this.AddCustomLableModelObj.Custom_label_Check = this.ModelObj.Custom_label_Check;
    //   this.AddCustomLableModelObj.workOrder_ID = this.ModelObj.workOrder_ID;
    //   this.AddCustomLableModelObj.PhotoLabel_Valtype = this.ModelObj.PhotoLabel_Valtype;
    //   this.AddCustomLableModelObj.PhotoLabel_Client_Id = this.ModelObj.PhotoLabel_Client_Id;
    //   this.AddCustomLableModelObj.PhotoLabel_WorkType_Id = this.ModelObj.PhotoLabel_WorkType_Id;
    //   this.AddCustomLableModelObj.PhotoLabel_Customer_Id = this.ModelObj.PhotoLabel_Customer_Id;
    //   this.AddCustomLableModelObj.PhotoLabel_Loan_Id = this.ModelObj.PhotoLabel_Loan_Id;
    //   this.AddCustomLableModelObj.UserID = this.ModelObj.UserID;

    //   this.AddCustomLableModelObj.Type = 2;
    // }
  // }


  GetSingleData() {
    debugger;
    this.ViewCustomPhotoLableModelObj.PhotoLabel_pkeyID = this.WorkOrderObj;
    this.ViewCustomPhotoLableModelObj.Type = 2;
    this.xViewCustomPhotoLabelServices.ViewCustomData(this.ViewCustomPhotoLableModelObj)
    .subscribe( response => {
      debugger;
      console.log('GetSingleData', response);

      this.AddCustomLableModelObj.PhotoLabel_Name = 
      response[0][0][0].PhotoLabel_Name;
      this.AddCustomLableModelObj.PhotoLabel_Client_Id = 
      response[0][0][0].PhotoLabel_Client_Id;
      this.AddCustomLableModelObj.PhotoLabel_WorkType_Id = 
      response[0][0][0].PhotoLabel_WorkType_Id;
      this.AddCustomLableModelObj.PhotoLabel_Customer_Id = 
      response[0][0][0].PhotoLabel_Customer_Id;
      this.AddCustomLableModelObj.PhotoLabel_Loan_Id = 
      response[0][0][0].PhotoLabel_Loan_Id;
      this.AddCustomLableModelObj.PhotoLabel_IsActive = 
      response[0][0][0].PhotoLabel_IsActive;

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;

      this.button = "Update";
    });
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code

}
