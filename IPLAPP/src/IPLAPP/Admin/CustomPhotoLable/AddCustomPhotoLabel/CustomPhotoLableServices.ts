import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddCustomLableModel} from './CustomPhotoLableModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import { HomepageServices } from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddCustomPhotoServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data
  private apiUrlPOST = BasetUrl.Domain +"api/WOCTASK/postCustomPhotoLabel";


  public CustomPhotoDataPost(Modelobj:AddCustomLableModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.PhotoLabel_pkeyID = Modelobj.PhotoLabel_pkeyID;
    ANYDTO.PhotoLabel_Name = Modelobj.PhotoLabel_Name;
    ANYDTO.PhotoLabel_IsCustom = Modelobj.PhotoLabel_IsCustom;
    ANYDTO.Custom_label_Check = Modelobj.Custom_label_Check;
    ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
    ANYDTO.PhotoLabel_Valtype = Modelobj.PhotoLabel_Valtype;
    ANYDTO.PhotoLabel_Client_Id = Modelobj.PhotoLabel_Client_Id;
    ANYDTO.PhotoLabel_WorkType_Id = Modelobj.PhotoLabel_WorkType_Id;
    ANYDTO.PhotoLabel_Customer_Id  = Modelobj.PhotoLabel_Customer_Id;
    ANYDTO.PhotoLabel_Loan_Id  = Modelobj.PhotoLabel_Loan_Id;

    ANYDTO.PhotoLabel_IsActive = Modelobj.PhotoLabel_IsActive;
    ANYDTO.PhotoLabel_IsDelete = Modelobj.PhotoLabel_IsDelete;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;
    if(Modelobj.PhotoLabel_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.PhotoLabel_IsDelete){

      ANYDTO.Type = 4;
    }



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }



  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Invalid request...please try again later...");
  }
}

