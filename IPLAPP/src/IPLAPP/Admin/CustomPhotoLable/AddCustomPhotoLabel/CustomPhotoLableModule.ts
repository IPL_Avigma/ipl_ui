import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import{CustomPhotoLableComponent} from "./CustomPhotoLableComponent";
import {AddCustomPhotoServices} from './CustomPhotoLableServices';

import {WorkOrderDrodownServices} from '../../../CommonServices/commonDropDown/DropdownService';




const AddCustomPhotoLablleRouts = [
  { path: "lableAdd/:id", component: CustomPhotoLableComponent }
];

@NgModule({
  declarations: [CustomPhotoLableComponent],
  imports: [
    RouterModule.forChild(AddCustomPhotoLablleRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,



  ],
  providers: [AddCustomPhotoServices,WorkOrderDrodownServices],
  bootstrap: [CustomPhotoLableComponent]
})

export class CustomPhotoLableModule {}
