import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";



import {ViewCustomPhotoLabelServices} from './ViewCustomPhotoLableServices';
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import {ViewCustomPhotoLabelModel} from './ViewCustomPhotoLableModel';

import {AddCustomPhotoServices} from '../AddCustomPhotoLabel/CustomPhotoLableServices';

import {AddCustomLableModel} from '../AddCustomPhotoLabel/CustomPhotoLableModel';

import { State } from "@progress/kendo-data-query";
import {DataStateChangeEvent} from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewCustomPhotoLable.html"
})
export class ViewCustomPhotoLabelComponent implements OnInit {


  ViewCustomPhotoLabelModelObj:ViewCustomPhotoLabelModel = new ViewCustomPhotoLabelModel();
  AddCustomLableModelObj:AddCustomLableModel = new AddCustomLableModel();
  public griddata: any[];

  constructor(  private xViewCustomPhotoLabelServices: ViewCustomPhotoLabelServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private EncrDecr: EncrDecrServiceService,
    private xRouter: Router,
    private xAddCustomPhotoServices:AddCustomPhotoServices,
     ) {
    //alert("hi");
  }

  ngOnInit() {
    this.GetGridData();
  }

  //get grid
  GetGridData() {
    this.xViewCustomPhotoLabelServices
      .ViewCustomData(this.ViewCustomPhotoLabelModelObj)
      .subscribe(response => {
        console.log("resp custom photos view", response);
        this.griddata = response[0][0];
      });
  }

  // common code
  FilterCall() {

  }

  ClearData() {
    this.ViewCustomPhotoLabelModelObj = new ViewCustomPhotoLabelModel();
    //this.getautoUserdata();
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/customphoto/lableAdd"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.PhotoLabel_pkeyID);
    this.xRouter.navigate(["/customphoto/lableAdd", btoa(encrypted)]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {

      this.AddCustomLableModelObj.PhotoLabel_IsDelete = true;
      this.AddCustomLableModelObj.PhotoLabel_pkeyID = dataItem.PhotoLabel_pkeyID;

      this.xAddCustomPhotoServices
      .CustomPhotoDataPost(this.AddCustomLableModelObj)
      .subscribe(response => {
        console.log("resp data detele", response);

        this.GetGridData();


      });

    }
  }

  // clear data
  AddNewCustom() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/customphoto/lableAdd", 'new']);
  }



     //kendo check box event action

     public state: State = {};

     public dataStateChange(state: DataStateChangeEvent): void {
       this.state = state;


     }
}
