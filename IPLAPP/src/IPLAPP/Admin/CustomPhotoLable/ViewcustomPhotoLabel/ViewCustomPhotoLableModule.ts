import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule } from '@progress/kendo-angular-grid';

import { ViewCustomPhotoLabelComponent } from './ViewCustomPhotoLableComponent';



const NameRouts = [
  { path: "labelview", component: ViewCustomPhotoLabelComponent }
];

@NgModule({
  declarations: [ViewCustomPhotoLabelComponent],
  imports: [
    RouterModule.forChild(NameRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule

  ],
  providers: [],
  bootstrap: [ViewCustomPhotoLabelComponent]
})

export class ViewCustomPhotoLabelModule {}
