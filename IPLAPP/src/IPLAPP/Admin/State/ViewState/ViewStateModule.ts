import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ViewStateComponent } from './ViewStateComponent';



 const ViewStateRouts = [
  { path: "viewstate", component: ViewStateComponent }
];

@NgModule({
  declarations: [ViewStateComponent],
  imports: [
    RouterModule.forChild(ViewStateRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule,


  ],
  providers: [],
  bootstrap: [ViewStateComponent]
})

export class ViewStateModule {}
