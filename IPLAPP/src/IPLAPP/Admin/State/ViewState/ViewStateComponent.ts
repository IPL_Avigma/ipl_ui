import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewStateModel } from "./ViewStateModel";
import { AddStateServices } from "../AddState/AddStateServices";
import { AddStateModel } from "../AddState/AddStateModel";
import { ViewUserServices } from "./ViewStateServices";

import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewState.html"
})
export class ViewStateComponent implements OnInit {
  ViewStateModelObj: ViewStateModel = new ViewStateModel();
  AddStateModelObj: AddStateModel = new AddStateModel();
  public griddata: any[];

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private EncrDecr: EncrDecrServiceService,
    private xAddStateServices: AddStateServices,
    private xViewUserServices: ViewUserServices
  ) {

    this.GetGridData();
  }

  ngOnInit() {}

  // common code
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    // this.ViewUserModelObj.Type = 4;
    // this.xViewUserServices
    //   .ViewUserData(this.ViewUserModelObj)
    //   .subscribe(response => {
    //     console.log("User Details", response);
    //     this.griddata = response[0];
    //   });
  }

  ClearData() {
    this.ViewStateModelObj = new ViewStateModel();
    //this.getautoUserdata();
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    // debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/state/addstate"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.IPL_StateID);
    this.xRouter.navigate(["/state/addstate", btoa(encrypted)]);
  }
  //end
  // common code End

  
  
  // clear data
  AddNewState() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/state/addstate", 'new']);
  }



  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AddStateModelObj.IPL_StateID = dataItem.IPL_StateID;
      this.AddStateModelObj.IPL_IsDelete = true;

      this.xAddStateServices
        .StateDataPost(this.AddStateModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  //get grid
  GetGridData() {
    this.xViewUserServices
      .ViewStateData(this.ViewStateModelObj)
      .subscribe(response => {
        console.log("resp state", response);
        this.griddata = response[0];
      });
  }


  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

  }
}
