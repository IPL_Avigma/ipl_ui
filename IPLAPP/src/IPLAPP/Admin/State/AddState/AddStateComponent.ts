import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddStateModel} from "./AddStateModel";
import { AddStateServices } from "./AddStateServices";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ActivatedRoute } from '@angular/router';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ViewStateModel } from '../ViewState/ViewStateModel';
import { ViewUserServices } from '../ViewState/ViewStateServices';

@Component({
  templateUrl: "./AddState.html"
})
export class AddStateComponent implements OnInit {
  AddStateModelObj: AddStateModel = new AddStateModel();
  ViewStateModelObj: ViewStateModel = new ViewStateModel();

  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi


  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddStateServices: AddStateServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xViewUserServices: ViewUserServices
  ) {

  }

  testExtArray:any;
  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      StateName: ["", Validators.required],
      disstatea: ["", Validators.nullValidator],
    });
    this.getModelData();

    this.testExtArray = {
      SecureOnArrival: "No",
      SecureReason: {
        MissingDoors: true,
        DoorOpen: true,
        MissingLocks: true,
        WindowsOpenMortgagor: true,
        BrokenWindows: true,
        BrokenDoor: true,
        MissingWindows: true,
        BidsPendingProperty: true,
        DamagedLocks: true
      },
      Propertyboadedonarrival: "Yes",
      Moreboardingstillrequired: "More boarding still required",
      FirstFloorWindows: "34567654",
      PropertyIsNotSecureReason: {
        Missing_Door: false,
        Door_Open: true,
        Missing_Locks: false,
        Broken_Windows: false,
        Broken_Door: true,
        Missing_Windows: false,
        Bids_Pending_Property: true,
        Damaged_Locks: true
      }
    };
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;




    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    
    if(this.WorkOrderObj !== undefined) {
      
      this.AddStateModelObj.IPL_StateID = this.WorkOrderObj;
    
    } else {

      this.AddStateModelObj.IPL_StateID = 0;
    }
    
    this.xAddStateServices
      .StateDataPost(this.AddStateModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddStateModelObj.IPL_StateID = parseInt(response[0].IPL_StateID);

          this.MessageFlag = "State Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  WorkOrderObj: any;
  IsEditDisable = false;
  getModelData() {
    debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddStateModelObj = new AddStateModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Cust_Num_pkeyId', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
    }

    // if(this.WorkOrderObj == 0 || this.WorkOrderObj == undefined) {
    //   this.AddStateModelObj = new AddStateModel();
    // } else {
    //   this.GetSingleData();
    // }
  }


  //   if (this.ModelObj == undefined) {
  //     this.AddStateModelObj = new AddStateModel();
  //   } 
  //     else 
  //   {

  //     // this.AddStateModelObj.IPL_StateID = this.ModelObj;
  //     this.AddStateModelObj.IPL_StateName = this.ModelObj.IPL_StateName;
  //     this.AddStateModelObj.IPL_State_IsActive = this.ModelObj.IPL_State_IsActive;
  //     this.AddStateModelObj.IPL_StateID = this.ModelObj;

  //     this.formUsrCommonGroup.disable();
  //     this.IsEditDisable = true;

  //     this.button = "Update";
  //     this.AddStateModelObj.Type = 2;

  //   }
  // }
  // end code
  
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }

  GetSingleData() {
    debugger;
    this.ViewStateModelObj.IPL_StateID = this.WorkOrderObj;
    this.ViewStateModelObj.Type = 2;
    this.xViewUserServices.ViewStateData(this.ViewStateModelObj)
    .subscribe(response => {
      debugger;
      console.log("GetSingleData", response);

      this.AddStateModelObj.IPL_StateName =
        response[0][0].IPL_StateName;
      this.AddStateModelObj.IPL_State_IsActive = 
        response[0][0].IPL_State_IsActive;
        
        this.formUsrCommonGroup.disable();

        this.IsEditDisable = true;
        this.button = "Update";
        // this.ViewStateModelObj.Type = 2;
    });
  }


  // end code




}
