import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddStateComponent } from './AddStateComponent';
import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

export const AddStateRouts = [
  { path: "addstate/:id", component: AddStateComponent }
];

@NgModule({
  declarations: [AddStateComponent],
  imports: [
    RouterModule.forChild(AddStateRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    CommonDirectiveModule



  ],
  providers: [],
  bootstrap: [AddStateComponent]
})

export class AddStateModule {}
