import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddStateModel} from './AddStateModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import {HomepageServices} from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddStateServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostStateData";


  public StateDataPost(Modelobj:AddStateModel) {

    //debugger;
    let ANYDTO: any = {};
    ANYDTO.IPL_StateID = Modelobj.IPL_StateID;
    ANYDTO.IPL_StateName = Modelobj.IPL_StateName;
    ANYDTO.IPL_State_IsActive  = Modelobj.IPL_State_IsActive;
    ANYDTO.IPL_IsDelete  = Modelobj.IPL_IsDelete;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;
    if(Modelobj.IPL_StateID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.IPL_IsDelete){
      ANYDTO.Type = 4;
    }
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////////////////////////////////


  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert("Invalid User...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}

