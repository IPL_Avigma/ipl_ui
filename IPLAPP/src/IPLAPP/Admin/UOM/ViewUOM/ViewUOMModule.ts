import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ViewUOMComponent } from './ViewUOMComponent';



const ViewUOMComponentRouts = [
  { path: "viewuom", component: ViewUOMComponent }
];

@NgModule({
  declarations: [ViewUOMComponent],
  imports: [
    RouterModule.forChild(ViewUOMComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule,

  ],
  providers: [],
  bootstrap: [ViewUOMComponent]
})

export class ViewUOMModule {}
