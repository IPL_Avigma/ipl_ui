import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewUOMModel } from "./ViewUOMModel";
import { ViewUOMServices } from "./ViewUOMServices";
import { AddUOMServices } from "../AddUOM/AddUOMServices";

import { process, State } from "@progress/kendo-data-query";
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewUOM.html"
})
export class ViewUOMComponent implements OnInit {
  ViewUOMModelobj: ViewUOMModel = new ViewUOMModel();

  public griddata: any[];

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xViewUOMServices: ViewUOMServices,
    private xAddUOMServices: AddUOMServices,
    private EncrDecr: EncrDecrServiceService
  ) {
    this.GetGridData();
  }

  ngOnInit() {}

  // clear data
  AddNewUom() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/uom/adduom", 'new']);
  }

  // common code
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    // this.ViewUserModelObj.Type = 4;
    // this.xViewUserServices
    //   .ViewUserData(this.ViewUserModelObj)
    //   .subscribe(response => {
    //     console.log("User Details", response);
    //     this.griddata = response[0];
    //   });
  }

  //   ClearData() {
  //     this.ViewStateModelObj = new ViewStateModel();
  //     //this.getautoUserdata();
  //   }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/uom/adduom"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.UOM_pkeyId);
    this.xRouter.navigate(["/uom/adduom", btoa(encrypted)]);
  }
  //end
  // common code End
  
  
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.ViewUOMModelobj.UOM_pkeyId = dataItem.UOM_pkeyId;
      this.ViewUOMModelobj.UOM_IsDelete = true;

      this.xAddUOMServices
        .UOMDataPost(this.ViewUOMModelobj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  //get grid
  faltu: any;
  GetGridData() {
    this.xViewUOMServices
      .ViewUOMData(this.ViewUOMModelobj)
      .subscribe(response => {
        console.log("resp uom", response);
        this.griddata = response[0];
        this.faltu = response[0];
      });
  }





  //kendo check box event action

  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    // filter: {
    //   logic: "and",
    //   filters: [{ field: "UOM_Name", operator: "contains", value: "" }]
    // }
  };

  //public gridData: GridDataResult = process(this.griddata, this.state);

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

    //this.griddata = process(this.faltu, this.state);
  }
}
