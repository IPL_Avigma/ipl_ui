import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddUOMModel } from "./AddUOMModel";
import{AddUOMServices} from './AddUOMServices';

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ActivatedRoute } from '@angular/router';
import { ViewUOMServices } from '../ViewUOM/ViewUOMServices';
import { ViewUOMModel } from '../ViewUOM/ViewUOMModel';

@Component({
  templateUrl: "./AddUOM.html"
})

export class AddUomComponent implements OnInit {

    AddUOMModelobj: AddUOMModel = new AddUOMModel();
    ViewUOMModelobj: ViewUOMModel = new ViewUOMModel();

    submitted = false; // submitted
    button = "Save"; // buttom loading..
    isLoading = false; // buttom loading..
    formUsrCommonGroup: FormGroup;

    public contentx;
    MessageFlag: string;


constructor(
    private formBuilder: FormBuilder,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xAddUOMServices: AddUOMServices,
    private xmodalService: NgbModal,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xViewUOMServices: ViewUOMServices
){

}
ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
        UOMName: ["", Validators.required],
        disuom: ["", Validators.nullValidator],

      });
      this.getModelData();
}
get fx() {
    return this.formUsrCommonGroup.controls;
  }
 // submit form
 FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;




    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    
    if(this.WorkOrderObj !== undefined) {
      this.AddUOMModelobj.UOM_pkeyId = this.WorkOrderObj;
    } else {
      this.AddUOMModelobj.UOM_pkeyId = 0;
    }

    this.xAddUOMServices
      .UOMDataPost(this.AddUOMModelobj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddUOMModelobj.UOM_pkeyId = parseInt(response[0].UOM_pkeyId);

          this.MessageFlag = "UOM Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }




//get data
WorkOrderObj: any;
IsEditDisable = false;


getModelData() {
  debugger;
  // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

  const id1 = this.xRoute.snapshot.params['id'];
    
  if ( id1 == 'new' ) {
    
    this.AddUOMModelobj = new AddUOMModel();
    
  } else {

    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('UOM_pkeyId', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
   
  }
  
  // if (this.ModelObj == undefined) {
  //   this.AddUOMModelobj = new AddUOMModel();
  // } else {

  //   this.AddUOMModelobj.UOM_pkeyId = this.ModelObj.UOM_pkeyId;
  //   this.AddUOMModelobj.UOM_Name = this.ModelObj.UOM_Name;
  //   this.AddUOMModelobj.UOM_IsActive = this.ModelObj.UOM_IsActive;
  //   this.AddUOMModelobj.UserID = this.ModelObj.UserID;

  // }

}


  GetSingleData() {
    
    this.AddUOMModelobj.UOM_pkeyId = this.WorkOrderObj;
    this.AddUOMModelobj.Type = 2;
    
    this.xViewUOMServices.ViewUOMData(this.AddUOMModelobj)
    .subscribe( response => {
      debugger;
      console.log('GetSinlgeData', response);

      this.AddUOMModelobj.UOM_Name = 
      response[0][0].UOM_Name;

      this.AddUOMModelobj.UOM_IsActive = 
      response[0][0].UOM_IsActive;
    });
    
    this.formUsrCommonGroup.disable();
    this.IsEditDisable = true;

    this.button = "Update";
    // this.AddUOMModelobj.Type = 2;
  }

  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }


}
