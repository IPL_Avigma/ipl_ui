import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddUomComponent } from "./AddUOMComponent";

import { AddUOMServices } from "./AddUOMServices";


const AddUOMComponentRouts = [
  { path: "adduom/:id", component: AddUomComponent }
];
@NgModule({
  declarations: [AddUomComponent],
  imports: [
    RouterModule.forChild(AddUOMComponentRouts),
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [AddUOMServices],
  bootstrap: [AddUomComponent]
})

export class AddUOMModule {}
