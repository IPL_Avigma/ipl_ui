import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule } from '@progress/kendo-angular-grid';
import { ReportsComponent } from './ReportsDetailsComponent';
// import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

export const ReportsRouts = [
  { path: "reportsdetails", component: ReportsComponent }
];

@NgModule({
  declarations: [ReportsComponent],
  imports: [
    RouterModule.forChild(ReportsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule
    



  ],
  providers: [],
  bootstrap: [ReportsComponent]
})

export class ReportsModule {}
