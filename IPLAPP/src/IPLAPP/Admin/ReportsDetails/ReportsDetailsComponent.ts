import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {process, State } from "@progress/kendo-data-query";
import {DataStateChangeEvent} from "@progress/kendo-angular-grid";
import { ReportsModel} from "./ReportsDetailsModel";
import { ReportsServices } from "./ReportsDetailsService";
import { ThemeService } from 'ng2-charts';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { Router } from '@angular/router';



@Component({
  templateUrl: "./ReportsDetails.html"
})
export class ReportsComponent implements OnInit {
  reports:any;
   datalist:any; 
  formUsrCommonGroup: FormGroup;
  ReportsModelObj : ReportsModel = new ReportsModel();
   ReportColumns = [
    {
      field: 'Inv_Client_Invoice_Number',
      title: 'Invoice Number',
      width: '180'
    },
    {
      field: 'Inv_Client_CreatedOn',
      title: 'Invoice Date',
      width: '180'
    },
    
    {
      field: 'IPLNO',
      title: 'IPLNO',
      width: '180'
    },
    {
      field: 'ContractorName',
      title: 'Contractor',
      width: '180'
    },
    {
      field: 'address1',
      title: 'Address',
      width: '180'
    },
    {
      field: 'city',
      title: 'City',
      width: '180'
    },
    {
      field: 'IPL_StateName',
      title: 'State',
      width: '180'
    },
    {
      field: 'zip',
      title: 'Zip',
      width: '180'
    },
    {
      field: 'Loan_Number',
      title: 'Loan Number',
      width: '180'
    },
    {
      field: 'workOrderNumber',
      title: 'WO Number',
      width: '180'
    },
    {
      field: 'Client_Company_Name',
      title: 'Company Name',
      width: '120'
    },
    {
      field: 'WT_WorkType',
      title: 'Work Type',
      width: '180'
    },
    {
      field: 'dueDate',
      title: 'Due Date',
      width: '180'
    },
    {
      field: 'clientDueDate',
      title: 'client Due Date',
      width: '180'
    },
    {
      field: 'Inv_Client_Client_Total',
      title: 'Client Total',
      width: '180'
    },
    {
      field: 'Inv_Client_Client_Dis',
      title: 'Client Discount',
      width: '180'
    },
   
    
    {
      field: 'Inv_Client_Discout_Amount',
      title: 'Client Dis Amount',
      width: '180'
    },
    {
      field: 'Sent_to_Client',
      title: 'Sent to Client',
      width: '180'
    },
    {
      field: 'CordinatorName',
      title: 'Cordinator',
      width: '180'
    },
   
    {
      field: 'ProcessorName',
      title: 'Processor',
      width: '180'
    },
   
  ];
  constructor( private formBuilder: FormBuilder, private xmodalService: NgbModal,
    private xReportsServices: ReportsServices,private EncrDecr: EncrDecrServiceService,
    private xRouter: Router,) {
     
  }
 
 
  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
    
    });
   // this.getGridData();
  }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  groupList = [{ id:'ContractorName' , name: "Contractor" },{ id: 'ProcessorName', name: "Processor" },{ id: 'CordinatorName', name: "Coordinator" }];

  public aggregates: any[] = [{ field: 'Inv_Client_Client_Total', aggregate: 'sum' }];

    public state: State = {
        skip: 0,
        take: 15,
         group: [{ field: 'ContractorName', aggregates: this.aggregates }]
    };
   
    // getGridData(){
    //   debugger
    //   this.xReportsServices
    //   .GetReportDetail(this.ReportsModelObj)
    //   .subscribe(res =>{
    //     console.log('Report Responce',res)
        
    //     this.reports = res[0];
    //     this.griddetails()
    //   })
    // }

 
    
    gridData:any;
griddetails(){
  this.gridData = process(this.reports, this.state);
}


     dataStateChange(state: DataStateChangeEvent): void {
       debugger;
      if (state && state.group) {
        state.group.map(group => group.aggregates = this.aggregates);
      }

      this.state = state;

      
    }


headline: any;
  FormButton(content) {
   
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    // if((this.ReportsModelObj.InvoiceDateFrom == null || this.ReportsModelObj.InvoiceDateTo ==null)){
    //   this.error='Start date and end date are required.'
    
    //   this.isValidDate = false;
    // }

    // if((this.ReportsModelObj.InvoiceDateFrom != null && this.ReportsModelObj.InvoiceDateTo !=null) 
    // && (this.ReportsModelObj.InvoiceDateTo) < (this.ReportsModelObj.InvoiceDateFrom)){
    //   this.error='End date should be grater then start date.'
    //   this.isValidDate = false;
    // }
   


   
   if (this.ReportsModelObj.valFlag === "Client Invoice") {
    this.ReportsModelObj.Valtype = 1;
    this.headline = 'Client Invoice Report Details';
   }
   if (this.ReportsModelObj.valcon === "Contractor Invoice") {
    this.ReportsModelObj.Valtype = 2;
    this.headline = 'Contractor Invoice Report Details';
   }
    this.xReportsServices
    .GetReportDetail(this.ReportsModelObj)
    .subscribe(res =>{
      console.log('Report Responce',res)
      this.reports = res[0];
     
      this.state.group.push({ field:this.changedata, aggregates: this.aggregates })
      this.gridData = process(this.reports, this.state);
     
    })
  }
  changedata:any;
  GroupByChangeEvent(val)
  {
    this.changedata = val;
                                                                                                
  }

  showDetails(event, dataItem) {

    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.workOrder_ID);
    this.xRouter.navigate(["/client/clientresultinvoice/" + btoa(encrypted)]);
  }
  isValidDate:boolean = true;
  error:any;
  err:any;
  validateDates(sDate: string, eDate: string){
  debugger;
   
  }

}
