import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {BasetUrl} from '../../Utility/DomainUrl';
import { HomepageServices } from '../../Home/HomeServices';
import { ReportsModel} from "./ReportsDetailsModel";

@Injectable({
  providedIn: "root"
})

export class ReportsServices{
    public token: any;

    constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
      this.token = JSON.parse(localStorage.getItem('TOKEN'));
    }
    private apiUrlPOST = BasetUrl.Domain +"api/Report/GetReportDetail";


    public GetReportDetail(Modelobj:ReportsModel) {
  
      debugger;
      var ANYDTO: any = {};
     var data = {
        InvoiceDateFrom: Modelobj.InvoiceDateFrom,
        InvoiceDateTo: Modelobj.InvoiceDateTo,
        ReadyOfficeDateFrom: Modelobj.ReadyOfficeDateFrom,
        ReadyOfficeDateTo: Modelobj.ReadyOfficeDateTo,
        SentToClientDateFrom: Modelobj.SentToClientDateFrom,
        SentToClientDateTo: Modelobj.SentToClientDateTo,
        Contractor: 1,
           }
      ANYDTO.whereClause = JSON.stringify(data);
      ANYDTO.Type = Modelobj.Type;
      ANYDTO.Valtype = Modelobj.Valtype;
  
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            return data;
          }),
          catchError(this.xHomepageServices.CommonhandleError)
        );
    }
}