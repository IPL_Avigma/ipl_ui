import { Component, Injectable, OnInit } from "@angular/core";

import { ViewCustomerServices } from "./ViewCustomerServices";
import { ViewCustomerModel } from "./ViewCustomerModel";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { Router } from "@angular/router";
import {AddCustomerService} from '../AddCustomer/AddCustomerServices';
import {AddCustomerModel} from '../AddCustomer/AddCustomerModel';


import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';


@Component({
  templateUrl: "./ViewCustomer.html"
})
export class ViewCustomerComponent implements OnInit {
  public griddata: any[];

  ViewCustomerModelObj: ViewCustomerModel = new ViewCustomerModel();
  AddCustomerModelObj:AddCustomerModel = new AddCustomerModel();
  constructor(
    private xViewCustomerServices: ViewCustomerServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter:Router,
    private EncrDecr: EncrDecrServiceService,
    private xAddCustomerService:AddCustomerService,

  ) {
    this.GetGridData();
  }

  ngOnInit() {}

  //get grid
  GetGridData() {
    this.xViewCustomerServices
      .ViewCustomerData(this.ViewCustomerModelObj)
      .subscribe(response => {
        console.log("cuatomer nu", response);
        this.griddata = response[0];
      });
  }

 
  FilterCall() {
   
    this.ViewCustomerModelObj.Type = 3;
    this.xViewCustomerServices
      .ViewCustomerData(this.ViewCustomerModelObj)
      .subscribe(response => {
        console.log("cuatomer nu", response);
        this.griddata = response[0];
      });
  }

  ClearData() {
    this.ViewCustomerModelObj = new ViewCustomerModel();
    //this.getautoUserdata();
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/customer/addcustomer"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Cust_Num_pkeyId);
    this.xRouter.navigate(["/customer/addcustomer", btoa(encrypted)]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AddCustomerModelObj.Cust_Num_pkeyId = dataItem.Cust_Num_pkeyId;
      this.AddCustomerModelObj.Cust_Num_IsDelete = true;

      this.xAddCustomerService
        .CustomerNumberDataPost(this.AddCustomerModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  // clear data
  AddNewCustomer() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/customer/addcustomer", 'new']);
  }



   //kendo check box event action
   public state: State = {};
   public dataStateChange(state: DataStateChangeEvent): void {
     this.state = state;

   }
}
