import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule } from '@progress/kendo-angular-grid';

import { ViewCustomerComponent } from './ViewCustomerComponent';
import {ViewCustomerServices} from './ViewCustomerServices';

 const ViewCustomerRouts = [
  { path: "viewcustomer", component: ViewCustomerComponent }
];
@NgModule({
  declarations: [ViewCustomerComponent],
  imports: [
    RouterModule.forChild(ViewCustomerRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    GridModule

  ],
  providers: [ViewCustomerServices],
  bootstrap: [ViewCustomerComponent]
})

export class ViewCustomerModule {}
