import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddCustomerComponent } from './AddCustomerComponent';
import {AddCustomerService} from './AddCustomerServices';

export const AddCustomerRouts = [
  { path: "addcustomer/:id", component: AddCustomerComponent }
];
@NgModule({
  declarations: [AddCustomerComponent],
  imports: [
    RouterModule.forChild(AddCustomerRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,

  ],
  providers: [AddCustomerService],
  bootstrap: [AddCustomerComponent]
})

export class AddCustomerModule {}
