
import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddCustomerModel } from "./AddCustomerModel";
import { AddCustomerService } from './AddCustomerServices';
import { ViewCustomerModel } from "../ViewCustomer/ViewCustomerModel";
import {ViewCustomerServices} from '../ViewCustomer/ViewCustomerServices';
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: "./AddCustomer.html"
})
export class AddCustomerComponent implements OnInit {
  AddCustomerModelObj: AddCustomerModel = new AddCustomerModel();
  ViewCustomerModelObj: ViewCustomerModel = new ViewCustomerModel();
  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    //private xAddStateServices: AddStateServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xAddCustomerService:AddCustomerService,
    private xViewCustomerService:ViewCustomerServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService
  ) {

  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      CustomerNUM: ["", Validators.required],
      discost: ["", Validators.nullValidator],

    });
    this.getModelData();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    if(this.WorkOrderObj !== undefined) {
      this.AddCustomerModelObj.Cust_Num_pkeyId = this.WorkOrderObj;
      // this.AddCustomerModelObj.Type = 2;
    } else {
      this.AddCustomerModelObj.Cust_Num_pkeyId = 0;
    }
    
    this.xAddCustomerService
      .CustomerNumberDataPost(this.AddCustomerModelObj)
      .subscribe(response => {
        debugger
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddCustomerModelObj.Cust_Num_pkeyId = parseInt(response[0].Cust_Num_pkeyId);

          this.MessageFlag = "Customer Number Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);

          // this.ViewCustomerModelObj.Cust_Num_pkeyId = this.AddCustomerModelObj.Cust_Num_pkeyId;
          // this.GetSingleData();
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  WorkOrderObj: any;
  IsEditDisable = false;
  getModelData() {
    //debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddCustomerModelObj = new AddCustomerModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Cust_Num_pkeyId', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
    }


    // if (this.ModelObj == undefined) {
    //   this.AddCustomerModelObj = new AddCustomerModel();
    // } else {

    //   this.AddCustomerModelObj.Cust_Num_pkeyId = this.ModelObj;
    //   this.AddCustomerModelObj.Cust_Num_Number = this.ModelObj.Cust_Num_Number;
    //   this.AddCustomerModelObj.Cust_Num_Active = this.ModelObj.Cust_Num_Active;
    //   this.AddCustomerModelObj.Cust_Num_IsActive = this.ModelObj.Cust_Num_IsActive;
      

      // this.formUsrCommonGroup.disable();
      // this.IsEditDisable = true;

      // this.button = "Update";
      // this.AddCustomerModelObj.Type = 2;

    // }
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }

  GetSingleData() {
    //debugger;
    this.ViewCustomerModelObj.Cust_Num_pkeyId = this.WorkOrderObj;
    this.ViewCustomerModelObj.Type = 2;
    this.xViewCustomerService.ViewCustomerData(this.ViewCustomerModelObj)
    .subscribe(response => {
      //debugger;
      console.log("Get Single Data", response);

      this.AddCustomerModelObj.Cust_Num_Number =
        response[0][0].Cust_Num_Number;
      this.AddCustomerModelObj.Cust_Num_Active =
        response[0][0].Cust_Num_Active;
        this.formUsrCommonGroup.disable();
        this.IsEditDisable = true;
  
        this.button = "Update";
      //  this.AddCustomerModelObj.Type = 2;
    });
  }
  // end code
}

