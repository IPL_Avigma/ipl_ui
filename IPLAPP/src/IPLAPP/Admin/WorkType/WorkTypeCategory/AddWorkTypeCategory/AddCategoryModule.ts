import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { AddCategoryComponent } from './AddCategoryComponent';

import {AddCategoryServices} from './AddCategoryServices';
import {WorkOrderDrodownServices} from '../../../../CommonServices/commonDropDown/DropdownService';

const AddCategoryRouts =[

  {path:'addcategory/:id',component:AddCategoryComponent}

]
@NgModule({
  declarations: [AddCategoryComponent],
  imports: [
    RouterModule.forChild(AddCategoryRouts),
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

  ],
  providers: [AddCategoryServices,WorkOrderDrodownServices],
  bootstrap: [AddCategoryComponent]
})

export class AddCategoryModule {}
