import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddCategoryModel} from './AddCategoryModel';
import {BasetUrl} from '../../../../Utility/DomainUrl';
import {CategoryPopupModel} from './AddCategoryModel';
import {WorkTypeAutoInvoice} from './AddCategoryModel';
import { HomepageServices } from '../../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddCategoryServices{

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
     this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data
  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostWorkTypeDetails";


  public WorkCategoryPost(Modelobj:AddCategoryModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    //debugger;
    var ANYDTO: any = {};
    ANYDTO.WT_pkeyID = Modelobj.WT_pkeyID;
    ANYDTO.WT_WorkType= Modelobj.WT_WorkType;
    ANYDTO.WT_CategoryID= Modelobj.WT_CategoryID;
    ANYDTO.WT_YardMaintenance= Modelobj.WT_YardMaintenance;
    ANYDTO.WT_Active= Modelobj.WT_Active;
    ANYDTO.WT_Always_recurring= Modelobj.WT_Always_recurring;
    ANYDTO.WT_Recurs_Every= Modelobj.WT_Recurs_Every;
    ANYDTO.WT_Recurs_WeekID= Modelobj.WT_Recurs_WeekID;
    ANYDTO.WT_Limit_to= Modelobj.WT_Limit_to;
    ANYDTO.WT_Cutoff_Date= Modelobj.WT_Cutoff_Date;
    ANYDTO.WT_WO_ItemID= Modelobj.WT_WO_ItemID;
    ANYDTO.WT_Contractor_AssignmentID= Modelobj.WT_Contractor_AssignmentID;
    ANYDTO.WT_Ready_for_FieldID= Modelobj.WT_Ready_for_FieldID;
    ANYDTO.WT_IsInspection= Modelobj.WT_IsInspection;
    ANYDTO.WT_AutoInvoice= Modelobj.WT_AutoInvoice;
    ANYDTO.WT_IsActive= Modelobj.WT_IsActive;

    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.WT_assign_upon_comple = Modelobj.WT_assign_upon_comple;
    ANYDTO.Type = 1;
    if(Modelobj.WT_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.WT_IsDelete){
      ANYDTO.Type = 4;
    }


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  /////////////////////////////////////////////////////////////////

  private apiUrlPOST3 = BasetUrl.Domain +"api/RESTIPL/PostWorkTypeInvDetails";


  public WorkCategoryMasterInvoicePost(Modelobj3:WorkTypeAutoInvoice) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    //debugger;
    var ANYDTO: any = {};

    ANYDTO.Auto_Inv_Itm_PkeyID = Modelobj3.Auto_Inv_Itm_PkeyID;
    ANYDTO.Auto_Inv_Itm_StringArr = Modelobj3.Auto_Inv_Itm_StringArr;
    ANYDTO.Auto_Inv_Itm_WorkTypeID = Modelobj3.Auto_Inv_Itm_WorkTypeID;
    ANYDTO.UserID = Modelobj3.UserID;
    ANYDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST3, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }


  ////////////////////////////////////////////////////////////////
  // post pop category
  private apiUrlPOST1 = BasetUrl.Domain +"api/RESTIPL/PostWorkTypecatDetails";


  public WorkCategoryPOPUPPost(Modelobj1:CategoryPopupModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    //debugger;
    var ANYDTO: any = {};
    ANYDTO.Work_Type_Cat_pkeyID = Modelobj1.Work_Type_Cat_pkeyID;
    ANYDTO.Work_Type_Name = Modelobj1.Work_Type_Name;
    ANYDTO.Work_Type_NameArr = Modelobj1.Work_Type_NameArr;
    ANYDTO.UserID = Modelobj1.UserID;
    ANYDTO.Type = Modelobj1.Type;
    ANYDTO.Work_Type_IsActive = true;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST1, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  //delete work type cat
  private apiUrldel = BasetUrl.Domain +"api/RESTIPL/DeleteWorkTypecatDetails";


  public DeleteWorkCategoryPOPUP(Modelobj1:CategoryPopupModel) {
   
    //debugger;
    var ANYDTO: any = {};
    ANYDTO.Work_Type_Cat_pkeyID = Modelobj1.Work_Type_Cat_pkeyID;
    ANYDTO.Type = Modelobj1.Type;
    ANYDTO.Work_Type_IsActive = Modelobj1.Work_Type_IsActive;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrldel, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert('Invalid Request...');
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}

