import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";

import { AddCategoryModel } from "./AddCategoryModel";
import { CategoryPopupModel } from "./AddCategoryModel";
import { AddCategoryServices } from "./AddCategoryServices";
import { WorkTypeAutoInvoice } from "./AddCategoryModel";

import { WorkOrderDrodownServices } from "../../../../CommonServices/commonDropDown/DropdownService";
import { MasterlayoutComponent } from "../../../../Home/MasterComponent";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ActivatedRoute } from '@angular/router';
import { ViewWorkTypeCategoryModel } from '../ViewWorkTypeCategory/ViewWorkTypeCategoryModel';
import { ViewWorkTypeCategoryServices } from '../ViewWorkTypeCategory/ViewWorkTypeCategoryServices';

@Component({
  templateUrl: "./AddCategory.html"
})
export class AddCategoryComponent implements OnInit {
  RcuringFlag = false;
  InvoiceFlag = false;
  WKDivFlag = true;
  FormArrayVal = [];
  YESNOList: any;
  CategoryList: any;
  DaysList: any;
  WOItemList: any;
  ContractorAssignmentList: any;
  ReadyList: any;
  SelectName: any;
  InvoiceLineItem: any;

  submitted = false; // submitted;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  // only drop down
  dropCkck = false; // common
  ActiveValFlag = false;

  formUsrCommonGroup: FormGroup; // create obj

  AddCategoryModelObj: AddCategoryModel = new AddCategoryModel();
  ViewWorkTypeCategoryModelObj: ViewWorkTypeCategoryModel = new ViewWorkTypeCategoryModel();
  CategoryPopupModelObj: CategoryPopupModel = new CategoryPopupModel();
  WorkTypeAutoInvoiceObj: WorkTypeAutoInvoice = new WorkTypeAutoInvoice();

  constructor(
    private xmodalService: NgbModal,
    private formBuilder: FormBuilder,
    private xAddCategoryServices: AddCategoryServices,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xViewWorkTypeCategoryServices: ViewWorkTypeCategoryServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService
  ) {
    this.FormArrayVal = [{ Name: "0", LineItem: "0", Quntity: 0 }];
    this.popformArray = [{ Name: "" }];
    this.YESNOList = [{ Id: 1, Name: "YES" }, { Id: 2, Name: "NO" }];
    this.SelectName = [
      { Id: 1, Name: "Contractor" },
      { Id: 2, Name: "Client" }
    ];

    this.DaysList = [
      { Id: 1, Name: "Day(s)" },
      { Id: 2, Name: "Week(s)" },
      { Id: 1, Name: "Month(s)" }
    ];
    this.WOItemList = [
      { Id: 1, Name: "None" },
      { Id: 2, Name: "Use Auto Work Details" },
      { Id: 3, Name: "Use Previous Order" }
    ];
    this.ContractorAssignmentList = [
      { Id: 1, Name: "None" },
      { Id: 2, Name: "Use Admin Rules" },
      { Id: 3, Name: "Use Previous Order" }
    ];
    this.ReadyList = [
      { Id: 1, Name: "Assigned" },
      { Id: 2, Name: "Sent To Field" }
    ];

    this.InvoiceLineItemDropDown();
    this.GetWorkTypeGroup();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      WorkTypeVal: ["", Validators.required],

      disbledFaltu1:["",Validators.nullValidator],
      disbledFaltu2:["",Validators.nullValidator],
    });

    this.getModelData();
  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  //
  // main div hide n show
  WKHIDESHOW() {
    this.WKDivFlag = !this.WKDivFlag;
  }

  RecurringDiv(condition) {
    //debugger;
    if (condition.checked) {
      this.RcuringFlag = true;
    } else {
      this.RcuringFlag = false;
    }
  }
  InvoicegDiv(condi) {
    if (condi.checked) {
      this.InvoiceFlag = true;
    } else {
      this.InvoiceFlag = false;
    }
  }

  // Insert New Row
  AddMoreRow() {
    var data = { Name: 0, LineItem: 0, Quntity: null };
    this.FormArrayVal.push(data);
  }

  // remove row
  RemoveRow(index) {
    if (index != 0) {
      this.FormArrayVal.splice(index, 1);
    }
  }

  open(content) {
    this.WKDivFlag = false;
    this.xmodalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          this.WKDivFlag = true;
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.WKDivFlag = true;
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  // pop pop add
  popformArray = [];
  AddMoreRowCatepop() {
    debugger;
    var data = { Work_Type_Name: "", Work_Type_Cat_pkeyID: 0 };
    if (this.popformArray.length != 0) {
      if (
        this.popformArray[this.popformArray.length - 1].Work_Type_Name != ""
      ) {
        this.popformArray.push(data);
        console.log('1', this.popformArray)
      }
    } else {
      this.popformArray.push(data);
      console.log('2', this.popformArray)
    }
  }
  // popup remove
  RemovePOPdata(item,inx) {

 console.log('index :',inx);
    if (item.Work_Type_Cat_pkeyID != 0) {
      var cfrm = confirm("Are you want to delete this record...!");
      if (cfrm == true) {
        this.CategoryPopupModelObj.Work_Type_Cat_pkeyID = item.Work_Type_Cat_pkeyID;
        this.CategoryPopupModelObj.Work_Type_IsActive = false;
        this.CategoryPopupModelObj.Type = 3;
        this.xAddCategoryServices
        .DeleteWorkCategoryPOPUP(this.CategoryPopupModelObj)
        .subscribe(response => {
          console.log('delete manage :',response);
          this.GetWorkTypeGroup();
        })
      
      }
    
    }
    else
    {
      this.popformArray.splice(inx, 1);
      console.log('3', this.popformArray)
    }
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;
    // only drop down
    this.dropCkck = false;
    //alert(JSON.stringify(this.AddCategoryModelObj));

    // if (this.AddCategoryModelObj.WT_IsActive == false) {
    //   this.ActiveValFlag = true;
    //   this.dropCkck = true;
    // }
    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }
    this.isLoading = true;
    this.button = "Processing";
    
    if(this.WorkOrderObj !== undefined) {
      this.AddCategoryModelObj.WT_pkeyID = this.WorkOrderObj;
    } else {
      this.AddCategoryModelObj.WT_pkeyID = 0;
    }

    this.xAddCategoryServices
      .WorkCategoryPost(this.AddCategoryModelObj)
      .subscribe(response => {
        //debugger;
        console.log("resp main pkey data", response);

        if (response[0].Status == "1") {
          var WTPkey = response[0].WT_pkeyID;

          this.AddCategoryModelObj.WT_pkeyID = parseInt(response[0].WT_pkeyID);

          //this.AutoInvoice(WTPkey);

          this.MessageFlag = "Work Type Data Saved...!";
          this.isLoading = false;
          this.button = "update ";
          this.commonMessage(this.contentx);
        } else {
          this.MessageFlag =
            "something went wrongs...Please contact your system admin";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn end code
  // drop down valid or not
  Active_Method() {
    //alert('select');
    this.ActiveValFlag = false;
  }

  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  /// end common model

  // pop form
  FormButtonPOPUp(content) {
    this.contentx = content;
    //debugger;
    this.popformArray;
    if (this.popformArray[this.popformArray.length - 1].Work_Type_Name == "") {
      this.popformArray.splice(this.popformArray.length - 1, 1);
    }
    this.CategoryPopupModelObj.Work_Type_NameArr = this.popformArray;

    this.xAddCategoryServices
      .WorkCategoryPOPUPPost(this.CategoryPopupModelObj)
      .subscribe(response => {
        //debugger;
        console.log("resp peky", response);
        this.CategoryList = response[0];
        this.popformArray = response[0];

        this.MessageFlag = "Work Type Groups Saved...!";
        this.commonMessage(this.contentx);
      });
  }
  // pop up end button code

  AutoInvoice(PkeyId) {
    //debugger;

    this.WorkTypeAutoInvoiceObj.Auto_Inv_Itm_StringArr = this.FormArrayVal;
    this.WorkTypeAutoInvoiceObj.Auto_Inv_Itm_WorkTypeID = PkeyId;

    this.xAddCategoryServices
      .WorkCategoryMasterInvoicePost(this.WorkTypeAutoInvoiceObj)
      .subscribe(response => {
        console.log("peky inv", response);
      });
  }

  // drop down get
  InvoiceLineItemDropDown() {
    this.xWorkOrderDrodownServices
      .GetInvoiceitemDropdownGet()
      .subscribe(response => {
        console.log("drop data item ", response);
        this.InvoiceLineItem = response[0];
      });
  }
  //end code

  WorkOrderObj: any;
  IsEditDisable = false;
  
  
  getModelData() {
    debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddCategoryModelObj = new AddCategoryModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('WT_pkeyID', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
    }


    // if (this.ModelObj == undefined) {
    //   this.AddCategoryModelObj = new AddCategoryModel();
    // } else {
    //   this.WKDivFlag = true;
    //   this.AddCategoryModelObj.Type = this.ModelObj.Type;
    //   this.AddCategoryModelObj.UserID = this.ModelObj.UserID;
    //   this.AddCategoryModelObj.WT_WorkType = this.ModelObj.WT_WorkType;
    //   this.AddCategoryModelObj.WT_IsInspection = this.ModelObj.WT_IsInspection;
    //   this.AddCategoryModelObj.WT_pkeyID = this.ModelObj.WT_pkeyID;
    //   this.AddCategoryModelObj.WT_Always_recurring = this.ModelObj.WT_Always_recurring;
    //   this.AddCategoryModelObj.WT_IsActive = this.ModelObj.WT_IsActive;
    //   this.AddCategoryModelObj.WT_CategoryID =this.ModelObj.WT_CategoryID;
    //   this.AddCategoryModelObj.WT_assign_upon_comple =this.ModelObj.WT_assign_upon_comple;

    // }
  }


  GetSingleData() {
    debugger;
    this.ViewWorkTypeCategoryModelObj.WT_pkeyID = this.WorkOrderObj;
    this.ViewWorkTypeCategoryModelObj.Type = 2;

    this.xViewWorkTypeCategoryServices.ViewWorkTypeData(this.ViewWorkTypeCategoryModelObj)
    .subscribe( response => {
      debugger;
      console.log("GetSingleData", response);
      
      this.AddCategoryModelObj.WT_WorkType = 
      response[0][0].WT_WorkType;

      this.AddCategoryModelObj.WT_CategoryID =
      response[0][0].WT_CategoryID;

      this.AddCategoryModelObj.WT_Active =
      response[0][0].WT_Active;
    });

    this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;

      this.button = "Update";
      // this.AddCategoryModelObj.Type = 2;
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code

  // get work type group
  GetWorkTypeGroup() {
    this.xWorkOrderDrodownServices
      .GetWorkTypecatDetailsDropdownGet()
      .subscribe(response => {
        console.log("resp WT Grup", response);
        this.CategoryList = response[0];
        this.popformArray = response[0];
      });
  }
}
