import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ViewWorkTypeCategoryModel } from "./ViewWorkTypeCategoryModel";
import { MasterlayoutComponent } from "../../../../Home/MasterComponent";
import {ViewWorkTypeCategoryServices} from './ViewWorkTypeCategoryServices';
import {AddCategoryServices} from '../AddWorkTypeCategory/AddCategoryServices';
import {AddCategoryModel} from '../AddWorkTypeCategory/AddCategoryModel';
import {WorkOrderDrodownServices} from '../../../../CommonServices/commonDropDown/DropdownService';


import { State } from "@progress/kendo-data-query";
import {DataStateChangeEvent} from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewWorkTypeCategory.html"
})
export class ViewWorkTypeCategoryComponent implements OnInit {
  ViewWorkTypeCategoryModelObj: ViewWorkTypeCategoryModel = new ViewWorkTypeCategoryModel();
  AddCategoryModelObj:AddCategoryModel = new AddCategoryModel();
  public griddata: any[];
  CategoryList: any;

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private EncrDecr: EncrDecrServiceService,
    private xViewWorkTypeCategoryServices:ViewWorkTypeCategoryServices,
    private xAddCategoryServices:AddCategoryServices,
    private xWorkOrderDrodownServices:WorkOrderDrodownServices
  ) {



    this.getWorktypedata();
    this.GetWorkTypeGroup();

  }

  ngOnInit() {}


  getWorktypedata(){
    this.xViewWorkTypeCategoryServices
      .ViewWorkTypeData(this.ViewWorkTypeCategoryModelObj)
      .subscribe(response => {
        console.log("work type g", response);
        this.griddata = response[0];
      });
  }


  // clear data
  AddNewWorkType() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/worktype/addcategory", 'new']);
  }

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/worktype/addcategory"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.WT_pkeyID);
    this.xRouter.navigate(["/worktype/addcategory", btoa(encrypted)]);
  }

  //
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    this.ViewWorkTypeCategoryModelObj.Type = 4;

    this.xViewWorkTypeCategoryServices
      .ViewWorkTypeData(this.ViewWorkTypeCategoryModelObj)
      .subscribe(response => {
        console.log("User Details", response);
        this.griddata = response[0];
      });
  }

  ClearData() {
    this.ViewWorkTypeCategoryModelObj = new ViewWorkTypeCategoryModel();
    this.getWorktypedata();
  }
  SaveFilterData() {
    alert("save called");
  }
  //
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {

      this.AddCategoryModelObj.WT_pkeyID = dataItem.WT_pkeyID;
      this.AddCategoryModelObj.WT_IsDelete = true;

      this.xAddCategoryServices.WorkCategoryPost(this.AddCategoryModelObj)
      .subscribe(response => {
        console.log('delete response',response);
        this.getWorktypedata();

      });



    }
  }
  //end code
    // get work type group
    GetWorkTypeGroup() {
      this.xWorkOrderDrodownServices
        .GetWorkTypecatDetailsDropdownGet()
        .subscribe(response => {
          console.log("resp WT Grup", response);
          this.CategoryList = response[0];

        });
    }





     //kendo check box event action

  public state: State = {};

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;


  }

}
