import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';


import { ViewWorkTypeCategoryComponent } from './ViewWorkTypeCategoryComponent';
import {ViewWorkTypeCategoryServices} from './ViewWorkTypeCategoryServices';

const ViewWorkTypeCategoryRouts = [
  { path: "viewworktypecate", component: ViewWorkTypeCategoryComponent }
];
@NgModule({
  declarations: [ViewWorkTypeCategoryComponent],
  imports: [
    RouterModule.forChild(ViewWorkTypeCategoryRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    GridModule,

  ],
  providers: [ViewWorkTypeCategoryServices],
  bootstrap: [ViewWorkTypeCategoryComponent]
})

export class ViewWorkTypeCategoryModule {}
