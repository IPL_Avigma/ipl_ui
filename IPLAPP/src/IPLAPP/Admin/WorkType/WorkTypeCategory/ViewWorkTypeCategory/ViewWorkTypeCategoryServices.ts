
import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { ViewWorkTypeCategoryModel } from "./ViewWorkTypeCategoryModel";
import { BasetUrl } from "../../../../Utility/DomainUrl";
import { HomepageServices } from "../../../../Home/HomeServices";



@Injectable({
  providedIn: "root"
})
export class ViewWorkTypeCategoryServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data

  private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetWorkTypeDetails";

  public ViewWorkTypeData(Modelobj: ViewWorkTypeCategoryModel) {

    debugger;
    var ANYDTO: any = {};

    ANYDTO.WT_pkeyID = Modelobj.WT_pkeyID;

    Modelobj.MenuID = 1;
    Modelobj.UserID = 1;

    var obj = {
      WT_WorkType: Modelobj.WT_WorkType,
      WT_CategoryID: Modelobj.WT_CategoryID,
      WT_Always_recurring: Modelobj.WT_Always_recurring,
      WT_IsInspection: Modelobj.WT_IsInspection,
      WT_IsActive: Modelobj.WT_IsActive

    };

    ANYDTO.SearchMaster = {
      UserID: Modelobj.UserID,
      MenuID: Modelobj.MenuID
    };
    ANYDTO.WhereClause = Modelobj.WhereClause = "";
    ANYDTO.FilterData = JSON.stringify(obj);
    ANYDTO.Type = Modelobj.Type;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
    } else {
      alert('Invalid Request...');
    }
    alert("Something bad happened. please try again later...😌");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}
