import {
  Component,
  Injectable,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddUserServices } from "./AddUserServices";
import { AddUserModel, WorkOrderCustomize } from "./AddUserModel";

import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewUserServices } from "../ViewUser/ViewUserServices";
import { ViewUserModel } from "../ViewUser/ViewUserModel";
import { CommonDocumentServices } from "../../../CommonServices/DocumentUploadServices/DocumentUploadServices";
import { CommonDocumentUploadModel } from "../../../CommonServices/DocumentUploadServices/DocumentUploadModels";
import { BasetUrl } from "../../../Utility/DomainUrl";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: "./AddUser.html"
})
export class AddUserComponent implements OnInit {
  AddUserModelObj: AddUserModel = new AddUserModel();
  WorkOrderCustomizeObj: WorkOrderCustomize = new WorkOrderCustomize();
  ViewUserModelObj: ViewUserModel = new ViewUserModel();
  CommonDocumentUploadModelObj: CommonDocumentUploadModel = new CommonDocumentUploadModel();
  formUsrCommonGroup: FormGroup;
  submitted = false; // submitted;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..


  UserNameReadOnly = false;

  public contentx;
  MessageFlag: string; // custom msg sathi

  RecordValList: any;
  GroupList: any;
  YESNOList: any;
  CarrierList: any;
  StateList: any;
  TimeList: any;
  StartDateList: any;
  DocumentTypeList: any;
  OrderDisplayList: any;
  PastOrderDisplayList:any;
  BackgroundCheckProviderList: any;

  FormArrayVal = [];

  // only drop down
  dropCkck = false; // common

  RecordValFlag = false;
  GroupValFlag = false;
  DisplayWOValFlag = false;
  ActiveValFlag = false;
  HistoryValFlag = false;
  TimeValFlag = false;
  StateValFlag = false;

  IsPasswordDisable = false;

  imgpath: string; //img sath
  docPath: string; // document sathi
  BaseURLpath: string;

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddUserServices: AddUserServices,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xViewUserServices: ViewUserServices,
    private xCommonDocumentServices: CommonDocumentServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService
  ) {
    this.GetDropDowndata();
    this.BaseURLpath = BasetUrl.Domain;

  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      FirstName: ["", Validators.required],
      LastName: ["", Validators.required],
      EmailVal: ["", [Validators.required, Validators.email]],
      LoginNameVal: ["", Validators.required],
      PasswordVal: ["", [Validators.required, Validators.minLength(6)]],
      GroupVal: ["", Validators.required],
      desaddress :["", Validators.nullValidator],
      descity:["", Validators.nullValidator],
      desstate:["", Validators.nullValidator],
      deszip:["", Validators.nullValidator],
      descomp:["", Validators.nullValidator],
      desphone:["", Validators.nullValidator],
      disZone:["", Validators.nullValidator],
      disdiscount:["", Validators.nullValidator],
      disassign:["", Validators.nullValidator],
      discordinator:["", Validators.nullValidator],
      disprocessor:["", Validators.nullValidator],
      discontractor:["", Validators.nullValidator],
      disCriteria:["", Validators.nullValidator],
      disorder:["", Validators.nullValidator],
      dispast:["", Validators.nullValidator],
      disuser:["", Validators.nullValidator],
      disProvider:["", Validators.nullValidator],
      disID:["", Validators.nullValidator],
      disdocname:["", Validators.nullValidator],
      disrecdate:["", Validators.nullValidator],
      disexcdate:["", Validators.nullValidator],
      disnotdate:["", Validators.nullValidator],
      disalt:["", Validators.nullValidator],
      disemail:["", Validators.nullValidator],
      disCancelled:["", Validators.nullValidator],
      dismas:["", Validators.nullValidator],
      disfieldc:["", Validators.nullValidator],
      disDigest:["", Validators.nullValidator],
      disWo:["", Validators.nullValidator],
      disWoc:["", Validators.nullValidator],
      disWom:["", Validators.nullValidator],
      disWof:["", Validators.nullValidator],
      discom:["", Validators.nullValidator],
      imageInput:["", Validators.nullValidator],
      priview:["", Validators.nullValidator],

    });

    //
    this.OrderDisplayList = [
      { Id: "1", Name: "Show all orders" },
      { Id: "2", Name: "Show only assigned orders" },
      { Id: "3", Name: "Show orders based on client" }
    ];
    this.PastOrderDisplayList = [
      { Id: "1", Name: "Show all past orders" },
      // { Id: "2", Name: "Show past orders based on address" },
      { Id: "2", Name: "Show past orders based on client" },
      { Id: "3", Name: "Show past order Previously assigned to user" }
    ];
    this.BackgroundCheckProviderList = [
      { Id: "1", Name: "Option1" },
      { Id: "2", Name: "Option2" }
    ];

    this.DocumentTypeList = [{ Id: 1, Name: "PDF" }, { Id: 2, Name: "DOCX" }];
    this.FormArrayVal = [
      {
        IPL_PkeyID: 0,
        IPL_Address: "",
        IPL_City: "",
        IPL_State: "",
        IPL_Primary_Zip_Code: null
      }
    ];
    this.StartDateList = [
      { Id: 1, Name: "6 Hours" },
      { Id: 2, Name: "12 Hours" },
      { Id: 3, Name: "24 Hours" }
    ];
    this.TimeList = [{ Id: 1, Name: "CENTRAL" }, { Id: 2, Name: "PACIFIC" }];
    //this.StateList = [{ Id: 1, Name: "AK" }, { Id: 2, Name: "AL" }];
    this.CarrierList = [{ Id: 1, Name: "AT&T" }, { Id: 2, Name: "T-Mobile" }];
    this.YESNOList = [{ Id: 1, Name: "YES" }, { Id: 2, Name: "NO" }];

    // this setting for multiple drop down select values
    this.dropdownSettings = {
      singleSelection: false,
      idField: "Client_pkeyID",
      textField: "Client_Company_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 4,
      allowSearchFilter: true
    };
    ////////

    // becoz instance create nahi hot
    //this.getModelData();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;
    this.AddUserModelObj;

    ////
    // only drop down
    this.dropCkck = false;

    if (this.AddUserModelObj.User_Group == 0) {
      this.GroupValFlag = true;
      this.dropCkck = true;
    }

    if (this.dropCkck) {
      return;
    }

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    //alert(JSON.stringify(this.AddUserModelObj));
    
    if(this.ModelObj) {
      this.AddUserModelObj.User_pkeyID = this.ModelObj;
    }

    this.AddUserModelObj.StrAddressArray = this.FormArrayVal;

    this.AddUserModelObj.User_AssignClient = JSON.stringify(this.selectedItems);

    // set mutiple client here
    //this.MutipleNameSetFunProduct();

    // all valid data to save
    this.xAddUserServices
      .UsertDataPost(this.AddUserModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddUserModelObj.User_pkeyID = parseInt(response[0].User_pkeyID);
          this.CommonDocumentUploadModelObj.Common_pkeyID = this.AddUserModelObj.User_pkeyID;

          this.MessageFlag = "User Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
          // here call
          this.BackGroudIMGUpload();
        }
      });
  }
  // btn code end

  // drop down valid or not
  Record_Method() {
    //alert('select');
    this.RecordValFlag = false;
  }
  Group_Method() {
    this.GroupValFlag = false;
  }
  DisplayWOVal_Method() {
    this.DisplayWOValFlag = false;
  }
  Active_Method() {
    this.ActiveValFlag = false;
  }
  History_Method() {
    this.HistoryValFlag = false;
  }
  TimeVal_Method() {
    this.TimeValFlag = false;
  }
  State_Method() {
    this.StateValFlag = false;
  }

  /// this code for purpose details mutiple drop

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  onItemSelect(item: any) {
    console.log(item);
    console.log(JSON.stringify(this.selectedItems));
  }
  onSelectAll(items: any) {
    console.log(items);
    console.log(JSON.stringify(this.selectedItems));
  }

  /// end mutiple select

  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  /// end common model

  // clear form
  Clearx() {
    this.AddUserModelObj.User_FirstName = "";
    this.AddUserModelObj.User_LastName = "";
    this.AddUserModelObj.User_Leg_FirstName = "";
    this.AddUserModelObj.User_Leg_LastName = "";
  }
  // clear end

  // Insert New Row
  AddMoreRow() {
    var data = {
      IPL_PkeyID: 0,
      IPL_Address: "",
      IPL_City: "",
      IPL_State: "",
      IPL_Primary_Zip_Code: null
    };
    this.FormArrayVal.push(data);
  }

  // remove row
  RemoveRow(index) {
    if (index != 0) {
      this.FormArrayVal.splice(index, 1);
    }
  }
  //// end

  // zip code call
  onBlurZipCall() {
    //alert(this.AddUserModelObj.User_Zip);
  }
  // end

  clientList: any;
  GetDropDowndata() {
    this.xWorkOrderDrodownServices
      .DropdownGetGroupDetails()
      .subscribe(response => {
        console.log("GroupList", response);
        this.GroupList = response[0];

        // this.xWorkOrderDrodownServices.DropdownGetSystemOfRecordsData()
        // .subscribe(response =>{
        //   console.log(response);
        //   this.RecordValList = response[0];

        // });
      });

    //assign client get data
    this.xWorkOrderDrodownServices
      .DropdownGetClientnamesData()
      .subscribe(response => {
        console.log("client list", response);
        //this.clientList = response[0];
        this.dropdownList = response[0];

        this.GetStateDropDown();
      });
  }

  //end code

  ContactBtn = "Save Document";
  extensionval: any;
  finenameorginal: any;
  processImage(imageInput: any, content) {
    //debugger;
    this.contentx = content;
    const getnamefile = imageInput.files[0].name;
    this.finenameorginal = getnamefile;
    const extsn = getnamefile.split(".").pop();
    //alert(extsn);
    // here checking file extension
    if (extsn != "xlsx") {
      this.WorkOrderCustomizeObj.documentx = imageInput.files;
      this.extensionval = extsn;

      this.CommonDocumentUploadModelObj.documentx = imageInput.files;
      //this.PostUserDocumentMutiple();
    } else {
      this.MessageFlag = "Invalid File Format...!";
      this.commonMessage(this.contentx);
    }
  }
  // end code
  AddMoreDocument() {
    if (this.WorkOrderCustomizeObj.wo_Custo_DocType != "") {
      this.PostUserDocumentMutiple();
    }
  }

  multipledoc = [];
  multipledocPost = [];
  PostUserDocumentMutiple() {
    this.xCommonDocumentServices
      .CommonDocumentUpdate(this.CommonDocumentUploadModelObj)
      .subscribe(response => {
        console.log("response doc", response);
        var rspon = response.Message;
        var arr = [];
        arr = rspon.split("#");
        var msg = arr[0];
        if (arr[1]) {
          var data = {
            extensionval: this.extensionval,
            User_Doc_DocPath: BasetUrl.Domain + arr[1]
          };
          var datax = {
            extensionval: this.extensionval,
            User_Doc_DocPath: arr[1]
          };

          var documentarray = {
            User_Doc_pkeyID: 0,
            User_Doc_DocPath: arr[1],
            User_Doc_FileName: this.finenameorginal,
            User_Doc_ValType: this.WorkOrderCustomizeObj.wo_Custo_DocType,
            User_Doc_Exp_Date: this.WorkOrderCustomizeObj.wo_Custo_ExpDate,
            User_Doc_RecievedDate: this.WorkOrderCustomizeObj
              .wo_Custo_RecievedDate,
            User_Doc_NotificationDate: this.WorkOrderCustomizeObj
              .wo_Custo_NotificationDate,
            User_Doc_AlertUser: this.WorkOrderCustomizeObj.wo_Custo_AlertUser,
            User_Doc_UserID: 0,
            User_Doc_IsActive: true,
            User_Doc_IsDelete: false,
            UserID: 0,
            Type: 1
          };
          if (this.WorkOrderCustomizeObj.wo_Custo_DocType != "") {
            //only UI
            this.multipledoc.push(data);
            // post or
            this.multipledocPost.push(documentarray);
            this.ContactBtn = "Add More Document";
            this.AddUserModelObj.UserDocumentArray = this.multipledocPost;
          }

          this.finenameorginal = "";
          this.WorkOrderCustomizeObj.wo_Custo_DocType = "";
          this.WorkOrderCustomizeObj.wo_Custo_ExpDate = null;
          this.WorkOrderCustomizeObj.wo_Custo_RecievedDate = null;
          this.WorkOrderCustomizeObj.wo_Custo_NotificationDate = null;
          this.WorkOrderCustomizeObj.wo_Custo_AlertUser = false;
        }
        this.MessageFlag = msg;
        this.commonMessage(this.contentx);
      });
  }

  DeleteDocFile(indx,items) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.multipledoc.splice(indx, 1);
      this.multipledocPost.splice(indx, 1);

      this.CommonDocumentUploadModelObj.Common_pkeyID = items.User_Doc_pkeyID;
      //services call
      this.xCommonDocumentServices
      .DeleteUserDocumentMasterData(this.CommonDocumentUploadModelObj)
      .subscribe(response => {
        console.log("delete document respon ", response);

      });




    }
  }

  processImageBackground(imageInput: any, content) {
    //debugger;
    this.contentx = content;
    const getnamefile = imageInput.files[0].name;
    const extsn = getnamefile.split(".").pop();
    //alert(extsn);
    // here checking file extension
    if (extsn != "xlsx") {
      //this.WorkOrderCustomizeObj.documentx = imageInput.files;
      this.CommonDocumentUploadModelObj.documentx = imageInput.files;
    } else {
      this.MessageFlag = "Invalid File Format...!";
      this.commonMessage(this.contentx);
    }
  }
  // end

  BackGroudIMGUpload() {
    if (this.CommonDocumentUploadModelObj.documentx) {
      this.xCommonDocumentServices
        .ImageBackgroundUpdate(this.CommonDocumentUploadModelObj)
        .subscribe(response => {
          console.log("back Img", response);
          //debugger;
          var rspon = response.Message;
          var arr = [];
          arr = rspon.split("#");
          var msg = arr[0];
          console.log("document upload msg", msg);
          if (arr[1]) {
            this.imgpath = BasetUrl.Domain + arr[1];
          }
        });
    }
  }

  //end code

  ModelObj: any;
  IsEditDisable = false;

  getModelData() {
    //debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddUserModelObj = new AddUserModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('User_pkeyID', id);
    this.ModelObj = parseInt(id);
    }

    if (this.ModelObj == undefined) {
      this.AddUserModelObj = new AddUserModel();

      this.submitted = false; // submitted;
      this.button = "Save"; // buttom loading..
      this.isLoading = false; // buttom loading..

      // var xxx = "2019-08-17 06:30:00.000"; //2019-08-12T06:30:00
      // this.AddUserModelObj.AssociatedDevices = new Date(xxx);

    } else {
      console.log("grid to f", this.ModelObj);
      this.AddUserModelObj.Type = this.ModelObj.Type;
      this.AddUserModelObj.UserID = this.ModelObj.UserID;

      this.AddUserModelObj.User_pkeyID = this.ModelObj;

      this.ViewUserModelObj.User_pkeyID = this.ModelObj;

      this.GetsingleData();

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;
      this.IsPasswordDisable = true;

      this.button = "Update";
      this.AddUserModelObj.Type = 2;
      //this.GetDocumentFile();
      // get address multiple
    }
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code
  GetStateDropDown() {
    this.xWorkOrderDrodownServices.StateDropDownData().subscribe(response => {
      console.log("dowpstate", response);
      this.StateList = response[0];

      //sync call sathi
      this.getModelData();
    });
  }

  // get single data
  GetsingleData() {
    this.ViewUserModelObj.Single = true;

    if(this.ViewUserModelObj.User_pkeyID != 0){

      this.xViewUserServices
      .ViewUserData(this.ViewUserModelObj)
      .subscribe(response => {
        console.log("Get user single", response);

        if(response[0].length != 0){

          this.AddUserModelObj.User_pkeyID = response[0][0].User_pkeyID;
          this.AddUserModelObj.User_FirstName = response[0][0].User_FirstName;
          this.AddUserModelObj.User_CompanyName = response[0][0].User_CompanyName;
          this.AddUserModelObj.User_Email = response[0][0].User_Email;
          this.AddUserModelObj.User_CellNumber = response[0][0].User_CellNumber;
          this.AddUserModelObj.User_LoginName = response[0][0].User_LoginName;
          this.AddUserModelObj.User_Password = response[0][0].User_Password;
          this.AddUserModelObj.User_Tme_Zone = response[0][0].User_Tme_Zone;
          this.AddUserModelObj.User_Disc_percentage =
            response[0][0].User_Disc_percentage;
          this.AddUserModelObj.User_Auto_Assign = response[0][0].User_Auto_Assign;
          this.AddUserModelObj.User_Group = response[0][0].User_Group;
          this.AddUserModelObj.User_Cordinator = response[0][0].User_Cordinator;
          this.AddUserModelObj.User_Processor = response[0][0].User_Processor;
          this.AddUserModelObj.User_Contractor = response[0][0].User_Contractor;
          this.AddUserModelObj.User_OpenOrderDisCriteria =
            response[0][0].User_OpenOrderDisCriteria;
          this.AddUserModelObj.User_PastWorkOrder =
            response[0][0].User_PastWorkOrder;
          this.AddUserModelObj.User_PastOrderDisCriteria =
            response[0][0].User_PastOrderDisCriteria;
          this.AddUserModelObj.User_BackgroundCheckProvider =
            response[0][0].User_BackgroundCheckProvider;
          this.AddUserModelObj.User_BackgroundCheckId =
            response[0][0].User_BackgroundCheckId;
          this.AddUserModelObj.User_Email_New_Wo =
            response[0][0].User_Email_New_Wo;
          this.AddUserModelObj.User_Comments = response[0][0].User_Comments;
          this.AddUserModelObj.User_Alert_EmailReply =
            response[0][0].User_Alert_EmailReply;

          this.AddUserModelObj.User_Alert_EmailReply =
            response[0][0].User_Alert_EmailReply;
          this.AddUserModelObj.User_Alert_Ready_Office =
            response[0][0].User_Alert_Ready_Office;
          this.AddUserModelObj.User_Assi_Admin = response[0][0].User_Assi_Admin;
          this.AddUserModelObj.User_Emai_Reminders =
            response[0][0].User_Emai_Reminders;
          this.AddUserModelObj.User_Email_FollowUp =
            response[0][0].User_Email_FollowUp;
          this.AddUserModelObj.User_Email_Note = response[0][0].User_Email_Note;
          this.AddUserModelObj.User_Email_UnAssigned_Wo =
            response[0][0].User_Email_UnAssigned_Wo;
          this.AddUserModelObj.User_IsActive = response[0][0].User_IsActive;
          this.AddUserModelObj.User_IsDelete = response[0][0].User_IsDelete;
          this.AddUserModelObj.User_LastName = response[0][0].User_LastName;
          this.AddUserModelObj.User_Sys_Record = response[0][0].User_Sys_Record;
          this.AddUserModelObj.User_Text_New_Wo = response[0][0].User_Text_New_Wo;
          this.AddUserModelObj.User_Text_Note = response[0][0].User_Text_Note;
          this.AddUserModelObj.User_Text_Reminders =
            response[0][0].User_Text_Reminders;
          this.AddUserModelObj.User_Text_FollowUp =
            response[0][0].User_Text_FollowUp;
          this.AddUserModelObj.User_Text_UnAssigned_Wo =
            response[0][0].User_Text_UnAssigned_Wo;
          this.AddUserModelObj.User_Wo_History = response[0][0].User_Wo_History;

          this.AddUserModelObj.User_Text_Cancelled =
            response[0][0].User_Text_Cancelled;
          this.AddUserModelObj.User_Text_Field_Complete =
            response[0][0].User_Text_Field_Complete;

          this.AddUserModelObj.User_Text_New_Message =
            response[0][0].User_Text_New_Message;
          this.AddUserModelObj.User_Email_Cancelled =
            response[0][0].User_Email_Cancelled;
          this.AddUserModelObj.User_Email_Daily_Digest =
            response[0][0].User_Email_Daily_Digest;
          this.AddUserModelObj.User_Email_Field_Complete =
            response[0][0].User_Email_Field_Complete;
          this.AddUserModelObj.User_Email_New_Message =
            response[0][0].User_Email_New_Message;

          if (response[0][0].User_BackgroundDocPath != "") {
            this.imgpath =
              BasetUrl.Domain + response[0][0].User_BackgroundDocPath;
          }

          this.selectedItems = JSON.parse(response[0][0].User_AssignClient);

          this.FormArrayVal = response[1];

          this.CommonDocumentUploadModelObj.Common_pkeyID = this.AddUserModelObj.User_pkeyID;
          this.GetuserDocument();
        }


        this.UserNameReadOnly = true;

      });
    }

  }

  GetuserDocument() {
    this.xCommonDocumentServices
      .GetUserDocument(this.CommonDocumentUploadModelObj)
      .subscribe(response => {
        console.log("document respon s ", response);
        this.multipledocPost = response[0];
      });
  }

  // pass
  Passcode: any;
  generatePassword() {
    var length = 8,
      charset =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    //return retVal;
    this.Passcode = retVal;
  }

  ///

  onBlurUserName(lalala) {
    //alert(username);
    if(this.AddUserModelObj.User_pkeyID == 0){

      this.contentx = lalala;

      this.xAddUserServices.CheckUseName(this.AddUserModelObj)
      .subscribe(response =>{
        console.log('check response',response);
        if(response[0][0].UserCount != 0){
          this.AddUserModelObj.User_LoginName = "";


          this.MessageFlag = "UserName Already Existing..";
          this.commonMessage(this.contentx);
        }
      });

    }




  }
}
