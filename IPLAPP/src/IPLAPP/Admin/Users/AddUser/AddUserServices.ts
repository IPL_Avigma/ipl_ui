import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddUserModel} from './AddUserModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import { HomepageServices } from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddUserServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // post data
  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostUserData";


  public UsertDataPost(Modelobj:AddUserModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    debugger;
    var ANYDTO: any = {};
    ANYDTO.User_pkeyID  =   Modelobj.User_pkeyID;
    ANYDTO.Carrier  =    Modelobj.Carrier;
    ANYDTO.User_Comments  =    Modelobj.User_Comments;
    ANYDTO.User_Active = Modelobj.User_Active;
    ANYDTO.User_Alert_EmailReply  =    Modelobj.User_Alert_EmailReply;
    ANYDTO.User_Alert_Ready_Office  =    Modelobj.User_Alert_Ready_Office;
    ANYDTO.User_Assi_Admin  =    Modelobj.User_Assi_Admin;
    ANYDTO.User_Auto_Assign  =    Modelobj.User_Auto_Assign;
    ANYDTO.User_CompanyName  =   Modelobj.User_CompanyName;
    ANYDTO.User_Contractor  =    Modelobj.User_Contractor;
    ANYDTO.User_Disc_percentage  =    Modelobj.User_Disc_percentage;
    ANYDTO.User_Emai_Reminders  =   Modelobj.User_Emai_Reminders;
    ANYDTO.User_Email  =   Modelobj.User_Email;
    ANYDTO.User_Email_FollowUp  =  Modelobj.User_Email_FollowUp;
    ANYDTO.User_Email_New_Wo  =  Modelobj.User_Email_New_Wo;
    ANYDTO.User_Email_Note  =   Modelobj.User_Email_Note;
    ANYDTO.User_Email_UnAssigned_Wo  =   Modelobj.User_Email_UnAssigned_Wo;
    ANYDTO.User_FirstName  =   Modelobj.User_FirstName;
    ANYDTO.User_Group  =   Modelobj.User_Group;
    ANYDTO.User_LastName  =  Modelobj.User_LastName;
    ANYDTO.User_Leg_Address  =  Modelobj.User_Leg_Address;
    ANYDTO.User_Leg_Address1  =  Modelobj.User_Leg_Address1
    ANYDTO.User_Leg_City  =   Modelobj.User_Leg_City;
    ANYDTO.User_Leg_FirstName  =  Modelobj.User_Leg_FirstName;
    ANYDTO.User_Leg_LastName  =   Modelobj.User_Leg_LastName;
    ANYDTO.User_Leg_Notes  =   Modelobj.User_Leg_Notes;
    ANYDTO.User_Leg_State  =   Modelobj.User_Leg_State;
    ANYDTO.User_Misc_Contractor_Score  =   Modelobj.User_Misc_Contractor_Score;
    ANYDTO.User_Misc_Device_Id  =   Modelobj.User_Misc_Device_Id;
    ANYDTO.User_Misc_Insurance_Expire  =   Modelobj.User_Misc_Insurance_Expire;
    ANYDTO.User_Misc_Pruvan_Username  =   Modelobj.User_Misc_Pruvan_Username;
    ANYDTO.User_Misc_PushKey  =   Modelobj.User_Misc_PushKey;
    ANYDTO.User_Misc_Service_Id  =   Modelobj.User_Misc_Service_Id;
    ANYDTO.User_Misc_StartDate  =    Modelobj.User_Misc_StartDate;
    ANYDTO.User_Password  =    Modelobj.User_Password;
    ANYDTO.User_Sys_Record  =    Modelobj.User_Sys_Record;
    ANYDTO.User_Text_FollowUp  =   Modelobj.User_Text_FollowUp;
    ANYDTO.User_Text_New_Wo  =    Modelobj.User_Text_New_Wo;
    ANYDTO.User_Text_Note  =   Modelobj.User_Text_Note;
    ANYDTO.User_Text_Reminders  =   Modelobj.User_Text_Reminders;
    ANYDTO.User_Text_UnAssigned_Wo  =   Modelobj.User_Text_UnAssigned_Wo;
    ANYDTO.User_Tme_Zone  =   Modelobj.User_Tme_Zone;
    ANYDTO.User_Wo_History  =    Modelobj.User_Wo_History;
    ANYDTO.User_WorkOrder  =   Modelobj.User_WorkOrder;
    ANYDTO.User_Zip  =   Modelobj.User_Zip;
    ANYDTO.User_Leg_CellPhone  =   Modelobj.User_Leg_CellPhone;
    ANYDTO.User_CellNumber =  Modelobj.User_CellNumber;
    ANYDTO.User_LoginName = Modelobj.User_LoginName;

    ANYDTO.StrAddressArray  = Modelobj.StrAddressArray;

    ANYDTO.User_OpenOrderDisCriteria = Modelobj.User_OpenOrderDisCriteria;
    ANYDTO.User_PastWorkOrder = Modelobj.User_PastWorkOrder;
    ANYDTO.User_PastOrderDisCriteria = Modelobj.User_PastOrderDisCriteria;
    ANYDTO.User_BackgroundCheckProvider = Modelobj.User_BackgroundCheckProvider;
    ANYDTO.User_BackgroundCheckId = Modelobj.User_BackgroundCheckId;

    ANYDTO.User_SelectOrderDisCriteria = Modelobj.User_SelectOrderDisCriteria;

    ANYDTO.User_Processor  = Modelobj.User_Processor;
    ANYDTO.User_Cordinator  = Modelobj.User_Cordinator;


    ANYDTO.User_Email_Cancelled = Modelobj.User_Email_Cancelled;
    ANYDTO.User_Email_New_Message = Modelobj.User_Email_New_Message;
    ANYDTO.User_Email_Field_Complete = Modelobj.User_Email_Field_Complete;
    ANYDTO.User_Email_Daily_Digest = Modelobj.User_Email_Daily_Digest;

    ANYDTO.User_Text_Cancelled = Modelobj.User_Text_Cancelled;
    ANYDTO.User_Text_New_Message = Modelobj.User_Text_New_Message;
    ANYDTO.User_Text_Field_Complete = Modelobj.User_Text_Field_Complete;

    ANYDTO.User_AssignClient = Modelobj.User_AssignClient;

    ANYDTO.UserDocumentArray =  Modelobj.UserDocumentArray;



    ANYDTO.User_IsActive  = Modelobj.User_IsActive;
    ANYDTO.User_IsDelete  = Modelobj.User_IsDelete;
    ANYDTO.Type = 1;
    if(Modelobj.User_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.User_IsDelete){
      ANYDTO.Type = 4;
    }




    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }


  /////////////////////////////////////////////////////////
  // check user Name
   // post data
   private apiUrlPOST2 = BasetUrl.Domain +"api/RESTAuthentication/PostCheckUserNameData";


   public CheckUseName(Modelobj:AddUserModel) {
     ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
     //debugger;
     var ANYDTO: any = {};

     ANYDTO.User_LoginName = Modelobj.User_LoginName;

     let headers = new HttpHeaders({ "Content-Type": "application/json" });
     headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
     return this._http
       .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
       .pipe(
         tap(data => {

           return data;
         }),
         catchError(this.xHomepageServices.CommonhandleError)

       );
   }

   /////////////////////////////////////////////////////////////////


  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
    alert("Invalid Request...");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}

