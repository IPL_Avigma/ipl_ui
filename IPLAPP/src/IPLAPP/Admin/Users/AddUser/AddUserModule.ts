import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


import { AddUserComponent } from './AddUserComponent';
import {AddUserServices} from './AddUserServices';
import {ColorDirective} from '../../../AppDirectives/DirectiveColorChanges';
import {CommonDocumentServices} from '../../../CommonServices/DocumentUploadServices/DocumentUploadServices';


import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';
export const AdduserRouts =[

  {path:'adduser/:id',component:AddUserComponent}

]

@NgModule({
  declarations: [AddUserComponent,ColorDirective],
  imports: [
    RouterModule.forChild(AdduserRouts),
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    HttpClientModule,
    CommonDirectiveModule,
  ],

  providers: [AddUserServices,CommonDocumentServices],
  bootstrap: [AddUserComponent]
})

export class AddUserModule {}
