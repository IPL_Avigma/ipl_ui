import { Component, Injectable, OnInit } from "@angular/core";

import { Router } from "@angular/router";

import { ViewUserServices } from "./ViewUserServices";
import { ViewUserModel } from "./ViewUserModel";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";

import {AddUserModel} from '../AddUser/AddUserModel';
import {AddUserServices} from '../AddUser/AddUserServices';

import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";

import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewUser.html"
})
export class ViewUserComponent implements OnInit {
  ViewUserModelObj: ViewUserModel = new ViewUserModel();
  AddUserModelObj : AddUserModel = new AddUserModel();
  public griddata: any[];
  constructor(
    private xViewUserServices: ViewUserServices,private xAddUserServices:AddUserServices,private xWorkOrderDrodownServices:WorkOrderDrodownServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private EncrDecr: EncrDecrServiceService
  ) {
    //alert("hi");
    this.getautoUserdata();

    this.GetDropDowndata();
  }

  ngOnInit() {}

  getautoUserdata() {
    this.xViewUserServices
      .ViewUserData(this.ViewUserModelObj)
      .subscribe(response => {
        console.log("User Details", response);
        this.griddata = response[0];
      });
  }

  //
  // this code selected event row
  showDetails(event, dataItem) {
    // debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/user/adduser"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.User_pkeyID);
    this.xRouter.navigate(["/user/adduser", btoa(encrypted)]);
  }
  //end
  // clear data
  AddNewUser() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/user/adduser", 'new']);
  }

  // common code
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    this.ViewUserModelObj.Type = 4;

    this.xViewUserServices
      .ViewUserData(this.ViewUserModelObj)
      .subscribe(response => {
        console.log("User Details", response);
        this.griddata = response[0];
      });
  }

  ClearData() {
    this.ViewUserModelObj = new ViewUserModel();
    this.getautoUserdata();
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {

      this.AddUserModelObj.User_pkeyID = dataItem.User_pkeyID;
      this.AddUserModelObj.User_IsDelete = true;

      this.xAddUserServices.UsertDataPost(this.AddUserModelObj)
      .subscribe(response => {
        console.log('delete response',response);
        this.getautoUserdata();

      });



    }
  }
  //end code
  GroupList:any
  GetDropDowndata() {
    this.xWorkOrderDrodownServices
      .DropdownGetGroupDetails()
      .subscribe(response => {
        console.log('grop drop',response);
        this.GroupList = response[0];
      });

  }


  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

  }
}
