export class ViewUserModel {
  User_pkeyID: Number = 0;
  Type: Number = 3;
  User_FirstName: string = "";
  User_LastName: string = "";
  User_Address: string = "";
  User_City: string = "";
  User_State: string = "";
  User_Zip: string = "";
  User_CellNumber: string = "";
  User_CompanyName: string = "";
  User_LoginName: string = "";
  User_Email: string = "";
  User_Group: string = "";
  User_Misc_Contractor_Score: string = "";
  User_IsActive: string = "";
  MenuID: Number = 0;
  UserID: Number = 0;
  WhereClause: string = "";
  Single:boolean = false;
}
