import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';



import { ViewUserComponent } from './ViewUserComponent';
import {AddUserServices} from '../AddUser/AddUserServices';
import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";



export const ViewUserRouts =[

  {path:'viewduser',component:ViewUserComponent}

]


@NgModule({
  declarations: [ViewUserComponent],
  imports: [
    RouterModule.forChild(ViewUserRouts),
    CommonModule,
    GridModule,ExcelModule,
    FormsModule, ReactiveFormsModule,

  ],
  providers: [AddUserServices,WorkOrderDrodownServices],
  bootstrap: [ViewUserComponent]
})

export class UserViewModule {}
