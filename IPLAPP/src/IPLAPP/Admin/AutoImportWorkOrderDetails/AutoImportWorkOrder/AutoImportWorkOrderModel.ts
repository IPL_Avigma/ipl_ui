export class AutoImportWorkorderModel{
  
    WI_Pkey_ID :Number = 0;
    WI_ImportFrom :Number = 0;
    WI_SetClientCompany :Number = 0;
    WI_LoginName :string = '';
    WI_Password :string = '';
    WI_AlertEmail :string = '';
    WI_FriendlyName :string = '';
    WI_SkipComments :Number = 0;
    WI_SkipLineItems :Number = 0;
    WI_SetCategory :Number = 0;
    WI_StateFilter :Number = 0;
    WI_Discount_Import :Number = 0;
    WI_IsActive :Boolean = true;
    WI_IsDeleted :Boolean = false;
    Type :Number = 1;
    UserId :Number = 0;
    
}