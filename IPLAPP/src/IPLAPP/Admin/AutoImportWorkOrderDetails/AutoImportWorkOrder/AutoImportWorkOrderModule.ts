import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {AutoImportWorkOrderComponent} from "./AutoImportWorkOrderComponent"
import {AutoImportWorkOrderService} from "./AutoImportWorkOrderService"






const CompanyInfoRouts = [
  { path: "autoimportwo", component: AutoImportWorkOrderComponent }
];

@NgModule({
  declarations: [AutoImportWorkOrderComponent],
  imports: [
    RouterModule.forChild(CompanyInfoRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,NgbModule,
  

  ],
  providers: [AutoImportWorkOrderService],
  bootstrap: [AutoImportWorkOrderComponent]
})

export class AutoImportWorkOrderModule {}
