import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {AutoImportWorkorderModel} from './AutoImportWorkOrderModel';
import {AutoImportWorkOrderService} from './AutoImportWorkOrderService';
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import { from } from 'rxjs';



@Component({
  templateUrl: "./AutoImportWorkOrder.html"
})
export class AutoImportWorkOrderComponent implements OnInit {
 
    AutoImportWorkorderModelObj: AutoImportWorkorderModel = new AutoImportWorkorderModel();
    importvalFlag = false;// for dropdown
    importcatFlag = false;
    clientcompanyFlag = false;


    dropCkck = false;
    submitted = false; // submitted;
    formUsrCommonGroup: FormGroup;
    button = "Save"; // buttom loading..
    isLoading = false; // buttom loading..

    public contentx; // for common msg argument pass sathi
    MessageFlag: string; // custom msg sathi

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAutoImportWorkOrderService :AutoImportWorkOrderService,
    private xMasterlayoutComponent:MasterlayoutComponent,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices

  ) {
    this.GetCompanyDropDown();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
       Importval: ["", Validators.required],
        clientcompanyval: ["", Validators.required],
        LoginName: ["", Validators.required],
        Password: ["", Validators.required],
        activeatou: ["", Validators.required],
        alertemail: ["", Validators.nullValidator],
        friendlyname: ["", Validators.required],
        skipcomments: ["", Validators.nullValidator],
        skipline: ["", Validators.nullValidator],
        setcategory: ["", Validators.required],
        statefilter: ["", Validators.nullValidator],
        thisimport: ["", Validators.nullValidator],
       
      });
      this.getModelData();
  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;
  // only drop down
  this.dropCkck = false;


  if (this.AutoImportWorkorderModelObj.WI_ImportFrom == 0) {
    this.importvalFlag = true;
    
    //return;
    this.dropCkck = true;
  }
  if (this.AutoImportWorkorderModelObj.WI_SetClientCompany == 0) {
    this.importvalFlag = true;
    //return;
    this.dropCkck = true;
  }
  if (this.AutoImportWorkorderModelObj.WI_SetCategory == 0) {
    this.importvalFlag = true;
    //return;
    this.dropCkck = true;
  }
  

  if (this.dropCkck) {
    return;
  }

     // stop here if form is invalid
     if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    this.xAutoImportWorkOrderService
      .AutoImportWorkOrderPost(this.AutoImportWorkorderModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AutoImportWorkorderModelObj.WI_Pkey_ID = parseInt(response[0].WI_Pkey_ID);

          this.MessageFlag = "Auto ImportWorkOrder Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  //dropdown for work order import
  SetClientCompany: any;
  SetCategory: any;
  ImportFormdrd: any;

  GetCompanyDropDown() {
    this.xWorkOrderDrodownServices.DropdownGetWorkOrder().subscribe(response => {
      console.log("dowpcompany", response);
     this.SetClientCompany = response[0];
     this.SetCategory = response[2];
     this.ImportFormdrd = response[12];

    });
  }
  
  
  
  //ImportFormdrd = [{ id: 1, name: "A2Z Field" },{ id: 2, name: "A2Z Field Services" },{ id: 3, name: "Altisource" },{ id: 4, name: "Altisource - API" },{ id: 5, name: "Altisource - API v2" }];
  
  SkipComment = [{ id: 1, name: "Yes" },{ id: 2, name: "No" }];
  SkipLineItems = [{ id: 1, name: "Yes" },{ id: 2, name: "No" }];
  StateFilter = [{ id: 1, name: "Alabama" },{ id: 2, name: "Alaska" },{ id: 3, name: "Arizona" }];
 

  
  ModelObj: any;
  IsEditDisable = false;
  getModelData() {
    //debugger;
    this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    if (this.ModelObj == undefined) {
      this.AutoImportWorkorderModelObj = new AutoImportWorkorderModel();
    } else {

      this.AutoImportWorkorderModelObj.WI_Pkey_ID = this.ModelObj.UserID;
      this.AutoImportWorkorderModelObj.WI_ImportFrom = this.ModelObj.WI_ImportFrom;
      this.AutoImportWorkorderModelObj.WI_SetClientCompany = this.ModelObj.WI_SetClientCompany;
      this.AutoImportWorkorderModelObj.WI_LoginName = this.ModelObj.WI_LoginName;
      this.AutoImportWorkorderModelObj.WI_Password = this.ModelObj.WI_Password;
      this.AutoImportWorkorderModelObj.WI_AlertEmail = this.ModelObj.WI_AlertEmail;
      this.AutoImportWorkorderModelObj.WI_FriendlyName = this.ModelObj.WI_FriendlyName;
      this.AutoImportWorkorderModelObj.WI_SkipComments = this.ModelObj.WI_SkipComments;
      this.AutoImportWorkorderModelObj.WI_SkipLineItems = this.ModelObj.WI_SkipLineItems;
      this.AutoImportWorkorderModelObj.WI_SetCategory = this.ModelObj.WI_SetCategory;
      this.AutoImportWorkorderModelObj.WI_StateFilter = this.ModelObj.WI_StateFilter;
      this.AutoImportWorkorderModelObj.WI_Discount_Import = this.ModelObj.WI_Discount_Import;
      this.AutoImportWorkorderModelObj.WI_IsActive = this.ModelObj.WI_IsActive;
      this.AutoImportWorkorderModelObj.WI_Pkey_ID = this.ModelObj.WI_Pkey_ID;




      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;

      this.button = "Update";
      this.AutoImportWorkorderModelObj.Type = 2;
     
    }
  }
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  Import_form_Method(){
    this.importvalFlag = false;
  }
  clientcompany_Method(){
    this.importvalFlag = false;
  }
  clientcategory_Method(){
    this.importvalFlag = false;
  }

}