import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AutoImportWorkorderModel} from './AutoImportWorkOrderModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import {HomepageServices} from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AutoImportWorkOrderService {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostWorkOrderImportMasterDetails";


  public AutoImportWorkOrderPost(Modelobj:AutoImportWorkorderModel) {

    //debugger;
    let ANYDTO: any = {};
    ANYDTO.WI_Pkey_ID = Modelobj.WI_Pkey_ID;
    ANYDTO.WI_ImportFrom = Modelobj.WI_ImportFrom;
    ANYDTO.WI_SetClientCompany  = Modelobj.WI_SetClientCompany;
    ANYDTO.WI_LoginName  = Modelobj.WI_LoginName;
    ANYDTO.WI_Password  = Modelobj.WI_Password;
    ANYDTO.WI_AlertEmail  = Modelobj.WI_AlertEmail;
    ANYDTO.WI_FriendlyName  = Modelobj.WI_FriendlyName;
    ANYDTO.WI_SkipComments  = Modelobj.WI_SkipComments;
    ANYDTO.WI_SkipLineItems  = Modelobj.WI_SkipLineItems;
    ANYDTO.WI_SetCategory  = Modelobj.WI_SetCategory;
    ANYDTO.WI_StateFilter  = Modelobj.WI_StateFilter;
    ANYDTO.WI_Discount_Import  = Modelobj.WI_Discount_Import;
    ANYDTO.WI_IsActive  = Modelobj.WI_IsActive;
    ANYDTO.IPL_IsDelete  = Modelobj.WI_IsDeleted;
    ANYDTO.UserId  = Modelobj.UserId;
    ANYDTO.Type = 1;
    if(Modelobj.WI_Pkey_ID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.WI_IsDeleted){
      ANYDTO.Type = 4;
    }
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////////////////////////////////


  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401 ) {
      alert("Unauthorized User...");
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Invalid request...please try again later...");
  }
}

