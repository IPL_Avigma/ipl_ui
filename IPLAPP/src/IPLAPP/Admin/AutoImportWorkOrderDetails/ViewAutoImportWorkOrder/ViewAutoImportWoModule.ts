import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {ViewAutoImportWoComponent  } from './ViewAutoImportWoComponent';



 const ViewAutoimportRouts = [
  { path: "viewautoimport", component: ViewAutoImportWoComponent }
];

@NgModule({
  declarations: [ViewAutoImportWoComponent],
  imports: [
    RouterModule.forChild(ViewAutoimportRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    
    GridModule,
    NgbModule,


  ],
  providers: [],
  bootstrap: [ViewAutoImportWoComponent]
})

export class ViewAutoImportWoModule {}
