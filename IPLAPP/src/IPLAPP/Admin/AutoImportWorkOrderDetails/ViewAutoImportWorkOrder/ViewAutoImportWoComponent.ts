
import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import {viewAutoImportWorkorderModel} from "./ViewAutoImportWoModel";
import{AutoImportWorkorderModel} from "../AutoImportWorkOrder/AutoImportWorkOrderModel";
import{AutoImportWorkOrderService} from "../AutoImportWorkOrder/AutoImportWorkOrderService";
import {ViewAutoImportWoService} from "./ViewAutoImportWoService";


import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { from } from 'rxjs';

@Component({
  templateUrl: "./ViewAutoImportWo.html"
})
export class ViewAutoImportWoComponent implements OnInit {
  
    viewAutoImportWorkorderModelObj: viewAutoImportWorkorderModel = new viewAutoImportWorkorderModel();
    AutoImportWorkorderModelObj: AutoImportWorkorderModel = new AutoImportWorkorderModel();
  public griddata: any[];

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xAutoImportWorkOrderService:AutoImportWorkOrderService,
    private xViewAutoImportWoService : ViewAutoImportWoService,

 
  ) {

    this.GetGridData();
  }

  ngOnInit() {}

  // clear data
  AddNewAutoImportwo() {
    var faltu = undefined;

    this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/autoimport/autoimportwo"]);
  }

  // common code
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    // this.ViewUserModelObj.Type = 4;
    // this.xViewUserServices
    //   .ViewUserData(this.ViewUserModelObj)
    //   .subscribe(response => {
    //     console.log("User Details", response);
    //     this.griddata = response[0];
    //   });
  }

  ClearData() {
    this.viewAutoImportWorkorderModelObj = new viewAutoImportWorkorderModel();
   
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    this.xRouter.navigate(["autoimport/autoimportwo"]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AutoImportWorkorderModelObj.WI_Pkey_ID = dataItem.WI_Pkey_ID;
      this.AutoImportWorkorderModelObj.WI_IsDeleted = true;
      this.AutoImportWorkorderModelObj.WI_IsActive = false;

      this.xAutoImportWorkOrderService
        .AutoImportWorkOrderPost(this.AutoImportWorkorderModelObj)
        .subscribe(response => {
          console.log("delete response", response);
         
        });
        this.GetGridData();
    }
  }

  //get grid
  GetGridData() {
    this.xViewAutoImportWoService
      .ViewAutoImportData(this.viewAutoImportWorkorderModelObj)
      .subscribe(response => {
        console.log("resp autoImport", response);
        this.griddata = response[0];
      });
  }


  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

  }
}
