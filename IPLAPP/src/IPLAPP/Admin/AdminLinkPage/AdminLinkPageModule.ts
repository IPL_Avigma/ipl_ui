import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AdminLinkPageComponent } from './AdminLinkPageComponent';


export const AdminLinkPageRouts = [
  { path: "adminlinkpage", component: AdminLinkPageComponent }
];


@NgModule({
  declarations: [AdminLinkPageComponent],
  imports: [
    RouterModule.forChild(AdminLinkPageRouts),
    CommonModule

  ],
  providers: [],
  bootstrap: [AdminLinkPageComponent]
})

export class AdminLinkPageModule {}
