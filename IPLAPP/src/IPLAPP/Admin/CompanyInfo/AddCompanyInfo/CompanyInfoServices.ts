
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {CompanyInfoModel} from './CompanyInfoModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import {Documentmodel} from './CompanyInfoModel';
import {HomepageServices} from '../../../Home/HomeServices';

@Injectable({
  providedIn: "root"
})
export class CompanyInfoServices {

  public token: any;

  userData:any;
  pkeyuserId = 0;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {

    this.token = JSON.parse(localStorage.getItem('TOKEN'));

    this.userData = JSON.parse(localStorage.getItem('usertemp_'));
    if(this.userData != null){

      this.pkeyuserId = this.userData[0].User_pkeyID;
      console.log('userpkey id',this.pkeyuserId);
    }

  }
  // get user data
  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostAppCompanyInfoData";


  public CompanyInfotDataPost(Modelobj:CompanyInfoModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure

    debugger;
    var ANYDTO: any = {};

    ANYDTO.YR_Company_pkeyID = Modelobj.YR_Company_pkeyID;
    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.YR_Company_Address = Modelobj.YR_Company_Address;
    ANYDTO.YR_Company_City = Modelobj.YR_Company_City;
    ANYDTO.YR_Company_Con_Name = Modelobj.YR_Company_Con_Name;
    ANYDTO.YR_Company_Email = Modelobj.YR_Company_Email;
    ANYDTO.YR_Company_IsActive = Modelobj.YR_Company_IsActive;
    ANYDTO.YR_Company_Logo = Modelobj.YR_Company_Logo;
    ANYDTO.YR_Company_Name = Modelobj.YR_Company_Name;
    ANYDTO.YR_Company_PDF_Heading = Modelobj.YR_Company_PDF_Heading;
    ANYDTO.YR_Company_Phone = Modelobj.YR_Company_Phone;
    ANYDTO.YR_Company_Support_Email = Modelobj.YR_Company_Support_Email;
    ANYDTO.YR_Company_Support_Phone = Modelobj.YR_Company_Support_Phone;
    ANYDTO.YR_Company_Zip = Modelobj.YR_Company_Zip;
    ANYDTO.YR_Company_State = Modelobj.YR_Company_State;
    ANYDTO.YR_Company_IsDelete = Modelobj.YR_Company_IsDelete;

    ANYDTO.YR_Company_UserID = this.pkeyuserId;

    ANYDTO.Type = 1;

    if(Modelobj.YR_Company_pkeyID != 0){
      ANYDTO.Type = 2;
    }




    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////////////////////////
  //
  // add document
  public CommonDocumentUpdate(DocumentmodelObj: Documentmodel) {
    //debugger;
    var StrEditDTO: any = {};

    StrEditDTO.docx = DocumentmodelObj.documentx;

    // if(Modelobj.YR_Company_pkeyID != 0){
    //   ANYDTO.Type = 2;
    // }


    var data = new FormData();
    const pkeyId = DocumentmodelObj.Pkey_Id;

    for (var i = 0; i < StrEditDTO.docx.length; i++) {
      data.append(pkeyId, StrEditDTO.docx[i]);
    }

    const uploadapi = BasetUrl.Domain + "/api/RESTIPLUPLOAD/PostDocumentData";

    return this._http.post<any>(uploadapi, data).pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.handleError)
    );
  }

  //////////////////////////////////////////////////////////////////////////
  // get document
  private apiUrl = BasetUrl.Domain+"/api/RESTIPLUPLOAD/GetAppCompanyInfoImageData";

    public GetCompanyDocument(Modelobj:CompanyInfoModel)
    {
      //debugger;
      var GetDocDTO:any = {};
      GetDocDTO.App_Com_Img_pkeyID = 0;
      GetDocDTO.App_Com_Img_CompanyID = Modelobj.YR_Company_pkeyID;
      GetDocDTO.Type = 3;

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this._http.post<any>(this.apiUrl, GetDocDTO, { headers: headers })
       .pipe(tap(data =>
            {
                //console.log(data);
                return data;
            }),
                catchError(this.xHomepageServices.CommonhandleError)
            );
    }



  ////////////////////////////////////////////////////////////////////
    // get document
    private apiUrlGetSingle = BasetUrl.Domain+"api/RESTIPL/GetAppCompanySingle";

    public GetAppCompanySingle(Modelobj:CompanyInfoModel)
    {
      debugger;
      var AnyDTO:any = {};
     
      AnyDTO.YR_Company_UserID = Modelobj.UserID;
      AnyDTO.YR_Company_pkeyID = Modelobj.YR_Company_pkeyID;
      AnyDTO.Type = Modelobj.Type;

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this._http.post<any>(this.apiUrlGetSingle, AnyDTO, { headers: headers })
       .pipe(tap(data =>
            {
                //console.log(data);
                return data;
            }),
                catchError(this.xHomepageServices.CommonhandleError)
            );
    }





  ////////////////////////////////////////////////////////////////////










  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Invalid request... please try again later...");
  }
}


