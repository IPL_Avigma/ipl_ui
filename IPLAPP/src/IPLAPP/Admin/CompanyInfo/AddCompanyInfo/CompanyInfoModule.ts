import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";


import { CompanyInfoComponent } from './CompanyInfoComponent';
import {CompanyInfoServices} from './CompanyInfoServices';
import {WorkOrderDrodownServices} from '../../../CommonServices/commonDropDown/DropdownService';

import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

const CompanyInfoRouts = [
  { path: "companyinfo/:id", component: CompanyInfoComponent }
  // { path: "companyinfo", component: CompanyInfoComponent }

];

@NgModule({
  declarations: [CompanyInfoComponent],
  imports: [
    RouterModule.forChild(CompanyInfoRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,NgbModule,
    CommonDirectiveModule

  ],
  providers: [CompanyInfoServices,WorkOrderDrodownServices],
  bootstrap: [CompanyInfoComponent]
})

export class CompanyInfoModule {}
