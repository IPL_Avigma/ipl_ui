import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { CompanyInfoModel } from "./CompanyInfoModel";
import { CompanyInfoServices } from "./CompanyInfoServices";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { Router, ActivatedRoute } from "@angular/router";
import { BasetUrl } from "../../../Utility/DomainUrl";

import { Documentmodel } from "./CompanyInfoModel";
import {WorkOrderDrodownServices} from '../../../CommonServices/commonDropDown/DropdownService';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ViewCompanyInfoModel } from '../ViewCompanyInfo/ViewCompanyInfoModel';

@Component({
  templateUrl: "./CompanyInfo.html"
})
export class CompanyInfoComponent implements OnInit {
  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  dropCkck = false; // common only drop down

  StateFlag = false; // for dropdown

  StateList: any;

  CompanyInfoModelObj: CompanyInfoModel = new CompanyInfoModel();
  ViewCompanyInfoModelObj: ViewCompanyInfoModel = new ViewCompanyInfoModel();
  DocumentmodelObj: Documentmodel = new Documentmodel();

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xCompanyInfoServices: CompanyInfoServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xWorkOrderDrodownServices:WorkOrderDrodownServices
  ) {
    //alert('hi');
    //this.StateList = [{ Id: 1, Name: "AK" }, { Id: 2, Name: "AL" }];
    this.GetState();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      CompanyNameVal: ["", Validators.required],
      ContactNameVal: ["", Validators.required],
      EmailVal: ["", [Validators.required, Validators.email]],
      PhoneVal: ["", Validators.required],
      AddressVal: ["", Validators.required],
      CityVal: ["", Validators.required],
      ZipVal: ["", Validators.required],

      disbledFaltu: ["", Validators.nullValidator],
      disbledFaltu1: ["", Validators.nullValidator],
      disbledFaltu2: ["", Validators.nullValidator],
      disbledFaltux: ["", Validators.min],
      fileedit: ["", Validators.nullValidator],
    });

    //this.formUsrCommonGroup.disable();
    this.getModelData();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    debugger;
    this.contentx = content;
    debugger;
    this.submitted = true;
    this.CompanyInfoModelObj;

    ////
    // only drop down
    this.dropCkck = false;

    if (this.CompanyInfoModelObj.YR_Company_State == 0) {
      this.StateFlag = true;
      this.dropCkck = true;
    }

    if (this.dropCkck) {
      return;
    }

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    if(this.ModelObj) {
      this.ViewCompanyInfoModelObj.YR_Company_pkeyID  = this.ModelObj;
    } 

    // all valid data to save
    this.xCompanyInfoServices
      .CompanyInfotDataPost(this.CompanyInfoModelObj)
      .subscribe(response => {
        console.log("resp data pkey", response);
        //alert("Work Order Saved...!");

        if (response[0].Status != "0") {
          this.CompanyInfoModelObj.YR_Company_pkeyID = parseInt(
            response[0].YR_Company_pkeyID
          );
          this.MessageFlag = "Company information Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
          //alert('User Data Saved...!');
          this.DocumentCall(response[0].YR_Company_pkeyID);
        }
      });
  }
  // btn code end

  State_Method() {
    this.StateFlag = false;
  }

  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  /// end common model

  Clearx() {
    // this.button = "Save";
    // this.CompanyInfoModelObj.YR_Company_pkeyID = 0;
    // this.CompanyInfoModelObj.YR_Company_Address = "";
    // this.CompanyInfoModelObj.YR_Company_City  = "";
    // this.CompanyInfoModelObj.YR_Company_Con_Name  = "";
    // this.CompanyInfoModelObj.YR_Company_Email  = "";
    // this.CompanyInfoModelObj.YR_Company_IsActive = true;
  }

  ModelObj: any;
  IsEditDisable = false;
  getModelData() {
    debugger;

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.CompanyInfoModelObj = new CompanyInfoModel();
    } else {

    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      console.log('YR_Company_pkeyID', id);
      this.ModelObj = parseInt(id);
      this.GetSingleData();
    }
    
    
    // const id1 = this.xRoute.snapshot.params['id'];
    // let YR_Company_pkeyID: any;
    // if (id1 == undefined) {

    //   this.CompanyInfoModelObj.Type = 1;
    //   this.CompanyInfoModelObj.YR_Company_pkeyID = 0;
      
    // } else {

    //   let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    //   console.log('YR_Company_pkeyID', id);
    //    YR_Company_pkeyID = parseInt(id);
    // }
    
    
    
    //this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    //console.log('Model',this.ModelObj);




   // console.log('Pkey',this.ModelObj.pkeyuserId);
   // if (this.ModelObj == undefined)
  //  if(YR_Company_pkeyID ==null) 
  //   {
  //     this.CompanyInfoModelObj = new CompanyInfoModel();
  //   } 
    // else 
    // {
      // if (this.ModelObj != null ) 
      // {
        // this.CompanyInfoModelObj = new CompanyInfoModel();
        
        //   if(response[0].length != 0){

        //     this.CompanyInfoModelObj.UserID = response[0][0].UserID;
        // this.CompanyInfoModelObj.YR_Company_Address =  response[0][0].YR_Company_Address;
        // this.CompanyInfoModelObj.YR_Company_City =  response[0][0].YR_Company_City;
        // this.CompanyInfoModelObj.YR_Company_Con_Name =  response[0][0].YR_Company_Con_Name;
        // this.CompanyInfoModelObj.YR_Company_Email =  response[0][0].YR_Company_Email;
        // this.CompanyInfoModelObj.YR_Company_IsActive =  response[0][0].YR_Company_IsActive;
        // this.CompanyInfoModelObj.YR_Company_Logo =  response[0][0].YR_Company_Logo;
        // this.CompanyInfoModelObj.YR_Company_Name =  response[0][0].YR_Company_Name;
        // this.CompanyInfoModelObj.YR_Company_PDF_Heading =  response[0][0].YR_Company_PDF_Heading;
        // this.CompanyInfoModelObj.YR_Company_Phone =  response[0][0].YR_Company_Phone;
        // this.CompanyInfoModelObj.YR_Company_State =  response[0][0].YR_Company_State;
        // this.CompanyInfoModelObj.YR_Company_Support_Email =  response[0][0].YR_Company_Support_Email;
        // this.CompanyInfoModelObj.YR_Company_Support_Phone = response[0][0].YR_Company_Support_Phone;
        // this.CompanyInfoModelObj.YR_Company_Zip =  response[0][0].YR_Company_Zip;
        // this.CompanyInfoModelObj.YR_Company_pkeyID =  response[0][0].YR_Company_pkeyID;

       
        this.GetDocumentFile();


          // }








      // }
      //  else {
      //   this.CompanyInfoModelObj.Type = this.ModelObj.Type;
      //   this.CompanyInfoModelObj.UserID = this.ModelObj.UserID;
      //   this.CompanyInfoModelObj.YR_Company_Address = this.ModelObj.YR_Company_Address;
      //   this.CompanyInfoModelObj.YR_Company_City = this.ModelObj.YR_Company_City;
      //   this.CompanyInfoModelObj.YR_Company_Con_Name = this.ModelObj.YR_Company_Con_Name;
      //   this.CompanyInfoModelObj.YR_Company_Email = this.ModelObj.YR_Company_Email;
      //   this.CompanyInfoModelObj.YR_Company_IsActive = this.ModelObj.YR_Company_IsActive;
      //   this.CompanyInfoModelObj.YR_Company_Logo = this.ModelObj.YR_Company_Logo;
      //   this.CompanyInfoModelObj.YR_Company_Name = this.ModelObj.YR_Company_Name;
      //   this.CompanyInfoModelObj.YR_Company_PDF_Heading = this.ModelObj.YR_Company_PDF_Heading;
      //   this.CompanyInfoModelObj.YR_Company_Phone = this.ModelObj.YR_Company_Phone;
      //   this.CompanyInfoModelObj.YR_Company_State = this.ModelObj.YR_Company_State;
      //   this.CompanyInfoModelObj.YR_Company_Support_Email = this.ModelObj.YR_Company_Support_Email;
      //   this.CompanyInfoModelObj.YR_Company_Support_Phone = this.ModelObj.YR_Company_Support_Phone;
      //   this.CompanyInfoModelObj.YR_Company_Zip = this.ModelObj.YR_Company_Zip;
      //   this.CompanyInfoModelObj.YR_Company_pkeyID = this.ModelObj.YR_Company_pkeyID;

      //   this.formUsrCommonGroup.disable();

      //   this.IsEditDisable = true;
      //   this.button = "Update";
      //   this.CompanyInfoModelObj.Type = 2;
      //   this.GetDocumentFile();
      // }
    // }
  }
  // end code

  GetSingleData() {
    debugger;
        this.CompanyInfoModelObj.YR_Company_pkeyID = this.ModelObj;
        this.CompanyInfoModelObj.UserID = this.ModelObj;
        const User = JSON.parse(localStorage.getItem('usertemp_'));
        this.CompanyInfoModelObj.UserID = User[0].User_pkeyID;
        this.CompanyInfoModelObj.Type = 2;
        this.xCompanyInfoServices.GetAppCompanySingle(this.CompanyInfoModelObj)
        .subscribe(response => {
          debugger;
          console.log('response single comp', response);

          this.CompanyInfoModelObj.YR_Company_Name = 
          response[0][0].YR_Company_Name;
          this.CompanyInfoModelObj.YR_Company_Con_Name = 
          response[0][0].YR_Company_Con_Name;
          this.CompanyInfoModelObj.YR_Company_Email = 
          response[0][0].YR_Company_Email;
          this.CompanyInfoModelObj.YR_Company_Phone = 
          response[0][0].YR_Company_Phone;
          this.CompanyInfoModelObj.YR_Company_Address = 
          response[0][0].YR_Company_Address;
          this.CompanyInfoModelObj.YR_Company_City = 
          response[0][0].YR_Company_City;
          this.CompanyInfoModelObj.YR_Company_State = 
          response[0][0].YR_Company_State;
          this.CompanyInfoModelObj.YR_Company_Zip = 
          response[0][0].YR_Company_Zip;
          this.CompanyInfoModelObj.YR_Company_Logo = 
          response[0][0].YR_Company_Logo;
          this.CompanyInfoModelObj.YR_Company_Support_Email = 
          response[0][0].YR_Company_Support_Email;
          this.CompanyInfoModelObj.YR_Company_PDF_Heading = 
          response[0][0].YR_Company_PDF_Heading;
          this.CompanyInfoModelObj.YR_Company_IsActive = 
          response[0][0].YR_Company_IsActive;

          this.formUsrCommonGroup.disable();

          this.IsEditDisable = true;
          this.button = "Update";
          this.CompanyInfoModelObj.Type = 2;
        });


  }

  Back() {
    this.xRouter.navigate(["/company/viewcompany"]);
  }

  //end code

  imgpath: string; //img sath
  processImage(imageInput: any, content) {
    //debugger;
    console.log('document clinetCompany ka '+imageInput);
    this.contentx = content;
    const getnamefile = imageInput.files[0].name;
    const extsn = getnamefile.split(".").pop();
    //alert(extsn);
    // here checking file extension
    if (extsn != "xlsx") {
      this.DocumentmodelObj.documentx = imageInput.files;
    } else {
      this.MessageFlag = "Invalid File Format...!";
      this.commonMessage(this.contentx);
    }
  }
  // end code

  // enteract between save btn and document upload

  DocumentCall(pkeyId: any) {
    //debugger;
    if (this.DocumentmodelObj.documentx) {
      this.DocumentmodelObj.Pkey_Id = pkeyId;
      this.xCompanyInfoServices
        .CommonDocumentUpdate(this.DocumentmodelObj)
        .subscribe(response => {
          console.log("respo img doc cha", response);
          //debugger;
          var rspon = response.Message;
          var arr = [];
          arr = rspon.split("#");
          var msg = arr[0];
          console.log("document upload msg", msg);
          if (arr[1]) {
            this.imgpath = BasetUrl.Domain + arr[1];

            this.ImgFlag = false;
            this.GetExtension(arr[1]);
          }

          // this.MessageFlag = msg;
          // this.commonMessage(this.contentx);
        });
    }
  }

  // end code

  ///
  // check extension img or not
  ImgFlag = false;
  GetExtension(imgpathx: any) {
    var extsn = imgpathx.split(".").pop();
    if (
      extsn == "jpg" ||
      extsn == "png" ||
      extsn == "PNG" ||
      extsn == "JPG" ||
      extsn == "JPEG" ||
      extsn == "jpeg"
    ) {
      this.ImgFlag = true;
      // this is img
    } else if (extsn == "pdf" || extsn == "docx" || extsn == "doc") {
      this.ImgFlag = false;

      // this is other doc
    }
  }
  // end code

  GetDocumentFile() {
    this.xCompanyInfoServices
      .GetCompanyDocument(this.CompanyInfoModelObj)
      .subscribe(response => {
        console.log("Doc resp", response);
        //debugger;
        if (response[0].length != 0) {
          this.imgpath = BasetUrl.Domain + response[0][0].App_Com_Img_FilePath;

          this.GetExtension(response[0][0].App_Com_Img_FilePath);
        }
      });
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }

  //get state
  GetState(){
    this.xWorkOrderDrodownServices.StateDropDownData()
    .subscribe(response =>{
      console.log('resp state drop',response);
      this.StateList = response[0];
    });
  }
}
