import { Component, Injectable, OnInit } from "@angular/core";

import { Router } from "@angular/router";

import { NgbTypeahead } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  merge
} from "rxjs/operators";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewCompanyInfoModel,filterMasterModel } from "./ViewCompanyInfoModel";
import { ViewCompanyInfoServices } from "./ViewCompanyInfoServices";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewCompanyInfo.html"
})
export class ViewCompanyInfoComponent implements OnInit {
  ViewCompanyInfoModelObj: ViewCompanyInfoModel = new ViewCompanyInfoModel();
  filterMasterModelObj: filterMasterModel = new filterMasterModel();
  public griddata: any[];



  constructor(
    private xViewCompanyInfoServices: ViewCompanyInfoServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private EncrDecr: EncrDecrServiceService

  ) {
    //alert("hi");
    this.GetCompanyData();
  }

  ngOnInit() {}

  GetCompanyData(){
    this.xViewCompanyInfoServices.ViewCompanyInfoData(this.filterMasterModelObj)
    .subscribe(response =>{
      //console.log('list company',response);
      this.griddata = response[0];
    });
  }

   // this code selected event row
   showDetails(event, dataItem) {
   debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);

    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.YR_Company_pkeyID);
    this.xRouter.navigate(["/company/companyinfo", btoa(encrypted)]);
    // this.xRouter.navigate(["/company/companyinfo"]);


  }

  // clear data
  AddNewComapny(){
    debugger;
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/company/companyinfo", 'new']);

  }



  FilterCall(){
    //alert(JSON.stringify(this.filterMasterModelObj));
    this.filterMasterModelObj.Type = 3;

    this.xViewCompanyInfoServices.ViewCompanyInfoData(this.filterMasterModelObj)
    .subscribe(response =>{
      console.log('filter data',response)
      this.griddata = response[0];
    });
  }

  ClearData(){
    this.filterMasterModelObj = new filterMasterModel();
    this.GetCompanyData();
  }
  SaveFilterData(){
    alert('save called');
  }


}
