import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';


import { ViewCompanyInfoComponent } from './ViewCompanyInfoComponent';
import { ViewCompanyInfoServices } from "./ViewCompanyInfoServices";

const ViewCompanyRouts = [
  { path: "viewcompany", component: ViewCompanyInfoComponent }
];

@NgModule({
  declarations: [ViewCompanyInfoComponent],
  imports: [
    RouterModule.forChild(ViewCompanyRouts),
    CommonModule,
    HttpClientModule,
    GridModule,ExcelModule,
    NgbModule,
    FormsModule, ReactiveFormsModule,

  ],
  providers: [ViewCompanyInfoServices],
  bootstrap: [ViewCompanyInfoComponent]
})

export class ViewCompanyInfoModule {}
