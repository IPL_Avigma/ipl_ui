import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GridModule } from '@progress/kendo-angular-grid';

import { ViewRushComponent } from './ViewRushComponent';
import { ViewRushServices } from "./ViewRushServices";

 const ViewRushRouts = [
  { path: "viewrush", component: ViewRushComponent }
];

@NgModule({
  declarations: [ViewRushComponent],
  imports: [
    RouterModule.forChild(ViewRushRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    GridModule,

  ],
  providers: [ViewRushServices],
  bootstrap: [ViewRushComponent]
})

export class ViewRushModule {}
