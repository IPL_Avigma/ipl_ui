import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ViewRushServices } from "./ViewRushServices";
import { ViewRushModel } from "./ViewRushModel";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { AddRushServices } from "../AddRush/AddRushServices";
import { AddRushModel } from "../AddRush/AddRushModel";


import { State } from "@progress/kendo-data-query";
import {DataStateChangeEvent} from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewRush.html"
})
export class ViewRushComponent implements OnInit {
  ViewRushModelObj: ViewRushModel = new ViewRushModel();
  AddRushModelObj: AddRushModel = new AddRushModel();
  public griddata: any[];

  constructor(
    private xViewRushServices: ViewRushServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private EncrDecr: EncrDecrServiceService,
    private xAddRushServices: AddRushServices
  ) {
    this.GetGridData();
  }

  ngOnInit() {}

  //get grid
  GetGridData() {
    this.xViewRushServices
      .ViewStateData(this.ViewRushModelObj)
      .subscribe(response => {
        console.log("resp rush", response);
        this.griddata = response[0];
      });
  }

  // common code
  FilterCall() {
    //alert(JSON.stringify(this.filterMasterModelObj));
    // this.ViewUserModelObj.Type = 4;
    // this.xViewUserServices
    //   .ViewUserData(this.ViewUserModelObj)
    //   .subscribe(response => {
    //     console.log("User Details", response);
    //     this.griddata = response[0];
    //   });
  }

  ClearData() {
    this.ViewRushModelObj = new ViewRushModel();
    //this.getautoUserdata();
  }
  SaveFilterData() {
    alert("save called");
  }

  // common code End

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/rush/addrush"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.rus_pkeyID);
    this.xRouter.navigate(["/rush/addrush", btoa(encrypted)]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AddRushModelObj.rus_pkeyID = dataItem.rus_pkeyID;
      this.AddRushModelObj.rus_IsDelete = true;

      this.xAddRushServices
        .RushDataPost(this.AddRushModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  // clear data
  AddNewRush() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/rush/addrush", 'new']);
  }



     //kendo check box event action

     public state: State = {};

     public dataStateChange(state: DataStateChangeEvent): void {
       this.state = state;


     }
}
