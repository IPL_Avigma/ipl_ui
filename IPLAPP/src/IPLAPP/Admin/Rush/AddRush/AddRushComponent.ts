import { Component, Injectable, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AddRushModel } from "./AddRushModel";
import { AddRushServices } from "./AddRushServices";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ActivatedRoute } from '@angular/router';
import { ViewRushModel } from '../ViewRush/ViewRushModel';
import { ViewRushServices } from '../ViewRush/ViewRushServices';

@Component({
  templateUrl: "./AddRush.html"
})
export class AddRushComponent implements OnInit, OnDestroy {
  submitted = false;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  isLoading = false;

  public contentx;
  MessageFlag: string;

  AddRushModelObj: AddRushModel = new AddRushModel();
  ViewRushModelObj: ViewRushModel = new ViewRushModel();

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddRushServices: AddRushServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xViewRushServices: ViewRushServices
  ) {}

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      RushName: ["", Validators.required],
      disbledFaltu1: ["", Validators.nullValidator]
    });
    this.getModelData();
  }
  ngOnDestroy(): void {
    this.submitted = false;
    this.button = "Save";
    this.isLoading = false;
    this.AddRushModelObj = new AddRushModel();
  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    
    if(this.ModelObj) {
      this.AddRushModelObj.rus_pkeyID = this.ModelObj;
    }

    this.xAddRushServices
      .RushDataPost(this.AddRushModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddRushModelObj.rus_pkeyID = parseInt(response[0].rus_pkeyID);

          this.MessageFlag = "Rush Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  IsEditDisable = false;
  getModelData() {
    //debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddRushModelObj = new AddRushModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('rus_pkeyId', id);
    this.ModelObj = parseInt(id);
    }


    if (this.ModelObj == undefined) {
      this.AddRushModelObj = new AddRushModel();
    } else {
      this.GetSingleData();
      this.AddRushModelObj.rus_pkeyID = this.ModelObj.rus_pkeyID;
      this.AddRushModelObj.rus_Name = this.ModelObj.rus_Name;
      this.AddRushModelObj.rus_IsActive = this.ModelObj.rus_IsActive;
      this.AddRushModelObj.UserID = this.ModelObj.UserID;

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;

      this.button = "Update";
      this.AddRushModelObj.Type = 2;
    }
  }

  
  GetSingleData() {
    debugger;
    this.ViewRushModelObj.rus_pkeyID = this.ModelObj;
    this.ViewRushModelObj.Type = 2;
    this.xViewRushServices.ViewStateData(this.ViewRushModelObj)
    .subscribe(response => {
      debugger;
      console.log('GetSingleData', response);

      this.AddRushModelObj.rus_Name =
      response[0][0].rus_Name;
      this.AddRushModelObj.rus_IsActive = 
      response[0][0].rus_IsActive;
    });
  }


  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code

  emp: Number = 0;
}
