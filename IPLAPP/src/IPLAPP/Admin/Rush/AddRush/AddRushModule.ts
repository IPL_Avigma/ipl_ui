import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AddRushComponent } from './AddRushComponent';

import { AddRushServices } from "./AddRushServices";

 const AddRushRouts = [
  { path: "addrush/:id", component: AddRushComponent }
];



@NgModule({
  declarations: [AddRushComponent],
  imports: [
    RouterModule.forChild(AddRushRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    NgbModule

  ],
  providers: [AddRushServices],
  bootstrap: [AddRushComponent]
})

export class AddRushModule {}
