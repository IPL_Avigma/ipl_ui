import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ViewClientCompaniesModel,filterMasterModel } from "./ViewClientCompaniesModel";
import { ViewClientCompaniesServices } from "./ViewClientCompaniesServices";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { AddClientCompaniesModel } from "../AddClientCompanies/AddClientCompaniesModel";
import { AddClientCompaniesServices } from "../AddClientCompanies/AddClientCompaniesServices";
import {WorkOrderDrodownServices} from '../../../CommonServices/commonDropDown/DropdownService';



@Component({
  templateUrl: "./ViewClientCompanies.html"
})
export class ViewClientCompaniesComponent implements OnInit {
  ViewClientCompaniesModelObj: ViewClientCompaniesModel = new ViewClientCompaniesModel();
  filterMasterModelObj:filterMasterModel = new filterMasterModel();
  AddClientCompaniesModelobj: AddClientCompaniesModel = new AddClientCompaniesModel();
  public griddata: any[];
  constructor(
    private xViewClientCompaniesServices: ViewClientCompaniesServices,
    private xMasterlayoutComponent:MasterlayoutComponent,
    private xRouter:Router,
    private xAddClientCompaniesServices:AddClientCompaniesServices,
    private xWorkOrderDrodownServices :WorkOrderDrodownServices,
    private EncrDecr: EncrDecrServiceService,

  ) {
    //alert('hi');
    this.GetClientData();


  }

  ngOnInit() {}

  GetClientData() {
    this.filterMasterModelObj.Type = 3;
    this.xViewClientCompaniesServices
      .ClientComapnyViewData(this.filterMasterModelObj)
      .subscribe(response => {
        console.log("client grid data", response);
        this.griddata = response[0];

        this.GetStateDropDown();

      });
  }

    // this code selected event row
    showDetails(event, dataItem) {
      //debugger;
      //alert('work in progress...😊')

     // this.xMasterlayoutComponent.masterFunctionCall(dataItem);

      // this.xMasterlayoutComponent.masterFunctionCall(this.BindData);


      // this.GetWhichAction();


    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Client_pkeyID);
   this.xRouter.navigate(["/client/addclientcompanies", btoa(encrypted)]);
    // this.xRouter.navigate(["/client/addclientcompanies/" + 'ID']);




    }


    FilterCall(){
      //alert(JSON.stringify(this.filterMasterModelObj));
      this.filterMasterModelObj.Type = 4;

      this.xViewClientCompaniesServices.ClientComapnyViewData(this.filterMasterModelObj)
      .subscribe(response =>{
        //console.log('filter data',response)
        this.griddata = response[0];
      });
    }

    ClearData(){
      this.filterMasterModelObj = new filterMasterModel();
      this.GetClientData();
    }
    SaveFilterData(){
      alert('save called');
    }



    //
      // clear data
  AddNewClient(){

    // var faltu = undefined;

    //this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/client/addclientcompanies", 'new']);

  }
  // end code
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {

      this.AddClientCompaniesModelobj.Type = 4;
      this.AddClientCompaniesModelobj.Client_IsDelete = true;
      this.AddClientCompaniesModelobj.Client_pkeyID = dataItem.Client_pkeyID;

      this.xAddClientCompaniesServices
      .AddClientCompaniesPost(this.AddClientCompaniesModelobj)
      .subscribe(response => {
        console.log("resp delete", response);
        this.GetClientData();

      });
    }
  }

  StateArray:any
  GetStateDropDown(){
    this.xWorkOrderDrodownServices.StateDropDownData()
    .subscribe(response => {
      console.log('dowpstate',response);
      this.StateArray = response[0];
    });
  }



}
