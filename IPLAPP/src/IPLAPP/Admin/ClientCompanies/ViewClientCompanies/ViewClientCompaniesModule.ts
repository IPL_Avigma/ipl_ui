import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';


import { ViewClientCompaniesComponent } from './ViewClientCompaniesComponent';
import {ViewClientCompaniesServices} from './ViewClientCompaniesServices';
import {WorkOrderDrodownServices} from '../../../CommonServices/commonDropDown/DropdownService';

const ViewClientCompaniesRouts = [
  { path: "viewclientcompanies", component: ViewClientCompaniesComponent }
];
@NgModule({
  declarations: [ViewClientCompaniesComponent],
  imports: [
    RouterModule.forChild(ViewClientCompaniesRouts),
    CommonModule,
    HttpClientModule,
    GridModule,ExcelModule,
    FormsModule, ReactiveFormsModule,

  ],
  providers: [ViewClientCompaniesServices,WorkOrderDrodownServices],
  bootstrap: [ViewClientCompaniesComponent]
})

export class ViewClientCompaniesModule {}
