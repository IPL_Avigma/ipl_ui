import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddClientCompaniesModel } from "./AddClientCompaniesModel";
import { AddClientCompaniesServices } from "./AddClientCompaniesServices";
import { AddClientCompaniesStateMultipleModel } from "./AddClientCompaniesModel";
import { ClientContactList } from "./AddClientCompaniesModel";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import { ViewClientCompaniesServices } from "../ViewClientCompanies/ViewClientCompaniesServices";
import { filterMasterModel } from "../ViewClientCompanies/ViewClientCompaniesModel";
import { ActivatedRoute } from '@angular/router';
import { EncrDecrServiceService } from "../../../../app/encr-decr-service.service";

@Component({
  templateUrl: "./AddClientCompanies.html"
})
export class AddClientCompaniesComponent implements OnInit {
  submitted = false; // submitted;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  ContactBtn = "Save Contact";

  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  // only drop down
  dropCkck = false; // common
  ActiveValFlag = false;

  FormArrayVal = [];
  MultipleContact = [];

  Contactlist: any;

  AddClientCompaniesModelObj: AddClientCompaniesModel = new AddClientCompaniesModel();
  AddClientCompaniesStateMultipleModelobj: AddClientCompaniesStateMultipleModel = new AddClientCompaniesStateMultipleModel();
  ClientContactListObj: ClientContactList = new ClientContactList();

  filterMasterModelObj: filterMasterModel = new filterMasterModel();

  formUsrCommonGroup: FormGroup; // create obj

  YESNOList: any;
  DateTimeList: any;
  CheckInProvider: any;
  StateArray: any;
  BackgroundArry: any;

  ListClientContact: [ClientContactList];

  constructor(
    private xmodalService: NgbModal,
    private formBuilder: FormBuilder,
    private xRoute: ActivatedRoute,
    private xAddClientCompaniesServices: AddClientCompaniesServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private EncrDecr: EncrDecrServiceService,
    private xViewClientCompaniesServices: ViewClientCompaniesServices
  ) {
    this.YESNOList = [{ Id: 1, Name: "YES" }, { Id: 2, Name: "NO" }];
    (this.DateTimeList = [
      { Id: 1, Name: "Do Not Print" },
      { Id: 2, Name: "Date/Time Ex. 1/23/2015 1:01 pm" },
      { Id: 3, Name: "Date Ex. 1/23/2015 1:01 pm" }
    ]),
      { Id: 4, Name: "Time Ex. 1:01 pm" };
    this.CheckInProvider = [
      { Id: 1, Name: "Aspen Grove Solutions" },
      { Id: 2, Name: "ServiceLink" }
    ];
    this.BackgroundArry = [
      { Id: "1", Name: "ONE" },
      { Id: "2", Name: "TWO" },
      { Id: "3", Name: "THREE" }
    ];

    this.FormArrayVal = [{ Address: "", City: "", State: "0", Zip: null }];

    this.Contactlist = [
      { heading: "Billing Dept" },
      { heading: "Tech Support" },
      { heading: "Region Coordinator" },
      { heading: "Supervisor" },
      { heading: "Manager" }
    ];

    this.GetStateDropDown();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      CompanyNameVal: ["", Validators.required],
      disbledFaltu23: ["", [Validators.required, Validators.email]],
      ActiveVal: ["", Validators.required],
      disbledFaltu: ["", Validators.nullValidator],
      disbledFaltu1: ["", Validators.nullValidator],
      disbledFaltu2: ["", Validators.nullValidator],

      disbledFaltu3: ["", Validators.nullValidator],
      disbledFaltu4: ["", Validators.nullValidator],
      disbledFaltu5: ["", Validators.nullValidator],
      disbledFaltu6: ["", Validators.nullValidator],
      disbledFaltu7: ["", Validators.nullValidator],
      disbledFaltu8: ["", Validators.nullValidator],
      disbledFaltu9: ["", Validators.nullValidator],

      disbledFaltu10: ["", Validators.nullValidator],
      disbledFaltu11: ["", Validators.nullValidator],
      disbledFaltu12: ["", Validators.nullValidator],
      disbledFaltu13: ["", Validators.nullValidator],
      disbledFaltu14: ["", Validators.nullValidator],

      disbledFaltu15: ["", Validators.nullValidator],
      disbledFaltu16: ["", Validators.nullValidator],
      disbledFaltu17: ["", Validators.nullValidator],

      disbledFaltu21: ["", Validators.nullValidator],
      disbledFaltu22: ["", Validators.nullValidator],

    });

    // becoz instance create nahi hot
    this.getModelData();
  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;
    // only drop down
    this.dropCkck = false;
    //alert(JSON.stringify(this.AddCategoryModelObj));

    if (this.AddClientCompaniesModelObj.Client_Active == false) {
      this.ActiveValFlag = true;
      this.dropCkck = true;
    }
    if (this.dropCkck) {
      return;
    }
    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }
    //this.AddMoreRowContact(); // for form when list edit or some value rahlili
    this.isLoading = true;
    this.button = "Processing";

    this.AddClientCompaniesModelObj.ClientContactList = this.MultipleContact;
    console.log("resp", this.MultipleContact);
    this.xAddClientCompaniesServices
      .AddClientCompaniesPost(this.AddClientCompaniesModelObj)
      .subscribe(response => {
        console.log("resp pkey", response);

        if (response[0].length != 0) {
          this.AddClientCompaniesModelObj.Client_pkeyID = parseInt(
            response[0].Client_pkeyID
          );

          this.MessageFlag = "Client Data Saved...!";
          this.isLoading = false;
          this.button = "update";
          this.commonMessage(this.contentx);

          this.filterMasterModelObj.Client_pkeyID = this.AddClientCompaniesModelObj.Client_pkeyID;

          this.GetSingleData(); // verify plz
        }
      });
  }
  // btn end code
  // drop down valid or not
  Active_Method() {
    //alert('select');
    this.ActiveValFlag = false;
  }

  // Insert New Row
  AddMoreRow() {
    var data = { State: "0", City: "" };
    this.FormArrayVal.push(data);
  }

  // remove row
  RemoveRow(index) {
    if (index != 0) {
      this.FormArrayVal.splice(index, 1);
    }
  }

  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  //delete this
  ADDAddClientCompaniesMutipleState() {
    this.AddClientCompaniesStateMultipleModelobj.Client_pkeyID;
    this.AddClientCompaniesStateMultipleModelobj.StrFormArrayVal = this.FormArrayVal;
    this.xAddClientCompaniesServices
      .AddClientCompaniesMutipleStatePost(
        this.AddClientCompaniesStateMultipleModelobj
      )
      .subscribe(response => {
        console.log(response);
      });
  }
  // end code

  AddMoreRowContact() {
    var data = {
      Clnt_Con_List_TypeName: this.AddClientCompaniesModelObj.Client_ContactTypex,
      Clnt_Con_List_pkeyID: this.AddClientCompaniesModelObj.Clnt_Con_List_pkeyID,
      Clnt_Con_List_Name: this.AddClientCompaniesModelObj.Client_ContactNamex,
      Clnt_Con_List_Email: this.AddClientCompaniesModelObj.Client_ContactEmailx,
      Clnt_Con_List_Phone: this.AddClientCompaniesModelObj.Client_ContactPhonex,
      Clnt_Con_List_IsActive: this.AddClientCompaniesModelObj
        .MainClnt_Con_List_Active,
      Clnt_Con_List_IsDelete: this.AddClientCompaniesModelObj
        .MainClnt_Con_List_IsDelete
    };

    console.log(data);

    if (this.AddClientCompaniesModelObj.Client_ContactTypex != "") {
      this.MultipleContact.push(data);
      this.ContactBtn = "Add More Contact";
    }

    this.AddClientCompaniesModelObj.Client_ContactTypex = "";
    this.AddClientCompaniesModelObj.Client_ContactNamex = "";
    this.AddClientCompaniesModelObj.Client_ContactEmailx = "";
    this.AddClientCompaniesModelObj.Client_ContactPhonex = "";
    this.AddClientCompaniesModelObj.MainClnt_Con_List_IsDelete = false;
    this.AddClientCompaniesModelObj.MainClnt_Con_List_Active = true;
  }

  EditRow(index) {
    this.AddClientCompaniesModelObj.Client_ContactTypex = this.MultipleContact[
      index
    ].Clnt_Con_List_TypeName;
    this.AddClientCompaniesModelObj.Clnt_Con_List_pkeyID = this.MultipleContact[
      index
    ].Clnt_Con_List_pkeyID;

    this.AddClientCompaniesModelObj.Client_ContactNamex = this.MultipleContact[
      index
    ].Clnt_Con_List_Name;
    this.AddClientCompaniesModelObj.Client_ContactEmailx = this.MultipleContact[
      index
    ].Clnt_Con_List_Email;
    this.AddClientCompaniesModelObj.Client_ContactPhonex = this.MultipleContact[
      index
    ].Clnt_Con_List_Phone;
    this.AddClientCompaniesModelObj.MainClnt_Con_List_IsDelete = this.MultipleContact[
      index
    ].Clnt_Con_List_IsDelete;
    this.AddClientCompaniesModelObj.MainClnt_Con_List_Active = this.MultipleContact[
      index
    ].Clnt_Con_List_IsActive;

    this.MultipleContact.splice(index, 1);

    this.ContactBtn = "Save Contact";
  }

  WorkOrderObj: any;
  IsEditDisable = false;
  getModelData() {
    debugger;
    const id1 = this.xRoute.snapshot.params['new'];
    if (id1 == 'new') {
      this.AddClientCompaniesModelObj = new AddClientCompaniesModel();
    } else {
      
      let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
      console.log('clientid', id);
      this.WorkOrderObj = parseInt(id);
    
    //debugger;
    //this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    // if (Client_pkeyID == 0) {
    //   this.AddClientCompaniesModelObj.Type = 1;
    //   this.AddClientCompaniesModelObj.UserID = 0;

    //   // this.AddClientCompaniesModelObj = new AddClientCompaniesModel();
    // } else {
    
      this.AddClientCompaniesModelObj.Client_pkeyID = this.WorkOrderObj;

      this.filterMasterModelObj.Client_pkeyID = this.WorkOrderObj;

      this.GetSingleData();

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;
      this.button = "Update";
      this.AddClientCompaniesModelObj.Type = 2;
    // }

  }
}
  
  // end code

  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code

  GetStateDropDown() {
    this.xWorkOrderDrodownServices.StateDropDownData().subscribe(response => {
      console.log("dowpstate", response);
      this.StateArray = response[0];
    });
  }

  //get single data
  GetSingleData() {
    this.filterMasterModelObj.Type = 2;
    this.filterMasterModelObj.Single = true;
    this.filterMasterModelObj.Client_pkeyID = this.WorkOrderObj;
    this.xViewClientCompaniesServices
      .ClientComapnyViewData(this.filterMasterModelObj)
      .subscribe(response => {
        console.log("single", response);

        this.AddClientCompaniesModelObj.Client_pkeyID =
          response[0][0].Client_pkeyID;
        this.AddClientCompaniesModelObj.Client_Company_Name =
          response[0][0].Client_Company_Name;
        this.AddClientCompaniesModelObj.Client_Billing_Address =
          response[0][0].Client_Billing_Address;
        this.AddClientCompaniesModelObj.Client_StateId =
          response[0][0].Client_StateId;
        this.AddClientCompaniesModelObj.Client_City =
          response[0][0].Client_City;
        this.AddClientCompaniesModelObj.Client_ZipCode =
          response[0][0].Client_ZipCode;
        this.AddClientCompaniesModelObj.Client_Discount =
          response[0][0].Client_Discount;
        this.AddClientCompaniesModelObj.Client_Contractor_Discount =
          response[0][0].Client_Contractor_Discount;

        this.AddClientCompaniesModelObj.Client_Photo_Resize_height =
          response[0][0].Client_Photo_Resize_height;
        this.AddClientCompaniesModelObj.Client_Photo_Resize_width =
          response[0][0].Client_Photo_Resize_width;
        this.AddClientCompaniesModelObj.Client_IPL_Mobile =
          response[0][0].Client_IPL_Mobile;
        this.AddClientCompaniesModelObj.Client_BackgroundProvider =
          response[0][0].Client_BackgroundProvider;
        this.AddClientCompaniesModelObj.Client_Lock_Order =
          response[0][0].Client_Lock_Order;
        this.AddClientCompaniesModelObj.Client_Comments =
          response[0][0].Client_Comments;

        this.AddClientCompaniesModelObj.Client_IsDelete =
          response[0][0].Client_IsDelete;


          this.AddClientCompaniesModelObj.Client_ContactEmail = response[0][0].Client_ContactEmail;
          this.AddClientCompaniesModelObj.Client_ContactName = response[0][0].Client_ContactName;
          this.AddClientCompaniesModelObj.Client_ContactPhone = response[0][0].Client_ContactPhone;


        this.MultipleContact = response[1];
        console.log('checkarray:',this.MultipleContact);
      });
  }
  //end code
}
