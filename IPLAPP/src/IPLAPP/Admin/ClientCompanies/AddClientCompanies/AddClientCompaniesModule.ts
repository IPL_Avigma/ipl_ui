import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddClientCompaniesComponent } from './AddClientCompaniesComponent';

import {AddClientCompaniesServices} from './AddClientCompaniesServices';
import {WorkOrderDrodownServices} from '../../../CommonServices/commonDropDown/DropdownService';
import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';


const AddClientCompaniesRouts =[

  {path:'addclientcompanies/:new',component:AddClientCompaniesComponent}

]

@NgModule({
  declarations: [AddClientCompaniesComponent],
  imports: [
    RouterModule.forChild(AddClientCompaniesRouts),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    CommonDirectiveModule

  ],
  providers: [AddClientCompaniesServices,WorkOrderDrodownServices],
  bootstrap: [AddClientCompaniesComponent]
})

export class AddClientCompaniesModule {}
