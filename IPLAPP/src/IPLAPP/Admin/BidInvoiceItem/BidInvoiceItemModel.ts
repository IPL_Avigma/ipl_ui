export class BidInvoiceItemModel {

Task_pkeyID:Number = 0;
Task_Name:string = "";
Task_Type:Number = 0;
Task_Photo_Label_Name:String = "";
Task_Group:Number = 0;
Task_UOM:Number = 0;
Task_Contractor_UnitPrice:Number;
Task_Client_UnitPrice:Number;
Task_IsActive:Boolean = true;
Task_IsDelete:Boolean = false;
Task_AutoInvoiceComplete:Boolean = false;
UserID:Number = 0;
Type: Number = 1;


}


export class Task_GroupPopupModel{
  Task_Group_pkeyID: Number = 0;
  Task_Group_Name:String;
  Task_Group_NameArray:any;
  Task_Group_IsActive: boolean = true;
  UserID: Number = 0;
  Type: Number = 1;

}
