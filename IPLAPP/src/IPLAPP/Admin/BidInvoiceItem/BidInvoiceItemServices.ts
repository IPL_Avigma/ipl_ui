
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {Task_GroupPopupModel,BidInvoiceItemModel} from './BidInvoiceItemModel';
import {BasetUrl} from '../../Utility/DomainUrl';
import { HomepageServices } from '../../Home/HomeServices';

@Injectable({
  providedIn: "root"
})


export class BidInvoiceItemServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // post

  //CreateUpdate_TaskMaster]
  //Task_Master
  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostTaskMaster";

  public TaskMasterPost(Modelobj:BidInvoiceItemModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.Task_pkeyID = Modelobj.Task_pkeyID;
    ANYDTO.Task_Type = Modelobj.Task_Type;
    ANYDTO.Task_Name = Modelobj.Task_Name;
    ANYDTO.Task_Group = Modelobj.Task_Group;
    ANYDTO.Task_Contractor_UnitPrice = Modelobj.Task_Contractor_UnitPrice;
    ANYDTO.Task_UOM = Modelobj.Task_UOM;
    ANYDTO.Task_Client_UnitPrice = Modelobj.Task_Client_UnitPrice;
    ANYDTO.Task_Photo_Label_Name = Modelobj.Task_Photo_Label_Name;
    ANYDTO.Task_IsActive = Modelobj.Task_IsActive;
    ANYDTO.Task_AutoInvoiceComplete = Modelobj.Task_AutoInvoiceComplete;

    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.Type = 1;
    if(Modelobj.Task_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.Task_IsDelete){
      ANYDTO.Type = 4;
    }


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  /////////////////////////////////////////////////////////////////

  private apiUrlPOST3 = BasetUrl.Domain +"api/RESTIPL/PostTaskGroupDetails";


  public TaskGroupPOPUPPost(Modelobj3:Task_GroupPopupModel) {
    ////debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    //debugger;
    var ANYDTO: any = {};

    ANYDTO.Task_Group_pkeyID = Modelobj3.Task_Group_pkeyID;
    ANYDTO.Task_GroupNameArr = Modelobj3.Task_Group_NameArray;
    ANYDTO.Task_Group_IsActive = Modelobj3.Task_Group_IsActive;
    ANYDTO.UserID = Modelobj3.UserID;
    ANYDTO.Type = Modelobj3.Type

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST3, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {

          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)

      );
  }

  ///////////////////////////////////////////////////

  //Get Task group
  private apiUrlGet6 =
  BasetUrl.Domain + "/api/RESTIPL/GetTaskGroupDetails";

public GetTaskGroupDetailsDropdownGet() {
  //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
  var ModelDTO: any = {};
  ModelDTO.Task_Group_pkeyID = 0;
  ModelDTO.Type = 1;


  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http
    .post<any>(this.apiUrlGet6, ModelDTO, { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
      //catchError( this.Errorcall.handleError)
    );
}




  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert("Unauthorized User...");
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Invalid request... please try again later...");
  }
}


