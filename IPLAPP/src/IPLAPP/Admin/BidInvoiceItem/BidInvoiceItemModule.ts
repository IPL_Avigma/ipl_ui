import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { BidInvoiceItemComponent } from './BidInvoiceItemComponent';

import {CommonDirectiveModule} from '../../AppDirectives/DirectiveModule';
import {WorkOrderDrodownServices} from '../../CommonServices/commonDropDown/DropdownService';

const BidInvoiceItemRouts = [
  { path: "bidinvoiceitem/:id", component: BidInvoiceItemComponent }
];
@NgModule({
  declarations: [BidInvoiceItemComponent],
  imports: [
    RouterModule.forChild(BidInvoiceItemRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    CommonDirectiveModule

  ],
  providers: [WorkOrderDrodownServices],
  bootstrap: [BidInvoiceItemComponent]
})

export class BidInvoiceItemModule {}
