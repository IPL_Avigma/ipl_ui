import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BidInvoiceItemModel } from "./BidInvoiceItemModel";

import { Task_GroupPopupModel } from "./BidInvoiceItemModel";
import { BidInvoiceItemServices } from "./BidInvoiceItemServices";
import { MasterlayoutComponent } from "../../Home/MasterComponent";
import {WorkOrderDrodownServices} from '../../CommonServices/commonDropDown/DropdownService';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./BidInvoiceItem.html"
})
export class BidInvoiceItemComponent implements OnInit {
  submitted = false;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  isLoading = false;

  public contentx;
  MessageFlag: string;

  TaskTypeList: any;
  popformArray = [];
  WKDivFlag = true;
  TaskGroupList: any;
  TaskUOMList: any;

  dataItemx:any;

  BidInvoiceItemModelObj: BidInvoiceItemModel = new BidInvoiceItemModel();

  Task_GroupPopupModelObj: Task_GroupPopupModel = new Task_GroupPopupModel();

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xBidInvoiceItemServices: BidInvoiceItemServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xWorkOrderDrodownServices:WorkOrderDrodownServices,
    private EncrDecr: EncrDecrServiceService
  ) {
    this.TaskTypeList = [
      { Id: 1, Name: "work" },
      { Id: 2, Name: "inspection" }
    ];

    this.popformArray = [{ Task_Group_Name: "", Task_Group_Name_pkeyID: 0 }];

    this.GetTaskGroup();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      TaskName: ["", Validators.required],
      ContractorUnitVal: ["", Validators.required],
      ClientUnitVal: ["", Validators.required]
      //disbledFaltu1: ["", Validators.nullValidator]
    });
    this.getModelData();
  }
  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";


    this.xBidInvoiceItemServices
      .TaskMasterPost(this.BidInvoiceItemModelObj)
      .subscribe(response => {
        //debugger;
        console.log("resp data pkey", response);
        if (response[0].Status != "0") {
          this.BidInvoiceItemModelObj.Task_pkeyID = parseInt(
            response[0].Task_pkeyID
          );

          this.MessageFlag = "Task Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);

          this.dataItemx = this.BidInvoiceItemModelObj;
          var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',  this.BidInvoiceItemModelObj.Task_pkeyID);
          this.xRouter.navigate(["/task/addinvoiceitems", btoa(encrypted)]);
          // this.xMasterlayoutComponent.masterFunctionCall(this.dataItemx);
          // this.xRouter.navigate(["/task/addinvoiceitems"]);
        }
      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  IsEditDisable = false;

  // copy name sathi
  onBlurUserName() {
    this.BidInvoiceItemModelObj.Task_Photo_Label_Name = this.BidInvoiceItemModelObj.Task_Name;
  }

  open(content) {
    this.WKDivFlag = false;
    this.xmodalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          this.WKDivFlag = true;
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.WKDivFlag = true;
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  // pop pop add
  AddMoreRowCatepop() {
    //debugger;
    var data = { Task_Group_Name: "", Task_Group_Name_pkeyID: 0 };
    if (this.popformArray.length != 0) {
      if (
        this.popformArray[this.popformArray.length - 1].Task_Group_Name != ""
      ) {
        this.popformArray.push(data);
      }
    } else {
      this.popformArray.push(data);
    }
  }
  // popup remove
  RemovePOPdata(inx, item) {
    if (inx != 0) {
      var cfrm = confirm("Delete this Record...!");
      if (cfrm == true) {
        //debugger;

        this.popformArray.splice(inx, 1);

        this.Task_GroupPopupModelObj.Type = 4;
        this.Task_GroupPopupModelObj.Task_Group_pkeyID = item.Task_Group_pkeyID;


        this.xBidInvoiceItemServices
          .TaskGroupPOPUPPost(this.Task_GroupPopupModelObj)
          .subscribe(response => {
            //debugger;
            console.log("resp delete peky task list", response);
            this.TaskGroupList = response[0];
            this.popformArray = response[0];

            // this.MessageFlag = "Task Group deleted...!";
            // this.commonMessage(this.contentx);
          });
      }
    }
  }

  // pop form
  FormButtonPOPUp(content) {
    this.contentx = content;
    //debugger;
    this.popformArray;
    if (this.popformArray[this.popformArray.length - 1].Task_Group_Name == "") {
      this.popformArray.splice(this.popformArray.length - 1, 1);
    }
    this.Task_GroupPopupModelObj.Task_Group_NameArray = this.popformArray;

    this.xBidInvoiceItemServices
      .TaskGroupPOPUPPost(this.Task_GroupPopupModelObj)
      .subscribe(response => {
        //debugger;
        console.log("resp peky task list", response);
        this.TaskGroupList = response[0];
        this.popformArray = response[0];

        this.MessageFlag = "Task Groups Saved...!";
        this.commonMessage(this.contentx);
      });
  }
  // pop up end button code

  // get work type group
  GetTaskGroup() {
    this.xBidInvoiceItemServices
      .GetTaskGroupDetailsDropdownGet()
      .subscribe(response => {
        console.log("resp Tsk  Grup", response);
        this.TaskGroupList = response[0];
        this.popformArray = response[0];

        this.getOUMDropdown();
      });
  }

  getModelData() {
    debugger;
    if (this.ModelObj == undefined) {
      this.BidInvoiceItemModelObj = new BidInvoiceItemModel();
    } else {
      this.BidInvoiceItemModelObj.Task_pkeyID = this.ModelObj.Task_pkeyID;
      this.BidInvoiceItemModelObj.Task_Name = this.ModelObj.Task_Name;
      this.BidInvoiceItemModelObj.Task_Group = this.ModelObj.Task_Group;
      this.BidInvoiceItemModelObj.Task_UOM = this.ModelObj.Task_UOM;
      this.BidInvoiceItemModelObj.Task_Type = this.ModelObj.Task_Type;
      this.BidInvoiceItemModelObj.Task_Contractor_UnitPrice = this.ModelObj.Task_Contractor_UnitPrice;
      this.BidInvoiceItemModelObj.Task_Client_UnitPrice = this.ModelObj.Task_Client_UnitPrice;
      this.BidInvoiceItemModelObj.Task_IsActive = this.ModelObj.Task_IsActive;
      this.BidInvoiceItemModelObj.Task_Photo_Label_Name = this.ModelObj.Task_Photo_Label_Name;

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;

      this.button = "Update";
      this.BidInvoiceItemModelObj.Type = 2;
    }
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code



  // GetUOM
  getOUMDropdown(){


    this.xWorkOrderDrodownServices.DropdownGetUOM()
    .subscribe(response => {

      console.log('OUM DROP DOWN',response);

      this.TaskUOMList = response[0];
    })
  }
}
