import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import {AddPhotoHeaderTemplatesModel} from './AddPhotoHeaderTemplatesModel';

@Component({
  templateUrl: "./AddPhotoHeaderTemplates.html"
})
export class AddPhotoHeaderTemplatesComponent implements OnInit {
  submitted = false;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  isLoading = false;

  public contentx;
  MessageFlag: string;

  dropdownList:any;

  AddPhotoHeaderTemplatesModelObj:AddPhotoHeaderTemplatesModel = new AddPhotoHeaderTemplatesModel();

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices
  ) {
    this.GetClientDropDown();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      //RushName: ["", Validators.required],
      //disbledFaltu1: ["", Validators.nullValidator]
    });
  }
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    // this.xAddRushServices
    //   .RushDataPost(this.AddRushModelObj)
    //   .subscribe(response => {
    //     console.log("resp data", response);
    //     if (response[0].Status != "0") {
    //       this.AddRushModelObj.rus_pkeyID = parseInt(response[0].rus_pkeyID);

    //       this.MessageFlag = "User Data Saved...!";
    //       this.isLoading = false;
    //       this.button = "Update";
    //       this.commonMessage(this.contentx);
    //     }
    //   });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  IsEditDisable = false;

  GetClientDropDown() {
    this.xWorkOrderDrodownServices
      .DropdownGetClientnamesData()
      .subscribe(response => {
        console.log("client list", response);
        //this.clientList = response[0];
        this.dropdownList = response[0];


      });
  }
}
