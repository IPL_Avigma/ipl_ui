import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddPhotoHeaderTemplatesComponent } from './AddPhotoHeaderTemplatesComponent';
import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";


const AddPhotoHeaderTemplatesComponentRouts = [
  { path: "addphotoheader", component: AddPhotoHeaderTemplatesComponent }
];
@NgModule({
  declarations: [AddPhotoHeaderTemplatesComponent],
  imports: [
    RouterModule.forChild(AddPhotoHeaderTemplatesComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,

  ],
  providers: [WorkOrderDrodownServices],
  bootstrap: [AddPhotoHeaderTemplatesComponent]
})

export class AddPhotoHeaderTemplatesModule {}
