import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddCategoryModel} from './AddMainCategoryModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import { HomepageServices } from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddCategoryServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data
  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostMainCateData";


  public CategoryDataPost(Modelobj:AddCategoryModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.Main_Cat_pkeyID = Modelobj.Main_Cat_pkeyID;
    ANYDTO.Main_Cat_Name = Modelobj.Main_Cat_Name;
    ANYDTO.Main_Cat_Back_Color = Modelobj.Main_Cat_Back_Color;

    ANYDTO.Main_Cat_Active  = Modelobj.Main_Cat_Active;
    ANYDTO.Main_Cat_IsActive  = Modelobj.Main_Cat_IsActive;
    ANYDTO.Main_Cat_IsDelete  = Modelobj.Main_Cat_IsDelete;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;
    if(Modelobj.Main_Cat_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.Main_Cat_IsDelete){
      ANYDTO.Type = 4;
    }




    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
//for categary delete
private apiUrlde = BasetUrl.Domain +"api/RESTIPL/DeleteMainCateData";


public deleteCategoryDataPost(Modelobj:AddCategoryModel) {

  debugger;
  var ANYDTO: any = {};
  ANYDTO.Main_Cat_pkeyID = Modelobj.Main_Cat_pkeyID;
 
  ANYDTO.Main_Cat_Active  = 0;
  ANYDTO.Main_Cat_IsActive  = 0;
  ANYDTO.Main_Cat_IsDelete  =1;
  ANYDTO.Type = 4;

  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http
    .post<any>(this.apiUrlde, ANYDTO, { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
      //catchError( this.Errorcall.handleError)
    );
}

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}



