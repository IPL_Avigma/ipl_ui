import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MasterlayoutComponent } from "src/IPLAPP/Home/MasterComponent";
import { AddCategoryModel } from "./AddMainCategoryModel";
import { AddCategoryServices } from "./AddMainCategoryServices";
import { ActivatedRoute } from '@angular/router';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ViewMainCategoryModel } from '../ViewMainCategory/ViewMainCategoryModel';
import { ViewMainCategoryServices } from '../ViewMainCategory/ViewMainCategoryServices';

@Component({
  templateUrl: "./AddMainCategory.html"
})
export class AddMainCategoryComponent implements OnInit {
  submitted = false;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  isLoading = false;

  public contentx;
  MessageFlag: string;

  AddCategoryModelObj: AddCategoryModel = new AddCategoryModel();
  ViewMainCategoryModelObj: ViewMainCategoryModel = new ViewMainCategoryModel();


  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xAddCategoryServices: AddCategoryServices,
    private xViewMainCategoryServices: ViewMainCategoryServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService
  ) {

  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      CategoryName: ["", Validators.required],
      disbledFaltu1: ["", Validators.nullValidator],
      disbledFaltu2: ["", Validators.nullValidator]
    });
    this.getModelData();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }
  // submit form
  

  FormButton(content) {
    this.contentx = content;
    debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    if ( this.AddCategoryModelObj.Main_Cat_Back_Color == "") {
      this.AddCategoryModelObj.Main_Cat_Back_Color = '#000000'
    }
  if (this.ModelObj == undefined) {
    this.AddCategoryModelObj.Main_Cat_pkeyID = 0;
  }else
  {
    this.AddCategoryModelObj.Main_Cat_pkeyID = this.ModelObj;
  }
 
    this.xAddCategoryServices
      .CategoryDataPost(this.AddCategoryModelObj)
      .subscribe(response => {
        debugger;
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddCategoryModelObj.Main_Cat_pkeyID = parseInt(
            response[0].Main_Cat_pkeyID
          );
        

          this.MessageFlag = "Category Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
        else{
          if (response[0] == 'Index was outside the bounds of the array.' ) {
            this.MessageFlag = "Some Think Wrong...!";
            this.commonMessage(this.contentx);
          }
        }

      });
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  IsEditDisable = false;
  getModelData() {
    debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddCategoryModelObj = new AddCategoryModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Main_Cat_pkeyID', id);
    this.ModelObj = parseInt(id);
    this.GetViewData();
    }


    // if (this.ModelObj == undefined) {
    //   this.AddCategoryModelObj = new AddCategoryModel();
    // } else {
    
    //   console.log('yaha se waha',this.ModelObj);
    //   this.AddCategoryModelObj.Main_Cat_pkeyID = this.ModelObj.Main_Cat_pkeyID;
    //   this.AddCategoryModelObj.Main_Cat_Name = this.ModelObj.Main_Cat_Name;
      

    //   this.AddCategoryModelObj.Main_Cat_Active = this.ModelObj.Main_Cat_Active;
    //   this.AddCategoryModelObj.Main_Cat_IsActive = this.ModelObj.Main_Cat_IsActive;
    //   this.AddCategoryModelObj.Main_Cat_IsDelete = this.ModelObj.Main_Cat_IsDelete;
    //   this.AddCategoryModelObj.UserID = this.ModelObj.UserID;


    //   this.formUsrCommonGroup.disable();
    //   this.IsEditDisable = true;

    //   this.button = "Update";
    //   this.AddCategoryModelObj.Type = 2;

    // }
  }

  GetViewData() {
    debugger;
    this.ViewMainCategoryModelObj.Main_Cat_pkeyID = this.ModelObj;
    this.ViewMainCategoryModelObj.Type = 2;
    this.xViewMainCategoryServices
      .ViewmainCategoryData(this.ViewMainCategoryModelObj)
      .subscribe(response => {
        debugger;
        console.log("View Category", response);
        
        this.AddCategoryModelObj.Main_Cat_Name = 
        response[0][0].Main_Cat_Name;

        if (this.ModelObj.Main_Cat_Back_Color != '' ) 
             {
              var str1 = new String( "#" ); 
              var str2 =  response[0][0].Main_Cat_Back_Color; 
              if (str2 == null ) {
                  str2 = '000000'
              }
           this.AddCategoryModelObj.Main_Cat_Back_Color = str1.concat( str2 ); 
           console.log('appanddata'+ this.AddCategoryModelObj.Main_Cat_Back_Color)
  
              }
             
        // this.AddCategoryModelObj.Main_Cat_Back_Color =
        // response[0][0].Main_Cat_Back_Color;
        this.AddCategoryModelObj.Main_Cat_Active =
        response[0][0].Main_Cat_Active;

        this.formUsrCommonGroup.disable();
        this.IsEditDisable = true;
  
        this.button = "Update";
        this.AddCategoryModelObj.Type = 2;
  
    });
  }


  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code
}
