import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddMainCategoryComponent } from './AddMainCategoryComponent';
import { AddCategoryServices } from "./AddMainCategoryServices";

const AddMainCategoryRouts = [
  { path: "addmaincategory/:id", component: AddMainCategoryComponent }
];
@NgModule({
  declarations: [AddMainCategoryComponent],
  imports: [
    RouterModule.forChild(AddMainCategoryRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule

  ],
  providers: [AddCategoryServices],
  bootstrap: [AddMainCategoryComponent]
})

export class AddMainCategoryModule {}
