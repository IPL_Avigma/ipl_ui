import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ViewMainCategoryComponent } from './ViewMainCategoryComponent';
import {ViewMainCategoryServices} from './ViewMainCategoryServices';
import { GridModule } from '@progress/kendo-angular-grid';

const ViewMainCategoryRouts = [
  { path: "viewmaincategory", component: ViewMainCategoryComponent }
];
@NgModule({
  declarations: [ViewMainCategoryComponent],
  imports: [
    RouterModule.forChild(ViewMainCategoryRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    GridModule,

  ],
  providers: [ViewMainCategoryServices],
  bootstrap: [ViewMainCategoryComponent]
})

export class ViewMainCategoryModule {}
