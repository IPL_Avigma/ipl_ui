import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { ViewMainCategoryServices } from "./ViewMainCategoryServices";
import { ViewMainCategoryModel } from "./ViewMainCategoryModel";

import { AddCategoryServices } from "../AddMainCategory/AddMainCategoryServices";
import { AddCategoryModel } from "../AddMainCategory/AddMainCategoryModel";

import { State } from "@progress/kendo-data-query";
import {DataStateChangeEvent} from "@progress/kendo-angular-grid";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewMainCategory.html"
})
export class ViewMainCategoryComponent implements OnInit {
  ViewMainCategoryModelObj: ViewMainCategoryModel = new ViewMainCategoryModel();
  AddCategoryModelObj: AddCategoryModel = new AddCategoryModel();
  public griddata: any[];

  constructor(
    private xViewMainCategoryServices: ViewMainCategoryServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private EncrDecr: EncrDecrServiceService,
    private xAddCategoryServices:AddCategoryServices,
  ) {
    this.GetGridData();
  }

  ngOnInit() {}
  //get grid
  GetGridData() {
    this.xViewMainCategoryServices
      .ViewmainCategoryData(this.ViewMainCategoryModelObj)
      .subscribe(response => {
        console.log("resp caetee", response);
        this.griddata = response[0];
      });
  }

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/category/addmaincategory"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Main_Cat_pkeyID);
    this.xRouter.navigate(["/category/addmaincategory", btoa(encrypted)]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AddCategoryModelObj.Main_Cat_pkeyID = dataItem.Main_Cat_pkeyID;
      this.AddCategoryModelObj.Main_Cat_IsDelete = true;

      this.xAddCategoryServices
        .deleteCategoryDataPost(this.AddCategoryModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          this.GetGridData();
        });
    }
  }

  // clear data
  AddNewRecords() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/category/addmaincategory", 'new']);
  }

      // common code
      FilterCall() {
        //alert(JSON.stringify(this.filterMasterModelObj));
        // this.ViewUserModelObj.Type = 4;
        // this.xViewUserServices
        //   .ViewUserData(this.ViewUserModelObj)
        //   .subscribe(response => {
        //     console.log("User Details", response);
        //     this.griddata = response[0];
        //   });
      }

      ClearData() {
        this.ViewMainCategoryModelObj = new ViewMainCategoryModel();
        this.GetGridData();
      }
      SaveFilterData() {
        alert("save called");
      }




       //kendo check box event action

     public state: State = {};

     public dataStateChange(state: DataStateChangeEvent): void {
       this.state = state;


     }
}
