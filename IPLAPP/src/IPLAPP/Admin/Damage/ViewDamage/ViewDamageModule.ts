import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ViewDamageComponent } from './ViewDamageComponent';

import {ViewDamageServices} from './ViewDamageServices';
import {AddDamageServices} from '../AddDamage/AddDamageServices';



const ViewDamageComponentRouts = [
  { path: "viewdamage", component: ViewDamageComponent }
];


@NgModule({
  declarations: [ViewDamageComponent],
  imports: [
    RouterModule.forChild(ViewDamageComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    GridModule,


  ],
  providers: [ViewDamageServices,AddDamageServices],
  bootstrap: [ViewDamageComponent]
})

export class ViewDamageModule {}
