import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { AddDamageModel, AddApplianceModel } from '../AddDamage/AddDamageModel';
import { BasetUrl } from "../../../Utility/DomainUrl";
import { HomepageServices } from "../../../Home/HomeServices";



@Injectable({
  providedIn: "root"
})
export class ViewDamageServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetDamageMaster";

  public ViewDamageData(Modelobj: AddDamageModel) {

    //debugger;
    var ANYDTO: any = {};


    ANYDTO.Damage_pkeyID = Modelobj.Damage_pkeyID;

    ANYDTO.Type =  Modelobj.Type;
    if(Modelobj.Damage_pkeyID != 0){

      ANYDTO.Type =  2;
    }


    // ANYDTO.User_pkeyID = Modelobj.User_pkeyID;

    // Modelobj.MenuID = 1;
    // Modelobj.UserID = 1;

    // var obj = {
    //   User_FirstName: Modelobj.User_FirstName,

    //   User_IsActive: Modelobj.User_IsActive
    // };

    // ANYDTO.SearchMaster = {
    //   UserID: Modelobj.UserID,
    //   MenuID: Modelobj.MenuID
    // };
    // ANYDTO.WhereClause = Modelobj.WhereClause = "";
    // ANYDTO.FilterData = JSON.stringify(obj);
    // ANYDTO.Type = Modelobj.Type;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  private apiUrlpost = BasetUrl.Domain + "api/WOCTASK/PostApplicantData";

  public AddApplianceData(Modelobj: AddApplianceModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.ApplianceMasterDTO = Modelobj.Appliancearr;
    ANYDTO.Appl_Wo_Id= Modelobj.Appl_Wo_Id;
    ANYDTO.Type = 1;


    // if(Modelobj.Appl_pkeyId != 0){

    //   ANYDTO.Type =  2;
    // }

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlpost, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }




  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert("Unauthorized User...");
      window.location.href = '/admin/login';
    } else {
      alert("Invalid User...");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Invalid request, please try again later...");
  }
}
