import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { AddDamageModel } from "../AddDamage/AddDamageModel";
import { AddDamageServices } from "../AddDamage/AddDamageServices";
import { ViewDamageServices } from "./ViewDamageServices";
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';

@Component({
  templateUrl: "./ViewDamage.html"
})
export class ViewDamageComponent implements OnInit {
  public griddata: any[];
  AddDamageModelObj: AddDamageModel = new AddDamageModel();

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xAddDamageServices: AddDamageServices,
    private xViewDamageServices: ViewDamageServices,
    private EncrDecr: EncrDecrServiceService,
  ) {
    this.GetGridData();
  }

  ngOnInit() {}

  // clear data
  AddNewDamages() {
    // var faltu = undefined;

    // this.xMasterlayoutComponent.masterFunctionCall(faltu);

    this.xRouter.navigate(["/damage/adddamage", 'new']);
  }

  // this code selected event row
  showDetails(event, dataItem) {
    debugger;
    console.log(dataItem);
    // this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    // this.xRouter.navigate(["/damage/adddamage"]);
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', dataItem.Damage_pkeyID);
    this.xRouter.navigate(["/damage/adddamage", btoa(encrypted)]);
  }
  //end
  // common code End
  deleteDetails(event, dataItem) {
    //debugger;
    var cfrm = confirm("Delete this Record...!");
    if (cfrm == true) {
      this.AddDamageModelObj.Damage_pkeyID = dataItem.Damage_pkeyID;
      this.AddDamageModelObj.Damage_IsDelete = true;

      this.xAddDamageServices
        .AddDamageDataPost(this.AddDamageModelObj)
        .subscribe(response => {
          console.log("delete response", response);
          // be care fully
          this.AddDamageModelObj.Type = 1;
          this.AddDamageModelObj.Damage_pkeyID = 0;
          this.GetGridData();
        });
    }
  }

  //get grid
  GetGridData() {
    this.xViewDamageServices
      .ViewDamageData(this.AddDamageModelObj)
      .subscribe(response => {
        console.log("resp damage", response);
        this.griddata = response[0];
      });
  }
}
