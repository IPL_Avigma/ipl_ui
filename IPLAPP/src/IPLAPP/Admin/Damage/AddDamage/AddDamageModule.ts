import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AddDamageComponent } from './AddDamageComponent';
import {AddDamageServices} from './AddDamageServices';
import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

const AddDamageComponentRouts = [
  { path: "adddamage/:id", component: AddDamageComponent }
];

@NgModule({
  declarations: [AddDamageComponent],
  imports: [
    RouterModule.forChild(AddDamageComponentRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    CommonDirectiveModule,


  ],
  providers: [AddDamageServices],
  bootstrap: [AddDamageComponent]
})

export class AddDamageModule {}
