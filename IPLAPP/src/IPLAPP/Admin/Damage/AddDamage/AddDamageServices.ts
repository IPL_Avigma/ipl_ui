import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddDamageModel} from './AddDamageModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import { HomepageServices } from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddDamageServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostDamageMaster";


  public AddDamageDataPost(Modelobj:AddDamageModel) {

    //debugger;
    var ANYDTO: any = {};
    ANYDTO.Damage_pkeyID = Modelobj.Damage_pkeyID;
    ANYDTO.Damage_Type = Modelobj.Damage_Type;
    ANYDTO.Damage_Int  = Modelobj.Damage_Int;
    ANYDTO.Damage_Location  = Modelobj.Damage_Location;
    ANYDTO.Damage_Qty  = Modelobj.Damage_Qty;
    ANYDTO.Damage_Estimate  = Modelobj.Damage_Estimate;
    ANYDTO.Damage_Disc  = Modelobj.Damage_Disc;
    ANYDTO.Damage_IsActive  = Modelobj.Damage_IsActive;
    ANYDTO.Damage_IsDelete  = Modelobj.Damage_IsDelete;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;
    if(Modelobj.Damage_pkeyID != 0){
      ANYDTO.Type = 2;
    }
    if(Modelobj.Damage_IsDelete){
      ANYDTO.Type = 4;
    }




    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }



  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Invalid request, please try again later...");
  }
}

