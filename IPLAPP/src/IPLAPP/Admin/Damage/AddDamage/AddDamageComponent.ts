import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";

import { AddDamageModel } from "./AddDamageModel";
import { AddDamageServices } from "./AddDamageServices";
import { ActivatedRoute } from '@angular/router';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ViewDamageModel } from '../ViewDamage/ViewDamageModel';
import { ViewDamageServices } from '../ViewDamage/ViewDamageServices';

@Component({
  templateUrl: "./AddDamage.html"
})
export class AddDamageComponent implements OnInit {
  
  AddDamageModelObj: AddDamageModel = new AddDamageModel();
  ViewDamageModelObj: ViewDamageModel = new ViewDamageModel();
  
  DamageTypeList: any;
  IntExtList: any;

  submitted = false;
  formUsrCommonGroup: FormGroup;
  button = "Save";
  isLoading = false;

  public contentx;
  MessageFlag: string;

  // only drop down
  dropCkck = false; // common

  DamageValFlag = false;
  ExtValFlag = false;

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xAddDamageServices: AddDamageServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xViewDamageServices: ViewDamageServices
  ) {
    this.DamageTypeList = [
      { Id: "1", Name: "Option1" },
      { Id: "2", Name: "Option2" }
    ];
    this.IntExtList = [{ Id: "1", Name: "Int" }, { Id: "2", Name: "Ext" }];
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      DamageName: ["", Validators.required],
      DamageInt: ["", Validators.nullValidator],
      DamageLocation: ["", Validators.nullValidator],
      DamageQuantity: ["", Validators.nullValidator],
      DamageEstimate: ["", Validators.nullValidator],
      DamageDiscription: ["", Validators.nullValidator],
      DamageActive: ["", Validators.nullValidator],
    });
    this.getModelData();
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    //debugger;
    this.submitted = true;

    ////
    // only drop down
    this.dropCkck = false;

    // if (this.AddDamageModelObj.Damage_Type == "0") {
    //   this.DamageValFlag = true;
    //   this.dropCkck = true;
    // }
    if (this.AddDamageModelObj.Damage_Int == "0") {
      this.ExtValFlag = true;
      this.dropCkck = true;
    }

    if (this.dropCkck) {
      return;
    }

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    
    if(this.WorkOrderObj !== undefined) {
      this.AddDamageModelObj.Damage_pkeyID = this.WorkOrderObj;
    } else {
      this.AddDamageModelObj.Damage_pkeyID = 0;
    }

    this.xAddDamageServices
      .AddDamageDataPost(this.AddDamageModelObj)
      .subscribe(response => {
        console.log("resp data", response);
        if (response[0].Status != "0") {
          this.AddDamageModelObj.Damage_pkeyID = parseInt(
            response[0].Damage_pkeyID
          );

          this.MessageFlag = "Damage Data Saved...!";
          this.isLoading = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
        }
      });
  }

  Damage_Method() {
    this.DamageValFlag = false;
  }
  Int_Method() {
    this.ExtValFlag = false;
  }
  // btn code end
  // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  WorkOrderObj: any;
  IsEditDisable = false;
  getModelData() {
    debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddDamageModelObj = new AddDamageModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Damage_pkeyID', id);
    this.WorkOrderObj = parseInt(id);
    this.GetSingleData();
   
    }
  }

  //   if (this.ModelObj == undefined) {
  //     this.AddDamageModelObj = new AddDamageModel();
  //   } else {
     
  //     // this.AddDamageModelObj.Damage_pkeyID = this.ModelObj.Damage_pkeyID;
  //     // this.AddDamageModelObj.Damage_Type = this.ModelObj.Damage_Type;
  //     // this.AddDamageModelObj.Damage_Int = this.ModelObj.Damage_Int;
  //     // this.AddDamageModelObj.Damage_IntName = this.ModelObj.Damage_IntName;
  //     // this.AddDamageModelObj.Damage_Location = this.ModelObj.Damage_Location;
  //     // this.AddDamageModelObj.Damage_Qty = this.ModelObj.Damage_Qty;
  //     // this.AddDamageModelObj.Damage_Estimate = this.ModelObj.Damage_Estimate;
  //     // this.AddDamageModelObj.Damage_Disc = this.ModelObj.Damage_Disc;
  //     // this.AddDamageModelObj.Damage_IsActive = this.ModelObj.Damage_IsActive;
  //     // this.AddDamageModelObj.Damage_IsDelete = this.ModelObj.Damage_IsDelete;

    
  //   }
  // }


  GetSingleData() {
    debugger;
    this.AddDamageModelObj.Damage_pkeyID = this.WorkOrderObj;
    this.AddDamageModelObj.Type = 2;
    this.xViewDamageServices
      .ViewDamageData(this.AddDamageModelObj)
      .subscribe(response => {
        console.log("view damage", response);
        
        this.AddDamageModelObj.Damage_Type = 
        response[0][0].Damage_Type;
        this.AddDamageModelObj.Damage_Int =
        response[0][0].Damage_Int;
        this.AddDamageModelObj.Damage_Location =
        response[0][0].Damage_Location;
        this.AddDamageModelObj.Damage_Qty =
        response[0][0].Damage_Qty;
        this.AddDamageModelObj.Damage_Estimate =
        response[0][0].Damage_Estimate;
        this.AddDamageModelObj.Damage_Disc =
        response[0][0].Damage_Disc;
        this.AddDamageModelObj.Damage_IsActive =
        response[0][0].Damage_IsActive;
        
        
        this.formUsrCommonGroup.disable();
        this.IsEditDisable = true;
    
        this.button = "Update";
        // this.AddDamageModelObj.Type = 2;
    });
  }
  // end code
  EditForms() {
    this.IsEditDisable = false;
    this.formUsrCommonGroup.enable();
  }
  // end code
}
