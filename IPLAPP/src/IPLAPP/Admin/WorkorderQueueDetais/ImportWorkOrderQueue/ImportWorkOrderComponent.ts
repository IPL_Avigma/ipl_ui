
import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import {ImportWorkOrderQueueDetailsModel} from "./ImportWorkOrderModel";

import{ImportWorkOrderQueueDetailServices} from "./ImportWorkOrderServices";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { from , Observable} from 'rxjs';
import {map} from "rxjs/operators";
import { __values } from 'tslib';
import { itemAt } from '@progress/kendo-angular-grid/dist/es2015/data/data.iterators';
import { anyChanged } from '@progress/kendo-angular-common';

@Component({
  templateUrl: "./ImportWorkOrder.html"
})
export class ImportWorkOrderQueueDetailsComponent  implements OnInit {
  
    ImportWorkOrderQueueDetailsModelObj: ImportWorkOrderQueueDetailsModel = new ImportWorkOrderQueueDetailsModel();
   ;
  public griddata: any[];
  public contentx; 
  MessageFlag: string; 


  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private modalService: NgbModal,
    private xImportWorkOrderQueueDetailServices : ImportWorkOrderQueueDetailServices,

 
  ) {

    this.GetGridData();
  }

  ngOnInit() {

  }
  showDetails(event, dataItem) {
    debugger;
    this.xMasterlayoutComponent.masterFunctionCall(dataItem);
    this.xRouter.navigate(["/queueworkorder/importworkorderqueue"]);
  }
 
  IsDisable: any;
  //get grid
  GetGridData() {
    debugger;
    this.xImportWorkOrderQueueDetailServices
      .WorkOrderimportQueueData(this.ImportWorkOrderQueueDetailsModelObj)
      .subscribe(response => {
        debugger;
        console.log("resp Importworkorderqueue", response);
        this.griddata = response[0];
        // let Wo_Count = response[0][0].Imrt_Wo_Count
        // console.log('xgh',Wo_Count)
      //  this.data();
       
      });
  } 
  // data(){
  //   debugger;
  //   this.griddata.map((value,index) =>{ 
  //     if(value.Imrt_Wo_Count != 0){
  //       this.IsDisable = false;
  //     }
  //     else {
  //       this.IsDisable = true;
  //     }
  //   });
  // }
   

  
  

  //Delete queue

  DeleteQueued(item){
    let Cnfm = confirm("Are you sure you want to remove this record..?");    
    if (Cnfm) {
   
    this.ImportWorkOrderQueueDetailsModelObj.Imrt_PkeyId = item.Imrt_PkeyId;
    this.ImportWorkOrderQueueDetailsModelObj.Type = 4;

    this.xImportWorkOrderQueueDetailServices
      .DeleteWorkOrderimportQueueData(this.ImportWorkOrderQueueDetailsModelObj)
      .subscribe(response => {
        console.log("Del Importworkorderqueue", response);
      
      });
     
    }
    this.GetGridData( );
  }
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }

  CkeckNowData(item){
    console.log('item',item);
   debugger;

  
    this.ImportWorkOrderQueueDetailsModelObj.Imrt_PkeyId = item.Imrt_PkeyId;
    this.ImportWorkOrderQueueDetailsModelObj.Imrt_Wo_Import_ID = item.Imrt_Wo_Import_ID;
    this.ImportWorkOrderQueueDetailsModelObj.Type = 5;
    this.xImportWorkOrderQueueDetailServices
    .CheckNowWorkOrderimportQueueData(this.ImportWorkOrderQueueDetailsModelObj)
    .subscribe(response => {
      debugger;
      console.log("UpdateChecknow", response);
      
    });
    this.GetGridData();
  }

//   redirect()
// {
//   this.xRouter.navigate(["/autoimport/autoimportwo"]);
// }
redirectview(){
  this.xRouter.navigate(["/autoimport/viewautoimport"]);
}
  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

  }
}
