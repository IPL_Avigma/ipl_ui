export class ImportWorkOrderQueueDetailsModel{
   
    Imrt_PkeyId : Number = 0;
    Imrt_Wo_ID : string = '';
    Imrt_Wo_Import_ID : Number = 0;
    Client_Company_Name : string = '';
    WI_FriendlyName : string = '';
    Imrt_Wo_Count : Number = 0;
    Imrt_Last_Run : Number = 0;
    Imrt_Next_Run : Number = 0;
    Imrt_Status_Msg : Number = 0;
    Imrt_State_Filter : Number = 0;
    Imrt_Active : Number = 0;
    Imrt_IsDelete : Number = 0;
    UserID : Number = 0;
    Type : Number = 1;
    UserId : Number = 0;
    
    }

 
    
  