import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {ImportWorkOrderQueueDetailsModel} from './ImportWorkOrderModel'
import { BasetUrl } from "../../../Utility/DomainUrl";
import { HomepageServices } from "../../../Home/HomeServices";



@Injectable({
  providedIn: "root"
})
export class ImportWorkOrderQueueDetailServices {
  public token: any;

  constructor(private _http: HttpClient, private _Route: Router,private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlimGet = BasetUrl.Domain + "api/RESTIPL/GetWorkOrderImportQueueTrans";

  public WorkOrderimportQueueData(Modelobj: ImportWorkOrderQueueDetailsModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.Imrt_PkeyId = 0;
    ANYDTO.Type =  1;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlimGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////// for Import Queue Delete
  private apiUrldel = BasetUrl.Domain + "api/RESTIPL/DeleteWorkOrderImportQueueTrans";

  public DeleteWorkOrderimportQueueData(Modelobj: ImportWorkOrderQueueDetailsModel) {

    //debugger;
    var ANYDTO: any = {};
    ANYDTO.Imrt_PkeyId = Modelobj.Imrt_PkeyId;
    ANYDTO.Type = 4;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrldel, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  //////////////////////
  private apiUrlchk = BasetUrl.Domain + "api/RESTIPL/PostWorkOrderImportQueueTrans";

  public CheckNowWorkOrderimportQueueData(Modelobj: ImportWorkOrderQueueDetailsModel) {

    //debugger;
    var ANYDTO: any = {};
    ANYDTO.Imrt_PkeyId = Modelobj.Imrt_PkeyId;
    ANYDTO.Imrt_Wo_Import_ID = Modelobj.Imrt_Wo_Import_ID;
    ANYDTO.Type = 5;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlchk, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }


/////////////////

  // common handler
  private handleError(error: HttpErrorResponse) {
   // alert("Something bad happened. please try again later...😌");
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
    alert("Invalid Request...");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
