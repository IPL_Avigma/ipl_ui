import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {ImportWorkOrderQueueDetailsComponent} from './ImportWorkOrderComponent';

const ImportWorkOrderQueueRouts =[

  {path:'importqueuedata',component:ImportWorkOrderQueueDetailsComponent}

]

@NgModule({
  declarations: [ImportWorkOrderQueueDetailsComponent],
  imports: [
    RouterModule.forChild(ImportWorkOrderQueueRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule,

  ],
  providers: [],
  bootstrap: [ImportWorkOrderQueueDetailsComponent]
})

export class ImportWorkOrderImportQueueDetailsModule {}
