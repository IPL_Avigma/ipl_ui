import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";

import{WorkOrderImportQueueModel, ImportWorkOrderDataModel} from '../WorkOrderImportQueue/WorkOrderImportQueueModel';
import{ImportWorkOrderQueueDetailsModel} from '../ImportWorkOrderQueue/ImportWorkOrderModel';

import { BasetUrl } from "../../../Utility/DomainUrl";
import { HomepageServices } from "../../../Home/HomeServices";



@Injectable({
  providedIn: "root"
})
export class WorkOrderImportQueueServices {
  public token: any;

  constructor(private _http: HttpClient, private _Route: Router,private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlqueueGet = BasetUrl.Domain + "api/WorkOrderImport/GetWorkOrderQueuedata";

  public importQueuedata(Modelobj: ImportWorkOrderQueueDetailsModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.Imrt_PkeyId = Modelobj.Imrt_PkeyId;
    ANYDTO.Type =  3;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlqueueGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.handleError)
        //catchError( this.Errorcall.handleError)
      );
  }


  ////////////////// Import workOder Data on button click
  private apiUrlwoda = BasetUrl.Domain + "api/WorkOrderImport/PostImportWorkOderDetailData";

  public ImportWorkOrderDetailsData(Modelobj: ImportWorkOrderDataModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.ImportWoArray = Modelobj.ImportWoArray;
    ANYDTO.WorkType = Modelobj.WorkTypelst;
    ANYDTO.Contractor = Modelobj.Contractorlst;
    ANYDTO.Processor = Modelobj.Processorlst;
    ANYDTO.cordinator = Modelobj.cordinatorlst;
    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.Type =  1;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlwoda, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.handleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
    } else {
    alert("Invalid Request...");
    }
    // alert("Something bad happened. please try again later...😌");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
