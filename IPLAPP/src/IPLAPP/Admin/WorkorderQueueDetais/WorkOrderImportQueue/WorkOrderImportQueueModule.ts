import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {WorkOrderImportQueueComponent} from './WorkOrderImportQueueComponent';

const ImportWorkOrderRouts =[

  {path:'importworkorderqueue',component:WorkOrderImportQueueComponent}

]

@NgModule({
  declarations: [WorkOrderImportQueueComponent],
  imports: [
    RouterModule.forChild(ImportWorkOrderRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    GridModule,

  ],
  providers: [],
  bootstrap: [WorkOrderImportQueueComponent]
})

export class WorkOrderImportQueueModule {}
