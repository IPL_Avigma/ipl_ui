export class WorkOrderImportQueueModel{

WorkOrderItem_PkeyId: Number = 0;
Description: String = '';
Qty: String = '';
Price: String = '';
Total: String = '';
Additional_Instructions: String = '';
WorkOrderMaster_API_Data_Pkey_Id : Number = 0;
IsActive: boolean = false;
IsDeleted: boolean = false;
UserId : Number = 0;
Type : Number = 1;

}
export class ImportWorkOrderDataModel{
    Pkey_Id : Number = 0;
    UserID : Number = 0;
    Type : Number = 1;
    cordinatorlst: Number = 0;
    Contractorlst: Number = 0;
    WorkTypelst: Number = 0;
    Processorlst: Number = 0;
   ImportWoArray : any;
       }