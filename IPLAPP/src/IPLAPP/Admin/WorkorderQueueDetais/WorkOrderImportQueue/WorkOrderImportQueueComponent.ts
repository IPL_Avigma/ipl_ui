
import { Component, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import {WorkOrderImportQueueModel,ImportWorkOrderDataModel} from "./WorkOrderImportQueueModel";
import{ImportWorkOrderQueueDetailsModel} from '../ImportWorkOrderQueue/ImportWorkOrderModel';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import{WorkOrderImportQueueServices} from "./WorkOrderImportQueueServices";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import { from } from 'rxjs';

@Component({
  templateUrl: "./WorkOrderImportQueue.html"
})
export class WorkOrderImportQueueComponent  implements OnInit {
  
    WorkOrderImportQueueModelObj: WorkOrderImportQueueModel = new WorkOrderImportQueueModel();
    ImportWorkOrderQueueDetailsModelObj : ImportWorkOrderQueueDetailsModel = new ImportWorkOrderQueueDetailsModel();
    ImportWorkOrderDataModelObj : ImportWorkOrderDataModel = new ImportWorkOrderDataModel();
   
  public griddata: any[];
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  formUsrCommonGroup: FormGroup;
  dropCkck = false;
  cordinatorvalFlag = false;
  contractorvalFlag = false;
  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xmodalService: NgbModal,
    private xRouter: Router,
    private xWorkOrderImportQueueServices : WorkOrderImportQueueServices,
    private formBuilder: FormBuilder,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices
 
  ) {
    this.GetCompanyDropDown();
    //drd call
    // this.GetGridDetails();
  }

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      cordinatorval: ["", Validators.required],
      contractorval: ["", Validators.required],
    
    
     });
     this.GetGridData();
  }

  get fx() {
    return this.formUsrCommonGroup.controls;
  }
 

 
  GetModel:any;
  jsondatalabel:any;
  instructiondata:any;
  //get grid
  GetGridData() {
    debugger;
    this.GetModel = this.xMasterlayoutComponent.masterFunctionGetdata();
   if (this.GetModel == undefined) 
   {
    this.xRouter.navigate(["/queueworkorder/importqueuedata"]);
   }
   else{
    this.ImportWorkOrderQueueDetailsModelObj.Imrt_PkeyId =  this.GetModel.Imrt_PkeyId;
    this.xWorkOrderImportQueueServices
      .importQueuedata(this.ImportWorkOrderQueueDetailsModelObj)
      .subscribe(response => {
        console.log("resp workorderqueue", response);
        debugger;
        this.jsondatalabel = response[0];
        this.instructiondata =  response[0].WorkOrderItemDetail;
       // this.griddata = response[1];
      
       // this.ImportWorkOrderQueueDetailsModelObj = this.jsondatalabel;

      });

   }
  
  }


  //kendo check box event action
  public state: State = {};
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;

  }

//import work order details data on button click
ImportArray = [];
isdisable = true;
SelectedImportWo(val,i)
{
  console.log('mohittestimport', val )
  if (val.chkflag == true) {
    this.ImportArray.push(val)
    this.isdisable = false;
  }

  else{
    this.ImportArray.splice(i, 1);
  }
  console.log('arraypush', this.ImportArray )
   
  }

  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }


  chkflag : any;
ImportWorkdata(content){
  debugger;
  this.dropCkck = false;

  if (this.ImportWorkOrderDataModelObj.cordinatorlst == 0) {
    this.cordinatorvalFlag = true;
    //return;
    this.dropCkck = true;
  }
  if (this.ImportWorkOrderDataModelObj.Contractorlst == 0) {
    this.contractorvalFlag = true;
    //return;
    this.dropCkck = true;
  }

  debugger;
  this.contentx = content;
  this.ImportWorkOrderDataModelObj.ImportWoArray =  this.ImportArray;

  console.log('importarr', this.ImportArray);

  this.xWorkOrderImportQueueServices
      .ImportWorkOrderDetailsData(this.ImportWorkOrderDataModelObj)
      .subscribe(response => {
        this.ImportArray = [];
        console.log("Import Button", response);
        this.chkflag = false;
        this.isdisable = true;
        this.MessageFlag = "Work Order Import Updated...!";
        this.commonMessage(this.contentx);
        this.GetGridData();
      });

}

contractor_Method(){
  this.contractorvalFlag = false;
}
cordinator_Method(){
  this.cordinatorvalFlag = false;
}
contractorlst : any;
cordinatorlst : any;
Worktypelst:any;
Processorlst:any;
GetCompanyDropDown() {
  this.xWorkOrderDrodownServices.DropdownGetWorkOrder().subscribe(response => {
    console.log("dropdowns", response);
  this.contractorlst = response[3];
  this.cordinatorlst = response[4];
  this.Worktypelst = response[1];
  this.Processorlst = response[5];
  });
}


}
