
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {AddInvoiceItemsModel} from './AddInvoiceItemsModel';
import {BasetUrl} from '../../../Utility/DomainUrl';
import { HomepageServices } from '../../../Home/HomeServices';


@Injectable({
  providedIn: "root"
})
export class AddInvoiceItemsServices {

  public token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  private apiUrlPOST = BasetUrl.Domain +"api/RESTIPL/PostTaskMasterFilterData";


  public FilterDataPost(Modelobj:AddInvoiceItemsModel) {

    //debugger;
    var ANYDTO: any = {};
    ANYDTO.Task_pkeyID = Modelobj.Task_pkeyID;
    ANYDTO.ArrayCustomPriceFilter = Modelobj.ArrayCustomPriceFilter;
    ANYDTO.ArrayDocument  = Modelobj.ArrayDocument;
    ANYDTO.ArrayPreset  = Modelobj.ArrayPreset;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;





    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

//////////////////////////////////////////////////////////
private apiUrlPOSTx = BasetUrl.Domain +"api/RESTIPL/GetTaskMasterFilterData";


  public FilterDataGet(Modelobj:AddInvoiceItemsModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.Task_pkeyID = Modelobj.Task_pkeyID;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOSTx, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
///////////////delete task child data

private apiUrldel = BasetUrl.Domain +"api/RESTIPL/DeleteTaskSettingChildMaster";


  public Deletetaskchild(Modelobj:AddInvoiceItemsModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.Task_sett_pkeyID = Modelobj.Task_sett_pkeyID;
    ANYDTO.Type = 4;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrldel, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
////////// 2 table child record delete

private apiUrldel2 = BasetUrl.Domain +"api/RESTIPL/DeleteWorkTypeTaskChildMaster";


  public Deletetaskchilddetail(Modelobj:AddInvoiceItemsModel) {

    debugger;
    var ANYDTO: any = {};
    ANYDTO.WT_Task_pkeyID = Modelobj.WT_Task_pkeyID;
    ANYDTO.Type = 4;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrldel2, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  //////////////////////////////////////////////////////////

  private apiUrlDelte = BasetUrl.Domain +"api/RESTIPL/DeletePresetTextTaskMasterFilterData";


  public PreTextDeleteDataPost(Modelobj:AddInvoiceItemsModel) {

    //debugger;
    let ANYDTO: any = {};
    ANYDTO.Task_Preset_pkeyId = Modelobj.Task_Preset_pkeyId;
    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.Type = 4;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlDelte, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

//////////////////////////////////////////////////////////

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert("Unauthorized User...");
      window.location.href = '/admin/login';
    } else {
      alert("Invalid Request...");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}


