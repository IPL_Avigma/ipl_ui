export class AddInvoiceItemsModel {
  Item_pkeyID: Number = 0;
  Item_Name: String = "";
  Item_Bid: Number = 0;
  Item_AlwaysShowClientWo: Boolean = true;
  Item_RequiredClientWO: Boolean = false;
  Item_LotSize: Number = 0;
  Item_Through: String = "";
  Item_AutoInvoiceClient: Number = 0;
  Item_Active: Number = 0;
  Item_IsActive: Boolean = false;
  UserID: Number = 0;
  Type: Number = 1;


  Task_pkeyID:Number = 0;
  Task_Pkey: Number = 0;

  Client_compay:Number =0;

  ArrayCustomPriceFilter:any;
  ArrayDocument:any;
  ArrayPreset:any;

  WT_Task_ID:Number = 0;

  Task_Preset_pkeyId: Number = 0;
  Task_sett_pkeyID: Number = 0;
  WT_Task_pkeyID: Number = 0;
}
