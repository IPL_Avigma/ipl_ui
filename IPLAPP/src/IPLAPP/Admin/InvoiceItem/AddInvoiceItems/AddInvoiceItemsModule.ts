import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

import { AddInvoiceItemsComponent } from './AddInvoiceItemsComponent';
import { BidInvoiceItemServices } from "../../BidInvoiceItem/BidInvoiceItemServices";
import {AddInvoiceItemsServices} from './AddInvoiceItemsServices';
import { UploadModule } from '@progress/kendo-angular-upload';
import { PanelBarModule } from '@progress/kendo-angular-layout';




const AddInvoiceItemsRouts =[

  {path:"addinvoiceitems/:id",component:AddInvoiceItemsComponent}

]



@NgModule({
  declarations: [AddInvoiceItemsComponent],
  imports: [
    RouterModule.forChild(AddInvoiceItemsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    NgMultiSelectDropDownModule.forRoot(),
    CommonDirectiveModule,
    UploadModule,
    PanelBarModule
  ],
  providers: [BidInvoiceItemServices,AddInvoiceItemsServices],
  bootstrap: [AddInvoiceItemsComponent]
})

export class AddInvoiceItemsModule {}
