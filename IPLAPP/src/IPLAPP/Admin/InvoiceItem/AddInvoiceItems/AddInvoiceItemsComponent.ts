import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { AddInvoiceItemsModel } from "./AddInvoiceItemsModel";
import { WorkOrderDrodownServices } from "../../../CommonServices/commonDropDown/DropdownService";
import { MasterlayoutComponent } from "../../../Home/MasterComponent";
import { BidInvoiceItemServices } from "../../BidInvoiceItem/BidInvoiceItemServices";

import { BidInvoiceItemModel } from "../../BidInvoiceItem/BidInvoiceItemModel";
import { AddInvoiceItemsServices } from "./AddInvoiceItemsServices";
import { BidInvoiceItemViewTaskServices } from '../../BidInvoiceItemViewTask/BidInvoiceItemViewTaskServices';
import { EncrDecrServiceService } from 'src/app/encr-decr-service.service';
import { ClientResultOldPhotoServices } from 'src/IPLAPP/ClientResult/ClientResultPhoto/ClientResultphotosOldServices';
import { ClientResultPhotoModel } from 'src/IPLAPP/ClientResult/ClientResultPhoto/ClientResultPhotoModel';
import { BasetUrl } from 'src/IPLAPP/Utility/DomainUrl';
import { BindDataModel } from 'src/IPLAPP/ClientResult/ClientResults/ClientResultModel';
import { SigleEditBoxModel, ClientResultInstructionModel } from 'src/IPLAPP/ClientResult/ClientResultInstruction/ClientResultInstructionModel';
import { ClientResultInstructionServices } from 'src/IPLAPP/ClientResult/ClientResultInstruction/ClientResultInstructionServices';

@Component({
  templateUrl: "./AddInvoiceItems.html",
  styleUrls: ['./AddInvoiceCustom.scss']
})
export class AddInvoiceItemsComponent implements OnInit {
  FormArrayVal = [];
  FormArrayValDocument = [];
  StateArray: any;
  FormArrayPresetVal: any;
  ShowonBidArray: any;
  ISActiveArray: any;
  StateList: any;
  CustomerNumberList: any;
  TaskTypeList: any;

  submitted = false; // submitted;
  formUsrCommonGroup: FormGroup;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
 isdisable = false;

  public contentx;
  MessageFlag: string;

  //dropdownList = [];
  myarrray: any;

  dropCkck = false; // common dropdown
  Item_BidValFlag = false;
  Active_ValFlag = false;

  AddInvoiceItemsModelObj: AddInvoiceItemsModel = new AddInvoiceItemsModel();

  // this form update task settings
  BidInvoiceItemModelObj: BidInvoiceItemModel = new BidInvoiceItemModel();
  ClientResultPhotoModelObj: ClientResultPhotoModel = new ClientResultPhotoModel();
  BindDataModelObj: BindDataModel = new BindDataModel();
  SigleEditBoxModelObj: SigleEditBoxModel = new SigleEditBoxModel();
  ClientResultInstructionModelObj: ClientResultInstructionModel = new ClientResultInstructionModel();


  private apiUrlGet =
  BasetUrl.Domain + "api/RESTIPLUPLOAD/PostUserDocumentUserImageBackground"; ///api/MultiPhoto/Save

  uploadSaveUrl = ""; // should represent an actual API endpoint
  uploadRemoveUrl = "removeUrl"; // should represent an actual API endpoint

  constructor(
    private formBuilder: FormBuilder,
    private xmodalService: NgbModal,
    private modalService: NgbModal,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xBidInvoiceItemServices: BidInvoiceItemServices,
    private xRouter: Router,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xAddInvoiceItemsServices: AddInvoiceItemsServices,
    private xBidInvoiceItemViewTaskServices: BidInvoiceItemViewTaskServices,
    private xClientResultOldPhotoServices: ClientResultOldPhotoServices,
    private xClientResultInstructionServices: ClientResultInstructionServices
  ) {

    this.uploadSaveUrl = this.apiUrlGet;

    this.FormArrayVal = [
      {
        Task_sett_State: [],
        Task_sett_Country: [],
        Task_sett_Zip: null,
        Task_sett_Customer: [],
        Task_sett_Contractor: [],
        Task_sett_Company: [],
        Task_sett_Lone: [],
        Task_sett_Con_Unit_Price: null,
        Task_sett_CLI_Unit_Price: null,
        Task_sett_Flat_Free: false,
        Task_sett_Price_Edit: false,
        Task_Work_TypeGroup: [],
        Task_sett_IsActive: true,
        Task_sett_IsDelete: false
      }
    ];
    this.FormArrayValDocument = [
      {
        WT_Task_Company: [],
        WT_Task_State: [],
        WT_Task_Customer: [],
        WT_Task_LoneType: [],
        WT_Task_WorkType: [],
        WT_Task_ClientDueDateTo: null,
        WT_Task_ClientDueDateFrom: null,
        WT_Task_IsActive: true,
        WT_Task_IsDelete: false
      }
    ];

    this.FormArrayPresetVal = [
      {
        Task_Preset_pkeyId: 0,
        Task_Preset_Text: "",
        Task_Preset_IsActive: true,
        Task_Preset_IsDelete: false
      }
    ];

    this.StateArray = [
      { Id: 1, Name: "LA" },
      { Id: 2, Name: "MO" }
    ];
    this.ShowonBidArray = [
      { Id: 1, Name: "YES" },
      { Id: 2, Name: "NO" }
    ];
    this.ISActiveArray = [
      { Id: 1, Name: "YES" },
      { Id: 2, Name: "NO" }
    ];
    this.TaskTypeList = [
      { Id: 1, Name: "work" },
      { Id: 2, Name: "inspection" }
    ];
  }
  dropdownList = [];

  selectedItems = [];
  dropdownSettings = {};
  dropdownSettingsState = {};
  dropdownSettingsContractor = {};
  dropdownSettingsCustomer = {};
  dropdownSettingsLoanType = {};
  dropdownSettingsCountry = {};
  dropdownSettingsWorkTypeList = {};
  dropdownSettingsWorkTypeCategory = {};

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      // ItemName: ["", Validators.required],
      // Item_BidVal:["", Validators.required],
      TaskName: ["", Validators.required],
      ContractorUnitVal: ["", Validators.required],
      ClientUnitVal: ["", Validators.required],
      TaskTypeVal: ["", Validators.nullValidator],
      TaskGroupVal: ["", Validators.nullValidator],
      TaskMeasureVal: ["", Validators.nullValidator],
      TaskLabelVal: ["", Validators.nullValidator],
      TaskCompleteVal: ["", Validators.nullValidator],
      TaskActiveVal: ["", Validators.nullValidator],
    
     
    });

    this.getModelData();

    this.GetDropDowndata();

    // this setting for multiple drop down select values
    this.dropdownSettings = {
      singleSelection: false,
      idField: "Client_pkeyID",
      textField: "Client_Company_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    // this setting for multiple drop down select values state
    this.dropdownSettingsState = {
      singleSelection: false,
      idField: "IPL_StateID",
      textField: "IPL_StateName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // this setting for multiple drop down select values Contractor
    this.dropdownSettingsContractor = {
      singleSelection: false,
      idField: "User_pkeyID",
      textField: "User_FirstName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // this setting for multiple drop down select values Customer
    this.dropdownSettingsCustomer = {
      singleSelection: false,
      idField: "Cust_Num_pkeyId",
      textField: "Cust_Num_Number",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsLoanType = {
      singleSelection: false,
      idField: "Loan_pkeyId",
      textField: "Loan_Type",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsCountry = {
      singleSelection: false,
      idField: "Country_pkeyId",
      textField: "Country_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsWorkTypeList = {
      singleSelection: false,
      idField: "WT_pkeyID",
      textField: "WT_WorkType",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    // this setting for multiple drop down select values Loan Type
    this.dropdownSettingsWorkTypeCategory = {
      singleSelection: false,
      idField: "Work_Type_Cat_pkeyID",
      textField: "Work_Type_Name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
  }
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  // shortcurt Namefor form sathi
  get fx() {
    return this.formUsrCommonGroup.controls;
  }

  // submit form
  FormButton(content) {
    this.contentx = content;
    debugger;
    this.submitted = true;
    this.isdisable = false;
    this.BidInvoiceItemModelObj;

    this.AddInvoiceItemsModelObj.Task_pkeyID = this.BidInvoiceItemModelObj.Task_pkeyID;

    this.AddInvoiceItemsModelObj.ArrayCustomPriceFilter = this.FormArrayVal;
    this.AddInvoiceItemsModelObj.ArrayDocument = this.FormArrayValDocument;
    this.AddInvoiceItemsModelObj.ArrayPreset = this.FormArrayPresetVal;

    console.log('arraydata', this.FormArrayPresetVal );

    // only drop down
    this.dropCkck = false;

  
    if (this.AddInvoiceItemsModelObj.Item_Active == 0) {
      this.Active_ValFlag = true;
      this.dropCkck = true;
    }

    // drop down
    // if (this.dropCkck) {
    //   return;
    // }

    // stop here if form is invalid
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";

    // care fulllllllllllllllllllllllllllllllllllllllllllllllly
    this.TaskSettingUpdateCall();

    //this. AllFilterSave();
  }
  // btn code end

  TaskSettingUpdateCall() {
    if(this.WorkOrderObj !== undefined) {
      
      this.BidInvoiceItemModelObj.Task_pkeyID = this.WorkOrderObj;
    
    } else {

      this.BidInvoiceItemModelObj.Task_pkeyID = 0;
    }
    this.xBidInvoiceItemServices
      .TaskMasterPost(this.BidInvoiceItemModelObj)
      .subscribe(response => {
        //debugger;
        console.log("resp data pkey", response);
        if (response[0].Status != "0") {
          this.BidInvoiceItemModelObj.Task_pkeyID = parseInt(
            response[0].Task_pkeyID
          );

          this.MessageFlag = "Task Data upated...!";
          this.isLoading = false;
          this.isdisable = false;
          this.button = "Update";
          this.commonMessage(this.contentx);
          this.AllFilterSave();
        }
      });
  }

  AllFilterSave() {
    this.xAddInvoiceItemsServices
      .FilterDataPost(this.AddInvoiceItemsModelObj)
      .subscribe(response => {
        console.log("response filter", response);
      });
  }
  // common message modal popup
  commonMessage(tent) {
    this.xmodalService
      .open(tent, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {},
        reason => {}
      );
  }
  /// end common model

  Item_Bid_Method() {
    this.Item_BidValFlag = false;
  }
  Active_Method() {
    this.Active_ValFlag = false;
  }

  // Insert New Row
  AddMoreRow() {
    var data = {
      Task_sett_State: [],
      Task_sett_Country: [],
      Task_sett_Zip: null,
      Task_sett_Customer: [],
      Task_sett_Contractor: [],
      Task_sett_Company: [],
      Task_sett_Lone: [],
      Task_sett_Con_Unit_Price: 0,
      Task_sett_CLI_Unit_Price: 0,
      Task_sett_Flat_Free: false,
      Task_sett_Price_Edit: false,
      Task_Work_TypeGroup: [],
      Task_sett_IsActive: true,
      Task_sett_IsDelete: false
    };
    this.FormArrayVal.push(data);
  }

  AddMoreRowDocument() {
    var data = {
      WT_Task_Company: [],
      WT_Task_State: [],
      WT_Task_Customer: [],
      WT_Task_LoneType: [],
      WT_Task_WorkType: [],
      WT_Task_ClientDueDateTo: null,
      WT_Task_ClientDueDateFrom: null,
      WT_Task_IsActive: true,
      WT_Task_IsDelete: false
    };
    this.FormArrayValDocument.push(data);
  }

  // remove row
  RemoveRow(itemdata,index) {
    let promp = confirm("Are you sure..?");
    if (promp) {
    if (itemdata.Task_sett_pkeyID != 0) {
      this.AddInvoiceItemsModelObj.Task_sett_pkeyID = itemdata.Task_sett_pkeyID;
      this.xAddInvoiceItemsServices
      .Deletetaskchild(this.AddInvoiceItemsModelObj)
      .subscribe(response => {

      })
     this.getModelData();
    }
    else{
 this.FormArrayVal.splice(index, 1);
    }
  }
  }
  RemoveRowDocument(val,index) {
    debugger;
    console.log('valchk',val)
    let del = confirm("Are you sure..?");
    if (del) {
    if (val.WT_Task_pkeyID != 0) {
      this.AddInvoiceItemsModelObj.WT_Task_pkeyID = val.WT_Task_pkeyID;
      this.xAddInvoiceItemsServices
      .Deletetaskchilddetail(this.AddInvoiceItemsModelObj)
      .subscribe(response => {

      })
     this.getModelData();
    }
    else{
      this.FormArrayValDocument.splice(index, 1);
    }
  }
  }

  ///
  // pretext delete code
  PreTextDeleteArry = [];
  RemoveRowPreset(index, item) {
    debugger;

    let conf = confirm("Are you sure delete this records");

    if (conf) {
      this.PreTextDeleteArry.push(item);
      this.FormArrayPresetVal.splice(index, 1);

      this.AddInvoiceItemsModelObj.Task_Preset_pkeyId = item.Task_Preset_pkeyId;

      this.xAddInvoiceItemsServices
        .PreTextDeleteDataPost(this.AddInvoiceItemsModelObj)
        .subscribe(response => {
          console.log("response filter delete pretest", response);
        });
    }
  }
  // Insert New Row
  AddMoreRowPreset() {
    debugger;
    var data = {
      Task_Preset_pkeyId: 0,
      Task_Preset_Text: "",
      Task_Preset_IsActive: true,
      Task_Preset_IsDelete: false
    };
    this.FormArrayPresetVal.push(data);
  }

  WorkOrderObj: any;
  getModelData() {
    //debugger;

    const id1 = this.xRoute.snapshot.params['id'];
    if ( id1 == 'new' ) {
      this.AddInvoiceItemsModelObj = new AddInvoiceItemsModel();
    } else {
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Task_pkeyID', id);
    this.WorkOrderObj = parseInt(id);
    this.GetTaskData();
      this.GetTaskGroup();
      // this.downloadDocument(Event, File);
    }
  
  }


  
  doclst: any;
  GetTaskData() {
    debugger;
    this.BidInvoiceItemModelObj.Task_pkeyID = this.WorkOrderObj;
    this.BidInvoiceItemModelObj.Type = 2;
    this.xBidInvoiceItemViewTaskServices
      .ViewTaskMasterData(this.BidInvoiceItemModelObj)
      .subscribe(response => {
        debugger
        this.doclst = response[1];
        console.log("2nd task list", response);
        // this.griddata = response[0];
        this.BidInvoiceItemModelObj.Task_pkeyID = response[0][0].Task_pkeyID;
        this.BidInvoiceItemModelObj.Task_Name = response[0][0].Task_Name;
        this.BidInvoiceItemModelObj.Task_Type = response[0][0].Task_Type;
        this.BidInvoiceItemModelObj.Task_Group =response[0][0].Task_Group;
        this.BidInvoiceItemModelObj.Task_Contractor_UnitPrice = response[0][0].Task_Contractor_UnitPrice;
        this.BidInvoiceItemModelObj.Task_Client_UnitPrice = response[0][0].Task_Client_UnitPrice;
        this.BidInvoiceItemModelObj.Task_Photo_Label_Name = response[0][0].Task_Photo_Label_Name;
        this.BidInvoiceItemModelObj.Task_UOM = response[0][0].Task_UOM;
        this.BidInvoiceItemModelObj.Task_AutoInvoiceComplete  = response[0][0].Task_AutoInvoiceComplete;
        this.BidInvoiceItemModelObj.Task_IsActive =response[0][0].Task_IsActive;


      
        this.isdisable = true;
       
        this.formUsrCommonGroup.disable();
        this.IsEditDisable = true;

        this.button = "Update";
      });
  }

  // get work type group
  TaskGroupList: any;
  GetTaskGroup() {
    this.xBidInvoiceItemServices
      .GetTaskGroupDetailsDropdownGet()
      .subscribe(response => {
        console.log("resp Tsk  Grup", response);
        this.TaskGroupList = response[0];

        this.getOUMDropdown();
      });
      this.GetFilterData();
  }

  // GetUOM
  TaskUOMList: any;
  getOUMDropdown() {
    this.xWorkOrderDrodownServices.DropdownGetUOM().subscribe(response => {
      console.log("OUM DROP DOWN", response);

      this.TaskUOMList = response[0];

     
    });
  }

  CleanArray = [];
  GetFilterData() {
    debugger
    this.AddInvoiceItemsModelObj.Task_pkeyID = this.WorkOrderObj;
    this.xAddInvoiceItemsServices
      .FilterDataGet(this.AddInvoiceItemsModelObj)
      .subscribe(response => {
        console.log("respon filter all", response);

        this.FormArrayVal = response[0];
        this.FormArrayValDocument = response[1];
        this.FormArrayPresetVal = response[2];

        //debugger;
        if (this.FormArrayPresetVal.length == 0) {
          this.FormArrayPresetVal = [
            {
              Task_Preset_pkeyId: 0,
              Task_Preset_Text: "",
              Task_Preset_IsActive: true,
              Task_Preset_IsDelete: false
            }
          ];
        }
        if (this.FormArrayVal.length > 0) {
          for (let i = 0; this.FormArrayVal.length > i; i++) {
            if (this.FormArrayVal[i].Task_sett_Company) {
              this.FormArrayVal[i].Task_sett_Company = JSON.parse(
                this.FormArrayVal[i].Task_sett_Company
              );
            }
            if (this.FormArrayVal[i].Task_sett_Contractor) {
              this.FormArrayVal[i].Task_sett_Contractor = JSON.parse(
                this.FormArrayVal[i].Task_sett_Contractor
              );
            }
            if (this.FormArrayVal[i].Task_sett_Country) {
              this.FormArrayVal[i].Task_sett_Country = JSON.parse(
                this.FormArrayVal[i].Task_sett_Country
              );
            }

            if (this.FormArrayVal[i].Task_sett_Customer) {
              this.FormArrayVal[i].Task_sett_Customer = JSON.parse(
                this.FormArrayVal[i].Task_sett_Customer
              );
            }

            if (this.FormArrayVal[i].Task_sett_Lone) {
              this.FormArrayVal[i].Task_sett_Lone = JSON.parse(
                this.FormArrayVal[i].Task_sett_Lone
              );
            }
            if (this.FormArrayVal[i].Task_sett_State) {
              this.FormArrayVal[i].Task_sett_State = JSON.parse(
                this.FormArrayVal[i].Task_sett_State
              );
            }
            if (this.FormArrayVal[i].Task_Work_TypeGroup) {
              this.FormArrayVal[i].Task_Work_TypeGroup = JSON.parse(
                this.FormArrayVal[i].Task_Work_TypeGroup
              );
            }
          }
        } else {
          this.FormArrayVal = [
            {
              Task_sett_State: [],
              Task_sett_Country: [],
              Task_sett_Zip: null,
              Task_sett_Customer: [],
              Task_sett_Contractor: [],
              Task_sett_Company: [],
              Task_sett_Lone: [],
              Task_sett_Con_Unit_Price: null,
              Task_sett_CLI_Unit_Price: null,
              Task_sett_Flat_Free: false,
              Task_sett_Price_Edit: false,
              Task_Work_TypeGroup: [],
              Task_sett_IsActive: true,
              Task_sett_IsDelete: false
            }
          ];
        }

        if (this.FormArrayValDocument.length > 0) {
          for (let i = 0; this.FormArrayValDocument.length > i; i++) {
            if (this.FormArrayValDocument[i].WT_Task_Company) {
              this.FormArrayValDocument[i].WT_Task_Company = JSON.parse(
                this.FormArrayValDocument[i].WT_Task_Company
              );
            }
            if (this.FormArrayValDocument[i].WT_Task_Customer) {
              this.FormArrayValDocument[i].WT_Task_Customer = JSON.parse(
                this.FormArrayValDocument[i].WT_Task_Customer
              );
            }
            if (this.FormArrayValDocument[i].WT_Task_LoneType) {
              this.FormArrayValDocument[i].WT_Task_LoneType = JSON.parse(
                this.FormArrayValDocument[i].WT_Task_LoneType
              );
            }
            if (this.FormArrayValDocument[i].WT_Task_State) {
              this.FormArrayValDocument[i].WT_Task_State = JSON.parse(
                this.FormArrayValDocument[i].WT_Task_State
              );
            }
            if (this.FormArrayValDocument[i].WT_Task_WorkType) {
              this.FormArrayValDocument[i].WT_Task_WorkType = JSON.parse(
                this.FormArrayValDocument[i].WT_Task_WorkType
              );
            }
          }
        } else {
          this.FormArrayValDocument = [
            {
              WT_Task_Company: [],
              WT_Task_State: [],
              WT_Task_Customer: [],
              WT_Task_LoneType: [],
              WT_Task_WorkType: [],
              WT_Task_ClientDueDateTo: null,
              WT_Task_ClientDueDateFrom: null,
              WT_Task_IsActive: true,
              WT_Task_IsDelete: false
            }
          ];
        }
      });
  }

  ///get drop
  CompanyList: any; // temp array
  CustomerList: any; // temp array
  WorkTypeList: any; // temp array
  CategoryList: any; // temp array
  ContractorList: any; // temp array
  CordinatorList: any;
  LoanTypeList: any;
  CountryList: any;
  WorkTypeCategory: any; //group

  GetDropDowndata() {
    this.xWorkOrderDrodownServices
      .DropdownGetWorkOrder()
      .subscribe(response => {
        console.log("drop down  data ", response);
        //debugger;
        if (response.length != 0) {
          this.StateList = response[6];
          this.CustomerNumberList = response[9];
          this.CompanyList = response[0];
          this.WorkTypeList = response[1];
          this.CategoryList = response[2];

          this.ContractorList = response[3];
         
          this.CordinatorList = response[4];
         
          this.LoanTypeList = response[10];

          this.dropdownList = response[0];

          this.CountryList = [{ Country_pkeyId: 1, Country_Name: "USA" }];

          this.WorkTypeCategory = response[11];
        }
      });
  }



  IsEditDisable = false;
  EditForms() {
    this.IsEditDisable = false;
    this.isdisable = false;
    this.formUsrCommonGroup.enable();
  }
  




  InstructionDataArray = [];
  DetailsDataArray = [];
  InstDataArray = [];


 

  
  
  
  // upload photos popup
  docsupload(DocmentUpload) {
    this.POPUPPhoto(DocmentUpload);
  }




  POPUPPhoto(DocmentUpload) {
    this.modalService
      .open(DocmentUpload, { size: "lg", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
         
        },
        reason => {
          // then call get
        
        }
      );
  }

  public displayError(e: ErrorEvent) {
    console.log(e);
    console.log("An error occurred");
  }
  public displaySuccess(e) {
    console.log("success");
    console.log("success", e);
    console.log("success File", e.files[0].rawFile);

    if (e.operation == "upload") {
      this.processDocument(e.files[0].rawFile);
    } else {
      alert("remove img called");
    }
  }


 





  task_id : any;
  processDocument(documentInput) {
    const id1 = this.xRoute.snapshot.params['id'];
    let id = this.EncrDecr.get('123456$#@$^@1ERF', atob(id1));
    console.log('Task_pkeyID', id);
    this.task_id = parseInt(id);
    debugger;
    if (true) {
      this.BindDataModelObj.Common_pkeyID = 0;
      this.BindDataModelObj.Client_Result_Photo_Ch_ID = this.task_id;
      this.BindDataModelObj.Client_Result_Photo_ID = 0;
      // this.BindDataModelObj.Client_PageCalled = 1;
      this.BindDataModelObj.documentx = documentInput;
      this.BindDataModelObj.Client_Result_Photo_FileName = documentInput.name;
      this.BindDataModelObj.Type = 1;
      this.BindDataModelObj.workOrderNumber = '0';
      this.BindDataModelObj.IPLNO = 'Task_Master_Document';
      this.xClientResultOldPhotoServices
        .CommonDocumentsUpdate(this.BindDataModelObj)
        .then((res) => {
          res.subscribe(() => {});
          this.getModelData();
        });
       
    }
  }
  test(val){
    console.log('checkbox',val)
  }




}
