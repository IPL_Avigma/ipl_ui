import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AccountingComponent } from './AccountingDetailsComponent';
// import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

export const ReportsRouts = [
  { path: "accountingdetails", component: AccountingComponent }
];

@NgModule({
  declarations: [AccountingComponent],
  imports: [
    RouterModule.forChild(ReportsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    



  ],
  providers: [],
  bootstrap: [AccountingComponent]
})

export class AccountingModule {}
