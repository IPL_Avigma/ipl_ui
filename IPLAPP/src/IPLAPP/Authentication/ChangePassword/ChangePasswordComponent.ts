import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";


import {ChangePasswordModel} from './ChangePasswordModel';
import {ChangePasswordServices} from './ChangePasswordServices';

@Component({
  templateUrl: "./ChangePassword.html"
})
export class ChangePasswordComponent implements OnInit {

  ChangePasswordModelObj:ChangePasswordModel = new ChangePasswordModel();
  GetLocal:any;

  buttonChange = "Change Password";


  isLoadingChange = false;
  submittedchange = false; // submitted;
  formUsrLoginGroupChnage: FormGroup; // create obj
  Realpassword:string ="";

  public contentx;
  MessageFlag: string;

  constructor(private xChangePasswordServices:ChangePasswordServices,private formBuilder: FormBuilder, private xmodalService: NgbModal,) {

    this.GetLocal = JSON.parse(localStorage.getItem("usertemp_"));

    this.GetPassWord();

  }

  ngOnInit() {
    this.formUsrLoginGroupChnage = this.formBuilder.group({

      PasswordOld: ["", Validators.required],
      PasswordNew: ["", [Validators.required, Validators.minLength(8)]],
      PasswordConfirm:["", Validators.required],

    },
     {
        validator: this.MustMatch("PasswordNew", "PasswordConfirm")
      }
    );
  }
  get fxChange() {
    return this.formUsrLoginGroupChnage.controls;
  }



  GetPassWord(){

    if(this.GetLocal){

      this.ChangePasswordModelObj.User_pkeyID = this.GetLocal[0].User_pkeyID;


    this.xChangePasswordServices.GetPasswordData(this.ChangePasswordModelObj)
    .subscribe(response =>{
      console.log("response pw and pkey", response);


      this.Realpassword = response[0][0].User_Password;
    });
    }





  }



    // change password
    FormButtonChange(content){

      this.contentx = content;
      this.submittedchange = true;

      if (this.formUsrLoginGroupChnage.invalid) {
        return;
      }
      if(this.Realpassword != this.ChangePasswordModelObj.User_PasswordOld){

        console.log("fkjdsgfkjdgskjf",this.Realpassword);
        alert('Old Password Wrong..!');
        return;
      }
      this.isLoadingChange = true;

      this.xChangePasswordServices.ChangePasswordData(this.ChangePasswordModelObj)
      .subscribe(reponse =>{
        console.log('change respose ',reponse);


        this.buttonChange = "Change Password";
        this.isLoadingChange = false;
        this.MessageFlag = "Password Change Successfully...!";
        this.commonMessage(this.contentx);
      });





    }

      // common message modal popup
  commonMessage(content) {
    this.xmodalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model





  // password match function
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }



}
