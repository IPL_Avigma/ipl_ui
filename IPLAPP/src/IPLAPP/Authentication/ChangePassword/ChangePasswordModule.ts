import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ChangePasswordComponent } from './ChangePasswordComponent';
import {ChangePasswordServices} from './ChangePasswordServices';



const ChangePasswordRouts = [
  { path: "change", component: ChangePasswordComponent }
];

@NgModule({
  declarations: [ChangePasswordComponent],
  imports: [
    RouterModule.forChild(ChangePasswordRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,HttpClientModule

  ],
  providers: [ChangePasswordServices],
  bootstrap: [ChangePasswordComponent]
})

export class ChangePasswordModule {}
