import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { LoginModel } from "./LoginModel";
import { BasetUrl } from "../../Utility/DomainUrl";
import {HomepageServices} from '../../Home/HomeServices'

@Injectable({
  providedIn: "root"
})
export class LoginServices {
  token: any;
  constructor(private _http: HttpClient, private _Route: Router,  private xHomepageServices: HomepageServices) {
    
    if (localStorage.getItem('TOKEN') != null)
    {
      this.token = JSON.parse(localStorage.getItem('TOKEN'));
    } 
    
  }

  private apiUrlPOST =
    BasetUrl.Domain + "api/RESTAuthentication/PostUserLoginData";
  public LoginuserPost(Modelobj: LoginModel) {
  debugger;
    var ANYDTO: any = {};
    ANYDTO.User_LoginName = Modelobj.User_LoginName;
    ANYDTO.User_Password = Modelobj.User_Password;
    ANYDTO.User_Token_val = Modelobj.tokendetails;
 
    ANYDTO.Type = Modelobj.Type;
     
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
       // catchError(this.xHomepageServices.CommonhandleError)
      );
  }
  ////////////////////////////////////////////////////////////////////////////
  // private apiUrlPOSTForToken =  BasetUrl.Domain + "/token";
  private apiUrlPOSTForToken =  BasetUrl.Domain + "/token";

  
  public LoginuserGetToken(Modelobj: LoginModel) {
    console.log('url', BasetUrl.Domain);
  debugger;
 
  

  //let auth = `grant_type=password&username=${params.userName}&password=${params.password}`;
 // let test = `grant_type=password&Username=f&Password=e&ClientId=1`;
    //return this.httpClient.post<LoggedinUser>(`${environment.webapiUrl}token`, data);


var userData = "username=" + Modelobj.User_LoginName + "&password=" + Modelobj.User_Password + "&grant_type=password";

  let headers = new HttpHeaders({ "Content-Type": "application/x-www-form-urlencoded",'No-Auth':'True' });
  return this._http
    .post<any>(this.apiUrlPOSTForToken, userData, { headers: headers })
    .pipe(
      tap(data => {
        console.log('tokendata',data)
        return data;
      }),
      // catchError(this.handleError)
    );
}
////////////////////////////////////////////////////////////////////////////

  private apiUrlPOST2 =   BasetUrl.Domain + "api/RESTAuthentication/PostUserForgotPasswordData";
  public ForgotpasswordPost(Modelobj: LoginModel) {
    //debugger;
    var ANYDTO: any = {};
    ANYDTO.User_LoginName = Modelobj.User_LoginNameForgot;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          return data;
        }),
      
      );
  }
   ////////////////////////////////////////////////////////////////////////////

   // change password;
   private apiUrlPOST3 =
   BasetUrl.Domain + "api/RESTAuthentication/PostUserChangePasswordData";
   public ChangepasswordPost(Modelobj: LoginModel) {
   debugger;
   var ANYDTO: any = {};
   ANYDTO.User_pkeyID = Modelobj.User_pkeyID;
   ANYDTO.User_Password = Modelobj.User_PasswordNew;
   ANYDTO.Type = 2;

   let headers = new HttpHeaders({ "Content-Type": "application/json" });
   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   return this._http
     .post<any>(this.apiUrlPOST3, ANYDTO, { headers: headers })
     .pipe(
       tap(data => {
         return data;
       }),
      
     );
 }

  // common handler
  // private handleError(error: HttpErrorResponse) {
  //   alert("Something bad happened. please try again later...😌");
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error("An error occurred:", error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` + `body was: ${error.error}`
  //     );
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError("Something bad happened; please try again later.");
  // }
}
