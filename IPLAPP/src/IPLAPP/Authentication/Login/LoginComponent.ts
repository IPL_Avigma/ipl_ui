import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { LoginModel } from "./LoginModel";
import { LoginServices } from "./LoginServices";

import {MasterlayoutComponent}  from '../../Home/MasterComponent';
import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";

import { AngularFireMessaging } from '@angular/fire/messaging'

@Component({
  templateUrl: "./LoginView.html"
})
export class LoginComponent implements OnInit {
  submitted = false; // submitted;
  submittedForgot = false; // submitted;
  submittedchange = false; // submitted;

  formUsrCommonGroup: FormGroup;
  button = "Log In";
  buttonforgot = "Sent Password";
  buttonChange = "Change Password";
  isLoading = false;
  isLoadingforgot = false;
  isLoadingChange = false;

  formUsrLoginGroup: FormGroup; // create obj
  formUsrLoginGroupforget: FormGroup; // create obj
  formUsrLoginGroupChnage: FormGroup; // create obj
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  UserLoginModel: LoginModel = new LoginModel(); // create obj
 
  Key: any;
  Realpassword:string ="";
  tokendata: any;
  constructor(
    private formBuilder: FormBuilder,
    private _Route: Router,
    private modalService: NgbModal,
    private xLoginServices: LoginServices,
    private xMasterlayoutComponent:MasterlayoutComponent,
    private EncrDecr: EncrDecrServiceService,
    private  messaging: AngularFireMessaging,
  ) {
    this.Key = "QWERTYUIOP";
    localStorage.clear();
    this.messaging.messaging.subscribe(
      (_messaging) => {
        // _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  ngOnInit() {
    this.formUsrLoginGroup = this.formBuilder.group({
      Eamilname: ["", Validators.required],
      passwordname: ["", Validators.required]
    });
    this.formUsrLoginGroupforget = this.formBuilder.group({
      Eamilnameforget: ["", Validators.required]

    });
    this.formUsrLoginGroupChnage = this.formBuilder.group({

      PasswordOld: ["", Validators.required],
      PasswordNew: ["", [Validators.required, Validators.minLength(8)]],
      PasswordConfirm:["", Validators.required],

    },
     {
        validator: this.MustMatch("PasswordNew", "PasswordConfirm")
      }
    );

    this.checkPermission();
  }

  checkPermission(){
    this.messaging.requestToken.subscribe(
      (token) => {
        console.log("Checking token: ", token);
      this.tokendata = token;
        this.xMasterlayoutComponent.masterFunctionCall(this.tokendata);
      },
      (err) => {
        console.error("Checking for error while generating token : ", err.data.message);
      }
    )
  }

  // saveToFirebaseDb(token){
    
  // }

  // shortcurt Namefor form sathi  ["", [Validators.required, Validators.minLength(6)]],
  get fx() {
    return this.formUsrLoginGroup.controls;
  }
  get fxforget() {
    return this.formUsrLoginGroupforget.controls;
  }
  get fxChange() {
    return this.formUsrLoginGroupChnage.controls;
  }

  // login form btn
  ContentChange:any;
  LoginBtn(content,Changepopup) {
    debugger;
    // why content use becoz bootstrap getting modal data content dynamically
    this.submitted = true;
    this.contentx = content;
    this.ContentChange = Changepopup;
    // stop here if form is invalid
    if (this.formUsrLoginGroup.invalid) {
      return;
    }
    this.isLoading = true;
    this.button = "Processing";
    this.TestToken();
    this.UserLoginModel.tokendetails = this.tokendata;
    this.UserLoginModel.Type = 2;
    localStorage.setItem("tempadmin", this.Key);
    localStorage.setItem("UserName",this.UserLoginModel.User_LoginName);
    
    //if (true
      // this.UserLoginModel.User_LoginName == "kevin" &&
      //  this.UserLoginModel.User_Password == "Kevin1234"
   // ) {
     // this.MessageFlag = "Login Successfully...!";

     
     //localStorage.setItem("TOKEN", JSON.stringify(response.access_token));
     // this._Route.navigate(["/home/index"]);
   // } //else {
  
      this.xLoginServices
        .LoginuserPost(this.UserLoginModel)
        .subscribe(response => {
          console.log("login respon", response);
          debugger;
         //this.xMasterlayoutComponent.masterFunctionCall(response);
          if (response[0].length != 0) {
           //this.TestToken();

            if(response[0][0].User_IsPasswordChange){

              var userval = response[0];
              this.MessageFlag = " Welcome back " + response[0][0].User_FirstName;
              this.button = "authenticating....";
              this.commonMessage(content);

              userval[0].User_Password = "xxxxxxxxxx";

              var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',JSON.stringify(userval) );
            
              localStorage.setItem("usertemp_",JSON.stringify(encrypted) );
          

              // localStorage.setItem("usertemp_", JSON.stringify(userval));
              localStorage.setItem("tempadmin", this.Key);

              const xxx = this.xMasterlayoutComponent.LocalGetmasterfunction();
              this.TestToken();

              this._Route.navigate(["/home/index"]);




            }else{
               debugger
              this.isLoading = false;
              this.button = "Log In";

              this.Realpassword = response[0][0].User_Password;

              this.UserLoginModel.User_pkeyID = response[0][0].User_pkeyID;



              this.commonMessageChange(Changepopup);
            }




          } else {
            this.MessageFlag = " Please enter valid Username and Password";
            this.commonMessage(content);
            localStorage.clear();

            this.isLoading = false;
            this.button = "Try Again";
          }
        });
    //}
  }

  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }

  ForgetPassword(content) {
    this.contentx = content;
    this.commonMessageforpasss(this.contentx);
  }
  commonMessageforpasss(content) {
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  commonMessageChange(Changepopup) {
    this.modalService
      .open(Changepopup, { ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }


  // submit form forgot
  FormButtonPassword(content) {

    this.submittedForgot = true;

    if (this.formUsrLoginGroupforget.invalid) {
      return;
    }

    this.contentx = content;
    ////debugger;

    this.isLoadingforgot = true;
    this.buttonforgot = "Processing";

    this.modalService.hasOpenModals();

    this.xLoginServices
      .ForgotpasswordPost(this.UserLoginModel)
      .subscribe(response => {
        console.log("resp data", response);
        if (true) {
          //this.UserLoginModel.pk = parseInt(response[0].rus_pkeyID);


          this.isLoadingforgot = false;
          this.buttonforgot = "Sent Password Again";
          this.MessageFlag = "Password Sent your Register Email Id";
          this.commonMessage(this.contentx);
        }
      });
  }
  // btn code end



  // change password
  FormButtonChange(){
    debugger
    this.submittedchange = true;


    if (this.formUsrLoginGroupChnage.invalid) {
      return;
    }
    if(this.Realpassword != this.UserLoginModel.User_PasswordOld){

      alert('Old Password Wrong..!');
      return;

    }




    this.isLoadingChange = true;



    this.xLoginServices.ChangepasswordPost(this.UserLoginModel)
    .subscribe(response =>{
      console.log('Change rsponse',response);

     this.buttonChange = "Redirecting Index page.....";
     this.isLoadingChange = true;



     this.UserLoginModel.User_LoginName = this.UserLoginModel.User_LoginName;
     this.UserLoginModel.User_Password =  this.UserLoginModel.User_PasswordNew;

     this.xLoginServices
     .LoginuserPost(this.UserLoginModel)
     .subscribe(response => {
       console.log("login respon change pas", response);
   debugger;
       if (response[0].length != 0) {


        var userval = response[0];

          //  this.MessageFlag = "Password Changed Successfully, Welcome back "+ response[0][0].User_FirstName;
          //  this.commonMessage(this.contentx);

           this.modalService.dismissAll();

           var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',JSON.stringify(userval) );
           console.log('encrypted',encrypted);
           localStorage.setItem("usertemp_",JSON.stringify(encrypted) );
           localStorage.setItem("tempadmin", this.Key);

           const xxx = this.xMasterlayoutComponent.LocalGetmasterfunction();

           this._Route.navigate(["/home/index"]);


       }

      });

    });


  }



   // password match function
   MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }


  TestToken(){
    debugger;
    this.UserLoginModel.Type = 1;
    //alert('called');
    this.xLoginServices.LoginuserGetToken(this.UserLoginModel)
    .subscribe(response => {
      console.log('token called', response);
     // localStorage.setItem("TokenVal", JSON.stringify(response));
      localStorage.setItem("TOKEN", JSON.stringify(response.access_token));
    });
  }
}
