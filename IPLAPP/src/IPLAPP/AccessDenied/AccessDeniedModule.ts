import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AccessDeniedComponent } from './AccessDeniedComponent';
// import {CommonDirectiveModule} from '../../../AppDirectives/DirectiveModule';

export const ReportsRouts = [
  { path: "accessdenied", component: AccessDeniedComponent }
];

@NgModule({
  declarations: [AccessDeniedComponent],
  imports: [
    RouterModule.forChild(ReportsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    



  ],
  providers: [],
  bootstrap: [AccessDeniedComponent]
})

export class AccessDeniedModule {}
