import { NgModule } from "@angular/core";
import {
  Routes,
  RouterModule,
  PreloadAllModules,
  NoPreloading
} from "@angular/router";
import { AdminLinkPageComponent } from "../Admin/AdminLinkPage/AdminLinkPageComponent";
import { CustomPreloadingStrategy } from "./RoutingPreloadModule";




const routes: Routes = [
  { path: "home/adminlinkpage", component: AdminLinkPageComponent },

  { path: "home", loadChildren: "../Home/HomeModule#HomeModule" },

  {
    path: "client",
    loadChildren:
      "../ClientResult/ClientResults/ClientResultModule#ClientResultModule",
    data: { preload: true}
  },
  //, delay: 4000
  {
    path: "workorder",
    loadChildren: "../WorkOrder/NewWorkOder/NewWorkOrderModule#WorkOrderModule",
    data: { preload: true }
  },

  {
    path: "workorder",
    loadChildren:
      "../WorkOrder/WorkOrderView/WorkOrderViewModule#WorkOrderViewModule",
    data: { preload: true }
  },

  {
    path: "admin",
    loadChildren: "../Authentication/Login/LoginModule#LoginModule"
  },

  // {path:'coverage',loadChildren:'../Contractors/CoverageMap/IPLAPP.CoverageMapModule#CoverageMapModule'},

  // {path:'mail',loadChildren:'../Contractors/CreateMassMail/IPLAPP.CreateMassMailModule#CreateMassMailModule'},

  // {path:'live',loadChildren:'../Contractors/LiveMap/IPLAPP.LiveMapModule#LiveMapModule'},

  // {path:'mail',loadChildren:'../Contractors/MassMail/IPLAPP.MassMailModule#MassMailModule'},

  // {path:'score',loadChildren:'../Contractors/ScoreCards/IPLAPP.ScoreCardModule#ScoreCardsModule'},

  //{path:'home/adminlinkpage',component:AdminLinkPageComponent},

  // {path:'client',loadChildren:'../ClientResult/ClientResultsInvoice/ClientResultsInvoiceModule#ClientResultsInvoiceModule'},

  // {path:'client',loadChildren:'../ClientResult/ClientResultField/ClientResultFieldModule#ClientResultFieldModule'},

  // {path:'client',loadChildren:'../ClientResult/ClientResultInstruction/ClientResultInstructionModule#ClientResultInstructionModule'},

  //{path:'client',loadChildren:'../ClientResult/ClientResultPhoto/ClientResultPhotoModule#ClientResultPhotoModule'},

  {
    path: "home",
    loadChildren:
      "../PagesLink/WorkOrderSettingsPage/WorkOrderSettingsPageModule#WorkOrderSettingsPageModule"
  },
  {
    path: "home",
    loadChildren:
      "../PagesLink/GeneralSettings/GeneralSettingsModule#GeneralSettingsModule"
  },

  {
    path: "password",
    loadChildren:
      "../Authentication/ChangePassword/ChangePasswordModule#ChangePasswordModule"
  },
  {
    path: "user",
    loadChildren: "../Admin/Users/AddUser/AddUserModule#AddUserModule"
  },
  {
    path: "user",
    loadChildren: "../Admin/Users/ViewUser/ViewUserModule#UserViewModule"
  },
  {
    path: "task",
    loadChildren:
      "../Admin/InvoiceItem/AddInvoiceItems/AddInvoiceItemsModule#AddInvoiceItemsModule"
  },

  {
    path: "dash",
    loadChildren:
      "../Admin/DashboardSetting/DashboardSettingModule#DashboardSettingModule"
  },
  {
    path: "com",
    loadChildren:
      "../Admin/CompanySetting/CompanySettingModule#CompanySettingModule"
  },

  //page link priority not change
  {
    path: "setx",
    loadChildren:
      "../Admin/WorkOrderSetting/WorkOrderSettingModule#WorkOrderSettingModule"
  },
  {
    path: "home",
    loadChildren:
      "../Admin/AdminLinkPage/AdminLinkPageModule#AdminLinkPageModule"
  },

  {
    path: "company",
    loadChildren:
      "../Admin/CompanyInfo/AddCompanyInfo/CompanyInfoModule#CompanyInfoModule"
  },
  {
    path: "company",
    loadChildren:
      "../Admin/CompanyInfo/ViewCompanyInfo/ViewCompanyInfoModule#ViewCompanyInfoModule"
  },

  {
    path: "client",
    loadChildren:
      "../Admin/ClientCompanies/AddClientCompanies/AddClientCompaniesModule#AddClientCompaniesModule"
  },
  {
    path: "client",
    loadChildren:
      "../Admin/ClientCompanies/ViewClientCompanies/ViewClientCompaniesModule#ViewClientCompaniesModule"
  },

  {
    path: "rush",
    loadChildren: "../Admin/Rush/AddRush/AddRushModule#AddRushModule"
  },
  {
    path: "rush",
    loadChildren: "../Admin/Rush/ViewRush/ViewRushModule#ViewRushModule"
  },

  {
    path: "group",
    loadChildren: "../Admin/Groups/AddGroups/AddGroupsModule#AddGroupsModule"
  },
  {
    path: "group",
    loadChildren: "../Admin/Groups/ViewGroups/ViewGroupsModule#ViewGroupsModule"
  },

  {
    path: "worktype",
    loadChildren:
      "../Admin/WorkType/WorkTypeCategory/AddWorkTypeCategory/AddCategoryModule#AddCategoryModule"
  },
  {
    path: "worktype",
    loadChildren:
      "../Admin/WorkType/WorkTypeCategory/ViewWorkTypeCategory/ViewWorkTypeCategoryModule#ViewWorkTypeCategoryModule"
  },

  {
    path: "state",
    loadChildren: "../Admin/State/AddState/AddStateModule#AddStateModule"
  },
  {
    path: "state",
    loadChildren: "../Admin/State/ViewState/ViewStateModule#ViewStateModule"
  },

  {
    path: "category",
    loadChildren:
      "../Admin/MainCategory/AddMainCategory/AddMainCategoryModule#AddMainCategoryModule"
  },
  {
    path: "category",
    loadChildren:
      "../Admin/MainCategory/ViewMainCategory/ViewMainCategoryModule#ViewMainCategoryModule"
  },

  {
    path: "customer",
    loadChildren:
      "../Admin/Customer/AddCustomer/AddCustomerModule#AddCustomerModule"
  },
  {
    path: "customer",
    loadChildren:
      "../Admin/Customer/ViewCustomer/ViewCustomerModule#ViewCustomerModule"
  },

  {
    path: "task",
    loadChildren:
      "../Admin/BidInvoiceItem/BidInvoiceItemModule#BidInvoiceItemModule"
  },
  {
    path: "task",
    loadChildren:
      "../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskModule#BidInvoiceItemViewTaskModule"
  },

  {
    path: "photoh",
    loadChildren:
      "../Admin/PhotoHeaderTemplates/AddPhotoHeaderTemplates/AddPhotoHeaderTemplatesModule#AddPhotoHeaderTemplatesModule"
  },

  {
    path: "loan",
    loadChildren:
      "../Admin/LoanType/AddLoanType/AddLoanTypeModule#AddLoanTypeModule"
  },
  {
    path: "loan",
    loadChildren:
      "../Admin/LoanType/ViewLoanType/ViewLoanTypeModule#ViewLoanTypeModule"
  },

  {
    path: "damage",
    loadChildren: "../Admin/Damage/AddDamage/AddDamageModule#AddDamageModule"
  },
  {
    path: "damage",
    loadChildren: "../Admin/Damage/ViewDamage/ViewDamageModule#ViewDamageModule"
  },

  {
    path: "uom",
    loadChildren: "../Admin/UOM/AddUOM/AddUOMModule#AddUOMModule"
  },
  {
    path: "uom",
    loadChildren: "../Admin/UOM/ViewUOM/ViewUOMModule#ViewUOMModule"
  },

  {
    path: "customphoto",
    loadChildren:
      "../Admin/CustomPhotoLable/AddCustomPhotoLabel/CustomPhotoLableModule#CustomPhotoLableModule"
  },
  {
    path: "customphoto",
    loadChildren:
      "../Admin/CustomPhotoLable/ViewcustomPhotoLabel/ViewCustomPhotoLableModule#ViewCustomPhotoLabelModule"
  },

  {
    path: "import",
    loadChildren:
      "../WorkOrder/ImportQueueWorkOrder/ImportQueueModule#ImportQueueModule"
  },
  {
    path: "docs",
    loadChildren:
      "../WorkOrder/FormsandDocs/FormsandDocsModule#FormsandDocsModule"
  },
  {
    path: "import",
    loadChildren:
      "../WorkOrder/ImportWorkOrder/ImportWorkOrderModule#ImportWorkOrderModule"
  },
  {
    path: "contractor",
    loadChildren: "../Contractors/Contractor/ContractorModule#ContractorModule"
  },

  {
    path: "autoimport",
    loadChildren: "../Admin/AutoImportWorkOrderDetails/AutoImportWorkOrder/AutoImportWorkOrderModule#AutoImportWorkOrderModule"
  },
  {
    path: "autoimport",
    loadChildren: "../Admin/AutoImportWorkOrderDetails/ViewAutoImportWorkOrder/ViewAutoImportWoModule#ViewAutoImportWoModule"
  },
  {
    path: "queueworkorder",
    loadChildren: "../Admin/WorkorderQueueDetais/WorkOrderImportQueue/WorkOrderImportQueueModule#WorkOrderImportQueueModule"
  },
{
  path: "queueworkorder",
    loadChildren: "../Admin/WorkorderQueueDetais/ImportWorkOrderQueue/ImportWorkOrderModule#ImportWorkOrderImportQueueDetailsModule"
  },
  {
    path: "report",
      loadChildren: "../Admin/ReportsDetails/ReportsDetailsModule#ReportsModule"
    },
    {
    path: "accounting",
      loadChildren: "../Admin/AccountingDetails/AccountingDetailsModule#AccountingModule"
    },
    {
      path: "support",
        loadChildren: "../Admin/SupportDetails/SupportDetailsModule#SupportModule"
      },
    {
      path: "access",
      loadChildren: "../AccessDenied/AccessDeniedModule#AccessDeniedModule"
    },

    // {
    //   path: "message",
    //   loadChildren: "../Message/MessageModule#MessageModule"
    // },
    {
      path: "formdoc",
      loadChildren: "../WorkOrder/DocumentAndForms/DocumentAndFormModule#DocumentAndFormModule"
    },
    {
      path: "userdata",
      loadChildren: "../UserProfile/UserProfileModule#UserProfileModule"
    },
    {
      path: "workorder",
      loadChildren: "../WorkOrder/WorkOrderMap/WorkOrderMapModule#WorkOrderMapModule"
    },
    {
      path: "contractor",
      loadChildren: "../Contractors/Coverage map/coverage-map-module#CoverageMapModule"
    },
   

  { path: "", redirectTo: "/admin/login", pathMatch: "full" }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: CustomPreloadingStrategy
    })
  ],
  exports: [RouterModule],
  providers: [CustomPreloadingStrategy]
})
export class AppRoutingModule {}
