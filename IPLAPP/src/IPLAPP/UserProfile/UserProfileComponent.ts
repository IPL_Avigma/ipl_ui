import { Component, Injectable, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MasterlayoutComponent } from "../Home/MasterComponent";
import{UserProfileModel} from './UserProfileModel'
import { from } from 'rxjs';
import {UserProfileService} from './UserProfileService'






@Component({
  templateUrl: "./UserProfile.html",
  styleUrls: ['./UserProfile.css']
})
export class UserProfileComponent implements OnInit {
 
  button = "Save"; 
  isLoading = false;

  public contentx; 
  MessageFlag: string; 
  FormDisabledCustom = false;
   IsEditDisable:boolean = false;
   formUsrCommonGroup: FormGroup; // create obj
   submitted = false; // submitted;
   submittedchange = false;
   UserProfileModelObj: UserProfileModel = new UserProfileModel();
  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xRoute : ActivatedRoute,
    private xUserProfileService : UserProfileService
  ) {

 
  }

 
  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
      userfirst: ["", Validators.required],
      userlast: ["", Validators.nullValidator],
      useremail: ["", Validators.email],
      useradd: ["", Validators.nullValidator],
      usercity: ["", Validators.nullValidator],
      userstate: ["", Validators.nullValidator],
      userpin:["", [Validators.nullValidator, Validators.minLength(6)]],
      usercompany: ["", Validators.nullValidator],
      usermob: ["", Validators.nullValidator],
      userlogin: ["", Validators.nullValidator],
      userpassword: ["", [Validators.required,Validators.minLength(8)]],
      userconpassword:["", Validators.required],
      selectimg:["", Validators.nullValidator],

    },
   
     {
        validator: this.MustMatch("userpassword", "userconpassword")
      }
    );
    this.GetProfileDetails();
  }
  get fxChange() {
    return this.formUsrCommonGroup.controls;
  }
  FullName: String;
GetProfileDetails(){

  // let User = JSON.parse(localStorage.getItem('usertemp_'));
   //console.log('user', User)
   //this.UserProfileModelObj.User_pkeyID = User[0].User_pkeyID;
    this.xUserProfileService
    .GetUserProfileData(this.UserProfileModelObj)
    .subscribe(response => {
      console.log('userdetails', response);
      this.UserProfileModelObj.User_pkeyID = response[0][0].User_pkeyID;
      this.UserProfileModelObj.User_FirstName = response[0][0].User_FirstName;
      this.UserProfileModelObj.User_LastName = response[0][0].User_LastName;
      this.UserProfileModelObj.User_LoginName = response[0][0].User_LoginName;
      this.UserProfileModelObj.User_Password = response[0][0].User_Password;
      this.UserProfileModelObj.User_PasswordConfirm = response[0][0].User_Password;
      this.UserProfileModelObj.User_CellNumber = response[0][0].User_CellNumber;
      this.UserProfileModelObj.User_CompanyName = response[0][0].User_CompanyName;
      this.UserProfileModelObj.User_ImagePath = response[0][0].User_ImagePath;
      this.UserProfileModelObj.User_Address = response[0][0].User_Address;
      this.UserProfileModelObj.User_City = response[0][0].User_City;
      this.UserProfileModelObj.User_Zip = response[0][0].User_Zip;
      this.UserProfileModelObj.User_State_strval = response[0][0].User_State_strval;
      this.UserProfileModelObj.User_Email = response[0][0].User_Email;

      this.FullName = response[0][0].User_FirstName + '_' + response[0][0].User_LastName;

      this.formUsrCommonGroup.disable();
      this.IsEditDisable = true;
      this.FormDisabledCustom = true;
      this.button = "Update";
    })

  }

  inputelement:any;
  uploadprofileimage(documentInput: any){
    var cfrm = confirm("Are you want to update this record then click on update button");
    if (cfrm == true) {
      this.inputelement = documentInput.target.files[0]
    }
   

  }

  FormButton(content) {
    this.contentx = content;
    debugger;
 
    this.submitted = true;
    this.submittedchange = true;
    if (this.UserProfileModelObj.User_Password == '') {
      alert ("Please enter Password"); 
    }

  else if (this.UserProfileModelObj.User_PasswordConfirm == '') {
    alert ("Please enter confirm password"); 
  }
    
      
// If Not same return False.     
else if (this.UserProfileModelObj.User_Password != this.UserProfileModelObj.User_PasswordConfirm) { 
    alert ("\nPassword did not match: Please try again...") 
    return false; 
} 
   
    if (this.formUsrCommonGroup.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = "Processing";
    console.log('check data', this.UserProfileModelObj)
  if (this.inputelement == '' || this.inputelement == undefined) 
  {
    this.xUserProfileService
    .UpdateUserProfileData( this.UserProfileModelObj)
    .subscribe(response => {
      console.log("updateprofile", response);
    
      this.MessageFlag = "Profile Updated...!";
      this.isLoading = false;
     
      this.commonMessage(this.contentx);
      this.IsEditDisable = true;
      this.GetProfileDetails();

    });
  }
  else
  {
    this.UserProfileModelObj.Filedata = this.inputelement;
    this.xUserProfileService
    .ImageUpdate(this.UserProfileModelObj)
    .then((res) => {
      res.subscribe(response => {
      console.log("updateprofile", response);
    
      this.MessageFlag = "Profile Updated...!";
      this.isLoading = false;
     
      this.commonMessage(this.contentx);
      this.IsEditDisable = true;
     
      this.GetProfileDetails();
    });
  });
  }
  
   
  }
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          //this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
 
  EditForms() {
    debugger
   this.IsEditDisable = false;
   
   this.formUsrCommonGroup.enable();

   this.FormDisabledCustom = false;
 }

// password match
 MustMatch(controlName: string, matchingControlName: string) {
   debugger;
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

}
