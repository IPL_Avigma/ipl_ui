import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { UserProfileComponent } from './UserProfileComponent';


export const ReportsRouts = [
  { path: "profile", component: UserProfileComponent }
];

@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    RouterModule.forChild(ReportsRouts),
    CommonModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    



  ],
  providers: [],
  bootstrap: [UserProfileComponent]
})

export class UserProfileModule {}
