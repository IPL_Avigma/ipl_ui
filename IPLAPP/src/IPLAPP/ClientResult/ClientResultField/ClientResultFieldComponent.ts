import { Component, Injectable, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";


import { MasterlayoutComponent } from "../../Home/MasterComponent";
import { TaskBidMasterModel } from '../ClientResults/ClientResultModel';
import { ClientResultServices } from '../ClientResults/ClientResultServices';
import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";

@Component({
  templateUrl: "./ClientResultField.html"
})
export class ClientResultFieldComponent implements OnInit {
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;

  constructor(
    private xRouter: Router,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private xClientResultServices: ClientResultServices,
    private EncrDecr: EncrDecrServiceService

  ) {
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      console.log('mohitdec',this.decuser);
      // if (this.decuser[0].GroupRoleId === 1) {
      //   this.hidetab = true;
        
      // }
      switch (this.decuser[0].GroupRoleId) {
        case 1:
          {
            this.OfficeResulth = false;
            this.processorh= false;
            this.tabhide = false;
         
            break;
          }
          case 2:
            {
              this.OfficeResulth = true;
              this.processorh= false;
              this.tabhide = true;
             
              break;
            }
            case 3:
              {
                this.OfficeResulth = false;
                 this.processorh= false;
                 this.tabhide = false;
              
                break;
              }
              case 4:
                {
                  this.OfficeResulth = false;
                  this.processorh= true;
                  this.tabhide = true;
                
                  break;
                }
        }

    }
  }
  ngOnInit() {
    this.getModelData();
  }

  ModelObj: any;
  BindData: any;
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel();

  getModelData() {
    //debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      console.log("response clinet but work order se click", response);

      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
      if (this.ModelObj == undefined) {
        this.xRouter.navigate(["/workorder/view"]);
      } else {
        console.log("work order obj fields", this.ModelObj);

        //this.BindData = this.ModelObj;
      }
    });
  }
}
