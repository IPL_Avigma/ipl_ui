import { Component, OnInit , Input,Output, EventEmitter} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { MasterlayoutComponent } from "../../Home/MasterComponent";
import { WorkOrderDrodownServices } from "../../CommonServices/commonDropDown/DropdownService";
import { ClientResultServices } from "../ClientResults/ClientResultServices";
import {
  TaskBidMasterModel,
  Task_Invoice_MasterModel,
  TaskDamageMasterModel,
  BindDataModel
} from "../ClientResults/ClientResultModel";

import { BidInvoiceItemViewTaskServices } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskServices";
import { BidInvoiceItemViewTaskModel } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskModel";

import { AddDamageModel, AddApplianceModel } from "../../Admin/Damage/AddDamage/AddDamageModel";
import { ViewDamageServices } from "../../Admin/Damage/ViewDamage/ViewDamageServices";

import { ClientResultPhotoModel } from "../ClientResultPhoto/ClientResultPhotoModel";

import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";


@Component({
  selector: "app-bid-client-result",
  templateUrl: "./CommonClientResultComponent.html"
})
export class CommonClientResultComponent implements OnInit {
  @Input() values: Array<Object>=[];
 

  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel(); // task
  Task_Invoice_MasterModelObj: Task_Invoice_MasterModel = new Task_Invoice_MasterModel();
  TaskDamageMasterModelObj: TaskDamageMasterModel = new TaskDamageMasterModel();

  BidInvoiceItemViewTaskModelObj: BidInvoiceItemViewTaskModel = new BidInvoiceItemViewTaskModel();

  AddDamageModelObj: AddDamageModel = new AddDamageModel();
  AddApplianceModelObj: AddApplianceModel = new AddApplianceModel();

  BindDataModelObj: BindDataModel = new BindDataModel();
  ClientResultPhotoModelObj: ClientResultPhotoModel = new ClientResultPhotoModel();

  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
 

  ClientResultBidArray = [];
  ClientResultCreateCompletionArray = [];
  ClientResultDamageArray = [];
  Appdata:any; 
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xClientResultServices: ClientResultServices,
    private xWorkOrderDrodownServices: WorkOrderDrodownServices,
    private xBidInvoiceItemViewTaskServices: BidInvoiceItemViewTaskServices,
    private xViewDamageServices: ViewDamageServices,
    private modalService: NgbModal,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService

  ) {
    debugger;
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      console.log('mohitdec',this.decuser);
      // if (this.decuser[0].GroupRoleId === 1) {
      //   this.hidetab = true;
        
      // }
      switch (this.decuser[0].GroupRoleId) {
        case 1:
          {
            this.OfficeResulth = false;
            this.processorh= false;
            this.tabhide = false;
         
            break;
          }
          case 2:
            {
              this.OfficeResulth = true;
              this.processorh= false;
              this.tabhide = true;
             
              break;
            }
            case 3:
              {
                this.OfficeResulth = false;
                 this.processorh= false;
                 this.tabhide = false;
              
                break;
              }
              case 4:
                {
                  this.OfficeResulth = false;
                  this.processorh= true;
                  this.tabhide = true;
                
                  break;
                }
        }

    }
  

    this.ClientResultBidArray = [
      {
        Task_Bid_pkeyID: 0,
        Task_Bid_TaskID: 0,
        Task_Bid_WO_ID: 0,
        Task_Bid_Qty: 1,
        Task_Bid_Uom_ID: 0,
        Task_Bid_Cont_Price: 0,
        Task_Bid_Cont_Total: 0,
        Task_Bid_Clnt_Price: 0,
        Task_Bid_Clnt_Total: 0,
        Task_Bid_Comments: "",
        Task_Bid_Violation: false,
        Task_Bid_damage: false,
        Task_Bid_IsActive: true,
        Task_Bid_IsDelete: false,
        Task_Bid_PresetTemp: "other",
        Task_Bid_PreTextHide: false
      }
    ];

    this.ClientResultDamageArray = [
      {
        Task_Damage_pkeyID: 0,
        Task_Damage_WO_ID: 0,
        Task_Damage_Task_ID: 0,
        Task_Damage_ID: 0,
        Task_Damage_Type: 0,
        Task_Damage_Int: "1",
        Task_Damage_Location: "",
        Task_Damage_Qty: 1,
        Task_Damage_Estimate: "",
        Task_Damage_Disc: "",
        Task_Damage_IsActive: true,
        Task_Damage_IsDelete: false,
        Task_Damage_Status: 0
      }
    ];

    this.ClientResultCreateCompletionArray = [{
      Task_Inv_pkeyID : 0,
      Task_Inv_TaskID: 0,
      Task_Inv_WO_ID: 0,
      Task_Inv_Qty: 0,
      Task_Inv_Uom_ID: 0,
      Task_Inv_Cont_Price: 0,
      Task_Inv_Cont_Total: 0,
      Task_Inv_Clnt_Price: 0,
      Task_Inv_Clnt_Total: 0,
      Task_Inv_Comments: '',
      Task_Inv_Violation: false,
      Task_Inv_damage: false,
      Task_Inv_IsActive: true,
      Task_Inv_Status:0,
    }]

    this.Appdata=[    
      {    
        "Id": 1,    
        "Name": "Yes"    
      },    
      {    
        "Id": 2,    
        "Name": "No"    
      },    
      {    
        "Id": 3,    
        "Name": "Missing"    
      },    
      {    
        "Id": 4,    
        "Name": "Damage"    
      }   
           
    ]    
  

    this.getModelData();

  }

  

  ngOnInit() {
  
  }

 

 
  // submit button call bid client
  ClientResultBidSumbit(content) {
    this.contentx = content;

    this.isLoading = true;
    this.button = "Processing";

    //this.CopyData();

    this.ClientResultBidArray;

    this.TaskBidMasterModelObj.ClientResultBidTaskArray = this.ClientResultBidArray;

    this.xClientResultServices
      .ClientResultTaskBidPost(this.TaskBidMasterModelObj)
      .subscribe(response => {
        console.log("post bid", response);

        this.isLoading = false;
        this.button = "Save";

        this.MessageFlag = "Data Saved...!";

        this.commonMessage(this.contentx);

        this.ClientResultTaskBidGetWorkOrderIdGet();
      });
  }

  // btn invoice
  ClientResultInvoiceSumbit(content) {
    this.contentx = content;

    this.isLoading = true;
    this.button = "Processing";

    //this.CopyData();

    this.ClientResultCreateCompletionArray;

    this.TaskBidMasterModelObj.ClientResultCreateCompletionArray = this.ClientResultCreateCompletionArray;

    this.xClientResultServices
      .ClientResultTaskInvoicePost(this.TaskBidMasterModelObj)
      .subscribe(response => {
        console.log("post invoice", response);

        this.isLoading = false;
        this.button = "Save";

        this.MessageFlag = "Data Saved...!";

        this.commonMessage(this.contentx);

        this.ClientResultTaskBidGetWorkOrderIdGet();
      });
  }

  ClientResultDamageSumbit(content) {
    debugger;
    this.contentx = content;

    this.isLoading = true;
    this.button = "Processing";

    //this.CopyData();

    this.ClientResultDamageArray;

    this.TaskBidMasterModelObj.ClientResultDamageArray = this.ClientResultDamageArray;

    this.xClientResultServices
      .ClientResultTaskDamagePost(this.TaskBidMasterModelObj)
      .subscribe(response => {
        console.log("post invoice", response);

        this.isLoading = false;
        this.button = "Save";

        this.MessageFlag = "Data Saved...!";

        this.commonMessage(this.contentx);

        this.ClientResultTaskBidGetWorkOrderIdGet();
      });
  }

  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  BindData: any;
  
  getModelData() {
    //debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      console.log("response clinet but work order se click", response);

      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
      if (this.ModelObj == undefined) {
        this.xRouter.navigate(["/workorder/view"]);
      } else {
        console.log("final bind Main Client M", this.ModelObj);
        this.TaskBidMasterModelObj.workOrder_ID = this.ModelObj.workOrder_ID;

        this.GetDropDownData();
      }
    });
  }

  TaskList: any;
  DamageList: any;
  UOMList: any;
  IntExtList: any;
  PresetText: any;
  PresetTemp: any;
  ApllianceName: any;
  GetDropDownData() {
    this.IntExtList = [{ Id: "1", Name: "Int" }, { Id: "2", Name: "Ext" }];

    this.xWorkOrderDrodownServices
      .DropdownGetClientResult()
      .subscribe(response => {
        console.log("drop d client data", response);

        //debugger;
        this.TaskList = response[0];
        // this for tast set invoice task list
        this.xMasterlayoutComponent.masterFunctionCallClinetTaskDropDown(
          this.TaskList
        );
        this.DamageList = response[1];
        this.UOMList = response[2];
        this.PresetText = response[3];
        this.ApllianceName = response[4];
        for (let i = 0; i < this.ApllianceName.length; i++) {
          this.ApllianceName[i].Appl_Status_Id = 1;
          
        }
      

        this.ClientResultTaskBidGetWorkOrderIdGet();
      });
  }
  GetMetaDataTaskPkeyidWise() {
    this.xBidInvoiceItemViewTaskServices
      .ViewTaskMasterData(this.BidInvoiceItemViewTaskModelObj)
      .subscribe(response => {
        console.log("task pkey id wise", response);

        //debugger;

        let task_pkey_id = this.BidInvoiceItemViewTaskModelObj.Task_pkeyID;

        for (let i = 0; i < this.ClientResultBidArray.length; i++) {
          let element = this.ClientResultBidArray[i].Task_Bid_TaskID;
          if (task_pkey_id == element) {
            this.ClientResultBidArray[i].Task_Bid_Uom_ID =
              response[0][0].Task_UOM;
            this.ClientResultBidArray[i].Task_Bid_Cont_Price =
              response[0][0].Task_Contractor_UnitPrice;
            this.ClientResultBidArray[i].Task_Bid_Clnt_Price =
              response[0][0].Task_Client_UnitPrice;

            this.ClientResultBidArray[i].Task_Bid_Cont_Total =
              response[0][0].Task_Contractor_UnitPrice;
            this.ClientResultBidArray[i].Task_Bid_Clnt_Total =
              response[0][0].Task_Client_UnitPrice;
          }
        }
        this.ClinetResultQtyBid_Method();
      });
  }
  // task meta call here
  ClinetResultBid_Method(item) {
    //debugger;

    this.BidInvoiceItemViewTaskModelObj.Task_pkeyID = item.Task_Bid_TaskID;

    this.GetMetaDataTaskPkeyidWise();
  }

  ClinetResultQtyBid_Method() {
    //this.CopyData();
    //this.Task_Invoice_MasterModelObj.Task_Inv_Qty = this.TaskBidMasterModelObj.Task_Bid_Qty;

    for (let i = 0; i < this.ClientResultBidArray.length; i++) {
      if (this.ClientResultBidArray[i].Task_Bid_Qty != "") {
        this.ClientResultBidArray[i].Task_Bid_Cont_Total =
          this.ClientResultBidArray[i].Task_Bid_Cont_Price *
          this.ClientResultBidArray[i].Task_Bid_Qty;
        this.ClientResultBidArray[i].Task_Bid_Clnt_Total =
          this.ClientResultBidArray[i].Task_Bid_Clnt_Price *
          this.ClientResultBidArray[i].Task_Bid_Qty;
      } else {
        //alert('plz enter number only');

        // care fully
        this.ClientResultBidArray[
          i
        ].Task_Bid_Cont_Total = this.ClientResultBidArray[
          i
        ].Task_Bid_Cont_Price;
        this.ClientResultBidArray[
          i
        ].Task_Bid_Clnt_Total = this.ClientResultBidArray[
          i
        ].Task_Bid_Clnt_Price;
      }
    }
  }
  ClinetResultBid_Cont_Price_Method() {
    for (let i = 0; i < this.ClientResultBidArray.length; i++) {
      if (this.ClientResultBidArray[i].Task_Bid_Cont_Price != "") {
        this.ClientResultBidArray[
          i
        ].Task_Bid_Cont_Total = this.ClientResultBidArray[
          i
        ].Task_Bid_Cont_Price;

        if (this.ClientResultBidArray[i].Task_Bid_Qty != "") {
          this.ClientResultBidArray[i].Task_Bid_Cont_Total =
            this.ClientResultBidArray[i].Task_Bid_Cont_Price *
            this.ClientResultBidArray[i].Task_Bid_Qty;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }

  ClinetResultBid_Clnt_Price_Method() {
    for (let i = 0; i < this.ClientResultBidArray.length; i++) {
      if (this.ClientResultBidArray[i].Task_Bid_Clnt_Price != "") {
        this.ClientResultBidArray[
          i
        ].Task_Bid_Clnt_Total = this.ClientResultBidArray[
          i
        ].Task_Bid_Clnt_Price;

        if (this.ClientResultBidArray[i].Task_Bid_Qty != "") {
          this.ClientResultBidArray[i].Task_Bid_Clnt_Total =
            this.ClientResultBidArray[i].Task_Bid_Clnt_Price *
            this.ClientResultBidArray[i].Task_Bid_Qty;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }

  // clinet result Bid Add more row
  data: any =[];
  AddMoreRowClientResultBid() {
    debugger;
  
    this.data = {
      Task_Bid_pkeyID: 0,
      Task_Bid_TaskID: 0,
      Task_Bid_WO_ID: 0,
      Task_Bid_Qty: 1,
      Task_Bid_Uom_ID: 0,
      Task_Bid_Cont_Price: 0,
      Task_Bid_Cont_Total: 0,
      Task_Bid_Clnt_Price: 0,
      Task_Bid_Clnt_Total: 0,
      Task_Bid_Comments: "",
      Task_Bid_Violation: false,
      Task_Bid_damage: false,
      Task_Bid_IsActive: true,
      Task_Bid_IsDelete: false,
      Task_Bid_PresetTemp: "other",
      Task_Bid_PreTextHide: false
    };

    this.ClientResultBidArray.push(this.data);
    this.AddMoreClinetResultInvoice();
  }

  BidDeleteArray = [];
  RemoveClientResultBid(index, Item) {
    debugger;
    let Cnfm = confirm("Are you sure remove this records..?");

    if (Cnfm) {
      if (Item.Task_Bid_pkeyID != 0) {
        this.BidDeleteArray = [];

        this.BidDeleteArray.push(Item);

        this.TaskBidMasterModelObj.ClientResultBidTaskArray = this.BidDeleteArray;

        this.TaskBidMasterModelObj.Type = 4;
        this.xClientResultServices
          .ClientResultTaskBidPost(this.TaskBidMasterModelObj)
          .subscribe(response => {
            console.log("post bid", response);
            debugger;
            if (index !== -1)  {
              this.ClientResultBidArray.splice(index, 1);
              this.ClientResultTaskBidGetWorkOrderIdGet();
            }
            // for reset sathi

            this.ClientResultTaskBidGetWorkOrderIdGet();
          });
      } else {
        this.ClientResultBidArray.splice(index, 1);
       
      }
    }

    if (this.ClientResultBidArray.length == 0) {
      this.AddMoreRowClientResultBid();
    }
   
  }

  // client result create completion
  AddMoreClinetResultInvoice() {
    let data = {
      Task_Inv_pkeyID: 0,
      Task_Inv_TaskID: 0,
      Task_Inv_Qty: 1,
      Task_Inv_Uom_ID: 0,
      Task_Inv_Cont_Price: 0,
      Task_Inv_Cont_Total: 0,
      Task_Inv_Clnt_Price: 0,
      Task_Inv_Clnt_Total: 0,
      Task_Inv_Comments: "",
      Task_Inv_Violation: false,
      Task_Inv_damage: false,
      Task_Inv_IsActive: true,
      Task_Inv_IsDelete: false,
      Task_Inv_Status: 0,
      Task_Inv_Auto_Invoice: 0
    };

    this.ClientResultCreateCompletionArray.push(data);
  }

  InvoiceDelete = [];
  clientResultInvocieRemove(index, INVOICE) {
    debugger;
    let Cnfm = confirm("Are you sure remove this records..?");

    if (Cnfm) {
      if (INVOICE.Task_Inv_pkeyID != 0) {
        this.InvoiceDelete.push(INVOICE);

        this.TaskBidMasterModelObj.ClientResultCreateCompletionArray = this.InvoiceDelete;

        this.TaskBidMasterModelObj.Type = 4;
        this.xClientResultServices
          .ClientResultTaskInvoicePost(this.TaskBidMasterModelObj)
          .subscribe(response => {
            console.log("post invoice", response);
            if (index !== -1) {
              this.ClientResultCreateCompletionArray.splice(index, 1);
              this.ClientResultTaskBidGetWorkOrderIdGet();
            }
            // this.TaskBidMasterModelObj.Type = 1;

            this.ClientResultTaskBidGetWorkOrderIdGet();
          });
      } else {
        this.ClientResultCreateCompletionArray.splice(index, 1);
      }
    }

    if (this.ClientResultCreateCompletionArray.length == 0) {
      this.AddMoreClinetResultInvoice();
    }
  }

  // client result Damage
  AddClientResultDamage() {
    let data = {
      Task_Damage_pkeyID: 0,
      Task_Damage_WO_ID: 0,
      Task_Damage_Task_ID: 0,
      Task_Damage_ID: 0,
      Task_Damage_Type: 0,
      Task_Damage_Int: "1",
      Task_Damage_Location: "",
      Task_Damage_Qty: 1,
      Task_Damage_Estimate: "",
      Task_Damage_Disc: "",
      Task_Damage_IsActive: true,
      Task_Damage_IsDelete: false,
      Task_Damage_Status: 0
    };
    this.ClientResultDamageArray.push(data);
  }

  DeleteDamage = [];
  RemoveClientResultDamage(index, DAMAGE) {
    let Cnfm = confirm("Are you sure remove this records..?");
    if (Cnfm) {
      if (DAMAGE.Task_Damage_pkeyID != 0) {
        this.DeleteDamage.push(DAMAGE);

        this.TaskBidMasterModelObj.Type = 4; // for delete

        this.TaskBidMasterModelObj.ClientResultDamageArray = this.DeleteDamage;

        this.xClientResultServices
          .ClientResultTaskDamagePost(this.TaskBidMasterModelObj)
          .subscribe(response => {
            console.log("post damage", response);
            if (index !== -1) {
              this.ClientResultDamageArray.splice(index, 1);
              this.ClientResultTaskBidGetWorkOrderIdGet();
            }

            // this.TaskBidMasterModelObj.Type = 1;

            this.ClientResultTaskBidGetWorkOrderIdGet();
          });
      } else {
        this.ClientResultDamageArray.splice(index, 1);
      }
    }
    if (this.ClientResultDamageArray.length == 0) {
      this.AddClientResultDamage();
    }
  }

  // for preset text

  toggleShow() {
    debugger;

    for (let i = 0; i < this.ClientResultBidArray.length; i++) {
      if (this.ClientResultBidArray[i].Task_Bid_PreTextHide) {
        this.ClientResultBidArray[i].Task_Bid_PreTextHide = true;
      }
    }
  }
  // end code

  /// Create complection start
  /*


  create completion code start
  copy


  */

  ClientResultCompletionInvoice(INVOICE) {
    this.BidInvoiceItemViewTaskModelObj.Task_pkeyID = INVOICE.Task_Inv_TaskID;

    this.xBidInvoiceItemViewTaskServices
      .ViewTaskMasterData(this.BidInvoiceItemViewTaskModelObj)
      .subscribe(response => {
        console.log("task pkey id wise invoice", response);

        //debugger;

        let task_pkey_id = this.BidInvoiceItemViewTaskModelObj.Task_pkeyID;

        for (
          let i = 0;
          i < this.ClientResultCreateCompletionArray.length;
          i++
        ) {
          let element = this.ClientResultCreateCompletionArray[i]
            .Task_Inv_TaskID;
          if (task_pkey_id == element) {
            this.ClientResultCreateCompletionArray[i].Task_Inv_Uom_ID =
              response[0][0].Task_UOM;
            this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Price =
              response[0][0].Task_Contractor_UnitPrice;
            this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Total =
              response[0][0].Task_Contractor_UnitPrice;
            this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Price =
              response[0][0].Task_Client_UnitPrice;
            this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Total =
              response[0][0].Task_Client_UnitPrice;
            this.ClientResultCreateCompletionArray[i].Task_Inv_Auto_Invoice =
              response[0][0].Task_AutoInvoiceComplete;
          }
        }
        this.ClientResultQtyCompletionInvoice_Method();
      });
  }

  ClientResultQtyCompletionInvoice_Method() {
    for (let i = 0; i < this.ClientResultCreateCompletionArray.length; i++) {
      if (this.ClientResultCreateCompletionArray[i].Task_Inv_Qty != "") {
        this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Total =
          this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Price *
          this.ClientResultCreateCompletionArray[i].Task_Inv_Qty;
        this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Total =
          this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Price *
          this.ClientResultCreateCompletionArray[i].Task_Inv_Qty;
      } else {
        //alert('plz enter number only');

        //carefully
        this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Cont_Total = this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Cont_Price;
        this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Clnt_Total = this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Clnt_Price;
      }
    }
  }

  ClientResultCompletionInvoiceCont_Total_Method() {
    for (let i = 0; i < this.ClientResultCreateCompletionArray.length; i++) {
      if (this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Price != "") {
        this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Cont_Total = this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Cont_Price;

        if (this.ClientResultCreateCompletionArray[i].Task_Inv_Qty != "") {
          this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Total =
            this.ClientResultCreateCompletionArray[i].Task_Inv_Cont_Price *
            this.ClientResultCreateCompletionArray[i].Task_Inv_Qty;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }

  ClientResultCompletionInvoiceClnt_Total_Method() {
    for (let i = 0; i < this.ClientResultCreateCompletionArray.length; i++) {
      if (this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Price != "") {
        this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Clnt_Total = this.ClientResultCreateCompletionArray[
          i
        ].Task_Inv_Clnt_Price;

        if (this.ClientResultCreateCompletionArray[i].Task_Inv_Qty != "") {
          this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Total =
            this.ClientResultCreateCompletionArray[i].Task_Inv_Clnt_Price *
            this.ClientResultCreateCompletionArray[i].Task_Inv_Qty;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }

  /*

Damage code here
plz carefull code here


*/

  ClientResult_DamageType(DAMAGE) {
    debugger;
    this.AddDamageModelObj.Damage_pkeyID = DAMAGE.Task_Damage_Type;

    this.xViewDamageServices
      .ViewDamageData(this.AddDamageModelObj)
      .subscribe(response => {
        console.log("resp damage pkey", response);

        for (let i = 0; i < this.ClientResultDamageArray.length; i++) {
          if (
            this.AddDamageModelObj.Damage_pkeyID ==
            this.ClientResultDamageArray[i].Task_Damage_Type
          ) {
            this.ClientResultDamageArray[i].Task_Damage_Int =
              response[0][0].Damage_Int;
            this.ClientResultDamageArray[i].Task_Damage_Location =
              response[0][0].Damage_Location;
            this.ClientResultDamageArray[i].Task_Damage_Qty =
              response[0][0].Damage_Qty;
            this.ClientResultDamageArray[i].Task_Damage_Estimate =
              response[0][0].Damage_Estimate;
            this.ClientResultDamageArray[i].Task_Damage_Disc =
              response[0][0].Damage_Disc;
          }
        }
      });
  }

  //text
  ClientrsultBid_PresetSET(item, index) {
    debugger;

    console.log("present seletct", item);

    for (let i = 0; i < this.ClientResultBidArray.length; i++) {
      if (i == index) {
        this.ClientResultBidArray[i].Task_Bid_Comments =
          this.ClientResultBidArray[i].Task_Bid_Comments +
          " " +
          this.ClientResultBidArray[i].Task_Bid_PresetTemp; //.Task_Preset_Text;
      }
    }
  }

  ClientResultBidGetArray: any;
  ClientResultCreateCompletionGetArray: any;
  ClientResultDamageArrayGet: any;

  ClientResultTaskBidGetWorkOrderIdGet() {
    this.TaskBidMasterModelObj.Type = 3;
    this.TaskBidMasterModelObj.Task_Bid_WO_ID = this.TaskBidMasterModelObj.workOrder_ID;

    this.xClientResultServices
      .ClientResultTaskBidGetWorkOrderId(this.TaskBidMasterModelObj)
      .subscribe(response => {
        console.log("Task Bid invoice and get by WO D set", response);

        //debugger;
        if (response[0][0].length != 0) {
          this.ClientResultBidGetArray = response[0][0];
          this.ClientResultBidArray = response[0][0];
        }
        if (response[1][0].length != 0) {
          this.ClientResultCreateCompletionGetArray = response[1][0];
          this.ClientResultCreateCompletionArray = response[1][0];
        }
        if (response[2][0].length != 0) {
          this.ClientResultDamageArray = response[2][0];
          this.ClientResultDamageArrayGet = response[2][0];
        }
        if (response[3][0].length != 0) {
          this.ApllianceName = response[3][0];
          //this.ClientResultDamageArrayGet = response[3][0];
        }

        //this.AsycWaitPhotoscall();
      });
  }
  AddApplianceData(item, content){
    debugger;
    {
       this.button = "Processing";
      this.contentx = content;
      this.AddApplianceModelObj.Appliancearr = item;
   this.AddApplianceModelObj.Appl_Wo_Id = this.ModelObj.workOrder_ID;
  
      this.xViewDamageServices
        .AddApplianceData(this.AddApplianceModelObj)
        .subscribe(response => {
          console.log("Add Appliance", response);
          this.MessageFlag = "Appliance Saved...!";

          this.commonMessage(this.contentx);
          this.button = "Save";


        });
      }
  }
getdata(item){
  debugger;
  
  console.log("valcheck",item);
     for (let i = 0; i < item.length; i++) {
       this.ClientResultBidArray.push(item[i]);
         }
         //item = [];
}

CompletionData(itex){
  debugger;
  
  console.log("comcheck",itex);
  for (let i = 0; i < itex.length; i++) {
    this.ClientResultCreateCompletionArray.push(itex[i]);
      }
     // itex = [] ;
       console.log("data",this.ClientResultCreateCompletionArray);
    
         
}

validate(evt) {
  debugger
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}



  // hitorycopy: any;
  // HistoryBidCopy(){
  //   debugger;
  //   this.hitorycopy = this.xMasterlayoutComponent.masterFunctionGetdata();

  //   this.ClientResultBidArray.push(this.hitorycopy);
  //   this.AddMoreRowClientResultBid();
  // }

  // Reset(){
  //   this.TaskBidMasterModelObj.workOrder_ID = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_pkeyID = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_TaskID = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_WO_ID = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_Qty  = "";
  //   this.TaskBidMasterModelObj.Task_Bid_Uom_ID = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_Cont_Price = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_Cont_Total = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_Clnt_Price = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_Clnt_Total = 0;
  //   this.TaskBidMasterModelObj.Task_Bid_Comments  = "";
  //   this.TaskBidMasterModelObj.Task_Bid_Violation = false;
  //   this.TaskBidMasterModelObj.Task_Bid_damage = false;
  //   this.TaskBidMasterModelObj.Task_Bid_IsActive = true;
  //   this.TaskBidMasterModelObj.Task_Bid_IsDelete = false;
  //   this.TaskBidMasterModelObj.UserID = 0;
  //   this.TaskBidMasterModelObj.Type = 1;
  //   this.ClientResultTaskBidGetWorkOrderIdGet();
  // }

}
