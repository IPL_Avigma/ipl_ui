import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from '@progress/kendo-angular-layout';

import { CommonClientResultComponent } from "./CommonClientResultComponent";
import {CommonDirectiveModule} from '../../AppDirectives/DirectiveModule';
import { BidInvoiceItemViewTaskServices } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskServices";
import { ClientResultServices } from "../ClientResults/ClientResultServices";
import { ClientResultOldPhotoServices } from "../ClientResultPhoto/ClientResultphotosOldServices";


@NgModule({
  declarations: [CommonClientResultComponent],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    CommonDirectiveModule,
    HttpClientModule,
    LayoutModule


  ],
  exports: [
    CommonClientResultComponent
  ],

  providers: [BidInvoiceItemViewTaskServices,ClientResultServices,ClientResultOldPhotoServices],
  bootstrap: [CommonClientResultComponent]
})

export class CommonClientResultModule {}
