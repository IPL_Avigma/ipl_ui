import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MasterlayoutComponent } from "../../Home/MasterComponent";
import {BindDataModel, TaskBidMasterModel, CopyWorkOderModel} from '../ClientResults/ClientResultModel';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ClientResultServices } from '../ClientResults/ClientResultServices';

import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";
import {WorkOrderModel,UpdateStausDataModel} from "../../WorkOrder/NewWorkOder/NewWorkOrderModel";
import {SaveWorkOrderServices} from "../../WorkOrder/NewWorkOder/NewWorkOrderServices"
import{ClientResultsInvoiceServices} from '../ClientResultsInvoice/ClientResultsInvoiceServices'
import{CommonStatusDTO} from './CommonStatusModel'
//import {CommonPhotosMetaComponent} from '../CommonPhotosMeta/CommonPhotosMetaComponent'


import { from } from 'rxjs';





@Component({
  selector: "app-header-client-result",
  templateUrl: "./CommonClientHeader.html"
})
export class CommonClientHeaderComponent implements OnInit {
  UpdateStausDataModelObj: UpdateStausDataModel =new UpdateStausDataModel();
  CommonStatusDTOObj: CommonStatusDTO = new CommonStatusDTO();
  BindDataModelObj: BindDataModel = new BindDataModel();
  CopyWorkOderModelObj: CopyWorkOderModel = new CopyWorkOderModel();
  TaskBidMasterModelObj:TaskBidMasterModel = new TaskBidMasterModel();
  WorkOrderModelObj:WorkOrderModel = new WorkOrderModel();
  MessageFlag: string;
  public contentx;
  HeaderCoditional:boolean = false;
  formUsrCommonGroup: FormGroup;
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;
  fieldtab:boolean = false;

  constructor(
    private xClientResultServices : ClientResultServices,
    private xSaveWorkOrderServices : SaveWorkOrderServices,
    private modalService: NgbModal,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xClientResultsInvoiceServices: ClientResultsInvoiceServices,
    private formBuilder: FormBuilder,
   //private methcall: CommonPhotosMetaComponent

  ) {
    //alert("hi");
    this.SetClass();

    //alert(this.xRouter.url);
    {
      if(localStorage.getItem('usertemp_') != null)
      {
        var encuser = JSON.parse(localStorage.getItem('usertemp_'));
        var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
        this.decuser  =JSON.parse(decval) ;
        console.log('mohitdec',this.decuser);
        // if (this.decuser[0].GroupRoleId === 1) {
        //   this.hidetab = true;
          
        // }
        switch (this.decuser[0].GroupRoleId) {
          case 1:
            {
              this.OfficeResulth = false;
              this.processorh= false;
              this.tabhide = false;
           
              break;
            }
            case 2:
              {
                this.OfficeResulth = false;
                this.processorh= false;
                this.tabhide = true;
                this.fieldtab = false;
               
                break;
              }
              case 3:
                {
                  this.OfficeResulth = false;
                   this.processorh= false;
                   this.tabhide = false;
                
                  break;
                }
                case 4:
                  {
                    this.OfficeResulth = false;
                    this.processorh= true;
                    this.tabhide = false;
                    this.fieldtab = true;
                  
                    break;
                  }
          }
  
      }
    }


  }
  

  ngOnInit() {
    this.formUsrCommonGroup = this.formBuilder.group({
    })
   
    this.getModelData();
  
     
   
  
  }

  GoToBack() {
    this.xRouter.navigate(["/workorder/view"]);
  }

  PrintWindow() {
    window.print();
  }

  EditWorkOrder() {
    debugger
   
  //  this.getModelData();
  //   Object.assign(this.BindData, {Edit: "1"});
    
  //   console.log("Editdetails", this.BindData);
  //   this.xMasterlayoutComponent.masterFunctionCall(this.BindData);
  //   this.xRouter.navigate(["/workorder/new"]);
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',workOrderID);
    this.xRouter.navigate(["workorder/createworkorder", btoa(encrypted)]);
  }

  RedirectToMessage(){
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',workOrderID);
    this.xRouter.navigate(["/client/messages", btoa(encrypted)]);

  }






  ResultClass1:string ="active";
  ResultClass2:string ="";
  ResultClass3:string ="";
  ResultClassP:string ="";
  ResultClass4:string ="";
  ResultClass5:string ="";
  SetClass(){
  let ClassVal =  this.xMasterlayoutComponent.masterFunctionCallClinetNavigateGet();

  if(ClassVal != undefined){

    this.ResultClass1 = ClassVal.Class1;
    this.ResultClass2 = ClassVal.Class2;
    this.ResultClass3 = ClassVal.Class3;
    this.ResultClass4 = ClassVal.Class4;
    this.ResultClassP = ClassVal.ClassP;
    this.ResultClass5 = ClassVal.ResultClass5;
  }
  }





  CleintResultMainCall(){
    if (this.workorder === undefined) {
      return;
    }
    this.ResultClass1 = "active";
    this.ResultClass2 ="";
    this.ResultClass3 ="";
    this.ResultClass4 ="";
    this.ResultClassP ="";
    this.ResultClass5 ="";
    this.GetWhichAction();

     // const getencryptWorkOrder = 
    // this.xRouter.navigate(["/client/clientresult/" + this.workorder]);
        var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workorder );
        this.xRouter.navigate(["/client/clientresult/" +  btoa(encrypted)]);
  }

  CleintResultfieldCall(){
   
  
    if (this.workorder === undefined) {
      return;
    }
    this.ResultClass2 = "active";
    this.ResultClass1 = "";
    this.ResultClassP ="";
    this.ResultClass3 ="";
    this.ResultClass4 ="";
    this.ResultClass5 ="";
    this.GetWhichAction();
    // this.xRouter.navigate(["/client/clientresultfield/" + this.workorder]);
      var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workorder );
      this.xRouter.navigate(["/client/clientresultfield/" +  btoa(encrypted)]);
    
  }
  ClentResultinsCall() {
   
    if (this.workorder === undefined) {
      return;
    }
    this.ResultClass3 ="active";
    this.ResultClass2 = "";
    this.ResultClass1 = "";
    this.ResultClassP ="";
    this.ResultClass4 ="";
    this.ResultClass5 ="";
    this.GetWhichAction();


    // this.xRouter.navigate(["/client/clientresultinstruction/" + this.workorder]);
      var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workorder );
      this.xRouter.navigate(["/client/clientresultinstruction/" +  btoa(encrypted)]);
  }
  ClentResultPhotoCall() {
    if (this.workorder === undefined) {
      return;
    }
    this.ResultClass2 = "";
    this.ResultClass1 = "";
    this.ResultClass3 ="";
    this.ResultClassP ="active";
    this.ResultClass4 ="";
    this.ResultClass5 ="";
    this.GetWhichAction();

    // this.xRouter.navigate(["/client/clientresultphoto/" + this.workorder]);
      var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workorder );
      this.xRouter.navigate(["/client/clientresultphoto/" +  btoa(encrypted)]);
  }
  MessagesDetails(){
    if (this.workorder === undefined) {
      return;
    }
    this.ResultClass2 = "";
    this.ResultClass1 = "";
    this.ResultClass3 ="";
    this.ResultClassP ="";
    this.ResultClass4 ="";
    this.ResultClass5 ="active";
    this.GetWhichAction();

    // this.xRouter.navigate(["/client/clientresultphoto/" + this.workorder]);
      var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workorder );
      this.xRouter.navigate(["/client/messages/" +  btoa(encrypted)]);
  }

  InvoiceCall() {

    if (this.workorder === undefined) {
      return;
    }
    this.ResultClass2 = "";
    this.ResultClass1 = "";
    this.ResultClass3 ="";
    this.ResultClassP ="";
    this.ResultClass4 ="active";
    this.GetWhichAction();

    this.getModelData();
    this.xMasterlayoutComponent.masterFunctionCallClinet(this.BindData);
    // this.xRouter.navigate(["/client/clientresultinvoice/" + this.workorder]);
      var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.workorder );
      this.xRouter.navigate(["/client/clientresultinvoice/" + btoa(encrypted)]);
  }




  GetWhichAction(){
   let ClassData = {
      Class1:this.ResultClass1,
      Class2:this.ResultClass2,
      Class3:this.ResultClass3,
      Class4:this.ResultClass4,
      ClassP:this.ResultClassP,
    }
    this.xMasterlayoutComponent.masterFunctionCallClinetNavigateSet(ClassData);
  }

  ModelObj: any;
  BindData: any;
  workorder: string;

 
  getModelData() {
    //debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);

    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      console.log("response clinet but work order se click", response);

      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);
      


      if (this.BindData != undefined) {
        if (this.BindData.status == 2 || this.BindData.status==3 ||this.BindData.status ==8 ||this.BindData.status ==9
          || this.BindData.status == 1 || this.BindData.status == 4) 
        {
          this.CommonStatusDTOObj.Status_ID =  0;
        }
        else
        {
          this.CommonStatusDTOObj.Status_ID =  parseInt(this.BindData.status);
        }
      
            }

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
      if (this.ModelObj == undefined) {
        // this.xRouter.navigate(["/workorder/view"]);
      } else {
        //this.DivHIDEx = true;
        console.log("work order obj instraction Common Header ", this.ModelObj);

      //this.BindDataModelObj.workOrderNumber = this.ModelObj.workOrderNumber;

      this.BindDataModelObj.IPLNO = this.ModelObj.IPLNO;
      this.BindData = this.ModelObj;
 this.workorder = this.BindData.workOrder_ID;
        //this.ClientResultPhotoModelobj.Client_Result_Photo_Wo_ID = this.ModelObj.workOrder_ID;
     
      
    
       
    
      
       
      }
    });
    this.GetStatusDropDown();
  }
  copydata:any;
  workid:any;
  CopyWorkOrderData(arg){
    debugger;
    this.CopyWorkOderModelObj.workOrder_ID = this.BindData.workOrder_ID;
    this.CopyWorkOderModelObj.WorkOderInfo = arg;
    this.CopyWorkOderModelObj.Type = 1;
    this.xClientResultServices
   .CopyWorkOrderDetailsPost(this.CopyWorkOderModelObj)
   .subscribe(response => {
     console.log("Copy WorkOder", response);
    this.workid = response[0].workOrder_ID;
    
     this.GetWorkOderCopyDetails();
   

   });
  }

  GetWorkOderCopyDetails(){
    debugger;
    this.TaskBidMasterModelObj.workOrder_ID =  this.workid;
    this.TaskBidMasterModelObj.Type = 1;

    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      debugger;
      console.log("Copyget WorkOder", response);
      this.copydata = response[0][0];
     let coppynu = Object.assign({Edit: "2"});
       this.xMasterlayoutComponent.masterFunctionCall(coppynu);
     // console.log("copyedit",this.copydata)
     var encrypted = this.EncrDecr.set('123456$#@$^@1ERF', this.copydata.workOrder_ID);
     this.xRouter.navigate(["workorder/createworkorder", btoa(encrypted)]);
      //this.xRouter.navigate(["/client/clientresult/" + this.copydata.workOrder_ID]);
      this.workorder =  this.copydata.workOrder_ID;
     

  })
}
commonMessage(content) {
  this.modalService
    .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
    .result.then(result => {}, reason => {});
}

WorkOrderDelete(content){
  this.contentx = content;
 
  let promp = confirm("Are you sure..?");
    if (promp) {
      if(this.ModelObj.workOrder_ID !="")
      {
 this.WorkOrderModelObj.workOrder_ID = this.ModelObj.workOrder_ID;
 this.WorkOrderModelObj.Type = 4;
this.xSaveWorkOrderServices.deleteworkorder(this.WorkOrderModelObj)
.subscribe(response => {
  debugger;
  if (response[0].Status == 1) {
    this.MessageFlag = "Record Deleted...!";
    this.commonMessage(this.contentx);
    this.xRouter.navigate(['/workorder/view']);
    console.log("Delete Work Order", response);
  }
  
})
      }
    }
}
Statuslst:any;
GetStatusDropDown()
{
  debugger
  this.CommonStatusDTOObj.Status_ID = 0;
  this.CommonStatusDTOObj.Type = 2;
  this.xClientResultsInvoiceServices
  .DropdownGetStatus(this.CommonStatusDTOObj)
  .subscribe(response => {
    console.log("status data", response);
    debugger;
    this.Statuslst = response[0];
   
})

}

//status Update sathi
statusData: any;
updatestatus:any;
StatusUpdate(){
  
  debugger;
  let User = JSON.parse(localStorage.getItem('usertemp_'));
  
 
  this.UpdateStausDataModelObj.workOrder_ID = this.BindData.workOrder_ID;
  this.UpdateStausDataModelObj.status = (this.CommonStatusDTOObj.Status_ID).toString();
  this.UpdateStausDataModelObj.UserId =  User[0].User_pkeyID;
  this.UpdateStausDataModelObj.Type = 1;
 this.xSaveWorkOrderServices.Workorderstatus(this.UpdateStausDataModelObj)
 .subscribe(response => {
  debugger
   console.log('update',response);
   this.statusData = response[0][0];
   alert('Status Updated...');
  //   this.updatestatus = Object.assign( this.ModelObj,{Status_Name:this.statusData.Status_Name});
  //  console.log('updatedata',this.updatestatus);
  
   this.CommonStatusDTOObj.Status_ID =  this.statusData.status;
   this.xMasterlayoutComponent.masterFunctionCall(this.statusData);
  
   this.InvoiceCall();
   
  //window.location.reload();
 });

//  var intstatus = parseInt((this.WorkOrderModelObj.status).toString());
//  this.CommonStatusDTOObj.Status_ID = intstatus
 
}




}
