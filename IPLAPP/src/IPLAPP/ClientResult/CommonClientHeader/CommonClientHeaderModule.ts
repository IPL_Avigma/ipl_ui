import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonClientHeaderComponent } from "./CommonClientHeaderComponent";
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [CommonClientHeaderComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
   
  ],
  exports: [
    CommonClientHeaderComponent
  ],

  providers: [],
  bootstrap: [CommonClientHeaderComponent]
})

export class CommonClientHeaderModule {}
