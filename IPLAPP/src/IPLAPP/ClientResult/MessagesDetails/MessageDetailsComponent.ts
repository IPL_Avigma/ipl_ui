import { Component } from '@angular/core';
//import { MessagingDetailsService } from '../MessagesDetails/MessageDetailsServices'
import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ClientResultServices } from "../ClientResults/ClientResultServices";
import { TaskBidMasterModel } from '../ClientResults/ClientResultModel';
import { AngularFireDatabase } from '@angular/fire/database'
import { AngularFireModule } from '@angular/fire'
import { MessageModelData } from './MessageDetailModel'
import { formatDate } from '@angular/common';
import { MessagingDetailsService } from './MessageDetailsServices'
//new imports
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { tap, map, switchMap, first } from 'rxjs/operators';
import { TouchSequence } from 'selenium-webdriver';
import { of } from 'rxjs';
import md5 from "md5";

@Component({

  templateUrl: "./MessageDetails.html",
  styleUrls: ['./MessageDetails.css']
})
export class MessageDetailsComponent {
  title = 'push-notification';
  message;
  today = new Date();
  jstoday = '';
  encuser: any;
  status: string;
  decuserr:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;
  fieldtab:boolean = false;


  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel();
  MessageModelDataObj: MessageModelData = new MessageModelData();
  constructor(//private messagingService: MessagingDetailsService,
    private EncrDecr: EncrDecrServiceService, private xRoute: ActivatedRoute,
    private xClientResultServices: ClientResultServices,
    private xdatabase: AngularFireDatabase,
    private xfirebase: AngularFireModule,
    private afAuth: AngularFireAuth,
    private xMessagingDetailsService: MessagingDetailsService
  ) { 
    {
      if(localStorage.getItem('usertemp_') != null)
      {
        var encuser = JSON.parse(localStorage.getItem('usertemp_'));
        var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
        this.decuserr  =JSON.parse(decval) ;
        console.log('user data:', this.decuserr);
        switch (this.decuserr[0].GroupRoleId) {
          case 1:
            {
              this.OfficeResulth = false;
              this.processorh= false;
              this.tabhide = false;
           
              break;
            }
            case 2:
              {
                this.OfficeResulth = false;
                this.processorh= false;
                this.tabhide = true;
                this.fieldtab = false;
               
                break;
              }
              case 3:
                {
                  this.OfficeResulth = false;
                   this.processorh= false;
                   this.tabhide = false;
                
                  break;
                }
                case 4:
                  {
                    this.OfficeResulth = false;
                    this.processorh= true;
                    this.tabhide = false;
                    this.fieldtab = true;
                  
                    break;
                  }
          }
  
      }
    }
  }
  ngOnInit() {

    this.encuser = localStorage.getItem('UserName');
    // this.checkUserOn();
    this.GetWorkOrderUserDetails();
  }

  // checkUserOn() {
  //   this.xdatabase.database.ref('.info/connected').on(
  //     "value", function (snap) {
  //       if (snap.val()) {
  //         this.status = "online"
  //       } else {
  //         this.status = "offline"
  //       }

  //     }.bind(this)
  //   )
  // }

  ModelObj: any;
  headwodata: any;
  GetWorkOrderUserDetails() {
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
      .WorkorderViewClient(this.TaskBidMasterModelObj)
      .subscribe(response => {
        console.log("meta data", response);
        this.ModelObj = response[0][0];
        this.headwodata = response[0];
        this.SaveFireDataBaseChate();
      })

  }

  SaveFireDataBaseChate() {

    if (localStorage.getItem('UserName') != '') {
      var encuser = localStorage.getItem('UserName');
      console.log('encuserdecdd', encuser);
    }

    let userRef = this.xdatabase.database.ref("users");
    let IPLNO = this.ModelObj.IPLNO;
    userRef.child(encuser).update({
      id: encuser,
      avatar: `http://gravatar.com/avatar/${md5(encuser)}?d=identicon`
    });
    // this.addGroup(userRef,encuser, workorderId);
    this.addMembers(encuser,IPLNO);
    this.displayMessages(IPLNO);
    // let UserData = this.xdatabase.object('Users/' + encuser + '/' + this.ModelObj.CORNT_User_LoginName)
    // UserData.update({ user_name: this.ModelObj.CORNT_User_LoginName });
    // this.xdatabase.object('Users/' + this.ModelObj.CORNT_User_LoginName + '/' + encuser).update({
    //   user_name: encuser,
    //   status: this.status
    // })
    // this.GetMessageDetailsData();
  }

  setLastUpdate(){
    // let updates = {};
    // let group = {
    //   groupId: workorderId,
    // };
    // updates[encuser + "/groups/" + workorderId] = group;
    // userRef
    // .update(updates)
    // .then(() => console.log("User Added"))
    // .catch(err => console.error("Error while adding user: ", err));
    let userRef = this.xdatabase.database.ref("users");
    if (localStorage.getItem('UserName') != ''){
      let logged_in_user = localStorage.getItem('UserName');
      userRef.child(logged_in_user)
      .child("groups")
      .child( this.ModelObj.IPLNO )
      .set({
        lastUpdate: firebase.database.ServerValue.TIMESTAMP,
        IPLNO: this.ModelObj.IPLNO,
      }).then(() => console.log("Time added"))
      .catch(err => console.error("Error in adding time: ", err));
    }
    
  }

  addMembers(encuser, IPLNO){
    let updates = {};
    let member = {
      member: encuser,
    };
    updates[IPLNO + "/" + encuser + "/"] = member;

    this.xdatabase.database.ref("groups").update(updates)
    .then(() => console.log("member added"))
    .catch(err => console.error("Error while adding members: ", err));
  }

  messageList = [];
  displayMessages(IPLNO){
    let messageRef = this.xdatabase.database.ref("messages");
    messageRef.child(IPLNO)
    .on("child_added", value => {
      console.log("value", value.val())
      this.messageList.push(value.val());
      console.log("Checking array: ", this.messageList);
    })
  }

  messages: string;
  async SentMessage(item) {
    let name =   this.decuserr[0].User_FirstName + " " + this.decuserr[0].User_LastName;
    
    this.messages = item.Message_Text;
    let messageRef = this.xdatabase.database.ref("messages");
    if(this.messages.length > 0){
      if (localStorage.getItem('UserName') != ''){
      let logged_in_user = localStorage.getItem('UserName');
      let messageId = (await messageRef.push()).key;
      let message = {
        message: this.messages,
        from: logged_in_user,
        time: firebase.database.ServerValue.TIMESTAMP,
        avatar: `http://gravatar.com/avatar/${md5(logged_in_user)}?d=identicon`,
         name : name
      }

      messageRef
      .child(this.ModelObj.IPLNO)
      .child(messageId)
      .update(message)
      .then(() => console.log("Message added"))
      .catch((err) => console.error("Error while entering text: ", err));

      this.setLastUpdate();
    }
   
  }

    //this.jstoday = formatDate(this.today, 'dd/MM/yyyy hh:mm a', 'en-US', '+0530');

    // if (item != '') {
    //   if (localStorage.getItem('UserName') != '') {
    //     var encuser = localStorage.getItem('UserName');
    //   }
    //   let Message_Id = this.xdatabase.database.ref('messages').child(encuser)
    //     .child(this.ModelObj.CORNT_User_LoginName).push().key;
    //   let updates = {};
    //   let Message = {
    //     message: item.Message_Text,
    //     time: firebase.database.ServerValue.TIMESTAMP,//this.jstoday,
    //     from: encuser
    //   }
    //   updates[
    //     "messages/" +
    //     encuser +
    //     "/" +
    //     this.ModelObj.CORNT_User_LoginName +
    //     "/" +
    //     Message_Id
    //   ] = Message;

    //   updates[
    //     "messages/" +
    //     this.ModelObj.CORNT_User_LoginName +
    //     "/" +
    //     encuser +
    //     "/" +
    //     Message_Id
    //   ] = Message;

    //   this.xdatabase.database.ref().update(updates);

    //   this.MessageModelDataObj.Message_Text = '';
    //   this.checkStatus();
    //   this.storemsgrecordsql();
    // }
    this.MessageModelDataObj.Message_Text ='';

  }
  decuser:any;
storemsgrecordsql(){
  debugger
  if (localStorage.getItem('usertemp_') != null || localStorage.getItem('usertemp_') != "") {
    var encryptuser = JSON.parse(localStorage.getItem('usertemp_'));
    var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encryptuser));
    this.decuser  =JSON.parse(decval) ;
  }
 
this.MessageModelDataObj.Msg_Wo_Id =  this.ModelObj.workOrder_ID;
this.MessageModelDataObj.Msg_Message_text =  this.messages;
this.MessageModelDataObj.Msg_From_UserId =  this.decuser[0].User_pkeyID;
this.MessageModelDataObj.Msg_To_UserId = 0;
this.MessageModelDataObj.Msg_To_UserId_A = 0;
this.MessageModelDataObj.Msg_To_UserId_B = 0;
this.MessageModelDataObj.Msg_Time =  '';
this.MessageModelDataObj.Msg_Status =  1;
this.MessageModelDataObj.Msg_Message_Id =  '';
this.xMessagingDetailsService
.WorkorderMessagePost(this.MessageModelDataObj)
.subscribe(response =>{
console.log("message send :",response)
})
  }

  checkStatus = async () => {
    let status = '';
    this.xdatabase.database.ref(
      "Users/" + this.encuser + "/" + this.ModelObj.CORNT_User_LoginName
    ).on(
      "value", (snapshot) => {
        console.log("User data: ", snapshot.val().status);
        status = snapshot.val().status;
        if (status !== "online") {
         // this.sentNotification();
        }
      }
    )
  }

  convertTime = (time) => {
    var months_arr = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var date = new Date(time);
    //Year
    var year = date.getFullYear();
    //Month
    var month = months_arr[date.getMonth()];
    //Day
    var day = date.getDate();
    //Hours
    var hours = date.getHours();
    //Minutes
    var minutes = "0" + date.getMinutes();

    var convDataTime = month + "-" + day + "-" + year + " " + hours + ":" + minutes.substr(-2);
    return convDataTime;

  }

  // sentNotification = async () => {
  //   this.xdatabase.database.ref(
  //     "Token/" + this.ModelObj.CORNT_User_LoginName
  //   ).on("value", (snapshot) => {
  //     console.log("Token: ", snapshot.val().token)
  //     this.xMessagingDetailsService
  //       .Sendnotification(this.messages, this.ModelObj.CORNT_User_LoginName, snapshot.val().token)
  //       .subscribe(response => {
  //         console.log("notify response: ", response)
  //       })
  //   })
  // }

  // messageList = [];
  GetMessageDetailsData() {
    this.xdatabase.database.ref('messages').child(this.encuser)
      .child(this.ModelObj.CORNT_User_LoginName).on('child_added', (value) => {
        console.log('value', value.val());

        this.messageList.push(value.val())
        

        console.log("Checking array", this.messageList);
      });
  }

  public trackItem(i, msg) {
    return msg.time;
  }



  ngOnDestroy() {
    this.xdatabase.object('Users/' + this.ModelObj.CORNT_User_LoginName + '/' + this.encuser).set({
      user_name: this.encuser,
      status: 'away'
    })
  }







}