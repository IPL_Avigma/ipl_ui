
import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {TaskBidMasterModel} from './ClientResultModel';
import {BasetUrl} from '../../Utility/DomainUrl';
import {ClientResultPhotoModel} from '../ClientResultPhoto/ClientResultPhotoModel';
import{CopyWorkOderModel} from '../ClientResults/ClientResultModel';
import { HomepageServices } from '../../Home/HomeServices';




@Injectable({
  providedIn: "root"
})
export class ClientResultServices {
  public Errorcall;
  private token: any; 
  
  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get user data
  private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetWorkOrderDataClientResult";


  public WorkorderViewClient(Modelobj:TaskBidMasterModel) {

    debugger;
    var ANYDTO: any = {};

        ANYDTO.workOrder_ID  = Modelobj.workOrder_ID;
        ANYDTO.Type = 1;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////////////////////////////
  private apiUrpost1 = BasetUrl.Domain + "api/WOCTASK/PostTaskBidData";


  public ClientResultTaskBidPost(Modelobj:TaskBidMasterModel) {

    debugger;
    var ANYDTO: any = {};

        ANYDTO.TaskBidMasterDTO  = Modelobj.ClientResultBidTaskArray;
        ANYDTO.Task_Bid_WO_ID = Modelobj.workOrder_ID;
        ANYDTO.UserID = Modelobj.UserID;
        ANYDTO.Type =  Modelobj.Type;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrpost1, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

   ////////////////////////////////////////////////////////////////////////
   private apiUrpost11 = BasetUrl.Domain + "api/WOCTASK/ClientResultTaskBidGetWorkOrderGet";


   public ClientResultTaskBidGetWorkOrderId(Modelobj:TaskBidMasterModel) {

     //debugger;
     var ANYDTO: any = {};

         ANYDTO.Task_Bid_WO_ID = Modelobj.Task_Bid_WO_ID;
         ANYDTO.Type =  Modelobj.Type;



     let headers = new HttpHeaders({ "Content-Type": "application/json" });
     headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
     return this._http
       .post<any>(this.apiUrpost11, ANYDTO, { headers: headers })
       .pipe(
         tap(data => {
           //console.log(data);
           return data;
         }),
         catchError(this.xHomepageServices.CommonhandleError)
         //catchError( this.Errorcall.handleError)
       );
   }


   ////////////////////////////////////////////////////////////////////////
  private apiUrpost1x = BasetUrl.Domain + "api/WOCTASK/ClientResultTaskInvoicePost";


  public ClientResultTaskInvoicePost(Modelobj:TaskBidMasterModel) {

    //debugger;
    var ANYDTO: any = {};

        ANYDTO.Task_Invoice_MasterDTO  = Modelobj.ClientResultCreateCompletionArray;
        ANYDTO.Task_Inv_WO_ID = Modelobj.workOrder_ID;
        ANYDTO.UserID = Modelobj.UserID;
        ANYDTO.Type =  Modelobj.Type;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrpost1x, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////////////////////////////
  private apiUrpost1xd = BasetUrl.Domain + "api/WOCTASK/ClientResultTaskDamagePost";


  public ClientResultTaskDamagePost(Modelobj:TaskBidMasterModel) {

   debugger;
    var ANYDTO: any = {};

        ANYDTO.TaskDamageMaster  = Modelobj.ClientResultDamageArray;
      
        ANYDTO.Task_Inv_WO_ID = Modelobj.workOrder_ID;
        ANYDTO.UserID = Modelobj.UserID;
        ANYDTO.Type =  Modelobj.Type;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrpost1xd, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////////////////////////////
  private apiUrpost1xx = BasetUrl.Domain + "Home/DownloadZipFile";

  public imagesDownLoadZip() {
    //debugger;
    var ANYDTO: any = {};

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    //headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrpost1xx, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );



  }

  ////////////////////////////////////////////////////////////////////////

    // get Single image data
    private apiUrlimageGet = BasetUrl.Domain + "api/MultiPhoto/GetSingleImagedata";



    public SingleImageData(Modelobj:ClientResultPhotoModel) {

      debugger;
      var ANYDTO: any = {};

          ANYDTO.Client_Result_Photo_StatusType  = 99;
          ANYDTO.Client_Result_Photo_Wo_ID  = Modelobj.Client_Result_Photo_Wo_ID;




      let headers = new HttpHeaders({ "Content-Type": "application/json" });
     // headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlimageGet, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            //console.log(data);
            return data;
          }),
          catchError(this.xHomepageServices.CommonhandleError)
          //catchError( this.Errorcall.handleError)
        );
    }

    ///////////////////////////////// Copy Work Order Api
    private apiUrpostcopyxd = BasetUrl.Domain + "api/WorkOrderImport/PostCopyWorkOrderData";


    public CopyWorkOrderDetailsPost(Modelobj:CopyWorkOderModel) {
  
     debugger;
      var ANYDTO: any = {};
         ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
         ANYDTO.WorkOderInfo = Modelobj.WorkOderInfo;
          ANYDTO.UserID = Modelobj.UserID;
          ANYDTO.Type =  Modelobj.Type;
  
  
  
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrpostcopyxd, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            //console.log(data);
            return data;
          }),
          catchError(this.xHomepageServices.CommonhandleError)
          //catchError( this.Errorcall.handleError)
        );
    }

////////////////// workOrder Hiistory

private apiUrposthistory = BasetUrl.Domain + "api/RESTIPL/GetWorkOrderHistory";


public WorkOrderHistoryPost(Modelobj:TaskBidMasterModel) {

 debugger;
  var ANYDTO: any = {};
     ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
      ANYDTO.Type =  Modelobj.Type;



  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http
    .post<any>(this.apiUrposthistory, ANYDTO, { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
      //catchError( this.Errorcall.handleError)
    );
}

/////////past wo history
private apiUrpast = BasetUrl.Domain + "api/RESTIPL/GetPastWorkOrderHistory";


public pastHistoryPost(Modelobj:TaskBidMasterModel) {

 debugger;
  var ANYDTO: any = {};
     ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
      ANYDTO.Type =  Modelobj.Type;



  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http
    .post<any>(this.apiUrpast, ANYDTO, { headers: headers })
    .pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
      //catchError( this.Errorcall.handleError)
    );
}



///////////////////////////

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
    alert("Invalid Request...");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}


