import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ClientResultComponent } from "./ClientResultComponent";
import { CommonDirectiveModule } from "../../AppDirectives/DirectiveModule";
import { ClientResultServices } from "./ClientResultServices";


import { ClientResultFieldComponent } from "../ClientResultField/ClientResultFieldComponent";

//common pcr 
import {ClientResultPCRComponent} from '../ClientResultPCR/ClientResultPCRComponent';
import {ClientResultPCRServices} from '../ClientResultPCR/ClientResultPCRServices';
import {ClientResultPcrJsonComponent} from '../ClientResultPcrJson/ClientResultPcrJsoncomponent';


// common module
import { CommonClientResultModule } from "../CommonClientResult/CommonClientResultModule";
import {CommonClientHeaderModule} from '../CommonClientHeader/CommonClientHeaderModule';
import {CommonPhotosMetaModule} from '../CommonPhotosMeta/CommonPhotosMetaModule';


// client photos
import { UploadModule } from '@progress/kendo-angular-upload';
import { PanelBarModule } from '@progress/kendo-angular-layout';
import { GridModule,ExcelModule } from '@progress/kendo-angular-grid';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ClientResultPhotoComponent } from "../ClientResultPhoto/ClientResultPhotoComponent";
import { ClientResultPhotoServices } from "../ClientResultPhoto/ClientResultPhotoServices";
import { ClientResultOldPhotoServices } from '../ClientResultPhoto/ClientResultphotosOldServices';


// instuction
import { ClientResultInstructionComponent } from "../ClientResultInstruction/ClientResultInstructionComponent";
import { ClientResultInstructionServices } from "../ClientResultInstruction/ClientResultInstructionServices";
import { EditorModule } from '@progress/kendo-angular-editor';
import {CommonClientResultComponent} from "../CommonClientResult/CommonClientResultComponent"

// invoice
import { ClientResultsInvoiceComponent } from '../ClientResultsInvoice/ClientResultsInvoiceComponent';
import { BidInvoiceItemViewTaskServices } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskServices";

// message
import{MessageDetailsComponent} from '../MessagesDetails/MessageDetailsComponent'
 //import {MessageDetailsModule} from '../MessagesDetails/MessageDetailsModule'
import { from } from 'rxjs';
import { MessagingDetailsService } from '../MessagesDetails/MessageDetailsServices';


//new

import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../../environments/environment';
import { AsyncPipe } from '../../../../node_modules/@angular/common';





const ClientResultRouts = [
  { path: "clientresult/:workorder", component: ClientResultComponent },
  { path: "clientresultfield/:workorder", component: ClientResultFieldComponent },
  { path: "clientresultphoto/:workorder", component: ClientResultPhotoComponent },
  { path: "clientresultinstruction/:workorder", component: ClientResultInstructionComponent },
  { path: "clientresultinvoice/:workorder", component: ClientResultsInvoiceComponent },
  { path: "clientresultpcr/:workorder", component: ClientResultPCRComponent },
  { path: "clientresultpcrjson/:workorder", component: ClientResultPcrJsonComponent },
  { path: "messages/:workorder", component: MessageDetailsComponent },
];

@NgModule({
  declarations: [ClientResultComponent, ClientResultFieldComponent,ClientResultPhotoComponent,ClientResultInstructionComponent,
    ClientResultsInvoiceComponent,ClientResultPCRComponent, ClientResultPcrJsonComponent, MessageDetailsComponent],
  imports: [
    RouterModule.forChild(ClientResultRouts),
    CommonModule,
    NgbModule,
    FormsModule,
    CommonDirectiveModule,
    CommonClientResultModule,
    HttpClientModule,
    UploadModule,
    CommonClientHeaderModule,
    CommonPhotosMetaModule,
   // MessageDetailsModule,
    EditorModule,
    PanelBarModule,
    GridModule,
    ExcelModule,
    CommonModule,
    FormsModule, ReactiveFormsModule,
     NgbModule,
      RouterModule,
      // BrowserModule,
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      AngularFireMessagingModule,
      AngularFireModule.initializeApp(environment.firebase),

  ],

  providers: [
    ClientResultServices,
    ClientResultOldPhotoServices,
    ClientResultInstructionServices,
    BidInvoiceItemViewTaskServices,
    ClientResultPCRServices,
    MessagingDetailsService,
    AsyncPipe,
    
    {
    provide: HTTP_INTERCEPTORS,
    useClass: ClientResultPhotoServices,
    multi: true
  }],
  bootstrap: [ClientResultComponent,CommonClientResultComponent,MessageDetailsComponent]
})
export class ClientResultModule {
  constructor(){
    console.log('Client Result M Model Loaded');
  }
}
