import { Component, Injectable, OnInit,Output,EventEmitter } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MasterlayoutComponent } from "../../Home/MasterComponent";
import { ClientResultServices } from "./ClientResultServices";
import {
  TaskBidMasterModel,
  Task_Invoice_MasterModel,
  TaskDamageMasterModel,
  BindDataModel
} from "./ClientResultModel";
import { BidInvoiceItemViewTaskModel } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskModel";

import { AddDamageModel } from "../../Admin/Damage/AddDamage/AddDamageModel";
import { ClientResultPhotoModel } from "../ClientResultPhoto/ClientResultPhotoModel";

import { ClientResultOldPhotoServices } from "../ClientResultPhoto/ClientResultphotosOldServices";

import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";
import {CommonClientResultComponent} from "../CommonClientResult/CommonClientResultComponent";


@Component({
  selector: "app-Property-History",
  templateUrl: "./ClientResult.html"
})
export class ClientResultComponent implements OnInit {
 
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel(); // task
  Task_Invoice_MasterModelObj: Task_Invoice_MasterModel = new Task_Invoice_MasterModel();
  TaskDamageMasterModelObj: TaskDamageMasterModel = new TaskDamageMasterModel();

  BidInvoiceItemViewTaskModelObj: BidInvoiceItemViewTaskModel = new BidInvoiceItemViewTaskModel();

  AddDamageModelObj: AddDamageModel = new AddDamageModel();

  BindDataModelObj: BindDataModel = new BindDataModel();
  ClientResultPhotoModelObj: ClientResultPhotoModel = new ClientResultPhotoModel();

  @Output() stockValueChange: EventEmitter<object> = new EventEmitter<object>();

  ModelObj: any;
  BindData: any;
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi

  ClientResultBidArray = [];
  ClientResultCreateCompletionArray = [];
  ClientResultDamageArray = [];
  HeaderCoditional:boolean = false;
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;



  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xClientResultServices: ClientResultServices,
    private modalService: NgbModal,
    private xClientResultOldPhotoServices: ClientResultOldPhotoServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    //private notify: CommonClientResultComponent,
   

  ) {
    debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    console.log('url','/client/clientresultinstruction/'+ workorder1 )
    if(this.xRouter.url ==='/client/clientresultinstruction/'+ workorder1 || this.xRouter.url ==='/client/clientresultfield/'+ workorder1){
     this.HeaderCoditional = true;
   }
  
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      console.log('mohitdec',this.decuser);
      // if (this.decuser[0].GroupRoleId === 1) {
      //   this.hidetab = true;
        
      // }
      switch (this.decuser[0].GroupRoleId) {
        case 1:
          {
            this.OfficeResulth = false;
            this.processorh= false;
            this.tabhide = false;
         
            break;
          }
          case 2:
            {
              this.OfficeResulth = true;
              this.processorh= false;
              this.tabhide = true;
             
              break;
            }
            case 3:
              {
                this.OfficeResulth = false;
                 this.processorh= false;
                 this.tabhide = false;
              
                break;
              }
              case 4:
                {
                  this.OfficeResulth = false;
                  this.processorh= true;
                  this.tabhide = false;
                
                  break;
                }
        }


    }
  
  
  }
  ngOnInit() {


    this.getModelData();
  }
  ngAfterViewInit() {
    window.scrollTo(0, 0);
 }


  
changeStockValue(p) {


}

  getModelData() {
    debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      console.log("response clinet but work order se click", response);

      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
      if (this.ModelObj == undefined) {
        this.xRouter.navigate(["/workorder/view"]);
      } else {
        console.log("final bind Main Client M", this.ModelObj);
      }
    });
    this.GetPastHistory();
    //this.GetWorkOrderHistory();
  }
  taskbid: any;
  taskcompletion: any;
  taskDamage: any;
  taskAppliance:any;
  pasthistory: any;
  // GetWorkOrderHistory(){
  //   debugger
  //   const workorder1 = this.xRoute.snapshot.params['workorder'];
  //   let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
  //   console.log('workOrderID', workOrderID);
  //   const workorder = parseInt(workOrderID);
  //   this.TaskBidMasterModelObj.workOrder_ID = workorder;
  //   this.xClientResultServices
  //   .WorkOrderHistoryPost(this.TaskBidMasterModelObj)
  //   .subscribe(response => {
  //     debugger
  //     console.log("Workorder History", response)
      
  //     this.taskbid = response[0];
  //     this.taskcompletion = response[1];
  //     this.taskDamage = response[2];
  //     this.taskAppliance = response[3];
     
     

  // })

  // }
//past wohistory
GetPastHistory(){
  debugger
  const workorder1 = this.xRoute.snapshot.params['workorder'];
  let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
  console.log('workOrderID', workOrderID);
  const workorder = parseInt(workOrderID);
  this.TaskBidMasterModelObj.workOrder_ID = workorder;
  this.xClientResultServices
  .pastHistoryPost(this.TaskBidMasterModelObj)
  .subscribe(response => {
    debugger
    console.log("past History", response)
    this.pasthistory = response[0];

})

}


  filterbidarr: any;
  class2:string ='tab-pane';
  class1:string = "active";
  class3:string = "";
  class4:string = "";
  class5:string = "";
  FilterPastHistory(event,dataItem){
    debugger
  

    this.TaskBidMasterModelObj.workOrder_ID = dataItem.workOrder_ID;
    this.TaskBidMasterModelObj.Type = 1;
    this.xClientResultServices
    .WorkOrderHistoryPost(this.TaskBidMasterModelObj)
    .subscribe(response => {
      debugger
      console.log("Workorder History", response)
    
      
      this.taskbid = response[0];
      this.taskcompletion = response[1];
      this.taskDamage = response[2];
      this.taskAppliance = response[3];
      this.bidhistoryopen();
     

  })
 

   }

   bidhistoryopen(){
    this.class1 = "";
    this.class2 = "active";
    this.class3 = "";
    this.class4 = "";
    this.class5 = "";
   }
   pasthistoryopen(){
    this.class1 = "active";
    this.class2 = "";
    this.class3 = "";
    this.class4 = "";
    this.class5 = "";
   }
   invhistoryopen(){
    this.class1 = "";
    this.class2 = "";
    this.class3 = "active";
    this.class4 = "";
    this.class5 = "";
   }
   damagehistoryopen(){
    this.class1 = "";
    this.class2 = "";
    this.class3 = "";
    this.class4 = "active";
    this.class5 = "";
   }
   applhistoryopen(){
    this.class1 = "";
    this.class2 = "";
    this.class3 = "";
    this.class4 = "";
    this.class5 = "active";
   }

// checkbox selection
chkflag = false;
chkdata: any;
chkarr =[];
completion = [];
bidchkcheck(item,i){
 
  debugger;
  if(item.chkflag == true){
    Object.assign(item, {Task_Bid_pkeyID: "0"});
    Object.assign(item, {Task_Bid_WO_ID: "0"});
    this.chkarr.push(item);
  }
  else{
    this.chkarr.slice(i,1);
  }
}
compchkcheck(item,i){
 
  debugger;
  if(item.chkflag == true){
    Object.assign(item, {Task_Inv_pkeyID: "0"});
    Object.assign(item, {Task_Inv_WO_ID: "0"});
  
    this.completion.push(item);
    console.log('add', this.completion)
   // this.stockValueChange.emit(item);
  }
  else{
    this.chkarr.slice(i,1);
    console.log('remove', this.chkarr)
  }
}





  // Using the function above to do the conversion (an image to base 64 format from its URL);

  // for upload image on click
  // imgpath: string; //img sath
  // processImage(imageInput: any, content) {
  //   debugger;
  //   console.log("Image Upload" + imageInput);
  //   this.contentx = content;
  //   const getnamefile = imageInput.files[0].name;
  //   const extsn = getnamefile.split(".").pop();
  //   //alert(extsn);
  //   // here checking file extension
  //   if (extsn != "xlsx") {
  //     this.BindDataModelObj.documentx = imageInput.files[0];
  //     this.BindDataModelObj.Client_Result_Photo_StatusType = 99;
  //     this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;// work order id
  //     this.BindDataModelObj.Type = 1;// nely entry
  //     if(this.BindDataModelObj.Client_Result_Photo_FilePath != ""){

  //       this.BindDataModelObj.Type = 2;// for update

  //     }

  //     this.DocumentCall();
  //   }

  // }

  // // end code

  // // enteract between save btn and document upload

  // DocumentCall() {
  //   debugger;
  //   this.xClientResultOldPhotoServices
  //     .CommonPhotosUpdate(this.BindDataModelObj)
  //     .subscribe(response => {
  //       console.log("Image Upload", response);

  //       this.modalService.dismissAll();



  //       this.MessageFlag = "Image Saved...!";

  //       this.commonMessage(this.contentx);


  //       // get refresh img care fully here

  //       this.getModelData();

  //       //  this.TaskNameArray = response[0];
  //     });
  // }

  // // common message modal popup
  // commonMessage(content) {
  //   this.modalService
  //     .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
  //     .result.then(result => {}, reason => {});
  // }
  // /// end common model

  // // end code

  // ///
  // // check extension img or not
  // ImgFlag = false;
  // GetExtension(imgpathx: any) {
  //   var extsn = imgpathx.split(".").pop();
  //   if (
  //     extsn == "jpg" ||  extsn == "png" || extsn == "PNG" ||
  //     extsn == "JPG" || extsn == "JPEG" || extsn == "jpeg"
  //   ) {
  //     this.ImgFlag = true;
  //     // this is img
  //   } else if (extsn == "pdf" || extsn == "docx" || extsn == "doc") {
  //     this.ImgFlag = false;

  //     // this is other doc
  //   }
  // }
  // // end code
  // // for model popup open
  // OpenModel(OpenModelcontent) {
  //   this.contentx = OpenModelcontent;
  //   this.commonimage(this.contentx);
  // }

  // // image message modal popup
  // commonimage(OpenModelcontent) {
  //   this.modalService
  //     .open(OpenModelcontent, { windowClass: "smModal" })
  //     .result.then(result => {}, reason => {});
  // }

  pcrform(){
    debugger;
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',this.ModelObj.workOrder_ID);
    this.xRouter.navigate(["/client/clientresultpcr/" + btoa(encrypted)]);
  }
  pcrjson(){
    debugger;
    var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',this.ModelObj.workOrder_ID);
    this.xRouter.navigate(["/client/clientresultpcrjson/" + btoa(encrypted)]);
  }


}
