import { Component, Injectable, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FileInfo } from "@progress/kendo-angular-upload";
import { BindDataModel, TaskBidMasterModel } from "../ClientResults/ClientResultModel";
import {
  ClientResultInstructionModel,
  InstructionMasterDrDNameModel,
  InstructionMasterTaskTypeModel,
  InstructionMasterTaskModel,
  SigleEditBoxModel,
  InstructionAcessLogModel
} from "./ClientResultInstructionModel";
import { MasterlayoutComponent } from "../../Home/MasterComponent";
import { ClientResultInstructionServices } from "./ClientResultInstructionServices";
import * as $ from "jquery";
import { ClientResultServices } from '../ClientResults/ClientResultServices';
import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";
import { BasetUrl } from "../../Utility/DomainUrl";
import { ClientResultOldPhotoServices } from '../ClientResultPhoto/ClientResultphotosOldServices';
import { parseTimelineCommand } from '@angular/animations/browser/src/render/shared';


@Component({
  templateUrl: "./ClientResultInstruction.html"
})
export class ClientResultInstructionComponent implements OnInit {
  BindDataModelObj: BindDataModel = new BindDataModel();
  ClientResultInstructionModelObj: ClientResultInstructionModel = new ClientResultInstructionModel();
  InstructionMasterTaskTypeModelObj: InstructionMasterTaskTypeModel = new InstructionMasterTaskTypeModel();
  InstructionMasterDrDNameModelObj: InstructionMasterDrDNameModel = new InstructionMasterDrDNameModel();
  InstructionMasterTaskModelObj: InstructionMasterTaskModel = new InstructionMasterTaskModel();
  InstructionAcessLogModelObj: InstructionAcessLogModel = new InstructionAcessLogModel();
  InstructionDataArray = [];
  DetailsDataArray = [];
  InstDataArray = [];
  show = false;
  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  tempInstr_Task_Id = 0;




  SigleEditBoxModelObj: SigleEditBoxModel = new SigleEditBoxModel();

  private apiUrlGet =
  BasetUrl.Domain + "api/RESTIPLUPLOAD/PostUserDocumentUserImageBackground"; // get document 

  uploadSaveUrl = ""; // should represent an actual API endpoint
  uploadRemoveUrl = "removeUrl"; // should represent an actual API endpoint
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;

  
  constructor(
    private xRouter: Router,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xClientResultInstructionServices: ClientResultInstructionServices,
    private modalService: NgbModal,
    private xRoute: ActivatedRoute,
    private xClientResultServices: ClientResultServices,
    private EncrDecr: EncrDecrServiceService,
    private xClientResultOldPhotoServices:ClientResultOldPhotoServices

  ) {
    this.uploadSaveUrl = this.apiUrlGet;
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      console.log('mohitdec',this.decuser);
      // if (this.decuser[0].GroupRoleId === 1) {
      //   this.hidetab = true;
        
      // }
      switch (this.decuser[0].GroupRoleId) {
        case 1:
          {
            this.OfficeResulth = false;
            this.processorh= false;
            this.tabhide = false;
         
            break;
          }
          case 2:
            {
              this.OfficeResulth = true;
              this.processorh= false;
              this.tabhide = true;
             
              break;
            }
            case 3:
              {
                this.OfficeResulth = false;
                 this.processorh= false;
                 this.tabhide = false;
              
                break;
              }
              case 4:
                {
                  this.OfficeResulth = false;
                  this.processorh= true;
                  this.tabhide = true;
                
                  break;
                }
        }

    }
  }
  ngOnInit() {
    this.getModelData();
  }
//strip html
 removeTags(str) {
   debugger
  if ((str===null) || (str===''))
  return false;
  else
  str = str.toString();
  return str.replace( /(<([^>]+)>)/ig, '');
}






  countx = 0;
  onearray = [];
  twoarray = [];
  threearray = [];

  ClientResultInstruCSumbit(content) {
    this.countx = 0;

    debugger;
    for (let i = 0; i < this.InstructionDataArray.length; i++) {
      this.countx++;
      this.InstructionDataArray[i].Instr_Action = this.countx;
    }

    this.contentx = content;

    this.isLoading = true;
    this.button = "Processing";

    this.ClientResultInstructionModelObj.Instr_WO_Id = this.ModelObj.workOrder_ID;
   
    this.ClientResultInstructionModelObj.InstructionDataArray = this.InstructionDataArray.concat(
      this.DetailsDataArray,
      this.InstDataArray
    );

    this.SigleEditBoxModelObj.Inst_Ch_Wo_Id = this.ModelObj.workOrder_ID;
    this.ClientResultInstructionModelObj.SingleEditBox = this.SigleEditBoxModelObj;
  
  //  let str = this.SigleEditBoxModelObj.Inst_Ch_Text.replace(/(<\?[a-z]*(\s[^>]*)?\?(>|$)|<!\[[a-z]*\[|\]\]>|<!DOCTYPE[^>]*?(>|$)|<!--[\s\S]*?(-->|$)|<[a-z?!\/]([a-z0-9_:.])*(\s[^>]*)?(>|$))/gi, '');
  //  var test1 = str.replace(/&nbsp;/g, " ");
  //  this.SigleEditBoxModelObj.Instr_Comand_Mobile = test1;

    this.xClientResultInstructionServices
      .InstructionPost(this.ClientResultInstructionModelObj)
      .subscribe(response => {
        console.log("response post cha child client", response);

        if(response[0][0].Inst_Ch_pkeyId != 0){

          console.log('pkey id child client ',response[0][0].Inst_Ch_pkeyId);

          this.SigleEditBoxModelObj.Inst_Ch_pkeyId = response[0][0].Inst_Ch_pkeyId;

        }

        this.isLoading = false;
        this.button = "Save";

        this.MessageFlag = "Data Saved...!";

        this.commonMessage(this.contentx);

        this.GetInstuctionDataMain();
      });
  }

  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  BindData: any;
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel();

  getModelData() {
    //debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      console.log("response clinet but work order se click", response);

      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
      if (this.ModelObj == undefined) {
        this.xRouter.navigate(["/workorder/view"]);
      } else {
        //this.DivHIDEx = true;
        console.log("work order obj instraction", this.ModelObj);

      this.BindDataModelObj.workOrderNumber = this.ModelObj.workOrderNumber;
      this.BindDataModelObj.address1 = this.ModelObj.address1;
      this.BindDataModelObj.Cont_Name = this.ModelObj.Cont_Name;
      this.BindDataModelObj.Cordinator_Name = this.ModelObj.Cordinator_Name;
      this.BindDataModelObj.Lock_Code = this.ModelObj.Lock_Code;
      this.BindDataModelObj.startDate = this.ModelObj.startDate;
      this.BindDataModelObj.Work_Type_Name = this.ModelObj.Work_Type_Name;
      this.BindDataModelObj.Lock_Location = this.ModelObj.Lock_Location;
      this.BindDataModelObj.Cust_Num_Number = this.ModelObj.Cust_Num_Number;
      this.BindDataModelObj.Key_Code = this.ModelObj.Key_Code;
      this.BindDataModelObj.Client_Company_Name = this.ModelObj.Client_Company_Name;
      this.BindDataModelObj.Gate_Code = this.ModelObj.Gate_Code;
      this.BindDataModelObj.BATF = this.ModelObj.BATF;
      this.BindDataModelObj.Lotsize = this.ModelObj.Lotsize;
      this.BindDataModelObj.rus_Name = this.ModelObj.rus_Name;
      this.BindDataModelObj.ClientMetaData = this.ModelObj.ClientMetaData;
      this.BindDataModelObj.Loan_Info = this.ModelObj.Loan_Info;
      this.BindDataModelObj.Broker_Info = this.ModelObj.Broker_Info;
      this.BindDataModelObj.Received_Date = this.ModelObj.Received_Date;
      this.BindDataModelObj.clientDueDate = this.ModelObj.clientDueDate;
      this.BindDataModelObj.Complete_Date = this.ModelObj.Complete_Date;
      this.BindDataModelObj.Cancel_Date = this.ModelObj.Cancel_Date;
      this.BindDataModelObj.IPLNO = this.ModelObj.IPLNO;

        //this.ClientResultPhotoModelobj.Client_Result_Photo_Wo_ID = this.ModelObj.workOrder_ID;
        this.BindData = this.ModelObj;
        //this.GetClientImages();
        this.getdropdowntask();
      }
    });
  }

  // get dropdown for type
  TypeNameArray: any;
  getdropdowntask() {
    debugger;
    this.xClientResultInstructionServices
      .InstructionTypeName(this.InstructionMasterTaskTypeModelObj)
      .subscribe(response => {
        console.log("TypeName", response);
        this.TypeNameArray = response[0];

        this.getdropdownTaskTypeName();
      });
  }

  validate(evt) {
    debugger
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  //get dropdown for Type Name
  taskTypeNameArray: any;
  getdropdownTaskTypeName() {
    debugger;
    this.xClientResultInstructionServices
      .InstructionTaskTypeName(this.InstructionMasterTaskModelObj)
      .subscribe(response => {
        console.log("taskTypeName", response);
        this.taskTypeNameArray = response[0];
        this.CommonMethodCall_Array();
        this.getdropdownTaskName();

      });
  }

  //get dropdown for Task Name
  TaskNameArray: any;
  getdropdownTaskName() {
    debugger;
    this.xClientResultInstructionServices
      .InstructionTaskNamedata(this.InstructionMasterDrDNameModelObj)
      .subscribe(response => {
        console.log("TaskName with con cli price", response);
        this.TaskNameArray = response[0];

        this.GetInstuctionDataMain();
      });
  }

  taskTypeNameArrayNew = [];
  taskTypeNameArrayNewOne = [];
  taskTypeNameArrayNewTwo = [];

  Instr_ValType: Number = 0;
  AddMoreInstruction(arg) {
    debugger;

    this.Instr_ValType = arg;

    let data = {
      Instr_pkeyId: 0,
      Instr_Task_Id: 0,
      Instr_Task_pkeyId: 0,
      Instr_WO_Id: 0,
      Instr_Task_Name: 0,
      Instr_Qty: 1,
      Instr_Contractor_Price: 0,
      Instr_Client_Price: 0,
      Instr_Contractor_Total: 0,
      Instr_Client_Total: 0,
      Instr_Action: 0,
      Instr_IsActive: true,
      Instr_IsDelete: false,
      UserID: 0,
      Instr_Details_Data: "",
    
      Instr_ValType: this.Instr_ValType,
      Instr_Qty_Text:0,
      Instr_Price_Text:0,
      Instr_Total_Text:0,
      Instr_Ch_pkeyId:0
    };

    if (arg == 1) {

      this.InstructionDataArray.push(data);
    }
    if (arg == 2) {
      this.DetailsDataArray.push(data);
    }
    if (arg == 3) {
      this.InstDataArray.push(data);
    }
    //this.CommonMethodCall_Array();
  }

  CommonMethodCall_Array() {
    for (let i = 0; i < this.taskTypeNameArray.length; i++) {
      if (this.taskTypeNameArray[i].Inst_Task_Type_pkeyId == 1) {
        let taskTypedata = this.taskTypeNameArray[i];
        this.taskTypeNameArrayNew.push(taskTypedata);
      }
      if (this.taskTypeNameArray[i].Inst_Task_Type_pkeyId == 2) {
        let taskdetail = this.taskTypeNameArray[i];
        this.taskTypeNameArrayNewOne.push(taskdetail);
      }
      if (this.taskTypeNameArray[i].Inst_Task_Type_pkeyId == 3) {
        let taskinst = this.taskTypeNameArray[i];
        this.taskTypeNameArrayNewTwo.push(taskinst);
      }
    }
  }
taskdetails = [];

  InstructionRemove(item,index) {
    debugger;
    let promp = confirm("Are you sure..?");
    if (promp) {

      if(item.Instr_pkeyId!="")
      {
     
        this.ClientResultInstructionModelObj.Instr_pkeyId = item.Instr_pkeyId;
        this.ClientResultInstructionModelObj.Type = 4;
        this.xClientResultInstructionServices
          .DeleteInstructionPost(this.ClientResultInstructionModelObj)
          .subscribe(response => {
            console.log("delete Instruction", response);

           // this.ClientResultInstructionModelObj.Type = 1;
            this.GetInstuctionDataMain()

          });




      }
      else{
        this.InstructionDataArray.splice(index, 1);
      }
    
    }
    if (this.InstructionDataArray.length == 0) {
      // this.AddMoreInstruction();
    }
  }
  DetailsRemove(index) {
    let promp = confirm("Are you sure..?");

    if (promp) {
      this.DetailsDataArray.splice(index, 1);
    }
    if (this.DetailsDataArray.length == 0) {
      // this.AddMoreInstruction();
    }
  }
  InstRemove(item,index) {
    let promp = confirm("Are you sure..?");

    if (promp) {
      if(item.Instr_pkeyId!="")
      {
     
        this.ClientResultInstructionModelObj.Instr_pkeyId = item.Instr_pkeyId;
        this.ClientResultInstructionModelObj.Type = 4;
        this.xClientResultInstructionServices
          .DeleteInstructionPost(this.ClientResultInstructionModelObj)
          .subscribe(response => {
            console.log("delete Instruction child", response);
            this.GetInstuctionDataMain()

          });

      }
      else{
        this.InstDataArray.splice(index, 1);
      }
      
     
    }
    if (this.InstDataArray.length == 0) {
      // this.AddMoreInstruction();
    }
  }

  arry = [];

  //get meta data task cha
  TaskNameMetaData_Method(item) {
    debugger;
    this.InstructionDataArray;
    for (let i = 0; i < this.InstructionDataArray.length; i++) {
      if (
        this.InstructionDataArray[i].Instr_Task_Name == item.Instr_Task_Name
      ) {
        for (let j = 0; j < this.TaskNameArray.length; j++) {
          if (this.TaskNameArray[j].Task_pkeyID == item.Instr_Task_Name) {
            this.InstructionDataArray[
              i
            ].Instr_Contractor_Price = this.TaskNameArray[
              j
            ].Task_Contractor_UnitPrice;
            this.InstructionDataArray[
              i
            ].Instr_Client_Price = this.TaskNameArray[j].Task_Client_UnitPrice;
          }
        }
      }
    }

    this.ClinetResultQtyInstrucation_Method();
  }

  // for table row up & down
  upmethod() {
    $(document).ready(function() {
      $(".up,.down").click(function() {
        var row = $(this).parents("tr:first");
        if ($(this).is(".up")) {
          row.insertBefore(row.prev());
        } else {
          row.insertAfter(row.next());
        }
      });
    });
  }

  // calculation here

  ClinetResultQtyInstrucation_Method() {
    for (let i = 0; i < this.InstructionDataArray.length; i++) {
      if (this.InstructionDataArray[i].Instr_Qty != "") {
        this.InstructionDataArray[i].Instr_Contractor_Total =
          this.InstructionDataArray[i].Instr_Contractor_Price *
          this.InstructionDataArray[i].Instr_Qty;
        this.InstructionDataArray[i].Instr_Client_Total =
          this.InstructionDataArray[i].Instr_Client_Price *
          this.InstructionDataArray[i].Instr_Qty;
      } else {
        //alert('plz enter number only');

        // care fully
        this.InstructionDataArray[i].Instr_Qty = 1;
        this.InstructionDataArray[i].Instr_Contractor_Total = this.InstructionDataArray[i].Instr_Contractor_Price;
        this.InstructionDataArray[i].Instr_Client_Total = this.InstructionDataArray[i].Instr_Client_Price;
      }
    }
  }

  ClinetResultInstCont_Price_Method() {
    for (let i = 0; i < this.InstructionDataArray.length; i++) {
      if (this.InstructionDataArray[i].Instr_Contractor_Price != "") {
        this.InstructionDataArray[
          i
        ].Instr_Contractor_Total = this.InstructionDataArray[
          i
        ].Instr_Contractor_Price;

        if (this.InstructionDataArray[i].Instr_Qty != "") {
          this.InstructionDataArray[i].Instr_Contractor_Total =
            this.InstructionDataArray[i].Instr_Contractor_Price *
            this.InstructionDataArray[i].Instr_Qty;
        }
      } else {
        // error hadler
      }
    }
  }

  ClinetResultInstClient_Price_Method() {
    for (let i = 0; i < this.InstructionDataArray.length; i++) {
      if (this.InstructionDataArray[i].Instr_Client_Price != "") {
        this.InstructionDataArray[
          i
        ].Instr_Client_Total = this.InstructionDataArray[i].Instr_Client_Price;

        if (this.InstructionDataArray[i].Instr_Qty != "") {
          this.InstructionDataArray[i].Instr_Client_Total =
            this.InstructionDataArray[i].Instr_Client_Price *
            this.InstructionDataArray[i].Instr_Qty;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }

  // end calc


  // start calc New Details
  ClinetResultQtyInstrucationDetails_Method(){
    for (let i = 0; i < this.DetailsDataArray.length; i++) {
      if (this.DetailsDataArray[i].Instr_Qty_Text != "") {
        this.DetailsDataArray[i].Instr_Total_Text = this.DetailsDataArray[i].Instr_Price_Text * this.DetailsDataArray[i].Instr_Qty_Text;

      } else {
        //alert('plz enter number only');

        // care fully
        this.DetailsDataArray[i].Instr_Total_Text = this.DetailsDataArray[i].Instr_Price_Text;
        this.DetailsDataArray[i].Instr_Qty_Text = 1;

      }
    }
  }

  ClinetResultInstContDetails_Price_Method(){

    for (let i = 0; i < this.DetailsDataArray.length; i++) {
      if (this.DetailsDataArray[i].Instr_Price_Text != "") {
        this.DetailsDataArray[i].Instr_Total_Text = this.DetailsDataArray[i].Instr_Price_Text;

        if (this.DetailsDataArray[i].Instr_Qty_Text != "") {
          this.DetailsDataArray[i].Instr_Total_Text = this.DetailsDataArray[i].Instr_Price_Text *  this.DetailsDataArray[i].Instr_Qty_Text;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }
  // end calc New Details


  // START INSTU NEW
  ClinetResultQtyInstrucationISTRUNEW_Method(){

    for (let i = 0; i < this.InstDataArray.length; i++) {
      if (this.InstDataArray[i].Instr_Qty_Text != "") {
        this.InstDataArray[i].Instr_Total_Text = this.InstDataArray[i].Instr_Price_Text * this.InstDataArray[i].Instr_Qty_Text;

      } else {
        //alert('plz enter number only');

        // care fully
        this.InstDataArray[i].Instr_Total_Text = this.InstDataArray[i].Instr_Price_Text;
        this.InstDataArray[i].Instr_Qty_Text = 1;

      }
    }
  }

  ClinetResultPriceInstrucationISTRUNEW_Method(){

    for (let i = 0; i < this.InstDataArray.length; i++) {
      if (this.InstDataArray[i].Instr_Price_Text != "") {
        this.InstDataArray[i].Instr_Total_Text = this.InstDataArray[i].Instr_Price_Text;

        if (this.InstDataArray[i].Instr_Qty_Text != "") {
          this.InstDataArray[i].Instr_Total_Text = this.InstDataArray[i].Instr_Price_Text *  this.InstDataArray[i].Instr_Qty_Text;
        }
      } else {
        //alert('plz enter number only');
      }
    }
  }



  // get main Instruction data
  documentdetailslst: any;
  GetInstuctionDataMain() {
    this.ClientResultInstructionModelObj.Instr_WO_Id = this.ModelObj.workOrder_ID;

    this.xClientResultInstructionServices
      .InstructionGetMain(this.ClientResultInstructionModelObj)
      .subscribe(response => {
        console.log("get inst Main", response);
        debugger;
        if (response[0].length != 0) {

          this.documentdetailslst = response[2];

          this.InstructionDataArray =[];
          this.DetailsDataArray = [];
          this.InstDataArray =  [];

          for (let i = 0; i < response[0].length; i++) {

            if(response[0][i].Instr_ValType == 1){

              const MainInst = response[0][i];
              this.InstructionDataArray.push(MainInst);
            }
            if(response[0][i].Instr_ValType == 2){

              const DetailsArr = response[0][i];
              this.DetailsDataArray.push(DetailsArr);
            }
            if(response[0][i].Instr_ValType == 3){

              const InstArr = response[0][i];
              this.InstDataArray.push(InstArr);
            }

          }
          if(response[1][0].length != 0){

            this.SigleEditBoxModelObj.Inst_Ch_pkeyId  = response[1][0].Inst_Ch_pkeyId;
            this.SigleEditBoxModelObj.Inst_Ch_Wo_Id  = response[1][0].Inst_Ch_Wo_Id;
            this.SigleEditBoxModelObj.Inst_Ch_Text  =  response[1][0].Inst_Ch_Text;
            this.SigleEditBoxModelObj.Inst_Ch_IsActive  = response[1][0].Inst_Ch_IsActive;
            this.SigleEditBoxModelObj.Inst_Ch_Delete  = response[1][0].Inst_Ch_Delete;
            this.SigleEditBoxModelObj.UserID  = response[1][0].UserID;

          }
        }
      });
      this.GetInstructionAcessdata();
  }

  // for access log 
  AccessLogData: any;

  GetInstructionAcessdata() {
    // debugger;
    this.xClientResultInstructionServices.InsructionAcessLogData(this.InstructionAcessLogModelObj)
    .subscribe( response => {
      console.log('Acces Log Data', response);

       this.AccessLogData = response[0];
    });
  }



//document upload code 
OpenDocumentUpload(contentpop) {
  this.commonPOPUPDocument(contentpop);
}
commonPOPUPDocument(content) {
  this.modalService
    .open(content, { size: "lg", ariaLabelledBy: "modal-basic-title" })
    .result.then(
      result => {
        this.GetInstuctionDataMain();
      },
      reason => {
        this.GetInstuctionDataMain();
      }
    );
}
public displayErrorDocument(e: ErrorEvent) {
  console.log(e);
  console.log("An error occurred");
}
public displaySuccessDocument(e) {
  console.log("success");
  console.log("success", e);
  console.log("success File", e.files[0].rawFile);

  if (e.operation == "upload") {
    this.processDocument(e.files[0].rawFile);
  } else {
    alert("remove img called");
  }
}


//document upload 

processDocument(documentInput) {
  debugger;
  if (true) {
    this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;
    this.BindDataModelObj.Client_Result_Photo_Ch_ID = this.SigleEditBoxModelObj.Inst_Ch_pkeyId;
    this.BindDataModelObj.Client_Result_Photo_ID = this.ModelObj.Inst_Doc_PkeyID;
    this.BindDataModelObj.Client_PageCalled = 1;
    this.BindDataModelObj.documentx = documentInput;
    this.BindDataModelObj.Client_Result_Photo_FileName = documentInput.name;
    this.BindDataModelObj.Type = 1;
    this.xClientResultOldPhotoServices
      .CommonDocumentsUpdate(this.BindDataModelObj)
      .then((res) => {
        res.subscribe(() => {});
        this.GetInstuctionDataMain();
      });
  }
}






}
