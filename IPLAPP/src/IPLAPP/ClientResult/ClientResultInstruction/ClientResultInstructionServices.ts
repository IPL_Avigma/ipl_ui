import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {ClientResultInstructionModel,InstructionMasterDrDNameModel,
  InstructionMasterTaskTypeModel,InstructionAcessLogModel,
    InstructionMasterTaskModel} from './ClientResultInstructionModel';
import {BasetUrl} from '../../Utility/DomainUrl';
import { HomepageServices } from '../../Home/HomeServices';




@Injectable({
  providedIn: "root"
})
export class ClientResultInstructionServices {
  public Errorcall;
  public token: any;
  
  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }
  // get Dropdown for Type
  private apiUrlGet = BasetUrl.Domain + "api/WOCTASK/GetInstructionType";


  public InstructionTypeName(Modelobj:InstructionMasterTaskTypeModel) {

    //debugger;
    let ANYDTO: any = {};

        ANYDTO.Ins_Type_pkeyId  = Modelobj.Ins_Type_pkeyId;
        ANYDTO.Type = Modelobj.Type;



    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  /////////////////////////////////////////////////////////////
   // get Dropdown for Type Name
   private apiUrltaskGet = BasetUrl.Domain + "api/WOCTASK/GetInstructionTask";


   public InstructionTaskTypeName(Modelobj:InstructionMasterTaskModel) {

     debugger;
     let ANYDTO: any = {};

         ANYDTO.Task_pkeyID  = Modelobj.Task_pkeyID;
         ANYDTO.Inst_Task_pkeyId  = Modelobj.Inst_Task_pkeyId;
         ANYDTO.Type = Modelobj.Type;



     let headers = new HttpHeaders({ "Content-Type": "application/json" });
     headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
     return this._http
       .post<any>(this.apiUrltaskGet, ANYDTO, { headers: headers })
       .pipe(
         tap(data => {
           //console.log(data);
           return data;
         }),
         catchError(this.xHomepageServices.CommonhandleError)
         //catchError( this.Errorcall.handleError)
       );
   }

   /////////////////////////////////////////////////////////////
   // get Dropdown for Task Name
   private apiUrltaskNameGet = BasetUrl.Domain + "api/WOCTASK/GetInstructionTaskName";


   public InstructionTaskNamedata(Modelobj:InstructionMasterDrDNameModel) {

     //debugger;
     let ANYDTO: any = {};

         ANYDTO.Inst_Task_pkeyId  = 2;
         ANYDTO.Type  = Modelobj.Type;



     let headers = new HttpHeaders({ "Content-Type": "application/json" });
     headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
     return this._http
       .post<any>(this.apiUrltaskNameGet, ANYDTO, { headers: headers })
       .pipe(
         tap(data => {
           //console.log(data);
           return data;
         }),
         catchError(this.xHomepageServices.CommonhandleError)
         //catchError( this.Errorcall.handleError)
       );
   }

   /////////////////////////////////////////////////////////////
 // get Dropdown for Task Name
 private PostInstrdct = BasetUrl.Domain + "api/WOCTASK/PostDeleteInstruction";


 public DeleteInstructionPost(Modelobj:ClientResultInstructionModel) {
  
   debugger;
   let ANYDTO: any = {};

       ANYDTO.Instr_pkeyId  = Modelobj.Instr_pkeyId;
       ANYDTO.Type  = Modelobj.Type;



   let headers = new HttpHeaders({ "Content-Type": "application/json" });
   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   return this._http
     .post<any>(this.PostInstrdct, ANYDTO, { headers: headers })
     .pipe(
       tap(data => {
         //console.log(data);
         return data;
       }),
       catchError(this.xHomepageServices.CommonhandleError)
       //catchError( this.Errorcall.handleError)
     );
 }
///////////////////////// delete intruction api

private PostInstruct = BasetUrl.Domain + "api/WOCTASK/PostInstruction";


public InstructionPost(Modelobj:ClientResultInstructionModel) {

  debugger;
  let ANYDTO: any = {};

      ANYDTO.InstctionMasterDTO  = Modelobj.InstructionDataArray;
   
      ANYDTO.Instruction_Master_ChildDTO  = Modelobj.SingleEditBox;
     // ANYDTO.Instruction_Master_ChildDTO  = Modelobj.Instr_Comand_Mobile;


      ANYDTO.Instr_WO_Id  = Modelobj.Instr_WO_Id;
      ANYDTO.Type  = Modelobj.Type;



   let headers = new HttpHeaders({ "Content-Type": "application/json" });
   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   return this._http
     .post<any>(this.PostInstruct, ANYDTO, { headers: headers })
     .pipe(
       tap(data => {
         //console.log(data);
         return data;
       }),
       catchError(this.xHomepageServices.CommonhandleError)
       //catchError( this.Errorcall.handleError)
     );
 }


  /////////////////////////////////////////////////////////////
 // get Dropdown for Task Name
 private GetInstructMain = BasetUrl.Domain + "api/WOCTASK/GetInstruction";


 public InstructionGetMain(Modelobj:ClientResultInstructionModel) {

   debugger;
   let ANYDTO: any = {};
       ANYDTO.Instr_pkeyId = Modelobj.Instr_pkeyId;
       ANYDTO.Instr_WO_Id  = Modelobj.Instr_WO_Id;
       ANYDTO.Type  = 2;



   let headers = new HttpHeaders({ "Content-Type": "application/json" });
   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
   return this._http
     .post<any>(this.GetInstructMain, ANYDTO, { headers: headers })
     .pipe(
       tap(data => {
         //console.log(data);
         return data;
       }),
       catchError(this.xHomepageServices.CommonhandleError)
       //catchError( this.Errorcall.handleError)
     );
 }
 ///////////////////////////////////////////////////
//  Get Acess log Data

private GetAccessLog = BasetUrl.Domain + "api/Access/GetAccessLogData";

public InsructionAcessLogData(Modelobj: InstructionAcessLogModel) {
  debugger;

  let ANYDTO: any = {};
      ANYDTO.Alm_Pkey = 0;
      ANYDTO.Type = 1;
      
  let  headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http
    .post<any>(this.GetAccessLog, ANYDTO, { headers: headers })
    .pipe(
      tap(data => {
        // console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
    );
}
 


  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    } else {
      alert('Invalid Request...');
    }
    alert("Something bad happened, please try again later...😌")
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}



