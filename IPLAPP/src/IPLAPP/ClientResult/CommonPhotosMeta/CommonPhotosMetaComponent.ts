import { Component, OnInit, Output,EventEmitter } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { MasterlayoutComponent } from "../../Home/MasterComponent";

import {BindDataModel,TaskBidMasterModel,
  Task_Invoice_MasterModel,
  TaskDamageMasterModel,} from '../ClientResults/ClientResultModel';
  import { ClientResultPhotoModel } from "../ClientResultPhoto/ClientResultPhotoModel";

  import { ClientResultServices } from "../ClientResults/ClientResultServices";

  import { ClientResultOldPhotoServices } from "../ClientResultPhoto/ClientResultphotosOldServices";

import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";



import {BasetUrl} from '../../Utility/DomainUrl';
import { UpdateStausDataModel } from 'src/IPLAPP/WorkOrder/NewWorkOder/NewWorkOrderModel';
import { SaveWorkOrderServices } from 'src/IPLAPP/WorkOrder/NewWorkOder/NewWorkOrderServices';
import{CommonStatusDTO} from '../CommonClientHeader/CommonStatusModel'


@Component({
  selector: "app-photosMeta-client-result",
  templateUrl: "./CommonPhotosMeta.html"
})
export class CommonPhotosMetaComponent implements OnInit {
  CommonStatusDTOObj: CommonStatusDTO =new CommonStatusDTO();
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel(); // task
  UpdateStausDataModelObj: UpdateStausDataModel = new  UpdateStausDataModel();

  HeaderCoditional:boolean = false; // instruction sathi


  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;

  BindDataModelObj: BindDataModel = new BindDataModel();
  ClientResultPhotoModelObj: ClientResultPhotoModel = new ClientResultPhotoModel();
  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private modalService: NgbModal,
    private xClientResultServices: ClientResultServices,
    private xClientResultOldPhotoServices: ClientResultOldPhotoServices,
    private xRoute: ActivatedRoute,
    private EncrDecr: EncrDecrServiceService,
    private xSaveWorkOrderServices: SaveWorkOrderServices
  ) {
    //alert("hi");
    this.getModelDatax();

    // alert(this.xRouter.url);
     debugger;
     const workorder1 = this.xRoute.snapshot.params['workorder'];
     console.log('url','/client/clientresultinstruction/'+ workorder1 )
     if(this.xRouter.url ==='/client/clientresultinstruction/'+ workorder1 || this.xRouter.url ==='/client/clientresultfield/'+ workorder1){
      this.HeaderCoditional = true;
    }
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      console.log('mohitdec',this.decuser);
      // if (this.decuser[0].GroupRoleId === 1) {
      //   this.hidetab = true;
        
      // }
      switch (this.decuser[0].GroupRoleId) {
        case 1:
          {
            this.OfficeResulth = false;
            this.processorh= false;
            this.tabhide = false;
         
            break;
          }
          case 2:
            {
              this.OfficeResulth = true;
              this.processorh= false;
              this.tabhide = true;
             
              break;
            }
            case 3:
              {
                this.OfficeResulth = false;
                 this.processorh= false;
                 this.tabhide = false;
              
                break;
              }
              case 4:
                {
                  this.OfficeResulth = false;
                  this.processorh= true;
                  this.tabhide = false;
                
                  break;
                }
        }

    }
   
  }

  ngOnInit() {


  }


  ModelObj: any;
  BindData: any;
  copydata: any;
  statusdetails: any;
 
  getModelDatax() {
    debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      debugger;
      console.log("response clinet but work order se click", response);

      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();

      
      
      if (this.ModelObj == undefined) {
        this.xRouter.navigate(["/workorder/view"]);
      } else {
        console.log("final bind Main Client CHA Comoon Photos", this.ModelObj);
       
        this.TaskBidMasterModelObj.workOrder_ID = this.ModelObj.workOrder_ID;
        this.BindDataModelObj.workOrderNumber =  this.ModelObj.workOrderNumber;
            this.BindDataModelObj.address1 = this.ModelObj.address1;
            this.BindDataModelObj.Cont_Name = this.ModelObj.Cont_Name;
            this.BindDataModelObj.Cordinator_Name = this.ModelObj.Cordinator_Name;
            this.BindDataModelObj.Lock_Code = this.ModelObj.Lock_Code;
            this.BindDataModelObj.startDate = this.ModelObj.startDate;
            this.BindDataModelObj.Work_Type_Name = this.ModelObj.Work_Type_Name;
            this.BindDataModelObj.Lock_Location = this.ModelObj.Lock_Location;
            this.BindDataModelObj.Cust_Num_Number =  this.ModelObj.Cust_Num_Number;
            this.BindDataModelObj.Key_Code = this.ModelObj.Key_Code;
            this.BindDataModelObj.Client_Company_Name =
            this.ModelObj.Client_Company_Name;
            this.BindDataModelObj.Gate_Code = this.ModelObj.Gate_Code;
            this.BindDataModelObj.BATF = this.ModelObj.BATF;
            this.BindDataModelObj.Lotsize = this.ModelObj.Lotsize;
            this.BindDataModelObj.rus_Name = this.ModelObj.rus_Name;
            this.BindDataModelObj.ClientMetaData = this.ModelObj.ClientMetaData;
            this.BindDataModelObj.Loan_Info = this.ModelObj.Loan_Info;
            this.BindDataModelObj.Broker_Info = this.ModelObj.Broker_Info;
            this.BindDataModelObj.Received_Date = this.ModelObj.Received_Date;
            this.BindDataModelObj.clientDueDate = this.ModelObj.clientDueDate;
            this.BindDataModelObj.Complete_Date = this.ModelObj.Complete_Date;
            this.BindDataModelObj.Cancel_Date = this.ModelObj.Cancel_Date;
            this.BindDataModelObj.IPLNO = this.ModelObj.IPLNO;
            this.BindDataModelObj.dueDate = this.ModelObj.dueDate;
  
            this.BindDataModelObj.WT_WorkType = this.ModelObj.WT_WorkType;
  
            this.BindDataModelObj.Client_Result_Photo_FileName = this.ModelObj.Client_Result_Photo_FileName;
            if(this.ModelObj.Client_Result_Photo_FilePath != null){
              this.BindDataModelObj.Client_Result_Photo_FilePath =  this.ModelObj.Client_Result_Photo_FilePath;
            }
  
  
            // asyc photos // get single photos
            this.ClientResultPhotoModelObj.IPLNO = this.ModelObj.IPLNO;
            this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID = this.ModelObj.workOrder_ID;
            this.ClientResultPhotoModelObj.Type = 1;
            this.ClientResultPhotoModelObj.Client_Result_Photo_ID = 0;
            this.BindDataModelObj.fulladdress = this.ModelObj.fulladdress;
            this.BindDataModelObj.Processor_Name = this.ModelObj.Processor_Name;
            this.BindDataModelObj.SentToClient_date = this.ModelObj.SentToClient_date;
            this.BindDataModelObj.OfficeApproved_date = this.ModelObj.OfficeApproved_date;
            this.BindDataModelObj.Field_complete_date = this.ModelObj.Field_complete_date;
            this.statusdetails = this.xMasterlayoutComponent.masterFunctionGetdata();
            console.log("mohitsat", this.statusdetails)
            if (this.statusdetails != undefined) {
              this.BindDataModelObj.Status_Name = this.statusdetails.Status_Name;
            }else
            {
              this.BindDataModelObj.Status_Name = this.ModelObj.Status_Name;
            }
         
  
           // this.StatusUpdate();
  
  
      }
    });
   
  }




  imgpath: string; //img sath
  processImage(imageInput: any, content) {


    debugger;

    
    if (imageInput.files.length ==  1) {
     
      this.isLoading = true;
      this.button = "Processing";
  
      console.log("Image Upload" + imageInput);
      this.contentx = content;
      const getnamefile = imageInput.files[0].name;
      const extsn = getnamefile.split(".").pop();
      //alert(extsn);
      // here checking file extension
      if (extsn != "xlsx") {
        this.BindDataModelObj.documentx = imageInput.files[0];
        this.BindDataModelObj.Client_Result_Photo_StatusType = 99;
        this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;// work order id
        this.BindDataModelObj.Type = 1;// nely entry
        if(this.BindDataModelObj.Client_Result_Photo_FilePath != ""){
  
          this.BindDataModelObj.Type = 2;// for update
  
        }
  
        this.DocumentCall();
      }
   
    }
    else
    {
      alert('Please Select Image First');
    }
    // else {
    //this.MessageFlag = "Invalid File Format...!";
    //this.commonMessage(this.contentx);
  }

  // end code

  // enteract between save btn and document upload

  DocumentCall() {
    debugger;
    this.xClientResultOldPhotoServices
      .CommonPhotosUpdate(this.BindDataModelObj)
      .then(res => {
        res.subscribe(response => {
          console.log("Image Upload", response);

          this.modalService.dismissAll();



          this.MessageFlag = "Image Saved...!";

          this.commonMessage(this.contentx);


          this.isLoading = false;
            this.button = "Save";

          // get refresh img care fully here

          this.GetImageWhenUpdateMeta();

          //  this.TaskNameArray = response[0];
        });
      })
    
  }

  // DocumentCall() {
  //   debugger;
  //   this.xClientResultOldPhotoServices
  //     .CommonPhotosUpdate(this.BindDataModelObj)
  //     //.subscribe(response => {
  //       //console.log("Image Upload", response);

  //       this.modalService.dismissAll();



  //       this.MessageFlag = "Image Saved...!";

  //       this.commonMessage(this.contentx);


  //       this.isLoading = false;
  //         this.button = "Save";

  //       // get refresh img care fully here

  //       this.GetImageWhenUpdateMeta();

  //       //  this.TaskNameArray = response[0];
  //    // });
  // }



  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {
        this.GetImageWhenUpdateMeta();
      }, reason => {
        this.GetImageWhenUpdateMeta();
      });
  }
  /// end common model

  // end code

  ///
  // check extension img or not
  ImgFlag = false;
  GetExtension(imgpathx: any) {
    var extsn = imgpathx.split(".").pop();
    if (
      extsn == "jpg" ||  extsn == "png" || extsn == "PNG" ||
      extsn == "JPG" || extsn == "JPEG" || extsn == "jpeg"
    ) {
      this.ImgFlag = true;
      // this is img
    } else if (extsn == "pdf" || extsn == "docx" || extsn == "doc") {
      this.ImgFlag = false;

      // this is other doc
    }
  }
  // end code
  // for model popup open
  OpenModel(OpenModelcontent) {
    this.contentx = OpenModelcontent;
    this.commonimage(this.contentx);
  }

  // image message modal popup
  commonimage(OpenModelcontent) {
    this.modalService
      .open(OpenModelcontent, { windowClass: "smModal" })
      .result.then(result => {}, reason => {});
  }



  //get refresh data
  
  GetImageWhenUpdateMeta(){
    this.TaskBidMasterModelObj.workOrder_ID = this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID;
      this.xClientResultServices
        .WorkorderViewClient(this.TaskBidMasterModelObj)
        .subscribe(response => {
          console.log("response clinet but work order se", response);
        
          this.BindData = response[0][0];
          this.BindDataModelObj.Client_Result_Photo_FilePath = this.BindData.Client_Result_Photo_FilePath;
          console.log('ckeckimgurl', this.BindDataModelObj.Client_Result_Photo_FilePath);
          this.xMasterlayoutComponent.masterFunctionCall(this.BindData);


          // refresh value set sathi
          this.getModelDatax();


        });
  }

//   statusData: any;
// StatusUpdate(){
//   debugger;
//   let User = JSON.parse(localStorage.getItem('usertemp_'));
  
 
//   this.UpdateStausDataModelObj.workOrder_ID = this.ModelObj.workOrder_ID;
//   this.UpdateStausDataModelObj.status = (this.ModelObj.status).toString();
//   this.UpdateStausDataModelObj.UserId =  User[0].User_pkeyID;
//   this.UpdateStausDataModelObj.Type = 1;
//  this.xSaveWorkOrderServices.Workorderstatus(this.UpdateStausDataModelObj)
//  .subscribe(response => {
//   debugger
//    console.log('update',response);
//    this.statusData = response[0][0];
//    this.BindDataModelObj.Received_Date =  this.statusData.Received_Date;
//    if (this.statusData.Status_Name ==='Office Approved') {
//     this.BindDataModelObj.Office_Approved = this.statusData.Status_Name
//    }
//    if (this.statusData.Status_Name ==='field Complete') {
//     this.BindDataModelObj.Office_Approved = this.statusData.Status_Name
//    }
  
//    this.BindDataModelObj.Complete_Date = this.statusData.Complete_Date;
//    this.BindDataModelObj.Cancel_Date = this.statusData.Cancel_Date;
  
  
//  });
 
 
// //  var intstatus = parseInt((this.WorkOrderModelObj.status).toString());
// //  this.CommonStatusDTOObj.Status_ID = intstatus
 
// }


}
