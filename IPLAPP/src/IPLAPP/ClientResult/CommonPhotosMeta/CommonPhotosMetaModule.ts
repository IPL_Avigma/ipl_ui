import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import { CommonPhotosMetaComponent } from "./CommonPhotosMetaComponent";



@NgModule({
  declarations: [CommonPhotosMetaComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    CommonPhotosMetaComponent
  ],

  providers: [],
  bootstrap: [CommonPhotosMetaComponent]
})

export class CommonPhotosMetaModule {}
