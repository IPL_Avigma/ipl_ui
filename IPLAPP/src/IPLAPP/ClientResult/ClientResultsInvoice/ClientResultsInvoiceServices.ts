import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import {ClientResultsInvoiceModel,Invoice_ContractorDTO,Invoice_ClientDTO} from './ClientResultsInvoiceModel';
import {BasetUrl} from '../../Utility/DomainUrl';
import { HomepageServices } from '../../Home/HomeServices';
import {CommonStatusDTO} from '../CommonClientHeader/CommonStatusModel';


@Injectable({
  providedIn: "root"
})
export class ClientResultsInvoiceServices {

  private token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse (localStorage.getItem('TOKEN'));
  }

  private apiUrlPOST = BasetUrl.Domain +"";


  public ContractorInvoicePost(Modelobj:ClientResultsInvoiceModel) {

    //debugger;
    var ANYDTO: any = {};

    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type = 1;






    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }


  /////////////////////////////////////////////////////////////////////
  private apiUrlPOST2 = BasetUrl.Domain +"api/WOCTASK/GetClientResultInvoiceWOIDAutoCom";


  public ContractorClientGetInvoiceData(Modelobj:ClientResultsInvoiceModel) {

    //debugger;
    var ANYDTO: any = {};

    ANYDTO.Inv_Con_Wo_ID  = Modelobj.workOrder_ID;
    ANYDTO.Task_Inv_Auto_Invoice = true;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST2, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////taskdrd
  private apiUrlGet10 =
  BasetUrl.Domain + "/api/RESTIPLDROPDOWN/GetDropDownDataClientResult";

public DropdownGetClientResult() {
  //debugger;
  let ModelDTO: any = {};

  let headers = new HttpHeaders({ "Content-Type": "application/json" });
  headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
  return this._http.post<any>(this.apiUrlGet10, { headers: headers }).pipe(
    tap(data => {
      //console.log(data);
      return data;
    }),
    catchError(this.xHomepageServices.CommonhandleError)
  );
}

///////////// status Dropdown
private apiUrlGet12 =
BasetUrl.Domain + "/api/RESTIPLDROPDOWN/GetStatusDropDown";

public DropdownGetStatus(Modelobj:CommonStatusDTO) {
debugger;
var ANYDTO: any = {};
//ANYDTO.Status_ID = Modelobj.Status_ID;
ANYDTO.whereclause = Modelobj.whereclause;
console.log('mohittest',Modelobj.whereclause)
ANYDTO.Type = Modelobj.Type;


let headers = new HttpHeaders({ "Content-Type": "application/json" });

return this._http.post<any>(this.apiUrlGet12,ANYDTO, { headers: headers }).pipe(
  tap(data => {
    //console.log(data);
    return data;
  }),
  catchError(this.xHomepageServices.CommonhandleError)
);
}


  /////////////////////////////////////////////////////////////////////

  //post Contractor
  private apiUrlPOST22 = BasetUrl.Domain +"api/WOCTASK/ClientResultContractorInvoice";


  public ContractorInvoiceDataPOST(Modelobj:Invoice_ContractorDTO) {

    debugger;
    let ANYDTO: any = {};

    ANYDTO.Inv_Con_pkeyId  = Modelobj.Inv_Con_pkeyId;
    ANYDTO.Inv_Con_Invoice_Id  = Modelobj.Inv_Con_Invoice_Id;
    ANYDTO.Inv_Con_TaskId  = Modelobj.Inv_Con_TaskId;
    ANYDTO.Inv_Con_Wo_ID  = Modelobj.Inv_Con_Wo_ID;
    ANYDTO.Inv_Con_Uom_Id  = Modelobj.Inv_Con_Uom_Id;
    ANYDTO.Inv_Con_Sub_Total  = Modelobj.Inv_Con_Sub_Total;
    ANYDTO.Inv_Con_ContDiscount  = Modelobj.Inv_Con_ContDiscount;
    ANYDTO.Inv_Con_ContTotal  = Modelobj.Inv_Con_ContTotal;
    ANYDTO.Inv_Con_Short_Note  = Modelobj.Inv_Con_Short_Note;
    ANYDTO.Inv_Con_Inv_Followup  = Modelobj.Inv_Con_Inv_Followup;
    ANYDTO.Inv_Con_Inv_Comment  = Modelobj.Inv_Con_Inv_Comment;
    ANYDTO.Inv_Con_Ref_ID  = Modelobj.Inv_Con_Ref_ID;
    ANYDTO.Inv_Con_Followup_Com  = Modelobj.Inv_Con_Followup_Com;
    ANYDTO.Inv_Con_Invoce_Num  = Modelobj.Inv_Con_Invoce_Num;
    ANYDTO.Inv_Con_Inv_Date  = Modelobj.Inv_Con_Inv_Date;
    ANYDTO.Inv_Con_Status  = Modelobj.Inv_Con_Status;
    ANYDTO.Inv_Con_DiscountAmount  = Modelobj.Inv_Con_DiscountAmount;
    ANYDTO.Inv_Con_IsActive  = Modelobj.Inv_Con_IsActive;
    ANYDTO.Inv_Con_IsDelete  = Modelobj.Inv_Con_IsDelete;
    ANYDTO.UserID  = Modelobj.UserID;
    ANYDTO.Type  = Modelobj.Type;
    ANYDTO.Inv_Con_Auto_Invoice  = Modelobj.Task_Inv_Auto_Invoice;
    ANYDTO.Invoice_Contractor_ChildDTO  = Modelobj.ContractorInvoiceArrayVal;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST22, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }


  /////////////////////////////////////////////////////////////////////

  //post Contractor
  private apiUrlPOST2442 = BasetUrl.Domain +"api/WOCTASK/ClientResultClientInvoice";


  public ClientInvoiceDataPOST(Modelobj:Invoice_ClientDTO) {

    //debugger;
    var ANYDTO: any = {};

     ANYDTO.Inv_Client_pkeyId = Modelobj.Inv_Client_pkeyId;
     ANYDTO.Inv_Client_Invoice_Id  = Modelobj.Inv_Client_Invoice_Id;
     ANYDTO.Inv_Client_WO_Id  = Modelobj.Inv_Client_WO_Id;
     ANYDTO.Inv_Client_Task_Id  = Modelobj.Inv_Client_Task_Id;
     ANYDTO.Inv_Client_Uom_Id  = Modelobj.Inv_Client_Uom_Id;
     ANYDTO.Inv_Client_Sub_Total  = Modelobj.Inv_Client_Sub_Total;
     ANYDTO.Inv_Client_Client_Dis  = Modelobj.Inv_Client_Client_Dis;
     ANYDTO.Inv_Client_Client_Total  = Modelobj.Inv_Client_Client_Total;
     ANYDTO.Inv_Client_Short_Note  = Modelobj.Inv_Client_Short_Note;
     ANYDTO.Inv_Client_Inv_Complete  = Modelobj.Inv_Client_Inv_Complete;
     ANYDTO.Inv_Client_Credit_Memo  = Modelobj.Inv_Client_Credit_Memo;
     ANYDTO.Inv_Client_Sent_Client  = Modelobj.Inv_Client_Sent_Client;
     ANYDTO.Inv_Client_Comp_Date  = Modelobj.Inv_Client_Comp_Date;
     ANYDTO.Inv_Client_Invoice_Number  = Modelobj.Inv_Client_Invoice_Number;
     ANYDTO.Inv_Client_Internal_Note  = Modelobj.Inv_Client_Internal_Note;
     ANYDTO.Inv_Client_Status  = Modelobj.Inv_Client_Status;
     ANYDTO.Inv_Client_Discout_Amount  = Modelobj.Inv_Client_Discout_Amount;
     ANYDTO.Inv_Client_IsActive  = Modelobj.Inv_Client_IsActive;
     ANYDTO.Inv_Client_IsDelete  = Modelobj.Inv_Client_IsDelete;
     ANYDTO.UserID  = Modelobj.UserID;
     ANYDTO.Type  = Modelobj.Type;
     ANYDTO.Invoice_Client_ChildDTO  = Modelobj.ClientInvoiceArrayVal;




    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST2442, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }



  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found..!');
    } else {
    alert("Invalid Request..");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}


