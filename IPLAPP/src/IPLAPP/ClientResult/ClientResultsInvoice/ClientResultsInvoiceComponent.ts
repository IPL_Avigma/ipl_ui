import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { MasterlayoutComponent } from "../../Home/MasterComponent";

import { BidInvoiceItemViewTaskServices } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskServices";
import { BidInvoiceItemViewTaskModel } from "../../Admin/BidInvoiceItemViewTask/BidInvoiceItemViewTaskModel";
import {
  ClientResultsInvoiceModel,
  Invoice_ContractorDTO,
  Invoice_ClientDTO
} from "./ClientResultsInvoiceModel";
import { ClientResultsInvoiceServices } from "./ClientResultsInvoiceServices";

import {BindDataModel, TaskBidMasterModel} from '../ClientResults/ClientResultModel';
import { ClientResultServices } from '../ClientResults/ClientResultServices';
import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";



@Component({
  templateUrl: "./ClientResultsInvoice.html"
})
export class ClientResultsInvoiceComponent implements OnInit {
  //DivHIDEx = false;

  ContractorInvoiceArray = [];
  ClientInvoiceArray = [];
  BidInvoiceItemViewTaskModelObj: BidInvoiceItemViewTaskModel = new BidInvoiceItemViewTaskModel();
  ClientResultsInvoiceModelObj: ClientResultsInvoiceModel = new ClientResultsInvoiceModel();

  Invoice_ContractorDTOObj: Invoice_ContractorDTO = new Invoice_ContractorDTO();

  Invoice_ClientDTOObj: Invoice_ClientDTO = new Invoice_ClientDTO();

  BindDataModelObj: BindDataModel = new BindDataModel();

  decuser:any;
  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;
  contprint:boolean = true;
  clientprint:boolean = true;

  constructor(
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRouter: Router,
    private xBidInvoiceItemViewTaskServices: BidInvoiceItemViewTaskServices,
    private xClientResultsInvoiceServices: ClientResultsInvoiceServices,
    private modalService: NgbModal,
    private xRoute: ActivatedRoute,
    private xClientResultServices: ClientResultServices,
    private EncrDecr: EncrDecrServiceService

  ) {
    if(localStorage.getItem('usertemp_') != null)
    {
      var encuser = JSON.parse(localStorage.getItem('usertemp_'));
      var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
      this.decuser  =JSON.parse(decval) ;
      console.log('mohitdec',this.decuser);
      // if (this.decuser[0].GroupRoleId === 1) {
      //   this.hidetab = true;
        
      // }
      switch (this.decuser[0].GroupRoleId) {
        case 1:
          {
            this.OfficeResulth = false;
            this.processorh= false;
            this.tabhide = false;
         
            break;
          }
          case 2:
            {
              this.OfficeResulth = true;
              this.processorh= false;
              this.tabhide = true;
             
              break;
            }
            case 3:
              {
                this.OfficeResulth = false;
                 this.processorh= false;
                 this.tabhide = false;
              
                break;
              }
              case 4:
                {
                  this.OfficeResulth = false;
                  this.processorh= true;
                  this.tabhide = true;
                
                  break;
                }
        }

    }

    this.getModelData();

    this.ContractorInvoiceArray = [
      {
        Inv_Con_Ch_pkeyId: 0,
        Inv_Con_Ch_TaskId: 0,
        Inv_Con_Ch_Uom_Id: 0,
        Inv_Con_Ch_Qty: 1,
        Inv_Con_Ch_Price: 0,
        Inv_Con_Ch_Total: 0,
        Inv_Con_Ch_Adj_Price: 0,
        Inv_Con_Ch_Adj_Total: 0,
        Inv_Con_Ch_Comment: "",
        Inv_Con_Ch_IsActive: true,
        Inv_Con_Ch_IsDelete: false,
        Inv_Con_Ch_InvoiceId: 0,
        Inv_Con_Ch_Client_ID: 0,
        Inv_Con_Ch_Flate_fee: false,
        Inv_Con_Ch_Discount: 0,
        Inv_Con_Ch_Temp_Total:0
      }
    ];

    this.ClientInvoiceArray = [
      {
        Inv_Client_Ch_pkeyId: 0,
        Inv_Client_Ch_Task_Id: 0,
        Inv_Client_Ch_Uom_Id: 0,
        Inv_Client_Ch_Qty: 0,
        Inv_Client_Ch_Price: 0,
        Inv_Client_Ch_Total: 0,
        Inv_Client_Ch_Adj_Price: 0,
        Inv_Client_Ch_Adj_Total: 0,
        Inv_Client_Ch_Comment: "",
        Inv_Client_Ch_IsActive: true,
        Inv_Client_Ch_IsDelete: false,
        Inv_Client_Ch_Invoice_Id: 0,
        Inv_Client_Ch_Flate_Fee: false,
        Inv_Client_Ch_Discount: 0,
        Inv_Client_Ch_Temp_Total:0
      }
    ];
  }

  ngOnInit() {
    //this.getModelData();
    this.getTaskList();
  }

  //drd tsk
  GetDrdData(){
    this.xClientResultsInvoiceServices
    .DropdownGetClientResult()
    .subscribe(response => {
      console.log("drop d client data", response);

     
      this.TaskList = response[0];
  })
 
}
validate(evt) {
  debugger
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}


  ClientResultContractionInvoiceSumbit(content) {
    debugger
    this.contentx = content;

    this.isLoading = true;
    this.button = "Processing";

    this.ContractorInvoiceArray;

    this.Invoice_ContractorDTOObj.ContractorInvoiceArrayVal = this.ContractorInvoiceArray;
    this.Invoice_ContractorDTOObj.Inv_Con_Wo_ID = this.ClientResultsInvoiceModelObj.workOrder_ID;
    this.Invoice_ContractorDTOObj.Type = 1;

    this.xClientResultsInvoiceServices
      .ContractorInvoiceDataPOST(this.Invoice_ContractorDTOObj)
      .subscribe(response => {
        console.log("post cont data", response);
        if(response[0].length != 0){
          this.contprint = false;
         
        this.Invoice_ContractorDTOObj.Inv_Con_pkeyId = response[0][0];

        this.isLoading = false;
        this.button = "Save";

        this.MessageFlag = "Data Saved...!";

        this.commonMessage(this.contentx);
        }else{

          alert('ENTERNAL SERVER ERROR ');
        }

      });
  }








  ClientResultClientInvoiceSumbit(content) {
    this.contentx = content;
    this.isLoading = true;
    this.button = "Processing";

    this.ClientInvoiceArray;

    this.Invoice_ClientDTOObj.ClientInvoiceArrayVal = this.ClientInvoiceArray;
    this.Invoice_ClientDTOObj.Inv_Client_WO_Id = this.ClientResultsInvoiceModelObj.workOrder_ID;

    this.xClientResultsInvoiceServices
      .ClientInvoiceDataPOST(this.Invoice_ClientDTOObj)
      .subscribe(response => {
        console.log("post clint invoice ", response);
        
        this.clientprint = false;
        if(response[0].length != 0){
          this.Invoice_ClientDTOObj.Inv_Client_pkeyId = response[0][0];


          this.isLoading = false;
          this.button = "Save";

          this.MessageFlag = "Data Saved...!";

          this.commonMessage(this.contentx);

        }else{

          alert('ENTERNAL SERVER ERROR ');
        }

      });
  }

  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  ModelObj: any;
  BindData: any;

  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi
  getModelData() {
    //debugger;
    // this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdataClinet();
    // if (this.ModelObj == undefined) {
    //   this.xRouter.navigate(["/workorder/view"]);
    // } else {
    //   //this.DivHIDEx = true;
    //   console.log("hfhs", this.ModelObj);
    //   this.BindData = this.ModelObj;

    //   this.BindDataModelObj.workOrderNumber = this.ModelObj.workOrderNumber;
    //   this.BindDataModelObj.IPLNO =  this.ModelObj.IPLNO;


    //   this.ClientResultsInvoiceModelObj.workOrder_ID = this.ModelObj.workOrder_ID;
      this.GetCon_ClintDataByWOId();
    //}
  }

  AddClientResultContractorInvoiceAddColl() {
    let data = {
      Inv_Con_Ch_pkeyId: 0,
      Inv_Con_Ch_TaskId: 0,
      Inv_Con_Ch_Uom_Id: 0,
      Inv_Con_Ch_Qty: 1,
      Inv_Con_Ch_Price: 0,
      Inv_Con_Ch_Total: 0,
      Inv_Con_Ch_Adj_Price: 0,
      Inv_Con_Ch_Adj_Total: 0,
      Inv_Con_Ch_Comment: "",
      Inv_Con_Ch_IsActive: true,
      Inv_Con_Ch_IsDelete: false,
      Inv_Con_Ch_InvoiceId: 0,
      Inv_Con_Ch_Client_ID: 0,
      Inv_Con_Ch_Flate_fee: false,
      Inv_Con_Ch_Discount: 0,
      Inv_Con_Ch_Temp_Total:0
    };
    this.ContractorInvoiceArray.push(data);
  }
  clientresultinvoiceRemove(index) {

    let prm = confirm('Are you sure delete this record');
    if(prm){

      if (index != 0) {
        this.ContractorInvoiceArray.splice(index, 1);
        // plz uncomment
        //this.SubTotalContractor();
        this.CommonCalculatorContractor();
      }
    }




  }

  TaskList: any;
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel();
  getTaskList() {
    debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
     // this.GetCon_ClintDataByWOId();
      console.log("response clinet but work order se click", response);

      // this.BindData = response[0][0];
      // this.xMasterlayoutComponent.masterFunctionCall(this.BindData);
      this.GetDrdData();

      // this.TaskList = this.xMasterlayoutComponent.masterFunctionGetdataClinetTaskDropDown();
      // if (this.TaskList != undefined) {
      //   console.log("task invoice cha", this.TaskList);
      // } else {
      //   this.xRouter.navigate(["/workorder/view"]);
      // }
    });
  }

  ClientResultCompletionInvoicePage(INVOICE) {
    //debugger;

    this.BidInvoiceItemViewTaskModelObj.Task_pkeyID = INVOICE.Inv_Con_Ch_TaskId;

    this.xBidInvoiceItemViewTaskServices
      .ViewTaskMasterData(this.BidInvoiceItemViewTaskModelObj)
      .subscribe(response => {
        console.log("task pkey id wise invoice page", response);

        //debugger;

        let task_pkey_id = this.BidInvoiceItemViewTaskModelObj.Task_pkeyID;

        for (let i = 0; i < this.ContractorInvoiceArray.length; i++) {
          let element = this.ContractorInvoiceArray[i].Inv_Con_Ch_TaskId;
          if (task_pkey_id == element) {
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Qty = 1;
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Price =
              response[0][0].Task_Contractor_UnitPrice;
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Total =
              response[0][0].Task_Contractor_UnitPrice;
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Total =
              response[0][0].Task_Contractor_UnitPrice;
          }
        }


        this.CommonCalculatorContractor();
        //this.SubTotalClientx();
      });
  }

  //button call
  PrintWindow() {
    window.print();
  }

  EditWorkOrder() {
    this.xRouter.navigate(["/workorder/new"]);
  }
  GoToBack() {
    this.xRouter.navigate(["/workorder/view"]);
  }

  ClentResultinsCall(){
    this.xRouter.navigate(["/client/clientresultinstruction"]);
  }

  // contractor remove
  conctRemoveArr = [];
  clientresultinvoiceContractorRemove(index:any,Items:any):void{
debugger;
    let comf = confirm('Are you sure delete this records');
    if(comf){

      this.ContractorInvoiceArray.splice(index, 1);
      this.CommonCalculatorContractor();
      //alert(JSON.stringify(Items));

      this.conctRemoveArr.push(Items);

      this.Invoice_ContractorDTOObj.ContractorInvoiceArrayVal = this.conctRemoveArr;
      this.Invoice_ContractorDTOObj.Inv_Con_Wo_ID = this.ClientResultsInvoiceModelObj.workOrder_ID;
      this.Invoice_ContractorDTOObj.Type = 4;

      this.xClientResultsInvoiceServices
        .ContractorInvoiceDataPOST(this.Invoice_ContractorDTOObj)
        .subscribe(response => {
          console.log("post cont data delete", response);

          this.Invoice_ContractorDTOObj.Type = 1;
          this.conctRemoveArr = [];


        });
    }


  }

  // client remove
  ClientRemoveArry = [];
  clientresultinvoiceRemoveClient(index:any,Items:any):void {
    let comf = confirm('Are you sure delete this records');
    if(comf){

      this.ClientInvoiceArray.splice(index, 1);
      this.CommonCalculatorClient();
      //alert(JSON.stringify(Items));
      this.ClientRemoveArry.push(Items);

      this.Invoice_ClientDTOObj.ClientInvoiceArrayVal = this.ClientRemoveArry;
      this.Invoice_ClientDTOObj.Inv_Client_WO_Id = this.ClientResultsInvoiceModelObj.workOrder_ID;
      this.Invoice_ClientDTOObj.Type = 4;

      this.xClientResultsInvoiceServices
      .ClientInvoiceDataPOST(this.Invoice_ClientDTOObj)
      .subscribe(response => {
        console.log("post clint invoice delete ", response);

        this.ClientRemoveArry = [];
        this.Invoice_ClientDTOObj.Type = 1;


      });
    }



  }

  //end remove code

  AddClientResultContractorInvoiceAddCollClinet() {
    let data = {
      Inv_Client_Ch_pkeyId: 0,
      Inv_Client_Ch_Task_Id: 0,
      Inv_Client_Ch_Uom_Id: 0,
      Inv_Client_Ch_Qty: 1,
      Inv_Client_Ch_Price: 0,
      Inv_Client_Ch_Total: 0,
      Inv_Client_Ch_Adj_Price: 0,
      Inv_Client_Ch_Adj_Total: 0,
      Inv_Client_Ch_Comment: "",
      Inv_Client_Ch_IsActive: true,
      Inv_Client_Ch_IsDelete: false,
      Inv_Client_Ch_Invoice_Id: 0,
      Inv_Client_Ch_Flate_Fee: false,
      Inv_Client_Ch_Discount: 0,
      Inv_Client_Ch_Temp_Total:0
    };

    this.ClientInvoiceArray.push(data);
  }

  ClientResultCompletionInvoicePageClinet(InvoiceClint) {
    this.BidInvoiceItemViewTaskModelObj.Task_pkeyID =
      InvoiceClint.Inv_Client_Ch_Task_Id;

    this.xBidInvoiceItemViewTaskServices
      .ViewTaskMasterData(this.BidInvoiceItemViewTaskModelObj)
      .subscribe(response => {
        console.log("task pkey id wise invoice page clint", response);

        //debugger;

        let task_pkey_id = this.BidInvoiceItemViewTaskModelObj.Task_pkeyID;

        for (let i = 0; i < this.ClientInvoiceArray.length; i++) {
          let element = this.ClientInvoiceArray[i].Inv_Client_Ch_Task_Id;
          if (task_pkey_id == element) {
            this.ClientInvoiceArray[i].Inv_Client_Ch_Price =
              response[0][0].Task_Client_UnitPrice;
            this.ClientInvoiceArray[i].Inv_Client_Ch_Total =
              response[0][0].Task_Client_UnitPrice;
          }
        }

        this.CommonCalculatorClient();
      });
  }





  ClientresultInvoiceConQty(){

    this.CommonCalculatorContractor();

  }

  ClientresultInvoiceConPrice(){

    this.CommonCalculatorContractor();
  }

  ClientresultInvoiceConAdjPrice(){

    this.CommonCalculatorContractor();
  }

  ClientresultInvoiceConDisCount(){

    this.CommonCalculatorContractor();
  }


  // common

  CommonCalculatorContractor(){


    //debugger;

    for (let i = 0; i < this.ContractorInvoiceArray.length; i++) {
          if (this.ContractorInvoiceArray[i].Inv_Con_Ch_Qty != "" ) {
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Total = this.ContractorInvoiceArray[i].Inv_Con_Ch_Price * this.ContractorInvoiceArray[i].Inv_Con_Ch_Qty;
          } else {
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Qty = 1;
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Total = this.ContractorInvoiceArray[i].Inv_Con_Ch_Price;
          }

          if(this.ContractorInvoiceArray[i].Inv_Con_Ch_Price != ""){

            this.ContractorInvoiceArray[i].Inv_Con_Ch_Total =  this.ContractorInvoiceArray[i].Inv_Con_Ch_Price *
                      this.ContractorInvoiceArray[i].Inv_Con_Ch_Qty;
          }else{
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Price = 0;
          }

          if(this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price != ""){



               this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Total =  parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) +
               parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price);

               this.ContractorInvoiceArray[i].Inv_Con_Ch_Temp_Total =  parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) +
               parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price);

          }else{
            //debugger;
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price = 0;
            this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Total =  parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) + 0;

            this.ContractorInvoiceArray[i].Inv_Con_Ch_Temp_Total =  parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) + 0;
          }

          if(this.ContractorInvoiceArray[i].Inv_Con_Ch_Discount != ""){


            this.ContractorInvoiceArray[i].Inv_Con_Ch_Temp_Total =  parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) +
               parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price);

            this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Total =  this.ContractorInvoiceArray[i].Inv_Con_Ch_Temp_Total - (this.ContractorInvoiceArray[i].Inv_Con_Ch_Temp_Total * this.ContractorInvoiceArray[i].Inv_Con_Ch_Discount) / 100;

            this.CommonSubTotalContractar();



          }else{

            this.ContractorInvoiceArray[i].Inv_Con_Ch_Discount = 0;

            this.ContractorInvoiceArray[i].Inv_Con_Ch_Temp_Total = parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) +
            parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price);


            this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Total = parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Total) +
            parseInt(this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Price);


            this.CommonSubTotalContractar();
          }



        }

  }

  CommonSubTotalContractar(){

    var countxx = 0;

    for (let i = 0; i < this.ContractorInvoiceArray.length; i++) {
      //const element = this.ContractorInvoiceArray[i];

      countxx = countxx +  this.ContractorInvoiceArray[i].Inv_Con_Ch_Adj_Total;
    }
     this.Invoice_ContractorDTOObj.Inv_Con_Sub_Total =  countxx;


     //this.printData();

  }


  // Discount Disabled when click check box true
  ClientResultContractorFlatFree(){

    //debugger;

    // using index base

    for (let i = 0; i < this.ContractorInvoiceArray.length; i++) {


      if(this.ContractorInvoiceArray[i].Inv_Con_Ch_Flate_fee){

        this.ContractorInvoiceArray[i].Inv_Con_Ch_Discount = 0;
      }

    }
    this.CommonCalculatorContractor();


  }



  ClinetResultClientQty(){

    this.CommonCalculatorClient();
  }
  ClinetResultInvoiceClientPrice(){

    this.CommonCalculatorClient();
  }
  ClientResultinvoiceAdjPrice(){

    this.CommonCalculatorClient();
  }
  ClientResultinvoiceDiscount(){

    this.CommonCalculatorClient();
  }


  CommonCalculatorClient(){

    for (let i = 0; i < this.ClientInvoiceArray.length; i++) {
      if (this.ClientInvoiceArray[i].Inv_Client_Ch_Qty != "" ) {
        this.ClientInvoiceArray[i].Inv_Client_Ch_Total = this.ClientInvoiceArray[i].Inv_Client_Ch_Price * this.ClientInvoiceArray[i].Inv_Client_Ch_Qty;
      } else {
        this.ClientInvoiceArray[i].Inv_Client_Ch_Qty = 1;
        this.ClientInvoiceArray[i].Inv_Client_Ch_Total = this.ClientInvoiceArray[i].Inv_Client_Ch_Price;
      }

      if(this.ClientInvoiceArray[i].Inv_Client_Ch_Price != ""){

        this.ClientInvoiceArray[i].Inv_Client_Ch_Total =  this.ClientInvoiceArray[i].Inv_Client_Ch_Price *
                  this.ClientInvoiceArray[i].Inv_Client_Ch_Qty;
      }else{
        this.ClientInvoiceArray[i].Inv_Client_Ch_Price = 0;
      }

      if(this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price != ""){



           this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Total =  parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) +
           parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price);

           this.ClientInvoiceArray[i].Inv_Client_Ch_Temp_Total =  parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) +
           parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price);

      }else{
        //debugger;
        this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price = 0;
        this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Total =  parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) + 0;

        this.ClientInvoiceArray[i].Inv_Client_Ch_Temp_Total =  parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) + 0;
      }

      if(this.ClientInvoiceArray[i].Inv_Client_Ch_Discount != ""){



        this.ClientInvoiceArray[i].Inv_Client_Ch_Temp_Total =  parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) +
           parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price);

        this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Total =  this.ClientInvoiceArray[i].Inv_Client_Ch_Temp_Total - (this.ClientInvoiceArray[i].Inv_Client_Ch_Temp_Total * this.ClientInvoiceArray[i].Inv_Client_Ch_Discount) / 100;

        this.CommonSubTotalClient();



      }else{

        this.ClientInvoiceArray[i].Inv_Client_Ch_Discount = 0;

        this.ClientInvoiceArray[i].Inv_Client_Ch_Temp_Total = parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) +
        parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price);


        this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Total = parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Total) +
        parseInt(this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Price);


        this.CommonSubTotalClient();
      }



    }

}

CommonSubTotalClient(){

var countxx = 0;

for (let i = 0; i < this.ClientInvoiceArray.length; i++) {
  //const element = this.ContractorInvoiceArray[i];

  countxx = countxx +  this.ClientInvoiceArray[i].Inv_Client_Ch_Adj_Total;
}
this.Invoice_ClientDTOObj.Inv_Client_Sub_Total =  countxx;

}


// diable discoutnt box when check click
ClientResultClinetFlatFree(){


  for (let i = 0; i < this.ClientInvoiceArray.length; i++) {

    if(this.ClientInvoiceArray[i].Inv_Client_Ch_Flate_Fee){

      this.ClientInvoiceArray[i].Inv_Client_Ch_Discount  = 0;
    }

  }
  this.CommonCalculatorClient();
}




///////////////////////////////////////////////////////////////
headerData: any;
  GetCon_ClintDataByWOId() {
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    const workorder = parseInt(workOrderID);
    this.ClientResultsInvoiceModelObj.workOrder_ID = workorder
    this.xClientResultsInvoiceServices
      .ContractorClientGetInvoiceData(this.ClientResultsInvoiceModelObj)
      .subscribe(response => {
        console.log("con_clinet get", response);

        if (response[0].length != 0) {
          this.ContractorInvoiceArray = response[0];
        }
        if (response[1].length != 0) {
          this.ClientInvoiceArray = response[1];
        }

        // contractor parent\
        if (response[2].length != 0) {
          this.Invoice_ContractorDTOObj.Inv_Con_pkeyId =
            response[2][0].Inv_Con_pkeyId;

          this.Invoice_ContractorDTOObj.Inv_Con_ContDiscount =
            response[2][0].Inv_Con_ContDiscount;
          this.Invoice_ContractorDTOObj.Inv_Con_ContTotal =
            response[2][0].Inv_Con_ContTotal;
          this.Invoice_ContractorDTOObj.Inv_Con_DiscountAmount =
            response[2][0].Inv_Con_DiscountAmount;
          this.Invoice_ContractorDTOObj.Inv_Con_Followup_Com =
            response[2][0].Inv_Con_Followup_Com;
          this.Invoice_ContractorDTOObj.Inv_Con_Inv_Comment =
            response[2][0].Inv_Con_Inv_Comment;
          this.Invoice_ContractorDTOObj.Inv_Con_Inv_Date =
            response[2][0].Inv_Con_Inv_Date;
          this.Invoice_ContractorDTOObj.Inv_Con_Inv_Followup =
            response[2][0].Inv_Con_Inv_Followup;
          this.Invoice_ContractorDTOObj.Inv_Con_Invoce_Num =
            response[2][0].Inv_Con_Invoce_Num;
          this.Invoice_ContractorDTOObj.Inv_Con_Invoice_Id =
            response[2][0].Inv_Con_Invoice_Id;
          this.Invoice_ContractorDTOObj.Inv_Con_IsActive =
            response[2][0].Inv_Con_IsActive;
          //this.Invoice_ContractorDTOObj.Inv_Con_IsDelete = response[2][0].Inv_Con_IsDelete;
          this.Invoice_ContractorDTOObj.Inv_Con_Ref_ID =
            response[2][0].Inv_Con_Ref_ID;
          this.Invoice_ContractorDTOObj.Inv_Con_Short_Note =
            response[2][0].Inv_Con_Short_Note;
          this.Invoice_ContractorDTOObj.Inv_Con_Status =
            response[2][0].Inv_Con_Status;
          this.Invoice_ContractorDTOObj.Inv_Con_Sub_Total =
            response[2][0].Inv_Con_Sub_Total;
          this.Invoice_ContractorDTOObj.Inv_Con_TaskId =
            response[2][0].Inv_Con_TaskId;
          this.Invoice_ContractorDTOObj.Inv_Con_Uom_Id =
            response[2][0].Inv_Con_Uom_Id;
          this.Invoice_ContractorDTOObj.Inv_Con_Wo_ID =
            response[2][0].Inv_Con_Wo_ID;

          this.Invoice_ContractorDTOObj.Task_Inv_Auto_Invoice =
            response[2][0].Task_Inv_Auto_Invoice;
        }

        if (response[3].length != 0) {
          this.Invoice_ClientDTOObj.Inv_Client_pkeyId =
            response[3][0].Inv_Client_pkeyId;

          this.Invoice_ClientDTOObj.Inv_Client_Auto_Invoice =
            response[3][0].Inv_Client_Auto_Invoice;
          //this.Invoice_ClientDTOObj.Inv_Client_Client_Dis = response[3][0].Inv_Client_Client_Dis;
          this.Invoice_ClientDTOObj.Inv_Client_Client_Total =
            response[3][0].Inv_Client_Client_Total;
          this.Invoice_ClientDTOObj.Inv_Client_Comp_Date =
            response[3][0].Inv_Client_Comp_Date;

            console.log('datecheck:', this.Invoice_ClientDTOObj.Inv_Client_Comp_Date)

          this.Invoice_ClientDTOObj.Inv_Client_Credit_Memo =
            response[3][0].Inv_Client_Credit_Memo;
          this.Invoice_ClientDTOObj.Inv_Client_Discout_Amount =
            response[3][0].Inv_Client_Discout_Amount;
          this.Invoice_ClientDTOObj.Inv_Client_Internal_Note =
            response[3][0].Inv_Client_Internal_Note;
          this.Invoice_ClientDTOObj.Inv_Client_Inv_Complete =
            response[3][0].Inv_Client_Inv_Complete;
          this.Invoice_ClientDTOObj.Inv_Client_Invoice_Id =
            response[3][0].Inv_Client_Invoice_Id;
          this.Invoice_ClientDTOObj.Inv_Client_Invoice_Number =
            response[3][0].Inv_Client_Invoice_Number;
          this.Invoice_ClientDTOObj.Inv_Client_IsActive =
            response[3][0].Inv_Client_IsActive;
          //this.Invoice_ClientDTOObj.Inv_Client_IsDelete = response[3][0].Inv_Client_IsDelete;
          this.Invoice_ClientDTOObj.Inv_Client_Sent_Client =
            response[3][0].Inv_Client_Sent_Client;
          this.Invoice_ClientDTOObj.Inv_Client_Short_Note =
            response[3][0].Inv_Client_Short_Note;
          this.Invoice_ClientDTOObj.Inv_Client_Status =
            response[3][0].Inv_Client_Status;
          this.Invoice_ClientDTOObj.Inv_Client_Sub_Total =
            response[3][0].Inv_Client_Sub_Total;
          this.Invoice_ClientDTOObj.Inv_Client_Task_Id =
            response[3][0].Inv_Client_Task_Id;
          this.Invoice_ClientDTOObj.Inv_Client_Uom_Id =
            response[3][0].Inv_Client_Uom_Id;
        }
        if (response[4].length != 0)
        {
        this.headerData = response[4]
        }

        this.CommonCalculatorContractor();
        this.CommonCalculatorClient();
      });
  }




  // print div
  PrintDiv(){

    this.buttonPrint = "Print";
    this.isLoadingPrint = false;


    var divToPrint = document.getElementById('DivIdToPrint');

    var newWin = window.open('', 'Print-Window');

    newWin.document.open();

    newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);


    this.PrintDivF = true;
  }

  //clientdiv print
  clientPrint(){

    this.buttoncPrint = "Client Print";
    this.isLoadingPrint = false;


    var Printdata = document.getElementById('Divprint');

    var newWin = window.open('', 'Print-Window');

    newWin.document.open();

    newWin.document.write('<html><body onload="window.print()">' + Printdata.innerHTML + '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);


    this.PrintDivcl= true;
  }



  AsycWaitPhotoscall(){


    debugger;
    var promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("Async Work Complete");
        resolve();
        // un comment this

         this.PrintDiv();

      }, 2000);
    });
  }
  AsycWaitPhotoscall1(){


    debugger;
    var promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("Async Work Complete");
        resolve();
        // un comment this

         this.clientPrint();

      }, 2000);
    });
  }



  buttonPrint = "Print";
  isLoadingPrint = false;

  printArray = [];

  PrintContent:any;
  PrintDivF = true;
  
  printData(InnerHTML){

    debugger;
    this.PrintDivF = false;
   

    this.isLoadingPrint = true;
    this.buttonPrint = "Processing";
    debugger;
    this.printArray = [];
    for (let i = 0; i < this.ContractorInvoiceArray.length; i++) {

      const taskpk = this.ContractorInvoiceArray[i].Inv_Con_Ch_TaskId;

      for (let j = 0; j < this.TaskList.length; j++) {
        const listpkey = this.TaskList[j].Task_pkeyID;

        if(taskpk == listpkey){


          this.ContractorInvoiceArray[i].Inv_Con_Inv_Comment = this.TaskList[j].Task_Name;


          let datax = this.ContractorInvoiceArray[i];
          this.printArray.push(datax);


        }


      }




    }

    console.log('print array ',this.printArray);

    //this.PrintDiv();

    this.AsycWaitPhotoscall();
  }


// client print details data



  clientprintArray = [];

  clientPrintContent:any;
  buttoncPrint = "Client Print";
  PrintDivcl = true;
  clientPrintdetails(InnerHTML){

    debugger;
    this.PrintDivcl = false;
   

    this.isLoadingPrint = true;
    this.buttoncPrint = "Processing";
    debugger;
    this.clientprintArray = [];
    for (let i = 0; i < this.ClientInvoiceArray.length; i++) {

      const taskpk = this.ClientInvoiceArray[i].Inv_Client_Ch_Task_Id;

      for (let j = 0; j < this.TaskList.length; j++) {
        const listpkey = this.TaskList[j].Task_pkeyID;

        if(taskpk == listpkey){


          this.ClientInvoiceArray[i].Inv_Client_Ch_Comment = this.TaskList[j].Task_Name;


          let cdata = this.ClientInvoiceArray[i];
          this.clientprintArray.push(cdata);


        }


      }




    }

    console.log('client print array ',this.clientprintArray);

    //this.PrintDiv();

    this.AsycWaitPhotoscall1();
  }


}
