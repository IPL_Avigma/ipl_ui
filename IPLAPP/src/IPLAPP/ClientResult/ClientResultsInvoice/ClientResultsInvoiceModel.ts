export class ClientResultsInvoiceModel {
  workOrder_ID: Number = 0;
  UserID: Number = 0;
  Type: Number = 0;
}

export class Invoice_ContractorDTO {
  Inv_Con_pkeyId: Number = 0;
  Inv_Con_Invoice_Id: Number = 0;
  Inv_Con_TaskId: Number = 0;
  Inv_Con_Wo_ID: Number = 0;
  Inv_Con_Uom_Id: Number = 0;
  Inv_Con_Sub_Total: Number = 0;
  Inv_Con_ContDiscount: Number = 12;
  Inv_Con_ContTotal: Number = 0;
  Inv_Con_Short_Note: string = "";
  Inv_Con_Inv_Followup: boolean = false;
  Inv_Con_Inv_Comment: string = "";
  Inv_Con_Ref_ID: string = "";
  Inv_Con_Followup_Com: boolean = false;
  Inv_Con_Invoce_Num: Number = 0;
  Inv_Con_Inv_Date: any;
  Inv_Con_Status: Number = 0;
  Inv_Con_DiscountAmount: Number = 0;
  Inv_Con_IsActive: boolean = true;
  Inv_Con_IsDelete: boolean = false;
  UserID: Number = 0;
  Type: Number = 1;
  Task_Inv_Auto_Invoice: boolean = true;

  Inv_Con_Auto_Invoice: boolean = true;

  ContractorInvoiceArrayVal:any;
}

export class Invoice_ClientDTO {
  Inv_Client_pkeyId:Number = 0;
  Inv_Client_Invoice_Id: Number = 0;
  Inv_Client_WO_Id: Number = 0;
  Inv_Client_Task_Id: Number = 0;
  Inv_Client_Uom_Id: Number = 0;
  Inv_Client_Sub_Total: Number = 0;
  Inv_Client_Client_Dis: Number = 12;
  Inv_Client_Client_Total: Number = 0;
  Inv_Client_Short_Note: string = "";
  Inv_Client_Inv_Complete: boolean = false;
  Inv_Client_Credit_Memo: boolean = false;
  Inv_Client_Sent_Client: any;
  Inv_Client_Comp_Date: any;
  Inv_Client_Invoice_Number: string = "";
  Inv_Client_Internal_Note: string = "";
  Inv_Client_Status: Number = 0;
  Inv_Client_Discout_Amount: Number = 0;
  Inv_Client_IsActive: boolean = true;
  Inv_Client_IsDelete: boolean = false;

  UserID: Number = 0;
  Type: Number = 1;


  ClientInvoiceArrayVal:any;

  Inv_Client_Auto_Invoice:boolean = false;

  Inv_Client_Inv_Date: any;
}
