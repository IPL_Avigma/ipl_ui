import { Injectable } from "@angular/core";
import { throwError, from } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { ClientResultPhotoModel, TaskBidPhoto,Custom_PhotoLabel,WorkOrder_CustomPhotoLabel } from "./ClientResultPhotoModel";
import { BasetUrl } from "../../Utility/DomainUrl";
import { BindDataModel } from "../ClientResults/ClientResultModel";
import { toDataURL } from '@progress/kendo-angular-excel-export';
import { ReturnStatement } from '@angular/compiler';
import { HomepageServices } from '../../Home/HomeServices';

@Injectable({
  providedIn: "root"
})
export class ClientResultOldPhotoServices {

  private token: any;

  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse(localStorage.getItem('TOKEN'));
  }

  // private apiUrlGet = BasetUrl.Domain + "api/MultiPhoto/GetCLientResultPhotos";
  private apiUrlGet =  "api/MultiPhoto/GetCLientResultPhotos";

  public ViewCLientImagesData(Modelobj: ClientResultPhotoModel) {
    //debugger;
    let ANYDTO: any = {};

    ANYDTO.Type = Modelobj.Type;
    ANYDTO.Client_Result_Photo_ID = Modelobj.Client_Result_Photo_ID;
    ANYDTO.Client_Result_Photo_Wo_ID = Modelobj.Client_Result_Photo_Wo_ID;
    ANYDTO.IPLNO = Modelobj.IPLNO;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////////////////////////
  //
  // add document for mutiple client phtotos

  public readFile(file, w, h) {
    const fr = new FileReader();
    return new Promise((resolve, reject) => {
      fr.onerror = (err) => {
        reject(err);
      }

      fr.onloadend = () => {
        const canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d');

        // set its dimension to target size
        canvas.width = w;
        canvas.height = h;

        const image = new Image();
        image.src = fr.result.toString();
        image.onload = function () {
          // draw source image into the off-screen canvas:
          ctx.drawImage(image, 0, 0, w, h);
          resolve(canvas.toDataURL());
        }
      }

      fr.readAsDataURL(file);
    });
  }

  public async SinglePhotoUpdate(Modelobj: BindDataModel, edited) {
    var StrEditDTO: any = {};

    StrEditDTO.docx = Modelobj.documentx;

    let ANYDTO: any = {};

    ANYDTO.workOrderNumber = Modelobj.workOrderNumber;
    ANYDTO.IPLNO = Modelobj.IPLNO;
    ANYDTO.Client_Result_Photo_StatusType =  Modelobj.Client_Result_Photo_StatusType;
    ANYDTO.Client_Result_Photo_Wo_ID = Modelobj.Common_pkeyID.toString();// work order nmber;
    ANYDTO.Client_Result_Photo_FileName = Modelobj.Client_Result_Photo_FileName;
    ANYDTO.Client_Result_Photo_FilePath = Modelobj.Client_Result_Photo_FilePath;
    ANYDTO.Type = Modelobj.Type;
    ANYDTO.Client_Result_Photo_IsActive = 1;
    ANYDTO.Client_Result_Photo_Type = 1;
    ANYDTO.Client_Result_Photo_Ch_ID = 0;
    ANYDTO.Image = StrEditDTO.docx;
    ANYDTO.ReqType = 1;  // 1 for Desktop 2 for Mobile
    ANYDTO.ContentType  = 1; //  // 1 for Image 2 for Doc

    const uploadapi = 'https://us-central1-rare-lambda-245821.cloudfunctions.net/app/upload';
    // const uploadapi = 'http://localhost:5000/upload';
    const ImageLg =  edited;
    const Image =  edited;
    const ImageSm =  edited;
    ANYDTO.Image = Image;
    ANYDTO.ImageLg = ImageLg;
    ANYDTO.ImageSm = ImageSm;
    return this._http.post<any>(uploadapi, ANYDTO).pipe(
      tap(data => {
        console.log('cloudfunctions FireBase Third Party tools',data);
        return data;
      }),
      catchError(this.handleError)
    );
  }

  public readDoc(file) {
    const fr = new FileReader();
    return new Promise((resolve, reject) => {
      fr.onerror = (err) => {
        reject(err);
      }

      fr.onloadend = () => {
        resolve(fr.result);
      }

      fr.readAsDataURL(file);
    });
  }
  public async CommonPhotosUpdate(Modelobj: BindDataModel) {
    debugger;
    var StrEditDTO: any = {};

    StrEditDTO.docx = Modelobj.documentx;

    // if(Modelobj.YR_Company_pkeyID != 0){
    //   ANYDTO.Type = 2;
    // }
    console.log(Modelobj, 'modelobj');

    var data = new FormData();
    const pkeyId = Modelobj.Common_pkeyID.toString();// work order nmber

    let ANYDTO: any = {};

    ANYDTO.workOrderNumber = Modelobj.workOrderNumber;
    ANYDTO.IPLNO = Modelobj.IPLNO;
    ANYDTO.Client_Result_Photo_StatusType =  Modelobj.Client_Result_Photo_StatusType;
    ANYDTO.Client_Result_Photo_Wo_ID = Modelobj.Common_pkeyID.toString();// work order nmber;
    ANYDTO.Client_Result_Photo_FileName = Modelobj.Client_Result_Photo_FileName;
    ANYDTO.Client_Result_Photo_IsActive = 1;
    ANYDTO.Client_Result_Photo_Type = 1;
   
  

    if (Modelobj.Client_Result_Photo_FileName == null) {
      ANYDTO.Client_Result_Photo_FileName = StrEditDTO.docx.name;
      console.log('dataimage', StrEditDTO.docx );
    }
    ANYDTO.Client_Result_Photo_FilePath = Modelobj.Client_Result_Photo_FilePath;
    ANYDTO.Type = Modelobj.Type;
    ANYDTO.Image = StrEditDTO.docx;
    ANYDTO.ReqType = 1;  // 1 for Desktop 2 for Mobile
    ANYDTO.ContentType  = 1;
    ANYDTO.Client_Result_Photo_Ch_ID = 0;
       ANYDTO.Client_Result_Photo_ID = 0 ;
    // if (Modelobj.Client_PageCalled == 3) {
    //   ANYDTO.ContentType  = 2; 
    // }
    // //this is instruction
    // else if (Modelobj.Client_PageCalled == 2) {
    //   ANYDTO.ContentType  = 2; 
    //   ANYDTO.Client_Result_Photo_Ch_ID = Modelobj.Client_Result_Photo_Ch_ID;
    //   ANYDTO.Client_Result_Photo_ID = Modelobj.Inst_Doc_PkeyID;

    // }
    // // this is for photos
    // else{
    //   ANYDTO.ContentType  = 1; //  // 1 for Image 2 for Doc
    //   ANYDTO.Client_Result_Photo_Ch_ID = 0;
    //   ANYDTO.Client_Result_Photo_ID = 0 ; //Modelobj.Client_Result_Photo_ID
    // }
   

    //const uploadapi = 'https://us-central1-rare-lambda-245821.cloudfunctions.net/app/upload';
    // const uploadapi = 'http://localhost:5000/upload';
    const uploadapi = BasetUrl.CloudURL;
    const ImageLg =  await this.readFile(StrEditDTO.docx, 1024, 768);
    const Image =  await this.readFile(StrEditDTO.docx, 640, 480);
    const ImageSm =  await this.readFile(StrEditDTO.docx, 320, 240);
    ANYDTO.Image = Image;
    ANYDTO.ImageLg = ImageLg;
    ANYDTO.ImageSm = ImageSm;
    return this._http.post<any>(uploadapi, ANYDTO).pipe(
      tap(data => {
        console.log('cloudfunctions FireBase Third Party tools',data);
        return data;
      }),
      catchError(this.handleError)
    );
  }

  public async CommonDocumentsUpdate(Modelobj: BindDataModel) {
    debugger;
    const StrEditDTO: any = {};
    StrEditDTO.docx = Modelobj.documentx;
    console.log(Modelobj, 'modelobj');
    // const pkeyId = Modelobj.Common_pkeyID.toString(); // work order nmber
    const pkeyId = Modelobj.workOrderNumber.toString();

    const ANYDTO: any = {};

    ANYDTO.workOrderNumber = Modelobj.workOrderNumber;
    ANYDTO.IPLNO = Modelobj.IPLNO;
    ANYDTO.Client_Result_Photo_StatusType =  Modelobj.Client_Result_Photo_StatusType;
    ANYDTO.Client_Result_Photo_Wo_ID = Modelobj.Common_pkeyID.toString(); // work order nmber;
    ANYDTO.Client_Result_Photo_FileName = Modelobj.Client_Result_Photo_FileName;
    ANYDTO.Client_Result_Photo_FilePath = Modelobj.Client_Result_Photo_FilePath;
   
    ANYDTO.Client_Result_Photo_IsActive = 1;
    ANYDTO.Client_Result_Photo_Type = 1;

   if (Modelobj.Client_PageCalled == 1) {
       ANYDTO.Client_PageCalled = 1;
      ANYDTO.Client_Result_Photo_Ch_ID = Modelobj.Client_Result_Photo_Ch_ID;
      ANYDTO.Client_Result_Photo_ID = Modelobj.Inst_Doc_PkeyID;

    }
    else if(Modelobj.Client_PageCalled == 3)
    {
      ANYDTO.Client_PageCalled = 3;
        ANYDTO.Client_Result_Photo_ID = Modelobj.Client_Result_Photo_ID;
        ANYDTO.Client_Result_Photo_ID = Modelobj.Inst_Doc_PkeyID;
        ANYDTO.Client_Result_Photo_Ch_ID = 0;
     
    }
    else if(Modelobj.Client_PageCalled == 4)
    {
        ANYDTO.Client_PageCalled = 4;
        ANYDTO.Client_Result_Photo_ID = Modelobj.Client_Result_Photo_ID;
        ANYDTO.Client_Result_Photo_Ch_ID = Modelobj.Client_Result_Photo_Ch_ID;
        ANYDTO.Client_Result_File_Desc = Modelobj.Client_Result_File_Desc;
      
       
     
    }
    else
    {
      ANYDTO.Client_Result_Photo_Ch_ID = Modelobj.Client_Result_Photo_Ch_ID;
      ANYDTO.Client_Result_Photo_ID = Modelobj.Client_Result_Photo_ID;
      ANYDTO.Client_PageCalled = 2;
    }


    
    ANYDTO.Type = Modelobj.Type;
    ANYDTO.Image = StrEditDTO.docx;
    ANYDTO.ReqType = 1;  // 1 for Desktop 2 for Mobile
    ANYDTO.ContentType  = 2; //  // 1 for Image 2 for Doc

    console.log(Modelobj.IPLNO);
    
   // const uploadapi = 'https://us-central1-rare-lambda-245821.cloudfunctions.net/app/upload';
   const uploadapi = BasetUrl.CloudURL;
    // const uploadapi = 'http://localhost:5000/upload';
    const doc = await this.readDoc(StrEditDTO.docx);
    ANYDTO.Image = doc;
    return this._http.post<any>(uploadapi, ANYDTO).pipe(
      tap(data => {
        console.log('cloudfunctions FireBase Third Party tools', data);
        return data;
      }),
      catchError(this.handleError)
    );
  }

  /////////////////////

   //////////////////////////////////////////////////////////////////// Delete Client Photo
   private apiUrlPOSTConuc =
   BasetUrl.Domain + "api/MultiPhoto/PostDeleteClientResultsPhotos";
 
 public DeleteCLientImagesData(Modelobj: ClientResultPhotoModel) {
   debugger;
   let ANYDTO: any = {};
 
   ANYDTO.Client_Result_Photo_ID = Modelobj.Client_Result_Photo_ID;
   ANYDTO.Type = 4;
  
 
   let headers = new HttpHeaders({ "Content-Type": "application/json" });
   return this._http
     .post<any>(this.apiUrlPOSTConuc, ANYDTO, { headers: headers })
     .pipe(
       tap(data => {
         //console.log(data);
         return data;
       }),
       catchError(this.handleError)
       //catchError( this.Errorcall.handleError)
     );
 }

  ///////////////////////////////////////////////////////////////GettaskbidPhotos
  // get task bid photos
  private apiUrlGettask = BasetUrl.Domain + "api/WOCTASK/GettaskbidPhotosdetails";

  public taskphotoClient(Modelobj: TaskBidPhoto) {
    //debugger;
    var ANYDTO: any = {};

    ANYDTO.Task_Bid_TaskID = Modelobj.Task_Bid_TaskID;
    ANYDTO.Task_Bid_WO_ID = Modelobj.Task_Bid_WO_ID;
    ANYDTO.Task_Bid_Sys_Type = 1;
    

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGettask, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////////////////////////
  private apiUrlGetMaster =
    BasetUrl.Domain + "api/MultiPhoto/GetCLientResultPhotosMaster";

  public ViewCLientImagesDataMaster(Modelobj: ClientResultPhotoModel) {
    //debugger;
    let ANYDTO: any = {};

    ANYDTO.Type = 1; //Modelobj.Type; // becoz complet f
    ANYDTO.Client_Result_Photo_ID = Modelobj.Client_Result_Photo_ID;
    ANYDTO.Client_Result_Photo_Wo_ID = Modelobj.Client_Result_Photo_Wo_ID;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGetMaster, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }



  ////////////////////////////////////////////////////

  //update Client Results Photos
  private apiUrlPOST = BasetUrl.Domain + "api/MultiPhoto/PostUpdateclientphoto";

  public UpdateDataPost(Modelobj: ClientResultPhotoModel) {
    debugger;
    let ANYDTO: any = {};
    ANYDTO.ImageArray = Modelobj.ImageArray;
    ANYDTO.Task_Bid_TaskID = Modelobj.Client_Result_Photo_TaskId;
    ANYDTO.Client_Result_Photo_StatusType =
      Modelobj.Client_Result_Photo_StatusType;
    ANYDTO.Client_Result_Photo_Task_Bid_pkeyID = Modelobj.Client_Result_Photo_Task_Bid_pkeyID;
     ANYDTO.Type = 5;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOST, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

   ////////////////////////////////////////////////////

  //custome photo post single
  private apiUrlPOSTx = BasetUrl.Domain + "api/WOCTASK/postCustomPhotoLabel";

  public CustomPhotoLabelPost(Modelobj: Custom_PhotoLabel) {
    debugger;
    let ANYDTO: any = {};
    ANYDTO.PhotoLabel_pkeyID = Modelobj.PhotoLabel_pkeyID;
    ANYDTO.PhotoLabel_Name = Modelobj.PhotoLabel_Name;
    ANYDTO.PhotoLabel_IsCustom = Modelobj.PhotoLabel_IsCustom;
    ANYDTO.PhotoLabel_IsActive = Modelobj.PhotoLabel_IsActive;
    ANYDTO.PhotoLabel_IsDelete = Modelobj.PhotoLabel_IsDelete;
    ANYDTO.Type = Modelobj.Type ;
    ANYDTO.workOrder_ID = Modelobj.workOrder_ID;
    ANYDTO.PhotoLabel_Valtype = 1;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOSTx, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  //custome photo get re
  private apiUrlGETx = BasetUrl.Domain + "api/WOCTASK/GetCustomPhotoLabel";

  public CustomPhotoLabelGET(model:ClientResultPhotoModel) {
    //debugger;
    let ANYDTO: any = {};
    ANYDTO.PhotoLabel_pkeyID = 0;
    ANYDTO.Type = 4;
    ANYDTO.workOrder_ID = model.Client_Result_Photo_Wo_ID;


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGETx, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }



  //custome photo post ALL Label
  private apiUrlPOSTxp = BasetUrl.Domain + "api/WOCTASK/PostWOCustomPhotoLabel";

  public WOCustomPhotoLabelPost(Modelobj: WorkOrder_CustomPhotoLabel) {
    debugger;
    let ANYDTO: any = {};
    ANYDTO.WorkOrderPhotoLabel_pkeyID = Modelobj.WorkOrderPhotoLabel_pkeyID;
    ANYDTO.WorkOrderPhotoLabel_WO_ID = Modelobj.WorkOrderPhotoLabel_WO_ID;
    ANYDTO.WorkOrderPhotoLabel_IsActive = Modelobj.WorkOrderPhotoLabel_IsActive;
    ANYDTO.WorkOrderPhotoLabel_IsDelete = Modelobj.WorkOrderPhotoLabel_IsDelete;
    ANYDTO.UserID = Modelobj.UserID;
    ANYDTO.Type = Modelobj.Type;
    ANYDTO.Custom_PhotoLabel_MasterDTO = Modelobj.Custom_PhotoLabel_MasterDTO;

       console.log('demolable', Modelobj.Custom_PhotoLabel_MasterDTO);


    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlPOSTxp, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        // catchError(this.handleError)
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }


  // common handler
  private handleError(error: HttpErrorResponse) {
    if(error.status == 401){
      alert('Unauthorized User...');
      window.location.href = '/admin/login';
    }else{
      alert("Invalid Request...");
    }
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable wi bad happenedth a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }

  public downloadImages(req: any) {
    debugger
    const downloadAPI = 'https://us-central1-rare-lambda-245821.cloudfunctions.net/app/downloadZip';
    // const downloadAPI = 'http://localhost:5000/downloadZip';
    return this._http.post(downloadAPI, req, {
      responseType: 'arraybuffer'
    }).pipe(tap(data => {
      return data;
    }), catchError(this.handleError))
  }

  public downloadImagesWithTimeStamp(req: any) {
    const downloadAPI = 'https://us-central1-rare-lambda-245821.cloudfunctions.net/app/downloadZipTimeStamp';
    // const downloadAPI = 'http://localhost:5000/downloadZipTimeStamp';
    return this._http.post(downloadAPI, req, {
      responseType: 'arraybuffer'
    }).pipe(tap(data => {
      return data;
    }), catchError(this.handleError))
  }

  public downloadSingleImage(req: any) {
    const downloadAPI = 'https://us-central1-rare-lambda-245821.cloudfunctions.net/app/downloadSingle';
    // const downloadAPI = 'http://localhost:5000/downloadSingle';
    return this._http.post(downloadAPI, req, {
      responseType: 'arraybuffer'
    }).pipe(tap(data => {
      return data;
    }), catchError(this.handleError))
  }
}