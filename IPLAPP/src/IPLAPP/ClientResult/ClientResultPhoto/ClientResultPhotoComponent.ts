import { Component, Injectable, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FileInfo } from "@progress/kendo-angular-upload";
import { BasetUrl } from "../../Utility/DomainUrl";
import { FileUploadmodel } from "./ClientResultPhotoModel";

import { ClientResultPhotoModel, TaskBidPhoto,Custom_PhotoLabel,WorkOrder_CustomPhotoLabel } from "./ClientResultPhotoModel";
import { ClientResultOldPhotoServices } from "./ClientResultphotosOldServices";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MasterlayoutComponent } from "../../Home/MasterComponent";

import { BindDataModel, TaskBidMasterModel } from "../ClientResults/ClientResultModel";
import * as $ from "jquery";
import { ClientResultServices } from '../ClientResults/ClientResultServices';

import { EncrDecrServiceService } from "../../../app/encr-decr-service.service";

@Component({
  templateUrl: "./ClientResultPhoto.html"
})
export class ClientResultPhotoComponent implements OnInit {
  ClientResultPhotoModelObj: ClientResultPhotoModel = new ClientResultPhotoModel();

  BindDataModelObj: BindDataModel = new BindDataModel();
  TaskBidPhotoobj: TaskBidPhoto = new TaskBidPhoto();

  Custom_PhotoLabelObj:Custom_PhotoLabel = new Custom_PhotoLabel();
  WorkOrder_CustomPhotoLabelObj:WorkOrder_CustomPhotoLabel = new WorkOrder_CustomPhotoLabel();
  panelNames = [];
  metadata: any;

  public contentPhotox; // for common msg argument pass sathi
  myFiles: string[] = [];
  sMsg: string = "";
  public contentx; // for common msg argument pass sathi
  MessageFlag: string; // custom msg sathi

  private apiUrlGet =
    BasetUrl.Domain + "api/RESTIPLUPLOAD/PostUserDocumentUserImageBackground"; ///api/MultiPhoto/Save

  uploadSaveUrl = ""; // should represent an actual API endpoint
  uploadRemoveUrl = "removeUrl"; // should represent an actual API endpoint


  DownloadBaseDomain:String ="";
  decuser:any;

  OfficeResulth:boolean = false;
  processorh:boolean = false;
  tabhide:boolean = false;
  fieldtab:boolean = false;

  constructor(
    private xRouter: Router,

    private xClientResultOldPhotoServices: ClientResultOldPhotoServices,
    private modalService: NgbModal,
    private xMasterlayoutComponent: MasterlayoutComponent,
    private xRoute: ActivatedRoute,
    private xClientResultServices: ClientResultServices,
    private EncrDecr: EncrDecrServiceService

  ) {
    {
      if(localStorage.getItem('usertemp_') != null)
      {
        var encuser = JSON.parse(localStorage.getItem('usertemp_'));
        var decval  = this.EncrDecr.get('123456$#@$^@1ERF', (encuser));
        this.decuser  =JSON.parse(decval) ;
        console.log('mohitdec',this.decuser);
        // if (this.decuser[0].GroupRoleId === 1) {
        //   this.hidetab = true;
          
        // }
        switch (this.decuser[0].GroupRoleId) {
          case 1:
            {
              this.OfficeResulth = false;
              this.processorh= false;
              this.tabhide = false;
           
              break;
            }
            case 2:
              {
                this.OfficeResulth = false;
                this.processorh= false;
                this.tabhide = true;
                this.fieldtab = false;
               
                break;
              }
              case 3:
                {
                  this.OfficeResulth = false;
                   this.processorh= false;
                   this.tabhide = false;
                
                  break;
                }
                case 4:
                  {
                    this.OfficeResulth = false;
                    this.processorh= true;
                    this.tabhide = false;
                    this.fieldtab = true;
                  
                    break;
                  }
          }
  
      }
    }

    this.uploadSaveUrl = this.apiUrlGet;
    this.DownloadBaseDomain = BasetUrl.Domain;
    this.CustomeLablepopformArray = [{ PhotoLabel_pkeyID: 0,PhotoLabel_Name: "",PhotoLabel_IsCustom:0,PhotoLabel_IsActive:true,PhotoLabel_IsDelete:false }];
    this.getModelData();

  }
  ngOnInit() {
    console.log('check entering');
    window.addEventListener('keydown', (e) => {
      console.log(e.key, "KeyPress");
      if (e.key === "Shift") {
        this.isShift = true;
      }
      console.log('kb_down', e.key);
      if (e.key === 'Control') {
        this.isCtrl = true;
      }
    })

    window.addEventListener('keyup', (e) => {
      console.log(e.key, "KeyUp");
      if (e.key === "Shift") {
        this.isShift = false;
      }
      console.log('kb_up' ,e.key);
      if (e.key === "Control") {
        this.isCtrl = false;
      }
    })
  }

  ModelObj: any;
  BindData: any;

  button = "Save"; // buttom loading..
  isLoading = false; // buttom loading..

  buttonSave = "Save";
  isLoadingSave = false;
  isShift = false;
  isCtrl = false;
  timeStampData = {
    date: '',
    printTime: false,
    modifyTime: '',
    isAM: true,
    lengthHours: 0,
    lengthMins: 0,
    printDateTimeExif: false,
    printDescription: false,
    stopPrinting: false,
  }

  buttonDelete = "Delete";
  isLoadingDelete  = false;
  isShown: boolean = true;
  sketchPad = null;
  divclass = "col-12";
   //Iscollep: boolean = true;
  hideme=[];
  TaskBidMasterModelObj: TaskBidMasterModel = new TaskBidMasterModel();

  headwodata: any;
  getModelData() {
    debugger;
    const workorder1 = this.xRoute.snapshot.params['workorder'];
    let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    console.log('workOrderID', workOrderID);
    const workorder = parseInt(workOrderID);
    this.TaskBidMasterModelObj.workOrder_ID = workorder;
    this.xClientResultServices
    .WorkorderViewClient(this.TaskBidMasterModelObj)
    .subscribe(response => {
      console.log("response clinet but work order se click", response);
       this.headwodata = response[0];
      this.BindData = response[0][0];
      this.xMasterlayoutComponent.masterFunctionCall(this.BindData);

      this.ModelObj = this.xMasterlayoutComponent.masterFunctionGetdata();
      if (this.ModelObj == undefined) {
        this.xRouter.navigate(["/workorder/view"]);
      } else {
        //this.DivHIDEx = true;
        console.log("work order obj", this.ModelObj);

      this.BindDataModelObj.workOrderNumber = this.ModelObj.workOrderNumber;

      this.BindDataModelObj.IPLNO = this.ModelObj.IPLNO;

        this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID = this.ModelObj.workOrder_ID;
        this.ClientResultPhotoModelObj.IPLNO = this.ModelObj.IPLNO;
        this.BindData = this.ModelObj;
        //this.GetClientImages();
        this.getTaskData();
      }
    });
  }

  InvoiceCall() {
    //debugger;
    this.xRouter.navigate(["/client/clientresultinvoice"]);
  }
  ClentResultCall() {
    this.xRouter.navigate(["/client/clientresult"]);
  }
  ClentResultField() {
    this.xRouter.navigate(["/client/clientresultfield"]);
  }
  ClentResultinsCall() {
    this.xRouter.navigate(["/client/clientresultinstruction"]);
  }

  public displayError(e: ErrorEvent) {
    console.log(e);
    console.log("An error occurred");
  }
  public displaySuccess(e) {
    console.log("success");
    console.log("success", e);
    console.log("success File", e.files[0].rawFile);

    if (e.operation == "upload") {
      this.processImage(e.files[0].rawFile);
    } else {
      alert("remove img called");
    }
  }

  public displayErrorDocument(e: ErrorEvent) {
    console.log(e);
    console.log("An error occurred");
  }
  public displaySuccessDocument(e) {
    console.log("success");
    console.log("success", e);
    console.log("success File", e.files[0].rawFile);

    if (e.operation == "upload") {
      this.processDocument(e.files[0].rawFile);
    } else {
      alert("remove img called");
    }
  }

  imgpath: string; //img sath
  TempResponse:any;
  // processImage(imageInput) {
  //   //debugger;

  //   if (true) {
  //     this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;
  //     this.BindDataModelObj.documentx = imageInput;
  //     this.BindDataModelObj.Type = 1;
  //     this.xClientResultOldPhotoServices
  //       .CommonPhotosUpdate(this.BindDataModelObj)
  //       .subscribe(response => {
  //         console.log("respo img doc cha", response);
  //         this.TempResponse = response;
  //         //debugger;
  //         //this.GetClientImages();
  //         // var rspon = response.Message;
  //         // var arr = [];
  //         // arr = rspon.split("#");
  //         // var msg = arr[0];
  //         // console.log("document upload msg", msg);

  //         // this.MessageFlag = msg;
  //         // this.commonMessage(this.contentx);
  //       });
  //   }
  // }
  processImage(imageInput) {
    debugger;

    if (true) {
      this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;
      this.BindDataModelObj.documentx = imageInput;
      this.BindDataModelObj.Client_Result_Photo_FileName = imageInput.name;
      this.BindDataModelObj.Type = 1;
      this.xClientResultOldPhotoServices
        .CommonPhotosUpdate(this.BindDataModelObj)
        .then((res) => {
          res.subscribe(response => {
            console.log("respo img doc cha", response);
            this.GetClientImages(); // to get the images so please dont commets
            // debugger;
            // this.GetClientImages();
            // var rspon = response.Message;
            // var arr = [];
            // arr = rspon.split("#");
            // var msg = arr[0];
            // console.log("document upload msg", msg);

            // this.MessageFlag = msg;
            // this.commonMessage(this.contentx);
          });
        })
    }
  }

  processDocument(documentInput) {
    debugger;
    if (true) {
      this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;
      this.BindDataModelObj.documentx = documentInput;
      this.BindDataModelObj.Client_Result_Photo_FileName = documentInput.name;
      this.BindDataModelObj.Client_PageCalled = 3;
      this.BindDataModelObj.Type = 1;
      this.BindDataModelObj.workOrderNumber = '0';
      this.BindDataModelObj.IPLNO = this.ModelObj.IPLNO;
      this.xClientResultOldPhotoServices
        .CommonDocumentsUpdate(this.BindDataModelObj)
        .then((res) => {
          res.subscribe(() => {});
        });
    }
  }

  // get Images Data Clean Images
  GetPhotos: any;
  TrashPhotos = [];
  unflag: any;
  taskname: any;
  BasetUrlval: any;
  documentdlst: any;
  deleteimg: any;
  GetClientImages() {
    debugger;
    this.xClientResultOldPhotoServices
      .ViewCLientImagesDataMaster(this.ClientResultPhotoModelObj)
      .subscribe(response => {
        console.log("ClientPhotos Clean M", response);
        this.GetPhotos = response[0];
        this.taskname = response[2];
        this.unflag = response[1];
        this.documentdlst = response[3];
        this.deleteimg = response[4];

        //this.BasetUrlval = BasetUrl.Domain;
        for (let i = 0 ; i < this.taskname ; i ++) {

        }

        this.GetCustomPhotoLabel();
      });

    //this.GetPhotos = this.xMasterlayoutComponent.MasterFunctionClientResultAsycPhotosGet();
    // console.log("ClientPhotos c to p ", this.GetPhotos);
    // this.BasetUrlval = BasetUrl.Domain;
  }







  popupImage: any;
  imgpIndex: any;
  imgNextpath: any;
  PhotoModel(messagecontent, itemval, index) {
    debugger;
    this.Count = 0;
    this.NextBtnHide = false;
    this.PrevBtnHide = false;
    this.metadata = itemval;
    console.log('latitude',this.metadata);
    this.contentPhotox = messagecontent;
    // this.popupImage = BasetUrl.Domain + itemval.Client_Result_Photo_FilePath;
    this.popupImage = itemval.Client_Result_Photo_FilePath;
    this.commonPhoto(this.contentPhotox, itemval);
  }
  CopyLink() {
       const el = document.createElement('textarea');
       el.value = this.popupImage;
       document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);
     }
  // common message modal popup

  curImage;
  isDragging = false;
  imageReady;
  startX = 0;
  startY = 0;
  endX = 0;
  endY = 0;
  release = false;
  drawText="";
  firstload = false;

  commonPhoto(content, itemval) {
    this.modalService
      .open(content, { size: "lg", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          this.Count = 0;
          //alert('closse');
        },
        reason => {
          this.Count = 0;
          //alert('closse');
        }
      );
      this.curImage = itemval;
      this.imageArray = [];
      const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
      const imgDrawn = new Image(640, 480);
      imgDrawn.crossOrigin = "*";
      imgDrawn.src = "https://cors-anywhere.herokuapp.com/" + itemval.Client_Result_Photo_FilePath;
      imgDrawn.onload = () => {
        this.imageReady = imgDrawn;
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, 640, 480);
        ctx.drawImage(this.imageReady, 0, 0);
        // console.log("1");
      }
      canvas.addEventListener('mousedown', (e) => {
        this.isDragging = true;
        this.startX = e.offsetX;
        this.startY = e.offsetY;
        this.release = false;
        const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
        const ctx = canvas.getContext('2d');
        ctx.filter = "brightness(100%)";
        if (this.editType === 5) {
          this.startDragOffset.x = e.offsetX - this.translatePos.x;
          this.startDragOffset.y = e.offsetY - this.translatePos.y;
        
        }
        if (this.editType === 1 || this.editType === 2 || this.editType === 3) {
          this.imageArray.push(this.imageReady.src);
        }
      }, false);

      canvas.addEventListener('mouseup', (e) => {
        this.isDragging = false;
        this.release = true;
        if (this.editType === 1 || this.editType === 2 || this.editType === 3) {
          const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
          this.imageReady.src = canvas.toDataURL();
        }
      }, false);

      canvas.addEventListener('mouseleave', () => {
        this.isDragging = false;
      })

      canvas.addEventListener('mousemove', (e) => {
        if (this.isDragging === true) {
          this.endX = e.offsetX;
          this.endY = e.offsetY;
          if (this.editType === 5) {
            console.log(this.startDragOffset);
            this.translatePos.x = e.offsetX - this.startDragOffset.x;
            this.translatePos.y = e.offsetY - this.startDragOffset.y;
          }
          this.drawImage();
        }
      }, false);

      canvas.addEventListener('wheel', (e) => {
        e.stopPropagation();
        if (this.editType === 5) {
          if (e.deltaY < 0) this.scale /= this.scaleMultiplier;
          else this.scale *= this.scaleMultiplier;
          this.drawImage();
        }
      }, false);
  }

  scale = 1.0;
  scaleMultiplier = 0.8;
  startDragOffset = {
    x: 0,
    y: 0
  };
  translatePos = {
    x: 320,
    y: 240
  };

  imageArray = [];

  drawImage() {
    const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
    const ctx = canvas.getContext('2d');
    ctx.drawImage(this.imageReady, 0, 0, 640, 480);
    if (this.isDragging) {
      if (this.editType === 0) {
        ctx.save();
        ctx.fillStyle="#000000bb";
        ctx.fillRect(this.startX, this.startY, Math.abs(this.endX - this.startX), Math.abs(this.endY - this.startY));
        ctx.restore();
      }
      if (this.editType === 1) {
        ctx.save();
        ctx.strokeStyle="#ff0000";
        ctx.beginPath();
        const w = Math.abs(this.endX - this.startX)/2;
        ctx.arc(this.startX + w, this.startY + w,w, 0, Math.PI*2);
        ctx.stroke();
        ctx.restore();
      }
      if (this.editType === 2) {
        ctx.save();
        ctx.strokeStyle="#ff0000";
        ctx.strokeRect(this.startX, this.startY, Math.abs(this.endX - this.startX), Math.abs(this.startY - this.endY));
        ctx.restore();
      }
      if (this.editType === 3) {
        ctx.save();
        ctx.beginPath();
        ctx.fillStyle="#ff0000";
        ctx.font=`${Math.abs(this.startY-this.endY)}px Impact`;
        ctx.fillText(this.drawText, this.startX, this.endY);
        ctx.closePath();
        ctx.restore();
      }
      if (this.editType === 6) {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle="#ff0000";
        ctx.moveTo(this.startX, this.startY);
        ctx.lineTo(this.endX, this.endY);
        ctx.stroke();
        ctx.restore();
      }
    }
    if (this.editType === 5) {
      ctx.clearRect(0, 0, 640, 480);
      ctx.save();
      ctx.translate(this.translatePos.x, this.translatePos.y);
      ctx.scale(this.scale, this.scale);
      ctx.translate(-this.translatePos.x, -this.translatePos.y);
      ctx.drawImage(this.imageReady, 0, 0, 640, 480);
      ctx.restore();
    }
  }

  percentage = 0;
  length = 0;
  measuring = false;

  init() {
    this.percentage = 0;
    this.length = 0;
    this.measuring = false;
    this.scale = 1.0;
    this.scaleMultiplier = 0.8;
    this.startDragOffset = {
      x: 0,
      y: 0
    };
    this.translatePos = {
      x: 320,
      y: 240
    };
    this.isDragging = false;
    this.startX = 0;
    this.startY = 0;
    this.endX = 0;
    this.endY = 0;
    this.release = false;
    this.drawText="";
  }
  savePixels() {
    if (this.startX !== this.endX && this.startY !== this.endY && this.length !== 0) {
      this.percentage = Math.sqrt(Math.pow(this.startX - this.endX, 2) + Math.pow(this.startY - this.endY, 2)) / this.length;
    }
  }

  calcSquare() {
    const calc = Math.sqrt(Math.pow(this.startX - this.endX, 2) + Math.pow(this.startY - this.endY, 2)) / this.percentage;
    alert(`Length is ${calc}.`);
  }

  showMeasure(isMeasuring) {
    this.measuring = isMeasuring;
    if (this.measuring) this.editType = 6;
    else this.editType = 0;
  }

  editType = 0;
  brightness = 100;
  editImage(type) {
    const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
    const ctx = canvas.getContext('2d');
    switch(type) {
      case 'crop':
        this.editType === 0 ? this.editType = 4 : this.editType = 0;
        if (this.editType === 4) {
          this.imageArray.push(this.imageReady.src);
          ctx.drawImage(this.imageReady, this.startX, this.startY, Math.abs(this.startX - this.endX), Math.abs(this.startY - this.endY), 0, 0, 640, 480);
          this.imageReady.src = canvas.toDataURL();
        }
        break;
      case 'lighter':
        this.imageArray.push(this.imageReady.src);
        ctx.filter = `brightness(105%)`;
        this.imageReady.src = canvas.toDataURL();
        break;
      case 'darker':
        this.imageArray.push(this.imageReady.src);
        ctx.filter = `brightness(95%)`;
        this.imageReady.src = canvas.toDataURL();
        break;
      case 'rotatel':
        this.imageArray.push(this.imageReady.src);
        ctx.clearRect(0, 0, 640, 480);
        ctx.save();
        ctx.translate(320, 240);
        ctx.rotate((Math.PI / 180) * 90);
        ctx.translate(-320, -240);
        ctx.drawImage(this.imageReady, 0, 0);
        ctx.restore();
        this.imageReady.src = canvas.toDataURL();
        this.drawImage();
        break;
      case 'rotater':
        this.imageArray.push(this.imageReady.src);
        ctx.clearRect(0, 0, 640, 480);
        ctx.save();
        ctx.translate(320, 240);
        ctx.rotate((Math.PI / 180) * -90);
        ctx.translate(-320, -240);
        ctx.drawImage(this.imageReady, 0, 0);
        ctx.restore();
        this.imageReady.src = canvas.toDataURL();
        this.drawImage();
        break;
      case 'circle':
        this.editType = 1;
        break;
      case 'square':
        this.editType = 2;
        break;
      case 'text':
        this.editType = 3;
        break;
      case 'zoom':
        this.editType = 5;
        break;
    }
    this.drawImage();
  }

  Undo() {
    const length = this.imageArray.length;
    console.log(length);
    
    if (length > 0) {
      this.imageReady.src = this.imageArray.pop();
      const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
      const ctx = canvas.getContext('2d');
      ctx.drawImage(this.imageReady, 0, 0, 640, 480);
    }
  }

  date = new Date();
  codeGenerated = '';
  evtMsg: any;
  randomString() 
  {
    debugger;
 const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
 const stringLength = 10;
 let randomstring = '';
 for (let i = 0; i < stringLength; i++) {
 const rnum = Math.floor(Math.random() * chars.length);
 randomstring += chars.substring(rnum, rnum + 1);
}
this.codeGenerated = randomstring;
 return 0;
  }





  SaveChange(content) {
    debugger;
    this.contentx = content;
    const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
    const img = canvas.toDataURL();
    this.BindDataModelObj.Common_pkeyID = this.ModelObj.workOrder_ID;
//    this.BindDataModelObj.Client_Result_Photo_FileName = this.curImage.Client_Result_Photo_FileName.slice(-3)+'_edited.jpg';
   this.randomString();
    console.log('code',this.codeGenerated );
    this.BindDataModelObj.Client_Result_Photo_FileName = this.curImage.Client_Result_Photo_FileName+'_'+ this.codeGenerated +'_edited.jpg';

    console.log("Filename",this.curImage.Client_Result_Photo_FileName);
    console.log("newFilename",this.BindDataModelObj.Client_Result_Photo_FileName);
    this.BindDataModelObj.Type = 1;
    this.xClientResultOldPhotoServices
    .SinglePhotoUpdate(this.BindDataModelObj, img)
    .then((res) => {
      res.subscribe(response => {
        this.MessageFlag = "Photo changes Saved...";
       
     
        this.commonMessage(this.contentx);
        console.log("respo img doc cha", response);
        this.GetClientImages(); // to get the images so please dont commets
      })
    });
  }

  // next img
  Count = 0;
  NextBtnHide = false;
  PrevBtnHide = false;
  nextImag(index, position) {
    //this.Count = 0;
    console.log(index, "index");
    if (position == 1) {
      //next
      this.Count = this.Count + 0;

      this.imgpIndex = 0;
      this.Count++;



      this.PrevBtnHide = false;
    } else {
      this.Count = this.Count + index;
      this.imgpIndex = 0;
      this.Count--;
      this.NextBtnHide = false;
    }

    if (this.GetPhotos.length > this.Count) {
      if (this.Count == -1) {
        this.PrevBtnHide = true;
      } else {
        //===================================
        this.popupImage =
          // BasetUrl.Domain +
          this.GetPhotos[this.Count].Client_Result_Photo_FilePath;
          this.metadata.Client_Result_Photo_GPSLatitude = this.GetPhotos[this.Count].Client_Result_Photo_GPSLatitude;
          this.metadata.Client_Result_Photo_GPSLongitude = this.GetPhotos[this.Count].Client_Result_Photo_GPSLongitude;
          this.metadata.Client_Result_Photo_UploadTimestamp = this.GetPhotos[this.Count].Client_Result_Photo_UploadTimestamp;
          
         
          console.log('l',  this.metadata.Client_Result_Photo_GPSLatitude);
          console.log('lo', this.metadata.Client_Result_Photo_GPSLongitude);
          console.log('d',  this.metadata.Client_Result_Photo_UploadTimestamp);
        // this.curImage = this.popupImage;
        const canvas = <HTMLCanvasElement>document.getElementById('image-editor');
        const imgDrawn = new Image(640, 480);
        this.imageArray = [];
        this.init();
        imgDrawn.crossOrigin = "*";
        imgDrawn.src = "https://cors-anywhere.herokuapp.com/" + this.popupImage;
        imgDrawn.onload = () => {
          this.imageReady = imgDrawn;
          this.imageReady.crossOrigin = "*";
          const ctx = canvas.getContext('2d');
          ctx.clearRect(0, 0, 640, 480);
          ctx.drawImage(this.imageReady, 0, 0);
        }
      }
    } else {
      this.NextBtnHide = true;
    }
  }

  PrevImag() {}

  /// end common model
  /// end common model
  getFileDetails(e) {
    //console.log (e.target.files);
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);
    }
  }

  bid(bidcontent) {
    this.contentx = bidcontent;
    this.commonBid(this.contentx);
  }
  // bid message modal popup
  commonBid(bidcontent) {
    this.modalService
      .open(bidcontent, { windowClass: "xlModal" })
      .result.then(result => {
       this.getTaskData();
      
      }, reason => {
      this.getTaskData();
      });
     
  }
  /// end common model
  Completion(invcontent) {
    this.contentx = invcontent;
    this.commoninv(this.contentx);
  }
  commoninv(invcontent) {
    this.modalService
      .open(invcontent, { windowClass: "xlModal" })
      .result.then(result => {
        this.getTaskData();
        
      }, reason => {
        this.getTaskData();
      });
  }
  Damage(damagecontent) {
    this.contentx = damagecontent;
    this.commondamage(this.contentx);
  }
  commondamage(damagecontent) {
    this.modalService
      .open(damagecontent, { windowClass: "xlModal" })
      .result.then(result => {
        this.getTaskData();
       
      }, reason => {
         this.getTaskData();
      });
  }

  // upload photos popup
  OpenPhotoUpload(contentpop) {
    this.commonPOPUPPhoto(contentpop);
  }

  OpenTimeStamp(contentpop) {
    this.commonPOPUPPhoto(contentpop);
  }

  commonPOPUPPhoto(content) {
    this.modalService
      .open(content, { size: "lg", ariaLabelledBy: "modal-basic-title" })
      .result.then(
        result => {
          this.GetClientImages();
        },
        reason => {
          // then call get
          this.GetClientImages();
        }
      );
  }

  OpenDocumentUpload(contentpop) {
    this.commonPOPUPDocument(contentpop);
  }

  commonPOPUPDocument(content) {
    this.modalService
      .open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        result => {
          this.GetClientImages();
        },
        reason => {
          // then call get
          this.GetClientImages();
        }
      );
  }

  /// end  up

  EditWorkOrder() {
    debugger;
    this.xMasterlayoutComponent.masterFunctionCall(this.BindData);
    this.xRouter.navigate(["/workorder/new"]);
    // const workorder1 = this.xRoute.snapshot.params['workorder'];
    // let workOrderID = this.EncrDecr.get('123456$#@$^@1ERF', atob(workorder1));
    // console.log('woid',workOrderID);
    // var encrypted = this.EncrDecr.set('123456$#@$^@1ERF',workOrderID);
    // console.log('woid',encrypted);
    // this.xRouter.navigate(["/workorder/createworkorder/new", btoa(encrypted)]);
  }

  download() {
    debugger;
    //alert("work in progress");
    console.log(this.dataSend);
    if (this.dataSend.length === 0) {
      this.DownloadPhotosAll();
    } else {
      this.DownloadPhotos();
    }
    // this.DownloadPhotosAll();


  }
  GoToBack() {
    this.xRouter.navigate(["/workorder/view"]);
  }

  GetTaskPhotos: any;
  bidItems = [];
  completionItems = [];
  damageItems = [];
  customItems = [];
  getTaskData() {
    debugger;
    this.TaskBidPhotoobj.Task_Bid_WO_ID = this.ModelObj.workOrder_ID;
    this.xClientResultOldPhotoServices
      .taskphotoClient(this.TaskBidPhotoobj)
      .subscribe(response => {
        debugger;
        console.log("TaskClientPhotos LEFT SITE NAME TASK", response);
        this.GetTaskPhotos = response[1];
        this.BasetUrlval = BasetUrl.Domain;
        console.log(this.GetTaskPhotos);

        for (let i = 0 ; i < this.GetTaskPhotos.length ; i ++) {
          const name = this.GetTaskPhotos[i].ButtonName1;
          console.log("but", name);
          if (name === "Bid") {
            this.bidItems.push(this.GetTaskPhotos[i]);
          } else if (name === "Before") {
            this.completionItems.push(this.GetTaskPhotos[i]);
          } else if (name === "Damage") {
            this.damageItems.push(this.GetTaskPhotos[i]);
          } else if (name === "Label") {
            this.customItems.push(this.GetTaskPhotos[i]);
          }
        }


        this.GetClientImages();


      });
  }

  //bid task hide n show

  bidTaskDiv:boolean = false;
  BidVal:string = "";
  BidShowNHide(arg:any){
    //alert('called');
    debugger;
    this.BidVal = arg;
    this.bidTaskDiv = !this.bidTaskDiv;

    this.TaskBidLeftSiteCount();

  }

   // get count img for task bid sathi

   ImgCountx:Number = 0;
  
   TaskBidLeftSiteCount(){
    for (let i = 0; i < this.taskname.length; i++) {
      const buttonyui = this.taskname[i].Items;
      const TaskNameReal = this.taskname[i].Task_Photo_Label_Name;
      for (let k = 0; k < this.GetTaskPhotos.length; k++) {
        const elementTaskNameLeft = this.GetTaskPhotos[k].Task_Photo_Label_Name;
        if(TaskNameReal == elementTaskNameLeft){
          this.ImgCountx = 0;
          for (let j = 0; j < buttonyui.length; j++) {
            this.ImgCountx = buttonyui[j].Items.length;
            let obj = {keycount: this.ImgCountx}
            this.GetTaskPhotos.push(obj);
           
          }
          // this.GetTaskPhotos[k].ImgCount = this.ImgCountx;
        }
        break;
      }
    }
   
  }














  div2: any;
  openNav() {
debugger;
    // document.getElementById("mySidenav").style.width = "250px";
    this.isShown = ! this.isShown;
    if( this.isShown== true){
      this.divclass = "col-12";

    }
    else{

      this.divclass = "col-9";
      this.div2 = "col-3";

    }


  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  chkflag = false;
  name: any;
  dataSend = [];
  chkimgAllPhotos(val, i) {
    console.log('valch',val)
    debugger
    if (val.chkflag == true) {
      if (this.isCtrl !== true) {
        let first = 0, last = 0;
  
        if (this.isShift === false) {
          for (let i = 0; i < this.GetPhotos.length; i++) {
            if (this.GetPhotos[i].Client_Result_Photo_ID !== val.Client_Result_Photo_ID) {
              this.GetPhotos[i].chkflag = false;
              
            
            } else {
              first = i;
              last = i;
            }
          }
        } else {
        
          for (let i = 0; i < this.GetPhotos.length; i++) {
            if (this.GetPhotos[i].chkflag) {
              first = i;
              break;
            }
          }
  
        for (let i = this.GetPhotos.length - 1 ; i >= 0 ; i --) {
          if (this.GetPhotos[i].chkflag) {
            last = i;
            break;
          }
        }
  
        }
  
        this.dataSend = [];
        for (let i = first; i <= last; i++) {
  
         
            this.GetPhotos[i].chkflag = true;
            this.dataSend.push({
              Client_Result_Photo_FileName: this.GetPhotos[i].Client_Result_Photo_FileName,
              Client_Result_Photo_ID: this.GetPhotos[i].Client_Result_Photo_ID
            });
            console.log('chkdataitem',this.dataSend)
         
           
               
           
        
        }
      } else {
        for (let i = 0; i < this.GetPhotos.length; i++) {
          if (this.GetPhotos[i].Client_Result_Photo_ID === val.Client_Result_Photo_ID) {
            this.dataSend.push({
              Client_Result_Photo_FileName: this.GetPhotos[i].Client_Result_Photo_FileName,
              Client_Result_Photo_ID: this.GetPhotos[i].Client_Result_Photo_ID
            });
          }
        }
        console.log("sendData: ", this.dataSend);
      }
    }
    else{
      this.dataSend.splice(i, 1)
      console.log('mohits', this.dataSend)

    }
  

  }

  AddTaskPhotoSelect1(item, msgConent,btnName){

console.log("checkbit: ", item);
    debugger;
    this.addbef(item, msgConent,btnName);
  }
  AddTaskPhotoSelect2(item, msgConent,btnName){

    this.addbef(item, msgConent,btnName);
  }
  AddTaskPhotoSelect3(item, msgConent,btnName){

    this.addbef(item, msgConent,btnName);
  }
  val: Number;Client_Result_Photo_FileName
  addbef(arg, content,btnName) {
    debugger;
    if (btnName == "Bid") {
      this.val = 4;
    }
    if (btnName == "Before") {
      this.val = 1;
    }
    if (btnName == "During") {
      this.val = 2;
    }
    if (btnName == "After") {
      this.val = 3;
    }
    if (btnName == "Damage") {
      this.val = 5;
    }
    if (btnName == "Label") {
      this.val = 6;
    }
    if (btnName == "Inspection") {
      this.val = 7;
    }

    debugger
    if(this.dataSend.length != 0){

      this.ClientResultPhotoModelObj.ImageArray = this.dataSend;
      this.ClientResultPhotoModelObj.Client_Result_Photo_TaskId =
        arg.Task_Bid_TaskID;
      this.ClientResultPhotoModelObj.Client_Result_Photo_Task_Bid_pkeyID =
        arg.Task_Bid_pkeyID;
      this.ClientResultPhotoModelObj.Client_Result_Photo_StatusType = this.val;
      this.ClientResultPhotoModelObj.Type = 5;

      this.xClientResultOldPhotoServices
        .UpdateDataPost(this.ClientResultPhotoModelObj)
        .subscribe(response => {
          console.log("resp data task left pop update photos", response);

          this.MessageFlag = "Successfully Added...!";
          this.commonMessage(content);

          this.getTaskData();
          this.dataSend = [];
          this.ClientResultPhotoModelObj.ImageArray = [];


          // gettting refresh images
          this.GetClientImages();



        });
       
    }
    else
    {
      alert('Please select photo(s) first..!');
    }

  }

  // common message modal popup
  commonMessage(content) {
    this.modalService
      .open(content, { size: "sm", ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }
  /// end common model

  AllPhotosToggleShow = false;
  AllPhotosToggle() {
    this.AllPhotosToggleShow = !this.AllPhotosToggleShow;
  }
  

  TrashPhotosToggleShow = false;
  TrashPhotosToggle() {
    this.TrashPhotosToggleShow = !this.TrashPhotosToggleShow;
  }

  UnflaggedShow = false;
  UnflaggedToggle() {
    this.UnflaggedShow = !this.UnflaggedShow;
  }


  TaskViseShow = false;
  TaskViseTogle(){


    this.TaskViseShow = !this.TaskViseShow;
  }

  Documentshow = false;
  DocumentViseTogle(){


    this.Documentshow = !this.Documentshow;
  }
  Delshow = false;
  DeleteViseTogle(){
    this.Delshow = !this.Delshow;
  }



  DownloadImgSend = [];
  // download call all flag images
  DownloadPhotosAll(){

    this.DownloadImgSend = [];
    const labelArray = [];

    for (let i = 0; i < this.GetPhotos.length; i++) {

      const imgpath = this.GetPhotos[i].Client_Result_Photo_FileName;
      let label = '';
      for (let j = 0 ; j < this.taskname.length ; j ++) {
        if (this.taskname[j].Items[0].Items[0].Client_Result_Photo_FileName === imgpath) {
          label = this.taskname[j].Items[0].Task_Photo_Button_Name + '_';
        }
      }
      labelArray.push(label);

      this.DownloadImgSend.push(imgpath);
    }

    // console.log('img push all ',this.DownloadImgSend);

    // window.open(BasetUrl.Domain+"/DownloadFile/DownloadZipFile?ImgPath="+ this.DownloadImgSend, "_blank");

    if (this.timeStampData.stopPrinting) {
      const req = {
        FolderName: this.ModelObj.IPLNO,
        Files: this.DownloadImgSend,
        Labels: labelArray
      };
      this.xClientResultOldPhotoServices.downloadImages(req).subscribe(res => {
        if (window.navigator && window.navigator.msSaveBlob) {
          // Internet Explorer workaround
          const blob = new Blob([res], {
            type: 'arraybuffer'
          });
  
          window.navigator.msSaveBlob(blob, this.ModelObj.IPLNO + '-ipl.zip');
        } else {
          const binaryData = [];
          binaryData.push(res);
          const downloadLink = window.URL.createObjectURL(
            new Blob([res], {
              type: 'arraybuffer'
            })
          );
          const element = document.createElement('a');
          element.href = downloadLink;
          element.download = this.ModelObj.IPLNO + '-ipl.zip';
          document.body.appendChild(element);
          element.click();
        }
      });
    } else {
      const req = {
        FolderName: this.ModelObj.IPLNO,
        Files: this.DownloadImgSend,
        Labels: labelArray,
        TimeStamps: this.timeStampData
      };
      this.xClientResultOldPhotoServices.downloadImagesWithTimeStamp(req).subscribe(res => {
        if (window.navigator && window.navigator.msSaveBlob) {
          // Internet Explorer workaround
          const blob = new Blob([res], {
            type: 'arraybuffer'
          });
  
          window.navigator.msSaveBlob(blob, this.ModelObj.IPLNO + '-ipl.zip');
        } else {
          const binaryData = [];
          binaryData.push(res);
          const downloadLink = window.URL.createObjectURL(
            new Blob([res], {
              type: 'arraybuffer'
            })
          );
          const element = document.createElement('a');
          element.href = downloadLink;
          element.download = this.ModelObj.IPLNO + '-ipl.zip';
          document.body.appendChild(element);
          element.click();
        }
      });
    }
   }

   DownloadPhotos() {
    this.DownloadImgSend = [];
    const labelArray = [];
    for (let i = 0; i < this.dataSend.length; i++) {
      const imgpath = this.GetPhotos[i].Client_Result_Photo_FileName;
      let label = '';
      for (let j = 0 ; j < this.taskname.length ; j ++) {
        if (this.taskname[j].Items[0].Items[0].Client_Result_Photo_FileName === imgpath) {
          label = this.taskname[j].Items[0].Task_Photo_Button_Name + '_';
        }
      }
      labelArray.push(label);

      this.DownloadImgSend.push(imgpath);
    }
    if (this.timeStampData.stopPrinting) {
      const req = {
        FolderName: this.ModelObj.IPLNO,
        Files: this.DownloadImgSend,
        Labels: labelArray
      };
      this.xClientResultOldPhotoServices.downloadImages(req).subscribe((res: any) => {
        if (window.navigator && window.navigator.msSaveBlob) {
          // Internet Explorer workaround
          const blob = new Blob([res], {
            type: 'arraybuffer'
          });
  
          window.navigator.msSaveBlob(blob, this.ModelObj.IPLNO + '-ipl.zip');
        } else {
          const binaryData = [];
          binaryData.push(res);
          const downloadLink = window.URL.createObjectURL(
            new Blob([res], {
              type: 'arraybuffer'
            })
          );
          const element = document.createElement('a');
          element.href = downloadLink;
          element.download = this.ModelObj.IPLNO + '-ipl.zip';
          document.body.appendChild(element);
          element.click();
        }
      });
    } else {
      const req = {
        FolderName: this.ModelObj.IPLNO,
        Files: this.DownloadImgSend,
        Labels: labelArray,
        TimeStamps: this.timeStampData
      };
      this.xClientResultOldPhotoServices.downloadImagesWithTimeStamp(req).subscribe((res: any) => {
        if (window.navigator && window.navigator.msSaveBlob) {
          // Internet Explorer workaround
          const blob = new Blob([res], {
            type: 'arraybuffer'
          });
  
          window.navigator.msSaveBlob(blob, this.ModelObj.IPLNO + '-ipl.zip');
        } else {
          const binaryData = [];
          binaryData.push(res);
          const downloadLink = window.URL.createObjectURL(
            new Blob([res], {
              type: 'arraybuffer'
            })
          );
          const element = document.createElement('a');
          element.href = downloadLink;
          element.download = this.ModelObj.IPLNO + '-ipl.zip';
          document.body.appendChild(element);
          element.click();
        }
      });
    }
   }

   UnflagImgDownload(){

    this.DownloadImgSend = [];

     for (let i = 0; i < this.unflag.length; i++) {

      let imgpath = this.unflag[i].Client_Result_Photo_FilePath;

      this.DownloadImgSend.push(imgpath);
     }
    //  window.open(BasetUrl.Domain+"/DownloadFile/DownloadZipFile?ImgPath="+ this.DownloadImgSend, "_blank");
     window.open("/DownloadFile/DownloadZipFile?ImgPath="+ this.DownloadImgSend, "_blank");
   }

   TaskImagesDownLoad(TaskItems){

    debugger
    this.DownloadImgSend = [];



    for (let i = 0; i < TaskItems.length; i++) {

     for (let j = 0; j < TaskItems[i].Items.length; j++) {

       let imgpath= TaskItems[i].Items[j].Client_Result_Photo_FilePath;

       this.DownloadImgSend.push(imgpath);
     }

    }

    // window.open(BasetUrl.Domain+"/DownloadFile/DownloadZipFile?ImgPath="+ this.DownloadImgSend, "_blank");
    window.open("/DownloadFile/DownloadZipFile?ImgPath="+ this.DownloadImgSend, "_blank");

   }



   /// add custom lable

   AddCustomLabel(POPUP){
    //this.contentx = POPUP;
    this.CustomePOPUP(POPUP);
   }

   CustomePOPUP(content) {
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(result => {}, reason => {});
  }


  // pop pop add
  CustomeLablepopformArray = [];
  AddMoreRowCatepop() {
    //debugger;
    var data = { PhotoLabel_pkeyID: 0,PhotoLabel_Name: "",PhotoLabel_IsCustom:0,PhotoLabel_IsActive:true,PhotoLabel_IsDelete:false };
    if (this.CustomeLablepopformArray.length != 0) {
      if (
        this.CustomeLablepopformArray[this.CustomeLablepopformArray.length - 1].PhotoLabel_Name != ""
      ) {
        this.CustomeLablepopformArray.push(data);
      }
    } else {
      this.CustomeLablepopformArray.push(data);
    }
  }
  // popup remove
  RemovePOPdata(inx) {
    if (inx != 0) {
      this.CustomeLablepopformArray.splice(inx, 1);
    }
  }


  // pop form
  cutom:any;
  CustomPhotoArray = [];
  CustomPhotoLabelSubmit(content) {
    this.contentx = content;
     debugger;



     if(this.Custom_PhotoLabelObj.PhotoLabel_Name != ""){


      this.Custom_PhotoLabelObj.workOrder_ID = this.ClientResultPhotoModelObj.Client_Result_Photo_Wo_ID;
      this.Custom_PhotoLabelObj.Type = 5
      this.xClientResultOldPhotoServices.CustomPhotoLabelPost(this.Custom_PhotoLabelObj)
      .subscribe(response =>{
        debugger;
        console.log('post respo Custome ph',response);
      //  this.cutom = response[0][0];


        if(response[0][0].PhotoLabel_pkeyID != ""){

          this.MessageFlag = "Added...!";
        }else{
          this.MessageFlag = "Allready Added...!";
        }


        this.commonMessage(this.contentx);

        this.Custom_PhotoLabelObj.PhotoLabel_Name = "";

        if(response[0][0].length != 0){

          this.CustomPhotoArray = response[0][0];


        }
      });

     }else{
      this.MessageFlag = "Please Enter Name...!";
      this.commonMessage(this.contentx);
     }

  }
  isdisable : any;
  GetCustomPhotoLabel(){
    this.xClientResultOldPhotoServices.CustomPhotoLabelGET(this.ClientResultPhotoModelObj)
    .subscribe(response => {
      console.log('respon get custome photos',response);
      this.CustomPhotoArray = response[0][0];
      debugger;


    });
  }


  // pop up end button code

  Custom_label_CheckMethod(){
    //alert('click');
  }



  AddWO_CustomPhotos(content){
   debugger;
    this.contentx = content;

    this.WorkOrder_CustomPhotoLabelObj.WorkOrderPhotoLabel_WO_ID = this.ModelObj.workOrder_ID;
    this.WorkOrder_CustomPhotoLabelObj.Type = 1;
    this.WorkOrder_CustomPhotoLabelObj.Custom_PhotoLabel_MasterDTO = this.CustomPhotoArray;

    console.log(',chkdata',this.CustomPhotoArray);

    this.isLoading = true;
    this.button = "Processing";

    this.xClientResultOldPhotoServices.WOCustomPhotoLabelPost(this.WorkOrder_CustomPhotoLabelObj)
    .subscribe(response => {
      console.log('respinse fucl',response);
     
      this.isLoading = false;
        this.button = "Save";
     
        this.getTaskData();
    

      this.MessageFlag = "Label Data Saved...!";
      this.commonMessage(this.contentx);


    });



  }


  SingleClick_CustomPhotos(items){

    //alert(JSON.stringify(items));


    for (let i = 0; i < this.CustomPhotoArray.length; i++) {

      if(this.CustomPhotoArray[i].PhotoLabel_Name == items.PhotoLabel_Name){

        this.CustomPhotoArray[i].showBtn = 'show';
      }


    }

    console.log('when click btn' , this.CustomPhotoArray);

  }
  SingleClick_CustomPhotosDelete(items,index,content){

    //alert(JSON.stringify(items));

    const cnfm = confirm('Are you Sure delete this records..?');

    if(cnfm){

      this.Custom_PhotoLabelObj.PhotoLabel_pkeyID = items.PhotoLabel_pkeyID;
      this.Custom_PhotoLabelObj.Type = 6;

      this.xClientResultOldPhotoServices.CustomPhotoLabelPost(this.Custom_PhotoLabelObj)
      .subscribe(response =>{
        console.log('post respo Custome ph del',response);
        this.MessageFlag = "Deleted..!";
        this.contentx = content;
        this.commonMessage(this.contentx);
        this.Custom_PhotoLabelObj.Type = 1;

        this.CustomPhotoArray.splice(index,1);


      });

      //this.CustomPhotoArray.splice(1,index);
    }


    console.log('when click btn' , this.CustomPhotoArray);

  }


  SaveClick_CustomPhotos(items){

    alert(JSON.stringify(items));
  }


  DeleteClick_CustomPhotos(items, index){

    const cnfm = confirm('Are you Sure delete this records..?');

    if(cnfm){


      this.CustomPhotoArray.splice(1,index);
    }

  }


  CancelClick_CustomPhotos(items){

    for (let i = 0; i < this.CustomPhotoArray.length; i++) {

      if(this.CustomPhotoArray[i].showBtn == items.showBtn){

        this.CustomPhotoArray[i].showBtn = undefined;
      }


    }

    console.log('when cancel click btn' , this.CustomPhotoArray);
  }

  // download particular image
  downloadDocument($event) {
    this.dataSend.push({
      Client_Result_Photo_FileName: 'AAA.docx'
    });
    this.downloadimg($event, '', 'AAA.docx');
  }

  downloadimg($event, item, fileName) {
    $event.preventDefault();
    const req = {
      FolderName: this.ModelObj.IPLNO,
      File: fileName
    };
    console.log(fileName);
    this.xClientResultOldPhotoServices.downloadSingleImage(req).subscribe((res: any) => {
      if (window.navigator && window.navigator.msSaveBlob) {
        // Internet Explorer workaround
        const blob = new Blob([res], {
          type: 'arraybuffer'
        });

        window.navigator.msSaveBlob(blob, fileName.slice(0, -3) + 'png');
      } else {
        const binaryData = [];
        binaryData.push(res);
        const downloadLink = window.URL.createObjectURL(
          new Blob([res], {
            type: 'arraybuffer'
          })
        );
        const element = document.createElement('a');
        element.href = downloadLink;
        element.download = fileName.slice(0, -3) + 'png';
        document.body.appendChild(element);
        element.click();
      }
    });
  }
// for delete Image 
  deletePhotos() {
    let Cnfm = confirm("Are you sure remove this records..?");    
    if (Cnfm) {
      for (let i = 0; i < this.dataSend.length; i++) {
        const imgpath = this.GetPhotos[i].Client_Result_Photo_ID;
        this.ClientResultPhotoModelObj.Client_Result_Photo_ID  = imgpath;
        this.xClientResultOldPhotoServices
        .DeleteCLientImagesData(this.ClientResultPhotoModelObj)
        .subscribe(response => {
          console.log("deletePhoto", response);
          this.GetClientImages();
        });
      }
    }
  }
  RemoveImage(itemval) {
    debugger;
    let Cnfm = confirm("Are you sure remove this records..?");    
    if (Cnfm) {
      this.ClientResultPhotoModelObj.Client_Result_Photo_ID  = itemval.Client_Result_Photo_ID;
      this.xClientResultOldPhotoServices
      .DeleteCLientImagesData(this.ClientResultPhotoModelObj)
      .subscribe(response => {
        console.log("deletePhoto", response);
        this.GetClientImages();
      });
    }

   

  }


 





}
