import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";

import { BasetUrl } from "../../Utility/DomainUrl";
import { HomepageServices } from '../../Home/HomeServices';

@Injectable({
  providedIn: "root"
})


export class WorkOrderDrodownServices {
  public Errorcall;
  public token: any;
  constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
    this.token = JSON.parse (localStorage.getItem('TOKEN'));
  }
  // get drop down data following data

  private apiUrlGet = BasetUrl.Domain + "/api/RESTIPL/GetDropDownData";

  public DropdownGet() {
    debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    var ModelDTO: any = {};

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
   headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http.post<any>(this.apiUrlGet, { headers: headers }).pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
      //catchError( this.Errorcall.handleError)
    );
  }

  ////////////////////////////////////////////////

  //admin madhe add user
  //GetGroupDetails
  private apiUrlGet2 = BasetUrl.Domain + "/api/RESTIPL/GetGroupDetails";

  public DropdownGetGroupDetails() {
    //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    var ModelDTO: any = {};
    ModelDTO.Grp_pkeyID = 0;
    ModelDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet2, ModelDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ////////////////////////////////////////////////
  //admin madhe add user
  private apiUrlGet3 = BasetUrl.Domain + "/api/RESTIPL/GetSystemOfRecordsData";

  public DropdownGetSystemOfRecordsData() {
    //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    var ModelDTO: any = {};
    ModelDTO.Sys_Of_Rcd_PkeyID = 0;
    ModelDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet3, ModelDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }
  ///////////////////////////////////////////////////
  private apiUrlGet4 =
    BasetUrl.Domain + "/api/RESTIPL/GetClientDataForDropDown";

  public DropdownGetClientnamesData() {
    //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    var ModelDTO: any = {};

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet4, ModelDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////////////////////////////////////
  //GetInvoiceitem for work type
  private apiUrlGet5 = BasetUrl.Domain + "/api/RESTIPL/GetInvoiceitem";

  public GetInvoiceitemDropdownGet() {
    //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    var ModelDTO: any = {};
    ModelDTO.Inv_Itm_pkeyID = 0;
    ModelDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet5, ModelDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////////////////////////////////////

  //GetWorkTypecatDetails Group ka data
  private apiUrlGet6 = BasetUrl.Domain + "/api/RESTIPL/GetWorkTypecatDetails";

  public GetWorkTypecatDetailsDropdownGet() {
    //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    var ModelDTO: any = {};
    ModelDTO.Work_Type_Cat_pkeyID = 0;
    ModelDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet6, ModelDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////////////////////////////////////

  // get drop down data following data

  private apiUrlGet7 =
    BasetUrl.Domain + "/api/RESTIPLDROPDOWN/GetDropDownDataWorkOder";

  public DropdownGetWorkOrder() {
    //debugger; // why user this bcoz form validation aslo data binding sent to server and gettong error occure
    let ModelDTO: any = {};

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
   // headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http.post<any>(this.apiUrlGet7, { headers: headers }).pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
    );
  }

  ////////////////////////////////////////////////
  private apiUrlGet8 = BasetUrl.Domain + "api/RESTIPL/GetStateDetail";

  public StateDropDownData() {
    //debugger;
    let ANYDTO: any = {};

    ANYDTO.IPL_StateID = 0;
    ANYDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http
      .post<any>(this.apiUrlGet8, ANYDTO, { headers: headers })
      .pipe(
        tap(data => {
          //console.log(data);
          return data;
        }),
        catchError(this.xHomepageServices.CommonhandleError)
        //catchError( this.Errorcall.handleError)
      );
  }

  ///////////////////////////////////////////////////

  // get drop down data following data

  private apiUrlGet10 =
    BasetUrl.Domain + "/api/RESTIPLDROPDOWN/GetDropDownDataClientResult";

  public DropdownGetClientResult() {
    //debugger;
    let ModelDTO: any = {};

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http.post<any>(this.apiUrlGet10, { headers: headers }).pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
    );
  }

  ////////////////////////////////////////////////

  // get drop down UOM data
  private apiUrlGet101 =
    BasetUrl.Domain + "/api/RESTIPLDROPDOWN/GetDropDownUOM";

  public DropdownGetUOM() {
    //debugger;
    let ModelDTO: any = {};
    ModelDTO.Type = 1;

    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this._http.post<any>(this.apiUrlGet101,ModelDTO, { headers: headers }).pipe(
      tap(data => {
        //console.log(data);
        return data;
      }),
      catchError(this.xHomepageServices.CommonhandleError)
    );
  }

  ////////////////////////////////////////////////

  // common handler
  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Unauthorized User Found..!');
    } else {
    alert("Invalid Request..");
    }

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something's wrong, please try again later...");
  }
}
