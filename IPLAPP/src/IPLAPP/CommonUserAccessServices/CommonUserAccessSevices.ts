import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";

import {BasetUrl} from "../Utility/DomainUrl";
import { HomepageServices } from '../Home/HomeServices';
import{UserAccessModel} from "./CommonUserAccessModel"



@Injectable({
  providedIn: "root"
})
export class CommonMenuServices {
    public Errorcall;
    public token: any;
    constructor(private _http: HttpClient, private _Route: Router, private xHomepageServices: HomepageServices) {
     // this.token = JSON.parse (localStorage.getItem('TOKEN'));
    }
   
   
    private apiUrlGet = BasetUrl.Domain + "api/RESTIPL/GetUserMenuAccess";

    public UserAccessGet(Modelobj:UserAccessModel) {
      debugger;
      this.token = JSON.parse (localStorage.getItem('TOKEN'));
      let ANYDTO: any = {};
  
     ANYDTO.MenuID = Modelobj.MenuID;
      ANYDTO.UserID = Modelobj.UserID;
  
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
      return this._http
        .post<any>(this.apiUrlGet, ANYDTO, { headers: headers })
        .pipe(
          tap(data => {
            //console.log(data);
            return data;
          }),
          catchError(this.xHomepageServices.CommonhandleError)
          //catchError( this.Errorcall.handleError)
        );
    }

}