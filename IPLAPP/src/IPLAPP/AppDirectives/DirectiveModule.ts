
import { NgModule } from '@angular/core';


import {NumberDirective} from './DirectiveNumbersOnly';
import {AlphabetDirective} from './DirectiveAlphabetsCheck';


@NgModule({
  declarations: [
    NumberDirective,AlphabetDirective
  ],
  exports: [
    NumberDirective,AlphabetDirective
  ]
})

export class CommonDirectiveModule {}
