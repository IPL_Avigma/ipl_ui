import { Directive, ElementRef, HostListener, Input, Renderer2, HostBinding } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[AppTestDir]'
})
export class ColorDirective {

  constructor(private el: ElementRef,private rendere:Renderer2) { }


@HostBinding('style.backgroundColor') bgColor = 'orange';


 @HostListener('click') myClick(){

   //this.rendere.setStyle(this.el.nativeElement,'backgroundColor', 'blue');
   this.bgColor = 'blue';
 }



}
